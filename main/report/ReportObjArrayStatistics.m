function ReportObjArrayStatistics(myPara, mySel)
% Report some meta statistic on array of objects 

validateattributes(myPara, {'HeightParameters'}, {'vector'}, mfilename, 'myPara');
validateattributes(mySel, {'Selection'}, {'vector'}, mfilename, 'mySel');

imAll = [mySel.image];
mySelNames = {imAll.name};

% link parameters and selections 'manually'
[~, indexSel] = ismember({myPara.scanname}, mySelNames);
selRel = mySel(indexSel);

% relate to other objects
imRel = unique(([selRel.image]));

% Get some related data
parameters = { ...
    'Scanname', 'is3D', 'isExport', 'isDocuOri', ...
    'Orientation', 'Magnification', ...
    'RunNo', 'SampleID', 'IterationNo', ...
    'Force', 'NumCycCum', 'WorkpieceID'};
data = QuerryMeta( parameters, {myPara.scanname} );
% data = QuerryMeta({'Scanname', 'Force', 'NumCycCum'}, {myPara.scanname});


fprintf('\n');


%% Statistics on HeightParameters
fprintf('Height Parameters:\n');

fprintf('# samples: %u\n', length(myPara));
fprintf('Mean Sa: %4.2f\n', mean([myPara.Sa]));
fprintf('Var Sa:  %4.2f\n', var([myPara.Sa]));
fprintf('Mean poly area: %4.2f\n', mean([myPara.area]));
fprintf('Var poly area:  %4.2f\n', var([myPara.area]));

fprintf('\n');


%% Selections
fprintf('Selected Polygons:\n');

fprintf('# polygons: %u\n', length(selRel));

fprintf('\n');

%% Images
fprintf('Evaluated Images:\n');

fprintf('# images: %u\n', length(imRel));
fprintf('Mean # polygons/ im: %4.2f\n', mean(cellfun(@length, {imRel.selections}))); 
% fprintf('Var # polygons/ im: %3.2f\n:', var(cellfun(@length, {imRel.selections})));


fprintf('\n');


%% Experiments
fprintf('Related Experiments:\n')

[G, GID] = findgroups(data(:, {'Force', 'NumCycCum'}));


for i=1:height(GID)
    force       = GID{i, 1};
    duration    = GID{i, 2};
    
    fprintf('Force Level: %3.2f\n', force);
    fprintf('\tDuration: %u\n', duration);
    
    reportGroup(data(G == i, :));
end


function s = reportGroup(groupData)
    s = struct;

    [Gsub, GIDsub] = findgroups(groupData.RunNo);
    
    s.Runs = GIDsub;
    
    for j=1:length(GIDsub)
        s.NumScans3d50(j)   = length(unique( ...
            groupData{groupData.Magnification== 50 & Gsub==j, 'Scanname'}));
        s.NumScans3d100(j)  = length(unique( ...
            groupData{groupData.Magnification==100 & Gsub==j, 'Scanname'}));
        s.NumScansDoc(j)    = length(unique( ...
            groupData{groupData.Magnification < 50 & Gsub==j, 'Scanname'}));
        s.MedianScansDocMag(j) = median( ...
            groupData{groupData.Magnification < 50 & Gsub==j, 'Magnification'});
    end
    
    s.NumNotExported = length(unique( ...
            groupData{groupData.is3D & ~groupData.isExport, 'Scanname'}));
            
    % Print out statistics
    fprintf('\t\t\t# runs:\t\t\t%u\n',        length(s.Runs));
    fprintf('\t\t\t# scans 50x:\t\t%u\n',     sum(s.NumScans3d50));
    fprintf('\t\t\t# scans 100x:\t\t%u\n',    sum(s.NumScans3d100));
    fprintf('\t\t\t# not exported:\t\t%u\n',  s.NumNotExported);
    fprintf('\t\t\t# doc scans:\t\t%u\n',     sum(s.NumScansDoc));
    fprintf('\t\t\tmedian doc mag:\t\t%u\n',  median(s.MedianScansDocMag));
end

end


