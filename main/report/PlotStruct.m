function fig = PlotStruct(stats1, varargin)
% This functions takes structs as input and plots the content in 
% boxplots. Each struct gets one axis. All axis are in one figure.
% Each field in the struct gets its own boxplot. The name of the boxplot
% is the fieldname.

% CHECK USER INPUT ********************************************************
validateattributes(stats1, {'struct'}, {'size', [1 1]}, '', 'struct');

Stats = {stats1};

% Default values:
yMax = [];
isYaxisEqual = true;

for i=1:length(varargin)
    % Collect optional structs
    if isstruct(varargin{i})
        Stats = [Stats varargin(i)];
    end
    
    % Collect optional name-value pairs
    if ischar(varargin{i})
        switch varargin{i}
            case 'ymax'
                yMax = varargin{i+1};
                validateattributes(yMax, ...
                    {'numeric'}, {'scalar'}, '', 'Ymax');
            case 'equalyaxis'
                isYaxisEqual = varargin{i+1};
                validateattributes(isYaxisEqual, ...
                    {'logical'}, {'scalar'}, '', 'isYaxisEqual');
            otherwise
                error('MWA:PlotStruct:invalidInput', ...
                    '%s is not a valid name-value pairs.', ...
                    varargin{i});
        end
    end
end
        
% for i=1:length(varargin)
%     if isstruct(varargin{i})
%         Stats = [Stats varargin(i)];
%         
%     elseif isnumeric(varargin{i}) && isscalar(varargin{1})
%         Ymax = varargin{i};
%         break;
%     else
%         error('MWA:PlotStruct:invalidInput', ...
%             ['Input must be one or more structs followed optionally by' ...
%             ' a y-axis limit.']);
%     end
% end

        
numStructs = length(Stats);

numBoxes = 0;

minelGlob = 0;
maxelGlob = 0;

fig = figure();
set(fig, 'Name', 'Statistics plot');
set(fig, 'Units', 'pixels');

axText = axes('Position', [0 0 1 1], 'Parent', fig, 'Visible', 'off');

offset = 0.02;
a = (1-offset)/numStructs;
axWidth     = a-0.003;  %a - 0.1*a;
axHeight    = 1;
axYpos      = 0;
axDelta     = a;

lw = 1;

for i_plot=1:numStructs
    
    stats = Stats{i_plot};
    
    Fn = fieldnames(stats);
    numFields = length(Fn);
    
    Ax(i_plot) = axes('Parent', fig);
    
%     if (i_plot==1) 
%         dim = [0.08+axDelta*(i_plot-1) axYpos axWidth+0.02 axHeight];
%     else
%         dim = [0.1+axDelta*(i_plot-1) axYpos axWidth axHeight];
%     end

    dim = [offset+axDelta*(i_plot-1) axYpos axWidth axHeight];
    
    set(Ax(i_plot), 'OuterPosition', dim);
    set(Ax(i_plot), 'Tag', inputname(i_plot));

    hold(Ax(i_plot), 'on');

    titletext = regexprep(inputname(i_plot), '_', ' ');
    
    ti = title(Ax(i_plot), titletext);
    set(ti, 'FontSize', 12);
    set(ti, 'FontName', 'Arial');
    set(ti, 'Interpreter','latex');
    
    xTickLabels = cell(1, numFields);
        
    for i_box=1:numFields
        
        b = boxplot(Ax(i_plot), ...
            stats.(Fn{i_box}), ...
            'positions', i_box, 'width', .5);
        set(b, 'color', 'b');
        set(b, 'LineWidth', lw);

        minelGlob = min([minelGlob min(stats.(Fn{i_box}))]);
        maxelGlob = max([maxelGlob max(stats.(Fn{i_box}))]);
        
        % add # samples to boxplot label
        
        xTickLabels{i_box} = sprintf( ...
            '\\begin{tabular}{c} %s \\\\ %u\\end{tabular}', ...
            Fn{i_box}, ...
            length(stats.(Fn{i_box})));
        
        
%         XLabelN = [XLabelN sprintf('n=%u  ', length(stats.(Fn{i_box})))];
          
        numBoxes = numBoxes + 1;
    end

    
    set(Ax(i_plot), 'Xlim', [0 numFields+1]);
    set(Ax(i_plot), 'XTick', 1:numFields);    
    set(Ax(i_plot), 'XTickLabel', xTickLabels);
    set(Ax(i_plot), 'TickLabelInterpreter', 'latex');

    hold(Ax(i_plot), 'off');
    
%     %axes(atext);
%     axes('Position', [dim(1) 0 dim(3) 1], 'Visible', 'off');
%     text(0.15, 0.06, XLabelN);
    
%     if (i_plot == 1)
%         yl = ylabel(Ax(i_plot), '$[um]$','FontSize',12);
%         set(yl, 'FontSize', 12)
%         set(yl, 'FontName', 'Arial');
%         set(yl, 'Interpreter','latex');
%     end

end

figWidth = numBoxes * 60 + 100;
set(fig, 'Position', [700 60 figWidth 500]);

set(Ax, 'FontSize', 10);
set(Ax, 'FontName', 'Arial');

set(Ax, 'Xgrid', 'on');
set(Ax, 'Ygrid', 'on');

% Set yaxis range
if isYaxisEqual
    set(Ax, 'Ylim', [minelGlob maxelGlob+(maxelGlob-minelGlob)*0.05]);
else
    % leave axis scaled individually
    set(Ax, 'YlimMode', 'auto');
end
    
if isempty(yMax)
    % do not change max value of y range
else
    axYlim = get(Ax, 'Ylim');
    
    isGreaterYmax = axYlim(:,2) > yMax;
    set(Ax(isGreaterYmax), 'Ylim', [minelGlob yMax]);
end

% else
%     set(Ax, 'Ylim', [minelGlob yMax]);
%     text(0.5, 0.02, ...
%         sprintf('Samples greater %.0f are not shown', yMax), ...
%         'Parent', axText, ...
%         'HorizontalAlignment', 'center');
% end

% set(Ax, 'YTick', floor(minelGlob):ceil(maxelGlob));


end

