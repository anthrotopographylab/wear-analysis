function ReportScans(samples, measurements)
% Report scans as and corresponding elements with MATLABs DOM tools.
% Corresponding elements must be linked to the scan in order to be
% reported.
% USAGE:
% ReportScans( Scans )
%
% Print various elements of the data to pdf.
% - The RGB scan including selections
% - Metadata of the RGB scan including: logbook, logfile, infofile
% - For each selection: 3D scan, histogram, parameter values
%
% The order of the scans in the report is following the order of Parameters.


global ...
    META ...
    DIR_BASE ...
    DIR_RESULTS

dirReport = fullfile(DIR_RESULTS, 'report');
counter = 0;

% CHECK INPUT *************************************************************
validateattributes(samples, {'SampleHistory'}, {'vector'}, mfilename, 'samples');


% if (length(varargin) == 0)
%     % Sort the metadata
%     [namelist, indexMeta, indexScan] = ...
%         IntersectMulti(META, Scans);
% 
%     MetaSort    = META(indexMeta);
%     ScanSort    = Scans(indexScan, :);
%     
%     ParaSort = [];
%     SelectSort = [];
% end
% if (length(varargin) == 2)
%     Selections = varargin{1};
%     Parameters = varargin{2};
%     
%     % Sort the metadata
%     [namelist, indexPara, indexMeta, indexScan, indexSelect] = ...
%         IntersectMulti(Parameters, META, Scans, Selections);
% 
%     MetaSort    = META(indexMeta);
%     ScanSort    = Scans(indexScan, :);
%     SelectSort  = Selections(indexSelect, :);
%     ParaSort    = Parameters(indexPara, :);
% end
% if (length(varargin) == 1 || length(varargin) > 2)
%     error('MWA:%s:ReportData', ...
%         'Number inputs must be 1 or 3', mfilename);
% end


% MAIN ********************************************************************

import mlreportgen.dom.*;

report = Document(fullfile(dirReport, 'tmp'), 'html');
%report.StreamOutput = true;
% report.OutputPath = fullfile(dirResults, 'tmp');
open(report);

p = Paragraph('Session on Force Duration - Report');
p.Style = {OuterMargin('0.5in','0in','0in','12pt')};
append(report, p);

% Table of contents
% Need MATLAB 2016b for TOC!!!
% toc = append(report, TOC(2));
% toc.Style = {PageBreakBefore(true)};


% Show sample specific information
for l=1:length(samples)

mySample = samples(l);

report.append(Heading(1, sprintf('Stone sample S%u', mySample.SampleID)));

% Show parameters of each experimental run (trial)
report.append(Heading(2, 'Summary'));

table = FormalTable(4);
table.Border = 'single';
table.ColSep = 'single';
table.RowSep = 'single';

% Table header
row = TableRow;
append(row, TableEntry('Trial No.'));
append(row, TableEntry('Force [N]'));
append(row, TableEntry('# Cycles'));
append(row, TableEntry('Date'));

table.Header.append(row);

% Fill table body
for i = 1:mySample.numRuns
  row = TableRow;
  append(row, TableEntry(num2str(mySample.Runs(i).IterationNo)));
  append(row, TableEntry(num2str(mySample.Runs(i).Force)));
  append(row, TableEntry(num2str(mySample.Runs(i).NumCycles)));
  append(row, TableEntry(char(mySample.Runs(i).Date)));
  table.append(row);
end

append(report, table);

% Add data common to all runs
report.append(Text(sprintf('Workpiece: %s', mySample.Workpiece)));
report.append(Text(sprintf('Workpiece ID: %s', mySample.WorkpieceID)));



 % Other sample specific information:
% reprort.append(Text(sprintf('Raw Material: %s', mySample.StoneType)));
% reprort.append(Text(sprintf('Total # Trials: %s', mySample.numRuns)));
% reprort.append(Text(sprintf('Total # Cycles: %s', mySample.NumCycCum)));

% Add images of raw areas

% Details of each experimental run
for i=1:mySample.numRuns
    myrun = mySample.Runs(i);
    
    report.append(Heading(2, sprintf('Trial %u', myrun.IterationNo)));
    
    % Add photos of the experiment
    report.append(Heading(3, 'Before and after trial'));
    photos = myrun.Photo;
    if isempty(photos)
        report.append(Text('No photo available'));
    else
        if isempty(photos{:})
            report.append(Text('No photo available'));
        else
            for j=1:length(photos{:})
                path = fullfile(myrun.Directory, photos{:}{j});
                im = Image(path{:});
                im.Height   = '400px';
                im.Width    = '500px';
                append(report, im);
            end
        end
    end
    
    % Add trajectory
    report.append(Heading(3, 'Tool trajectory'));
    tr = myrun.Trajectory;
    if isempty(tr)
        report.append(Text('No trajectory available'));
    else
        fig = figure;
        fig.Visible = 'off';
        hold on;
        tr.CartPosX.plot;
        tr.CartPosY.plot;
        tr.CartPosZ.plot;
        hold off;
           
        traj = addFigure(fig);

        traj.Height     = '600px';
        traj.Width      = '700px';
        append(report, traj);
    end
    
    % Add microscopic images related through Measurements
    measurements = myrun.getLinksTo('Measurement');
    
    for k=1:length(measurements)
        mymeas = measurements(k);
        
        report.append(Heading(3, sprintf('Measurement on area %u', k)));
        
         for j=1:length(mymeas.PosDoc)
%             fig = figure;
%             fig.Visible = 'off';
%             
%             ax = axes('Parent', fig);
%             
%             imshow(mymeas.PosDoc(j).ImageRGB, 'Parent', ax);
% 
%             saveas(fig, 'tmp.png');
%             close(fig);

            im = Image(mymeas.PosDoc(j).pathRGB);

%             im.Height   = '600px';
%             im.Width    = '700px';
            append(report, im);
         end

        report.append(Heading(4, sprintf('50x of area %u', k)));
        
        if isempty(mymeas.Im50x)
            report.append(Text('No image available'));
        end
        
        for j=1:length(mymeas.Im50x)
%             fig = figure;
%             fig.Visible = 'off';
% 
%             imshow(mymeas.Im50x(j).ImageRGB);
% 
%             saveas(fig, 'tmp.png');
%             close(fig);

            im = Image(mymeas.Im50x(j).pathRGB);

%             im.Height   = '600px';
%             im.Width    = '700px';
            append(report, im);
        end

        report.append(Heading(4, sprintf('100x of area %u', k)));
        
        if isempty(mymeas.Im100x)
            report.append(Text('No image available'));
        end
        
        for j=1:length(mymeas.Im100x)
%             fig = figure;
%             fig.Visible = 'off';
% 
%             imshow(mymeas.Im100x(j).ImageRGB);
% 
%             saveas(fig, 'tmp.png');
%             close(fig);

            im = Image(mymeas.Im100x(j).pathRGB);

%             im.Height   = '600px';
%             im.Width    = '700px';
            append(report, im);
        end    
    end
    
    
    
end

end 


% Images:
% impath = fullfile(DIR_BASE, '../data/scans/', 'S121_use4_pos13_x100$3D/', 'texture.bmp');
% im = Image(impath);
% append(report, im);

% p = Paragraph('Session on Force Duration - Report.');
% p.Style = {'KeepWithNext'};
% append(rpt,p);

append(report, ['Creation date: ', date]);

close(report);

rptview(report.OutputPath);


    function im = addFigure(fig)
        
        counter = counter + 1;
        
        filename = sprintf('fig%u.png', counter);
        mypath = fullfile(dirReport, filename);
        
        saveas(fig, mypath);
        
        im = mlreportgen.dom.Image(mypath);
    end

end