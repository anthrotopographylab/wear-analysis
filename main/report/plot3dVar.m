function fig = plot3dVar(x, y, z, varargin)
%plot3dVar Plots 3D data on multiple plots, with x on xaxis an z on yaxis.
% Multiple z values with the same xy pair are represented as mean with
% errorbars. For each unique y value a separate subplot is created,
% therefore the number of unique values in y should be low.
%
% INPUT
% x, y, z:  Data vectors
% varFctr:  Variance factor: can be used to stretch the errorbars.

% Check optional input
if      nargin == 3
    varFctr = 1;
elseif  nargin == 4
    validateattributes(varargin{1}, ...
        {'numeric'}, {'scalar'}, mfilename, 'variance factor');
    varFctr = varargin{1};
else
    error('Too many input arguments')
end

validateattributes(x, {'numeric'}, {'vector'}, mfilename, 'x');
validateattributes(y, {'numeric'}, {'size', size(x)}, mfilename, 'y');
validateattributes(z, {'double', 'categorical'}, {'size', size(x)}, mfilename, 'z');

[g, idX, idY] = findgroups(x, y);

xUnique = unique(idX);  % x values of groups
yUnique = unique(idY);  % y values of groups

numG = max(g);
numX = length(xUnique);
numY = length(yUnique); % # unique y values among all groups

% Compute a single statistical value for each group
gMeanZ   = splitapply(@(myz)mean(myz, 'omitnan'), z, g);  % mean of each group
gVarZ    = splitapply(@(myz)var(myz, 'omitnan'), z, g);   % variance of each group
gNumN    = splitapply(@(myz)sum(~isnan(myz)), z, g);      % number of samples

% Plot values in separate plots for each y level. Each subplot shows z over x.
fig = figure;
title = 'Force vs. Duration';
set(fig, 'Name', title);
set(fig, 'Position', [1000, 350, 700, 500]);

ha = zeros(1, numY);
he = zeros(1, numY);

% Iterate through (discrete) y values
for i=1:numY
    ha(i) = subplot(numY, 1, numY+1-i);

    % Get all data points related to this y value, yi, and plot them.
    yi = yUnique(i);
    isGY = (idY == yi);

    he(i) = errorbar(idX(isGY), gMeanZ(isGY), varFctr*gVarZ(isGY));

    ylabel(num2str(yi));

    set(ha(i), 'Tag', sprintf('x%u', yi));

    % Add number of samples to the mean bar
    text(double(idX(isGY)), gMeanZ(isGY), cellstr(num2str(gNumN(isGY)')), ...
        'Parent', ha(i));
        
    grid on;
end

zMinGlob = min(min(gMeanZ-varFctr*gVarZ));
zMaxGlob = max(max(gMeanZ+varFctr*gVarZ));

%         xlabel(ha(1), 'duration [cycles]');

% Set y axis range of each plot
axis(ha, [0 max(xUnique)+50 0.95*zMinGlob 1.05*zMaxGlob]);

% Format boxplots
set(he, 'LineStyle', 'none');
set(he, 'Linewidth', 1.3);
set(he, 'Marker', '.');
set(he, 'MarkerSize', 10);


end

