function ReportData(Scans, varargin)
%ReportData(Scans, Selections, Parameters)
% Print various elements of the data to pdf.
% - The RGB scan including selections
% - Metadata of the RGB scan including: logbook, logfile, infofile
% - For each selection: 3D scan, histogram, parameter values
%
% The order of the scans in the report is following the order of Parameters.

global ...
    META ...
    DIR_BASE ...
    DIR_RESULTS

dirResults = DIR_RESULTS;

% CHECK INPUT *************************************************************
if (length(varargin) == 0)
    % Sort the metadata
    [namelist, indexMeta, indexScan] = ...
        IntersectMulti(META, Scans);

    MetaSort    = META(indexMeta);
    ScanSort    = Scans(indexScan, :);
    
    ParaSort = [];
    SelectSort = [];
end
if (length(varargin) == 2)
    Selections = varargin{1};
    Parameters = varargin{2};
    
    % Sort the metadata
    [namelist, indexPara, indexMeta, indexScan, indexSelect] = ...
        IntersectMulti(Parameters, META, Scans, Selections);

    MetaSort    = META(indexMeta);
    ScanSort    = Scans(indexScan, :);
    SelectSort  = Selections(indexSelect, :);
    ParaSort    = Parameters(indexPara, :);
end
if (length(varargin) == 1 || length(varargin) > 2)
    error('MWA:%s:ReportData', ...
        'Number inputs must be 1 or 3', mfilename);
end


% MAIN ********************************************************************

fileList = {};

% time to pause before figure printing to account for bug [sec]
pauseTime = 0.01; 

% Title figure to be placed at top of a page
[figTitle, txTitle] = newTitleFig('Title');

j = 1; % need j to record page numbers for bookmark

disp('Print scans');

for i=1:size(namelist, 1)
    
    % Reord page number for pdf bookmarks
    if i==1
        pageNums{1} = 1;
    elseif ~isempty(ParaSort)
        % Each "chapter" has one page for the rgb and j pages for 
        % the selections. 
        pageNums{i} = pageNums{i-1} + j + 1;
    else
        pageNums{i} = pageNums{i-1} + 1;
    end
    
    scanname = namelist{i};
    X = ScanSort{i,2};
    Y = ScanSort{i,3};
    scanD = ScanSort{i,4};
    scanRGB = ScanSort{i,5};
    
    thisMeta = MetaSort(i);
    
    % Display progress
    disp(scanname);
    
    % Resize if necessary
    if ~isequal( [size(scanRGB,1) size(scanRGB,2)], size(scanD) );
        scanRGB = imresize(scanRGB, size(scanD));
    end
    
    set(txTitle, 'String', sprintf('Scan %s ', scanname));
    
    if ~isempty(SelectSort)
        % MAKE SelectScanRegion NOT RECOMPUTING PARAMETERS
        thisSelection = SelectSort(i, :);
        
        figSelection = SelectScanRegionHL(ScanSort(i,:), thisSelection);
        set(figSelection, 'Visible', 'off');
    else
        figSelection = DisplayScan(scanRGB, X, Y);
        set(figSelection, 'Visible', 'off');     
    end

    % Write some metadata
    [figMeta, axMeta] = newDataFig();
    
    fnMeta = fieldnames(thisMeta);
    meta1 = rmfield(thisMeta, fnMeta(~ismember(fnMeta, { ...
        'RunNo', ...
        'ExperimentID', ...
        'ExperimentDate', ...
        'IterationNo', ...
        'ForceN', ...
        'NumCycAccu'})));
    
    info = thisMeta.ScanInfo;
    if isstruct(info)
        fnInfo = fieldnames(info);
        meta2 = rmfield(info, fnInfo(~ismember(fnInfo, { ...
            'ResVertUm', ...
            'ResLatUm', ...
            'Magnification', ...
            'Polarizer', ...
            'DurationSec', ...
            'Creator'})));
    else
        meta2 = struct;
    end
    
    metaDisp = catstruct(meta1, meta2);
    text(0.05, 0.5, ...
        evalc('disp(metaDisp)'), ...
        'Parent', axMeta, ...
        'VerticalAlignment', 'top');
    
    text(0.05, 0.7, ...
        sprintf('Total area: %0.4g', ...
        abs(X(1,1)-X(end,end)) * abs(Y(1,1)-Y(end,end))), ...
        'Parent', axMeta, ...
        'VerticalAlignment', 'top');
        
    
    % Write data from Parameters
    [figPara, axPara] = newDataFig();
    
    if ~isempty(ParaSort)
        thisParameters = ParaSort(i, :);

        paraDisp = num2str([ ...
            [1:length([thisParameters{2}.Sa])]' ...
            [thisParameters{2}.Sq]' ...
            [thisParameters{2}.Sa]' ...
            [thisParameters{2}.Ssk]' ...
            [thisParameters{2}.Sku]']);

        text(0.1, 0.6, ...
            'Sq             Sa              Ssk             Sku', ...
            'Parent', axPara, ...
            'VerticalAlignment', 'top');

        text(0.05, 0.5, ...
            paraDisp, ...
            'Parent', axPara, ...
            'VerticalAlignment', 'top');
        
        fileList{end+1} = PrintFigures(figTitle, figSelection, figMeta, figPara);
    else
        fileList{end+1} = PrintFigures(figTitle, figSelection, figMeta);
    end
    
    close(figSelection, figPara, figMeta);
 
    if ~isempty(ParaSort)
        % Plot histogram for each polygon
        for j=1:length(thisParameters{2})

            parameters = thisParameters{2}(j);

            % Plot framed and windowed selection with zoom
            figPoly = DisplayScan( ...
                [{sprintf('Area %u', j)} ...
                {parameters.frameX} {parameters.frameY} {parameters.frameZmean} {[]}]);
            set(figPoly, 'Visible', 'off');

            figHist = figure;
            histogram(parameters.frameZmean(parameters.frameRoi));
            xlabel('Distance [um]');
            ylabel('Ratio');
            title('Deviation from mean height of the filtered scan');
            set(figHist, 'Visible', 'off');

            % Show parameters of this selection
            [figPara, axPara] = newDataFig();

            paraDisp = evalc([ ...
                'disp(parameters), ' ...
                'disp(thisParameters{3}(j))']);

            text(0.05, 0.5, ...
                paraDisp, ...
                'Parent', axPara);

            % Bug fix to show colorbar properly
            pause(pauseTime);

            % Print figure to pdf
            fileList{end+1} = PrintFigures(figPoly, figHist, figPara);

            close(figPoly, figHist, figPara);
        end
    end
    
end

close(figTitle);

% BASH PROCESSING *********************************************************

% Format list of filenames and page nums to valid command arguments
spaceFiles = repmat({' '}, 1, length(fileList));
spaceBm = repmat({' '}, 1, length(namelist));

pageNums = cellfun(@num2str, pageNums, 'UniformOutput', false);

bsArgFiles = cell2mat(reshape([fileList;spaceFiles], 1, 2*length(fileList)));
bsArgBmNames = cell2mat(reshape([namelist';spaceBm], 1, 2*length(namelist)));
bsArgBmPages = cell2mat(reshape([pageNums;spaceBm], 1, 2*length(pageNums)));

filepath = [dirResults 'report_' date '.pdf'];

% Bash command for pdf concatenation
system(['pdftk ' bsArgFiles ' cat output ' filepath]);

% Perl script for bookmark generation
system(['./setBookmarks.pl ' filepath ' ' bsArgBmNames bsArgBmPages]);

% Delete temporary files
system(['rm ' bsArgFiles]);


% HELPER ******************************************************************

    function [fig, txt] = newTitleFig(title)
        
        fig = figure;
        set(fig, 'Units', 'centimeters', 'Position', [30 5 15 2]);
        set(fig, 'resize', 'off');
        set(fig, 'ToolBar', 'none');
        set(fig, 'MenuBar', 'none');
        set(fig, 'Visible', 'off');
        
        ax = axes('parent', fig);
        set(ax, 'Visible', 'off');
        
        txt = text(0.05, 0.5, title);
        set(txt, 'Parent', ax);
        set(txt, 'FontSize', 14);
        set(txt, 'Interpreter', 'none');
         
    end

    function [fig, ax] = newDataFig()
        
        fig = figure;
        set(fig, 'Units', 'centimeters', 'Position', [30 5 15 6]);
        set(fig, 'resize', 'off');
        set(fig, 'ToolBar', 'none');
        set(fig, 'MenuBar', 'none');
        set(fig, 'Visible', 'off');
        
        ax = axes('parent', fig);
        set(ax, 'Visible', 'off');
        
    end

end