function [fig50, fig100] = PlotForceDurationHL(Parameters, paraName)
% Plot surface roughness of the selections on a grid with duration on the x
% axis and force on the y axis. The value is expressed by a color code. 

% name is the roughness parameter of choice representing surface 
% roughness.

global META

% Specify fieldnames of META
ME_NUM_CYC_ACCU = 'NumCycAccu';
ME_FORCE = 'ForceN';
ME_SID = 'SampleID';



% CHECK INPUT *************************************************************
validateattributes(Parameters, {'cell'}, {'ncols', 3}, mfilename, 'Parameters');
paraOptions = {'Sq', 'Sa', 'Ssk', 'Sku'};
if ~any(strcmp(paraName, paraOptions));
    error('MWA:PlotDurationHL:invalidInput', ...
        'paraName must be one of %s.', [paraOptions{:}]);
end


% COLLECT DATA ************************************************************

% Sync with metadata with scans of 
% 50x 
meta50 = META([META.Magnification]==50);

[~, indexMeta, indexPara] = ...
    IntersectMulti(meta50, Parameters);

if ~isempty(indexPara)
    fig50 = plotParameters(meta50(indexMeta), Parameters(indexPara, :));
    fig50.Children(2).Title.String = sprintf('Mean of %s on 50x', paraName);
end

% 100x
meta100 = META([META.Magnification]==100);

[~, indexMeta, indexPara] = ...
    IntersectMulti(meta100, Parameters);

if ~isempty(indexPara)
    fig100 = plotParameters(meta100(indexMeta), Parameters(indexPara, :));
    fig100.Children(2).Title.String = sprintf('Mean of %s on 100x', paraName);
end

% PLOT ********************************************************************

    function fig = plotParameters(metaSort, paraSort)

        value = [];
        SampPoints = [];

        for i=1:size(paraSort, 1)
            numS = length([paraSort{i,2}.Sa]);

            % Distinguish between raw and worn scan
            if isnan(metaSort(i).(ME_FORCE))
                SampPoints(end+1:end+numS, 1:2) = ...
                    repmat([0 0], numS, 1); 
            else
                SampPoints(end+1:end+numS, 1:2) = ...
                    repmat([metaSort(i).(ME_NUM_CYC_ACCU), metaSort(i).(ME_FORCE)], numS, 1); 
            end

            value(end+1:end+numS) = [paraSort{i,2}.(paraName)];
            %value = [value, [paraSort{i,2}.(paraName)]];
        end

        % Take the mean of samples of the same sampling point
        SampUnique = unique(SampPoints, 'rows');

        valueMean = zeros(1, size(SampUnique, 1));
        valueVar = zeros(1, size(SampUnique, 1));

        numSamp = zeros(1, size(SampUnique, 1));
        numStones = zeros(1, size(SampUnique, 1));

        sampNo = 1:size(SampUnique, 1);

        for i=1:size(SampUnique, 1)
            valueMean(i) = mean(value(SampPoints(:,1)==SampUnique(i,1) & ...
                SampPoints(:,2)==SampUnique(i,2)));

            valueVar(i) = var(value(SampPoints(:,1)==SampUnique(i,1) & ...
                SampPoints(:,2)==SampUnique(i,2)));

            % Get number of stones. Treat raw scan separately
            if (SampUnique(i, 1) == 0)
                numStones(i) = sum( ...
                    [metaSort.(ME_NUM_CYC_ACCU)]==0);
            else
                isSampP = ...
                    [metaSort.(ME_NUM_CYC_ACCU)]==SampUnique(i,1) & ...
                    [metaSort.(ME_FORCE)]==SampUnique(i,2);
                numStones(i) = length(unique([metaSort(isSampP).(ME_SID)]));
            end

            numSamp(i) = sum( ...
                SampPoints(:,1)==SampUnique(i,1) & ...
                SampPoints(:,2)==SampUnique(i,2));
        end


        fig = figure;
        title = 'Force vs. Duration';
        set(fig, 'Name', title);
        ax = axes('Parent', fig);

        colormap(ax, autumn);

        % Scale the dot size to be between 10 and 310 points^2 inverted
        % range of values.
        dotMin = 10;
        dotMax = 350;
        
%         dotSize = -(dotMax-dotMin)/(max(valueMean)-min(valueMean))*(valueMean-min(valueMean)) + dotMin;

        dotSize = repmat(400, 1, length(valueMean));
        
%         if all(valueMean >= 0)
%             dotSize = (-valueMean-min(-valueMean)) * 300 / (max(-valueMean)-min(-valueMean)) + 10;
%         else
%             assert(false);
%         end

        scatter(ax, SampUnique(:,1), SampUnique(:,2), dotSize, valueMean, 'filled');

        % Draw sample size on each dot
    %     SUCell = num2cell(SampUnique);
    %     sSCell = arrayfun(@num2str, numSamp', 'UniformOutput', false);
    %     cellfun(@text, SUCell(:,1), SUCell(:,2), sSCell);

        set(fig, 'defaulttextinterpreter', 'none');
    
        cb = colorbar;
        ylabel(cb, '<-- increasing abrasion [um]');

        set(ax, 'xlim', [0 max(SampUnique(:,1))+50]);
        set(ax, 'ylim', [0 max(SampUnique(:,2))+10]);

        xlabel(ax, 'duration [cycles]');
        ylabel(ax, 'force [N]');

        % Add some metadata below the plot
        % Enumerate all sample points
        text(SampUnique(:,1), SampUnique(:,2), num2str(sampNo'));
        
        
        set(ax, 'Units', 'pixels');
        set(fig, 'Units', 'pixels');

        deltaY = 300;   % pixels

        axPos = get(ax, 'Position');
        figPos = get(fig, 'Position');
        set(fig, 'Position', figPos + [0 -deltaY 0 deltaY]);
        set(ax, 'Position', axPos + [0 deltaY 0 0]);

        % Print header
        tab1 = -5;
        tab2 = 55;
        tab3 = 110;
        tab4 = 180;
        tab5 = 270;
        
        text(tab1, -20, 'Dot');
        text(tab2, -20, 'Mean');
        text(tab3, -20, 'Variance');
        text(tab4, -20, '# Polygons');
        text(tab5, -20, '# Stones');
        
        % Print data
        for i=1:size(SampUnique, 1)
            text(tab1, -20-5*i, num2str(sampNo(i)));
            text(tab2, -20-5*i, num2str(valueMean(i), '%5.3f'));
            text(tab3, -20-5*i, num2str(valueVar(i), '%6.4f'));
            text(tab4, -20-5*i, num2str(numSamp(i)));
            text(tab5, -20-5*i, num2str(numStones(i)));
        end

    end

end