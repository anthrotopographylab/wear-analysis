function ReportDatabase()
% Report some metadata of the databse. The main part shows statistics of
% topographic scans sorted by force level and duration. It should help to 
% determine which experiments or measurements are missing or have to be
% repeated.

% TO DO:
% Check on each table individually for missing values and report them.

% Get some related data
parameters = { ...
    'Scanname', 'is3D', 'isExport', 'isDocuOri', ...
    'Orientation', 'Magnification', ...
    'RunNo', 'SampleID', 'IterationNo', 'Date', ...
    'Force', 'NumCycCum', 'WorkpieceID'};
data = QuerryMeta( parameters );


% Find overall compelete records
isMissing = ismissing(data);
isComplete = ~any(isMissing, 2);

% Look for stone samples which are not jet scanned
isMissingAnalysis = ismissing(data(:, 'Scanname'));

fprintf('\nExperiments (Run ID) which were not scanned:\n');
disp(unique(data{isMissingAnalysis, 'RunNo'}));

% Distinguish complete records with and without value for orientation 
isOK    = ~any(ismissing(data(:, setdiff(parameters, 'Orientation'))), 2);
isOri   = ~ismissing(data(:, 'Orientation'));


% Go through all combinations of force levels and durations
% I) With orientation measurement
dataGotOri = data(isOK & isOri, :);
[G, GID] = findgroups(dataGotOri(:, {'Force', 'NumCycCum'}));

fprintf('\nI) Records having orientation value.\n');

for i=1:height(GID)
    force       = GID{i, 1};
    duration    = GID{i, 2};
    
    fprintf('Force Level: %3.2f\n', force);
    fprintf('\tDuration: %u\n', duration);
    
    reportGroup(dataGotOri(G == i, :));
end


% II) Without orientation measurement
dataNoOri = data(isOK & ~isOri, :);
[G, GID] = findgroups(dataNoOri(:, {'Force', 'NumCycCum'}));

fprintf('\nII) Records without value for orientation.\n');

for i=1:height(GID)
    force       = GID{i, 1};
    duration    = GID{i, 2};
    
    fprintf('Force Level: %3.2f\n', force);
    fprintf('\tDuration: %u\n', duration);
    
    reportGroup(dataNoOri(G == i, :));
end



% Show current iteration state of the experiments / stone samples
% expstate = QuerryMeta({'RunNo', 'SampleID', 'IterationNo'});
fprintf('\nLast experimental iteration:\n');
[esSort, iSort] = sortrows(data{:, {'IterationNo', 'SampleID'}}, [2, 1]);
[~, ia, ~] = unique(esSort(:,2), 'last', 'legacy');

disp(sortrows(data(iSort(ia), ...
    {'RunNo', 'SampleID', 'IterationNo', 'NumCycCum', 'Force', 'Date'})));


function s = reportGroup(groupData)
    s = struct;

    [Gsub, GIDsub] = findgroups(groupData.RunNo);
    
    s.Runs = GIDsub;
    
    for j=1:length(GIDsub)
        s.NumScans3d50(j)   = length(unique( ...
            groupData{groupData.Magnification== 50 & Gsub==j, 'Scanname'}));
        s.NumScans3d100(j)  = length(unique( ...
            groupData{groupData.Magnification==100 & Gsub==j, 'Scanname'}));
        s.NumScansDoc(j)    = length(unique( ...
            groupData{groupData.Magnification < 50 & Gsub==j, 'Scanname'}));
        s.MedianScansDocMag(j) = median( ...
            groupData{groupData.Magnification < 50 & Gsub==j, 'Magnification'});
    end
    
    s.NumNotExported = length(unique( ...
            groupData{groupData.is3D & ~groupData.isExport, 'Scanname'}));
            
    % Print out statistics
    fprintf('\t\t\t# runs:\t\t\t%u\n',          length(s.Runs));
    fprintf('\t\t\t# scans 50x:\t\t%u\n',     sum(s.NumScans3d50));
    fprintf('\t\t\t# scans 100x:\t\t%u\n',    sum(s.NumScans3d100));
    fprintf('\t\t\t# not exported:\t\t%u\n',  s.NumNotExported);
    fprintf('\t\t\t# doc scans:\t\t%u\n',     sum(s.NumScansDoc));
    fprintf('\t\t\tmedian doc mag:\t\t%u\n',  median(s.MedianScansDocMag));
end


end