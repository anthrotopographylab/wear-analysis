function [ reportTable ] = mlReportTable( mlTable )
%mlReportTable Converts a MATLAB table datatype to mlreportgen.dom.Table.
%   Takes mlTable as input and returns a mlreportgen.dom.Table, which
%   contains all rows of the variables of mlTable, which are of the
%   following datatype: numeric, charcell, ...

validateattributes(mlTable, {'table'}, {'2d', 'nonempty'}, mfilename, 'mlTable');
 
% Create new report table
reportTable =  mlreportgen.dom.FormalTable(4);
reportTable.Border = 'single';
reportTable.ColSep = 'single';
reportTable.RowSep = 'single';

variableNames = mlTable.Properties.VariableNames;

% Check datatype of each variable, remember it for char conversion later
variableIndex = double.empty;
variableCallback = {};

header =  mlreportgen.dom.TableRow;

for i=1:width(mlTable)
    
    % Check variable data type
    if isnumeric(mlTable{1, i}) 
        variableIndex(end+1) = i;
        variableCallback{end+1} = @num2str; 
    elseif iscellstr(mlTable{1,i})
        variableIndex(end+1) = i;
        variableCallback{end+1} = @char;
    elseif iscategorical(mlTable{1,i})
        variableIndex(end+1) = i;
        variableCallback{end+1} = @char;
    elseif isstring(mlTable{1,i})
        variableIndex(end+1) = i;
        variableCallback{end+1} = @char;
    else
        warning('Datatype of %s not supported. Skipp.', variableNames{i});
        continue;
    end
    
    % If test above is passed: Paste variable names into header
    header.append(mlreportgen.dom.TableEntry(variableNames{i}));

end

numVars = length(variableIndex);

% Append variable names as header
reportTable.Header.append(header);

% Fill table body
rowfun(@appendRow, mlTable);

% for i = 1:mySample.numRuns
%   header = TableRow;
%   append(header, TableEntry(num2str(mySample.Runs(i).IterationNo)));
%   append(header, TableEntry(num2str(mySample.Runs(i).Force)));
%   append(header, TableEntry(num2str(mySample.Runs(i).NumCycles)));
%   append(header, TableEntry(char(mySample.Runs(i).Date)));
%   table.append(header);
% end

    function out = appendRow(varargin)
        newrow = mlreportgen.dom.TableRow;
        
        for i=1:numVars
            if ismissing(varargin{variableIndex(i)})
                newrow.append(mlreportgen.dom.TableEntry(''));
            else
                convfun = variableCallback{i};
                newtext = convfun(varargin{variableIndex(i)});
                newrow.append(mlreportgen.dom.TableEntry(newtext));
            end
        end
        
        reportTable.append(newrow);
        out = 1;
    end


end

