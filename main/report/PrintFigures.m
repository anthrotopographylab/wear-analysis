function [filename] = PrintFigures( varargin )
% Print multiple figures into one page A4.
% If the height of the figure is too large scaling is applied keeping the
% aspect ratio.

global DIR_RESULTS;

% Handle User input
if ischar(varargin{end})
    filename = varargin{end};
    numFigures = length(varargin)-1;
else
    filename = './tmp.pdf';
    numFigures = length(varargin);
end

filename = [DIR_RESULTS 'tmp_1.pdf'];


% Create common figure where all input figures are copied to and arranged
% subsequently. This common figure has A4 size.
pageWidth   = 21.0;   % [cm]
pageHeight  = 29.7;   % [cm]
pageMargin  = 0.5;      % [cm]

figPosYSum = pageHeight;

figMaster = figure('Visible', 'off');
% set(figMaster, 'Visible', 'on');
set(figMaster, 'Units', 'centimeters'); 
set(figMaster, 'Position', [23 -5 pageWidth pageHeight]);
set(figMaster, 'resize', 'off');
set(figMaster, 'ToolBar', 'none');
set(figMaster, 'MenuBar', 'none');

set(figMaster, 'PaperUnits','centimeters');
set(figMaster, 'PaperSize', [pageWidth pageHeight]);
set(figMaster, 'PaperPositionMode', 'manual'); 
set(figMaster, 'PaperPosition', ...
    [pageMargin pageMargin pageWidth-2*pageMargin pageHeight-2*pageMargin]);

% Go through figures from input
for i=1:numFigures
    fig = varargin{i};
    set(fig, 'units', 'centimeters');
    figPos = get(fig, 'Position');
    
    % Find relevant objects
    figGraphics = findobj(fig, ...
        'Type', 'axes', '-or', ...
        'Type', 'legend', '-or', ...
        'Type', 'colorbar');
%         'Type', 'text');
    
    % Copy graphics objects into master figure and set new handle
    set(figGraphics, 'Units', 'centimeters');
    figGraphics = copyobj(figGraphics, figMaster);
    
    if figPos(4) > pageHeight
        % scale down to fit page height
        scaleH = pageHeight / figPos(4);
    else
        scaleH = 1;
    end
    
    if figPos(3)*scaleH > pageWidth
        %scale down to fit page width
        scaleW = pageWidth / (figPos(3)*scaleH);
    else
        scaleW = 1;
    end
    
    scale = scaleW*scaleH;
    figPosYSum = figPosYSum - figPos(4)*scale;
    
    % Shift and scale graphics objects
    for j=1:length(figGraphics)      
        if strcmp(figGraphics(j).Type, 'axes')
            pos = get(figGraphics(j), 'OuterPosition');
            posTrans = pos * scale + [0 figPosYSum 0 0];
            set(figGraphics(j), 'OuterPosition', posTrans);
        else
            pos = get(figGraphics(j), 'Position');
            posTrans = pos * scale + [0 figPosYSum 0 0];
            set(figGraphics(j), 'Position', posTrans);
        end
        
    end
    
    % Collect handles of graphics objects for each figure
    figMasterGraphics{i} = figGraphics;
    
end

% copyobj(figGraphics, figure);

% Rescale each graphics object to fit all on one page if necessary
if figPosYSum < 0
    % scale down and shift
    scale = pageHeight / (pageHeight-figPosYSum);
    deltaY = figPosYSum*scale;
    
    for i=1:length(figMasterGraphics)    
        figGraphics = figMasterGraphics{i};
        
        for j=1:length(figGraphics)
            if strcmp(figGraphics(j).Type, 'axes')
                pos = get(figGraphics(j), 'OuterPosition');
                posTrans = pos * scale - [0 deltaY 0 0];
                set(figGraphics(j), 'OuterPosition', posTrans);
            else
                pos = get(figGraphics(j), 'Position');
                posTrans = pos * scale -[0 deltaY 0 0];
                set(figGraphics(j), 'Position', posTrans);
            end
        end
 
    end
    
end

% Handle filename to prevent overwriting of existing files
% fnBase = regexprep(filename, '.\w+$', '');
% fnExt = regexprep(filename, '.*\.(\w+)$', '$1');
%  
% while exist(filename, 'file')
%     
%     postfNumStr = regexprep(fnBase, '.*_(\d+)', '$1');
%     postfNum = str2num(postfNumStr) + 1;
%     filename = strcat(fnBase, num2str(postfNum), '.', fnExt);
%     
% end
    
% increpemt filename counter until filename is unique
while exist(filename, 'file')
    fnBase = regexprep(filename, '_\d+.pdf', '');
    postfNumStr = regexprep(filename, '.*_(\d+).pdf', '$1');
    postfNum = str2num(postfNumStr) + 1;

    filename = [fnBase '_' num2str(postfNum) '.pdf'];
    
%     display(filename);
end

saveas(figMaster, filename, 'pdf'); 
% 
% % keyboard
% 
close(figMaster);



    
end
