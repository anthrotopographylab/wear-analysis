function [fig50, fig100] = PlotFD2HL(Parameters, paraName)
% Plot surface roughness of the selections on a grid with duration on the x
% axis and force on the y axis. Creates a separate subplot for each force 
% level. The values for each duration-force coordinate is given as mean and 
% variance.
%
% INPUT
% Parameters:   Holds the values for different roughness parameters
% paraName:     The roughness parameter to be plotted.


% Specify fieldnames of META
ME_NAME         = 'Scanname';
ME_CYCACCU      = 'NumCycCum';
ME_FORCE        = 'Force';
ME_SID          = 'SampleID';
ME_MAG          = 'Magnification';


% CHECK INPUT *************************************************************
validateattributes(Parameters, {'HeightParameters'}, {'vector'}, mfilename, 'Parameters');

paraOptions = {'Sq', 'Sa', 'Ssk', 'Sku', 'rep'};
if ~any(strcmp(paraName, paraOptions));
    error('MWA:PlotDurationHL:invalidInput', ...
        'paraName must be one of %s.', [paraOptions{:}]);
end


% COLLECT DATA ************************************************************

% Sync with metadata with scans of 
% 50x 
% meta50 = META([META.Magnification]==50);
% 
% [~, indexMeta, indexPara] = ...
%     IntersectMulti(meta50, Parameters);

meta = QuerryMeta( {ME_NAME, ME_MAG}, {Parameters.scanname});

% 50x
is50 = [meta.(ME_MAG)]==50;
if any(is50)
    fig50 = plotParameters(Parameters(is50));
    fig50.Children(1).Title.String = sprintf('Mean of %s on 50x', paraName);
end

% 100x
is100 = [meta.(ME_MAG)]==100;
if any(is100)
    fig100 = plotParameters(Parameters(is100));
    fig100.Children(1).Title.String = sprintf('Mean of %s on 100x', paraName);
end

% PLOT ********************************************************************

    function fig = plotParameters(paraMag)

        [metaMag] = QuerryMeta( ...
            {ME_NAME, ME_MAG, ME_FORCE, ME_CYCACCU}, ...
            {paraMag.scanname});
        
        [g, idForce, idDuration] = findgroups([metaMag.(ME_FORCE)], [metaMag.(ME_CYCACCU)]);
        
        forces       = unique(idForce);
        durations    = unique(idDuration);
        
        numG = max(g);
        numF = length(forces);
        numD = length(durations);
        
        vMean = splitapply(@mean, [paraMag.(paraName)]', g);
        vVar = splitapply(@var, [paraMag.(paraName)]', g);        
        
        % Plot values in separate plots for each force level. Each subplot
        % shows the roughness paramters over duration.
        fig = figure;
        title = 'Force vs. Duration';
        set(fig, 'Name', title);
        
        ha = zeros(1, numF);
        he = zeros(1, numF);
        
        for i=1:numF
            ha(i) = subplot(numF, 1, numF+1-i);
            force = forces(i);
            
            isGForce = (idForce == force);
            
            he(i) = errorbar(idDuration(isGForce), ...
                vMean(isGForce), 4*vVar(isGForce));
            
            ylabel([num2str(force), ' N']);
            grid on;
        end
        
        vMinGlob = min(min(vMean));
        vMaxGlob = max(max(vMean));
        
        xlabel(ha(1), 'duration [cycles]');
        axis(ha, [0 max(durations)+50 vMinGlob vMaxGlob]);
        
        set(he, 'LineStyle', 'none');
        set(he, 'Marker', 'x');
        set(he, 'MarkerSize', 8);
        

%         % Print header
%         tab1 = -5;
%         tab2 = 55;
%         tab3 = 110;
%         tab4 = 180;
%         tab5 = 270;
%         
%         text(tab1, -20, 'Dot');
%         text(tab2, -20, 'Mean');
%         text(tab3, -20, 'Variance');
%         text(tab4, -20, '# Polygons');
%         text(tab5, -20, '# Stones');
%         
%         % Print data
%         for i=1:size(SampUnique, 1)
%             text(tab1, -20-5*i, num2str(sampNo(i)));
%             text(tab2, -20-5*i, num2str(valueMean(i), '%5.3f'));
%             text(tab3, -20-5*i, num2str(valueVar(i), '%6.4f'));
%             text(tab4, -20-5*i, num2str(numSamp(i)));
%             text(tab5, -20-5*i, num2str(numStones(i)));
%         end

    end

end