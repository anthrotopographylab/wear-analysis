function fig = plot3dVarGroup(x, y, z, varargin)
% Plot surface roughness of the selections on a grid with duration on the x
% axis and force on the y axis. Creates a separate subplot for each force 
% level. The values for each duration-force coordinate is given as mean and 
% variance.
%
% INPUT
% Parameters:   Holds the values for different roughness parameters
% paraName:     The roughness parameter to be plotted.


% Specify fieldnames of META
ME_NAME         = 'Scanname';
ME_CYCACCU      = 'NumCycCum';
ME_FORCE        = 'Force';
ME_SID          = 'SampleID';
ME_MAG          = 'Magnification';


% CHECK INPUT *************************************************************
% validateattributes(Parameters, {'HeightParameters'}, {'vector'}, mfilename, 'Parameters');
% 
% paraOptions = {'Sq', 'Sa', 'Ssk', 'Sku', 'rep'};
% if ~any(strcmp(paraName, paraOptions));
%     error('MWA:PlotDurationHL:invalidInput', ...
%         'paraName must be one of %s.', [paraOptions{:}]);
% end


% COLLECT DATA ************************************************************

% Sync with metadata with scans of 
% 50x 
% meta50 = META([META.Magnification]==50);
% 
% [~, indexMeta, indexPara] = ...
%     IntersectMulti(meta50, Parameters);

% [metaMag] = QuerryMeta( ...
%     {ME_NAME, ME_MAG, ME_FORCE, ME_CYCACCU}, ...
%     {Parameters.scanname});
% 
% x = [metaMag.(ME_CYCACCU)];
% y = [metaMag.(ME_FORCE)];
% 
% z = [Parameters.(paraName)]';
% 
% fig = plotParameters(x, y, z);


% % 50x
% is50 = [meta.(ME_MAG)]==50;
% if any(is50)
%     fig50 = plotParameters(Parameters(is50));
%     fig50.Children(1).Title.String = sprintf('Mean of %s on 50x', paraName);
% end
% 
% % 100x
% is100 = [meta.(ME_MAG)]==100;
% if any(is100)
%     fig100 = plotParameters(Parameters(is100));
%     fig100.Children(1).Title.String = sprintf('Mean of %s on 100x', paraName);
% end

% PLOT ********************************************************************

% Check optional input
if      nargin == 3
    varFctr = 1;
elseif  nargin == 4
    validateattributes(varargin{1}, {'numeric'}, {'scalar'}, mfilename, 'variance factor');
    varFctr = varargin{1};
else
    error('Too many input arguments')
end


        validateattributes(x, {'numeric'}, {'vector'}, mfilename, 'x');
        validateattributes(y, {'numeric'}, {'size', size(x)}, mfilename, 'y');
        validateattributes(z, {'numeric', 'categorical'}, {'size', size(x)}, mfilename, 'z');
        
        xbin1 = [25 175];
        xbin2 = [210 290];
        xbin3 = [310 390];
        xbin4 = [410 490];
        
        xBins = [xbin1; xbin2; xbin3; xbin4];
        
        xBinIndex = NaN(size(y));
        xBinIndex(x > xbin1(1) & x < xbin1(2)) = 1;
        xBinIndex(x > xbin2(1) & x < xbin2(2)) = 2;
        xBinIndex(x > xbin3(1) & x < xbin3(2)) = 3;
        xBinIndex(x > xbin4(1) & x < xbin4(2)) = 4;
        
        [g, idX, idY] = findgroups(xBinIndex, y);
        
%         [g, idX, idY] = findgroups(x, y);
        
        xUnique = unique(idX);
        yUnique = unique(idY);
        
        numG = max(g);
        numX = length(xUnique);
        numY = length(yUnique);
        
        zMean   = splitapply(@mean, z, g);
        zVar    = splitapply(@var, z, g);
        
        fig = figure;
        title = 'Force vs. Duration';
        set(fig, 'Name', title);
        set(fig, 'Position', [1000, 350, 700, 500]);
        
        % Plot values in separate subplot for each y level. Each subplot
        % shows the z over x.
        ha = zeros(1, numY);
        he = zeros(1, numY);
        
        for i=1:numY
            ha(i) = subplot(numY, 1, numY+1-i);
            
            yi = yUnique(i);
            
            isGY = (idY == yi);
            
            he(i) = errorbar(idX(isGY), ...
                zMean(isGY), varFctr*zVar(isGY));
            
            ylabel(num2str(yi));
            
            set(ha(i), 'Tag', sprintf('x%u', yi));
            
            grid on;
        end
        
        % Set axis range
        zMinGlob = min(min(zMean-varFctr*zVar));
        zMaxGlob = max(max(zMean+varFctr*zVar));
        
%         xlabel(ha(1), 'duration [cycles]');
        deltaX = max(xUnique) - min(xUnique);
        deltaY = zMaxGlob - zMinGlob;
        axis(ha, [min(xUnique)-0.1*deltaX max(xUnique)+0.1*deltaX ...
            zMinGlob-0.1*deltaY zMaxGlob+0.1*deltaY]);
        
        % Set XTicks
        set(ha, 'XTick', xUnique);
        set(ha, 'XTickLabel', '');
        
        xLabel = cell(1, size(xBins, 1));
        
        for i=1:size(xBins, 1)
            xLabel{i} = regexprep(num2str(xBins(i,:)), '  ', '-');
        end
        
        set(ha(1), 'XTickLabel', xLabel);
        
        % Set errorbar format
        set(he, 'LineStyle', 'none');
        set(he, 'Linewidth', 1.3);
        set(he, 'Marker', '.');
        set(he, 'MarkerSize', 10);


    end

