function [fig100] = PlotParametersHL(Parameters, Selections, plotMode)
% Plot Parameters in Boxplots using PlotStruct.m
% Boxplots are sorted by magnification and according to the plot mode.
% The plot mode must be specified as one of:
% 'label'
% 'iteration'
% 'iter-label-max'
% 'force'
% 'force-iter-max'
% 

% TO DO: Write function to expand nested cells for selection in Parameters
% and sort from there directly.

FN_NAME     = 'Scanname';
FN_ITER_NO  = 'IterationNo';
FN_MAG      = 'Magnification';
FN_FORCE    = 'Force';
FN_SID      = 'SampleID';

% CHEKC INPUT *************************************************************
validateattributes(Parameters, {'HeightParameters'}, {'vector'}, mfilename, 'Parameters');
validateattributes(Selections, {'Selection'}, {'vector'}, mfilename, 'Selections');
validateattributes(plotMode, {'char'}, {'vector'}, mfilename, 'plotMode');

% Crop metadata
% [~, indexPara, indexMeta] = IntersectMulti(Parameters, META);

% link parameters and selections 'manually' and only take objects that
% have metadata 
[c, indexPara, indexSel] = intersect({Parameters.scanname}, {Selections.imageName});
paraSort = Parameters(indexPara);
selSort = Selections(indexSel);

[metaSort] = QuerryMeta( ...
    {FN_NAME, FN_MAG, FN_ITER_NO, FN_FORCE, FN_SID}, c);


% Sort values by parameter
% paraAll = [Parameters{:,2}];

% Initialize structs to be sent to PlotStruct
Sq_50x      = struct;
Sa_50x      = struct;
Ssk_50x     = struct;
Sku_50x     = struct;
Sq_100x     = struct;
Sa_100x     = struct;
Ssk_100x    = struct;
Sku_100x    = struct;

is100x = [metaSort.(FN_MAG)] == 100;
is50x = [metaSort.(FN_MAG)] == 50;

switch lower(plotMode)
    
    case {'label'}

        labels = categorical({selSort.label}, ...
            {'raw', 'low', 'medium', 'high'}, ...
            'Ordinal', true);      
        
        Sq = struct( ...
            'raw', [paraSort(labels == 'raw').Sq], ...
            'low', [paraSort(labels == 'low').Sq], ...
            'med', [paraSort(labels == 'medium').Sq], ...
            'high', [paraSort(labels == 'high').Sq]);
        
        Sa = struct( ...
            'raw', [paraSort(labels == 'raw').Sa], ...
            'low', [paraSort(labels == 'low').Sa], ...
            'med', [paraSort(labels == 'medium').Sa], ...
            'high', [paraSort(labels == 'high').Sa]);
        
        Ssk = struct( ...
            'raw', [paraSort(labels == 'raw').Ssk], ...
            'low', [paraSort(labels == 'low').Ssk], ...
            'med', [paraSort(labels == 'medium').Ssk], ...
            'high', [paraSort(labels == 'high').Ssk]);
        
        Sku = struct( ...
            'raw', [paraSort(labels == 'raw').Sku], ...
            'low', [paraSort(labels == 'low').Sku], ...
            'med', [paraSort(labels == 'medium').Sku], ...
            'high', [paraSort(labels == 'high').Sku]);
        
        
        Accuracy = struct( ...
            'raw', [paraSort(labels == 'raw').rep], ...
            'low', [paraSort(labels == 'low').rep], ...
            'med', [paraSort(labels == 'medium').rep], ...
            'high', [paraSort(labels == 'high').rep]);            
        
%         out = SortByLabel(paraSort(is50x, :), {'raw', 'low', 'medium', 'high'});
%         Sq_50x = struct( ...
%             'raw', [out{1}.Sq], ...
%             'low', [out{2}.Sq], ...
%             'med', [out{3}.Sq], ...
%             'high', [out{4}.Sq]);
% 
%         Sa_50x = struct( ...
%             'raw', [out{1}.Sa], ...
%             'low', [out{2}.Sa], ...
%             'med', [out{3}.Sa], ...
%             'high', [out{4}.Sa]);
% 
%         Ssk_50x = struct( ...
%             'raw', [out{1}.Ssk], ...
%             'low', [out{2}.Ssk], ...
%             'med', [out{3}.Ssk], ...
%             'high', [out{4}.Ssk]);
% 
%         Sku_50x = struct( ...
%             'raw', [out{1}.Sku], ...
%             'low', [out{2}.Sku], ...
%             'med', [out{3}.Sku], ...
%             'high', [out{4}.Sku]);
    
    case {'iteration'}
        
        isIter0 = [metaSort.(META_ITER_NO)] == 0;
        isIter1 = [metaSort.(META_ITER_NO)] == 1;
        isIter2 = [metaSort.(META_ITER_NO)] == 2;
        isIter3 = [metaSort.(META_ITER_NO)] == 3;
        isIter4 = [metaSort.(META_ITER_NO)] == 4;
        
        ParaIter0 = [paraSort{isIter0, 2}];
        
        for si={'Sq', 'Sa', 'Ssk', 'Sku'}
            
            si = si{1};
            
            plotStruct = struct( ...
                'Iter0', expandParameter(isIter0 & is50x, si), ...
                'Iter1', expandParameter(isIter1 & is50x, si), ...
                'Iter2', expandParameter(isIter2 & is50x, si), ...
                'Iter3', expandParameter(isIter3 & is50x, si), ...
                'Iter4', expandParameter(isIter4 & is50x, si));
            
            eval(strcat(si, '_50x', ' = plotStruct;'));
            
            plotStruct = struct( ...
                'Iter0', expandParameter(isIter0 & is100x, si), ...
                'Iter1', expandParameter(isIter1 & is100x, si), ...
                'Iter2', expandParameter(isIter2 & is100x, si), ...
                'Iter3', expandParameter(isIter3 & is100x, si), ...
                'Iter4', expandParameter(isIter4 & is100x, si));
            
            eval(strcat(si, '_100x', ' = plotStruct;'));
            
        end
        
    case {'iter-label-max'}
        
        isIter0 = [metaSort.(META_ITER_NO)] == 0;
        isIter1 = [metaSort.(META_ITER_NO)] == 1;
        isIter2 = [metaSort.(META_ITER_NO)] == 2;
        isIter3 = [metaSort.(META_ITER_NO)] == 3;
        isIter4 = [metaSort.(META_ITER_NO)] == 4;
        
        % loop over iteration no
        for iterNo = 0:4
            
            isIter = [metaSort.(META_ITER_NO)] == iterNo;
            
            fname = sprintf('Iter%u', iterNo);
            
            % loop over magnification
            for mi = [50 100];
            
                paraLabel = [];
                isMag = [];
                
                eval(sprintf('isMag = is%ux;', mi));

                ParaMagIter = paraSort(isMag & isIter, :);
                
                % loop over scans to filter selections
                for scani = 1:size(ParaMagIter, 1)
                    
                    paraLabelScan = SortByLabel(ParaMagIter(scani, :), ...
                        {'raw', 'low', 'medium', 'high'});

                    % only take areas of max wear classification
                    if     ~isempty([paraLabelScan{4}.Sq])
                        paraLabel = [paraLabel [paraLabelScan{4}]];
                    elseif ~isempty([paraLabelScan{3}.Sq])
                        paraLabel = [paraLabel [paraLabelScan{3}]];
                    elseif ~isempty([paraLabelScan{2}.Sq])
                        paraLabel = [paraLabel [paraLabelScan{2}]];
                    elseif ~isempty([paraLabelScan{1}.Sq])
                        paraLabel = [paraLabel [paraLabelScan{1}]];
                    else
                        % invalid classification
                        warning('MWA:PlotParametersHL:invalidLabel', ...
                            ['Invalid label name found on scan %s ', ...
                            'Skipping'], ...
                            ParaMagIter{scani, 1});
                    end
                    
                end
                    
                for si = {'Sq', 'Sa', 'Ssk', 'Sku'}
                    si = si{1};

                    paraBox = [paraLabel.(si)];
                    
                    eval(sprintf('%s_%ux.%s = paraBox;', si, mi, fname));

                end
            end
        end
            
    case {'force'}
        
        isF30 = [metaSort.(LB_FORCE)] == 30;
        isF60 = [metaSort.(LB_FORCE)] == 60;
        isF90 = [metaSort.(LB_FORCE)] == 90;
        isF110 = [metaSort.(LB_FORCE)] == 110;
        
        for si={'Sq', 'Sa', 'Ssk', 'Sku'}
            
            si = si{1};
            
            plotStruct = struct( ...
                'F30N', expandParameter(isF30 & is50x, si), ...
                'F60N', expandParameter(isF60 & is50x, si), ...
                'F90N', expandParameter(isF90 & is50x, si), ...
                'F110N', expandParameter(isF110 & is50x, si));
            
            eval(strcat(si, '_50x', ' = plotStruct;'));
            
            plotStruct = struct( ...
                'F30N', expandParameter(isF30 & is100x, si), ...
                'F60N', expandParameter(isF60 & is100x, si), ...
                'F90N', expandParameter(isF90 & is100x, si), ...
                'F110N', expandParameter(isF110 & is100x, si));
            
            eval(strcat(si, '_100x', ' = plotStruct;'));
            
        end
        
    case {'force-iter-max'}
        
        % Find scans of the moset recent iteration, i.e. max iteration num.
        sidIterSort = sortrows([ (1:length(metaSort))' ...
            [metaSort.(META_ITER_NO)]', ...
            [metaSort.(META_SID)]' ], [-2 3]);
        
        [sidIterUnique, ~, ~] = unique(sidIterSort(:, 2:3), 'rows', 'stable');
        
        [~, ia, ib] = unique(sidIterUnique(:,2), 'stable');
        
        indexSidIterMax = sidIterSort(ismember(sidIterSort(:,2:3), sidIterUnique(ia, :), 'rows'), 1);
        
        % linear to logical index:
        isIterMax = false(1, length(metaSort));
        isIterMax(indexSidIterMax) = true;
        
        
        isF30 = [metaSort.(LB_FORCE)] == 30;
        isF60 = [metaSort.(LB_FORCE)] == 60;
        isF90 = [metaSort.(LB_FORCE)] == 90;
        isF110 = [metaSort.(LB_FORCE)] == 110;
        
        for si={'Sq', 'Sa', 'Ssk', 'Sku'}
            
            si = si{1};
            
            plotStruct = struct( ...
                'F30N', expandParameter(isIterMax & isF30 & is50x, si), ...
                'F60N', expandParameter(isIterMax & isF60 & is50x, si), ...
                'F90N', expandParameter(isIterMax & isF90 & is50x, si), ...
                'F110N', expandParameter(isIterMax & isF110 & is50x, si));
            
            eval(strcat(si, '_50x', ' = plotStruct;'));
            
            plotStruct = struct( ...
                'F30N', expandParameter(isIterMax & isF30 & is100x, si), ...
                'F60N', expandParameter(isIterMax & isF60 & is100x, si), ...
                'F90N', expandParameter(isIterMax & isF90 & is100x, si), ...
                'F110N', expandParameter(isIterMax & isF110 & is100x, si));
            
            eval(strcat(si, '_100x', ' = plotStruct;'));
            
        end
        
    otherwise
        error('MWA:PlotParametersHL:invalidInput', ...
            ['plotMode must be one of ' ...
            '''label'', ''iteration'', ''iter-label-max'', ''force'', ''force-iter-max''.']);  
end

% PlotStruct(Sq_50x, Sa_50x, Ssk_50x, Sku_50x, 'equalyaxis', false);
% fig50 = gcf;

PlotStruct(Sq, Sa, Ssk, Sku, Accuracy, 'equalyaxis', false);
fig100 = gcf;

h=findobj(fig100, 'Tag', 'Sq');
h.YLabel.String = '[um]';

h=findobj(fig100, 'Tag', 'Sa');
h.YLabel.String = '[um]';

h=findobj(fig100, 'Tag', 'Ssk');
h.YLabel.String = '[-]';

h=findobj(fig100, 'Tag', 'Sku');
h.YLabel.String = '[-]';

h=findobj(fig100, 'Tag', 'Accuracy');
h.YLabel.String = '[m]';

    function valArray = expandParameter(isParameter, field)
        if any(isParameter)
            valStruct = [paraSort{isParameter, 2}];
            valArray = [valStruct.(field)];
        else
            valArray = [];
        end
    end



%     function out = sortByLabel(Para, labels)
%         % Sort on selection level
%         % Returns one or more struct vectors stored in a cell vector
%         for i=1:length(labels)
%             paraAll = [Para{:,2}];
%             isLabel = strcmp([Para{:,3}], labels{i});
%             paraLabel = paraAll(isLabel);
%             if isempty(paraLabel)
%                 out{i} = struct('Sq', NaN, 'Sa', NaN, 'Sku', NaN, 'Ssk', NaN);
%             else
%                 out{i} = paraLabel;
%             end
%         end
%     end



end

