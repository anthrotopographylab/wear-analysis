function [fig] = DisplayScan(varargin)
% Display surface scan. This can be either a depth or RGB image. 
% Display D-scan as image with pseudo color.
% Following ways of usage are supported:
% DisplayScan(scanD)
% DisplayScan(Scans)
% DisplayScan(scanD, zoom)
% DisplayScan(scanD, scanRGB)
% DisplayScan(Scans, zoom)
% DisplayScan(scanD, X, Y)
% DisplayScan(scanRGB, X, Y)
% DisplayScan(scan1, scan2, X, Y)
% DisplayScan(scan1, scan2, X, Y, zoom)


% CHECK USER INPUT ********************************************************
% default input values
X = [];
Y = [];
Scan2 = [];

valIni = 0.5;

zoom = [];
name = [];

if (isempty(varargin))
    error('MWA:DisplayScan:invalidInput', ...
        'Not enough input arguments');
end
if (length(varargin) == 1)
    input = varargin{1};
    
    if isnumeric(input)
        % THe input is a scan
        Scan1 = input;
    elseif iscell(input)
        % The input is a measurement consisting of
        % [scanname XCoord YCoord scanD scanRGB]
        validateattributes(input, ...
            {'cell'}, {'row', 'ncols', 5}, '', 'measurement');
        
        name = input{1};
        X = input{2};
        Y = input{3};
        Scan1 = input{4};
        Scan2 = input{5};
                
        scan1Res = [size(Scan1,1) size(Scan1,2)];
        validateattributes(Scan2, {'numeric'}, {'3d'}, '', 'Scan');
        validateattributes(X, {'numeric'}, {'size', scan1Res}, '', 'x coordinates');
        validateattributes(Y, {'numeric'}, {'size', scan1Res}, '', 'y coordinates');
    else
        error('MWA:DisplayScan:invalidInput', ...
            ['Single input must be either a scan or' ...
            'measurement stored cell']);
    end
    validateattributes(Scan1, {'numeric'}, {'3d'}, '', 'Scan');
end
if (length(varargin) == 2)
    if isnumeric(varargin{1})
        % Input 1 is a depth scan
        Scan1 = varargin{1};
        
        if isscalar(varargin{2})
            % Input 2 is zoom factor
            zoom = varargin{2};
        else
            % Input 2 is a RGB scan
            Scan2 = varargin{2};
            validateattributes(Scan2, {'numeric'}, {'3d'}, '', 'Scan');
        end
    elseif iscell(varargin{1})
        % The input is a measurement consisting of
        % [scanname XCoord YCoord scanD scanRGB]
        validateattributes(varargin{1}, ...
            {'cell'}, {'row', 'ncols', 5}, '', 'measurement');
        
        name =  varargin{1}{1};
        X =     varargin{1}{2};
        Y =     varargin{1}{3};
        Scan1 = varargin{1}{4};
        Scan2 = varargin{1}{5};
        
        % Input 2 is zoom factor
        zoom =  varargin{2};
        
        scan1Res = [size(Scan1,1) size(Scan1,2)];
        validateattributes(Scan2, {'numeric'}, {'3d'}, '', 'Scan');
        validateattributes(X, {'numeric'}, {'size', scan1Res}, '', 'x coordinates');
        validateattributes(Y, {'numeric'}, {'size', scan1Res}, '', 'y coordinates');
        validateattributes(zoom, {'numeric'}, {'scalar'}, '', 'zoom');
    else
        error('MWA:DisplayScan:invalidInput', ...
            ['First input must be either a scan or' ...
            'scans formated as cell']);
    end
    validateattributes(Scan1, {'numeric'}, {'3d'}, '', 'Scan');
end
if (length(varargin) == 3)
    Scan1 = varargin{1};
    X = varargin{2};
    Y = varargin{3};
    scan1Res = [size(Scan1,1) size(Scan1,2)];
    validateattributes(Scan1, {'numeric'}, {'3d'}, '', 'Scan');
    validateattributes(X, {'numeric'}, {'size', scan1Res}, '', 'x coordinates');
    validateattributes(Y, {'numeric'}, {'size', scan1Res}, '', 'y coordinates');
end
if (length(varargin) >= 4)
    Scan1 = varargin{1};
    Scan2 = varargin{2};
    X = varargin{3};
    Y = varargin{4};
    scan1Res = [size(Scan1,1) size(Scan1,2)];
    validateattributes(Scan1, {'numeric'}, {'3d'}, '', 'Scan');
    validateattributes(Scan2, {'numeric'}, {'3d'}, '', 'Scan');
    validateattributes(X, {'numeric'}, {'size', scan1Res}, '', 'x coordinates');
    validateattributes(Y, {'numeric'}, {'size', scan1Res}, '', 'y coordinates');
end
if (length(varargin) == 5)
    % Same pattern as at 4 input args
    % but one additional argument for initial slider value.
    zoom = varargin{5};
    validateattributes(zoom, {'numeric'}, {'scalar'}, '', 'zoom');
end
if (length(varargin) >= 6)
    error('Too many input arguments');
end
    

% PLOT ********************************************************************
scan1Res = [size(Scan1,1) size(Scan1,2)];

% Layout parameters in pixels
cbX = 30;
cbY = 50;
cbWidth = 20;
panelImWidthIni = 700;

if isempty(Scan2)
    panelCtrWidth = 0;
else
    panelCtrWidth = 100;
end

figPos  = [600 300 panelCtrWidth+panelImWidthIni 600];

slPos   = [20 40 20 150];

% Figure
fig = figure;
set(fig, 'Units', 'pixels');
set(fig, 'Position', figPos);
set(fig, 'SizeChangedFcn', {@fig_ResizeFun});

% Use panel to separate figure from colorbar and controls
panelIm = uipanel('Parent', fig);
panelCtr = uipanel('Parent', fig);

set(panelIm, 'Units', 'pixels', ...
    'Position', [panelCtrWidth 0 panelImWidthIni figPos(4)], ...
    'Tag', 'panelImage', ...
    'SizeChangedFcn', {@panelIm_ResizeFun});

set(panelCtr, 'Units', 'pixels', ...
    'Tag', 'panelControls', ...
    'Position', [0 0 panelCtrWidth figPos(4)]);

% Use two axes. One for each scan.
axLabel = axes('Parent', panelIm);
ax1 = axes('Parent', panelIm);
ax2 = axes('Parent', panelIm);

set([ax1 ax2 axLabel], 'Units', 'normalized');

% Draw dummy image for axes labels
imLabel = image(ones(size(Scan1)), 'CDataMapping', 'scaled', 'Parent', axLabel);

% Draw real images
im1 = image(Scan1, 'CDataMapping', 'scaled', 'Parent', ax1);

if ~isempty(Scan2)
    if ~isequal(size(Scan1), size(Scan2(:,:,1)))
        Scan2 = imresize(Scan2, size(Scan1));
    end
    
     im2 = image(Scan2, 'Parent', ax2);
    
    alpha(im1, 0.5);
    alpha(im2, 0.5);
    
    sl = uicontrol('Style','Slider', ...
        'Parent', panelCtr, ...
        'Units', 'pixels', ...
        'Position', slPos, ...
        'Value', valIni, ...
        'Callback', {@slider_callback1});
    
    slider_callback1([],[]);  % initial
end


% Set cordinate axes
if ~isempty([X Y])
    xscale = abs(min(min(X))-max(max(X)));
    yscale = abs(min(min(Y))-max(max(Y)));
    
    set([ax1 ax2 axLabel], 'XLim', [min(min(X)) max(max(X))]);
    set([ax1 ax2 axLabel], 'YLim', [min(min(Y)) max(max(Y))]);

    set([im1 imLabel], 'XData', [min(min(X)) max(max(X))]);
    set([im1 imLabel], 'YData', [min(min(Y)) max(max(Y))]);
    
    if ~isempty(Scan2)
            set([im2], 'XData', [min(min(X)) max(max(X))]);
            set([im2], 'YData', [min(min(Y)) max(max(Y))]);
    end
    
    xlabel(axLabel, 'x [um]');
    ylabel(axLabel, 'y [um]');
else    
    xscale = size(Scan1, 2);
    yscale = size(Scan1, 1);
end

% Keep aspect ration of the images
% set([ax1 ax2 axLabel], 'DataAspectRatio', [xscale yscale 1]);
set([ax1 ax2 axLabel], 'PlotBoxAspectRatioMode', 'manual', ...
    'PlotBoxAspectRatio', [xscale yscale 1]);

% Colorbars depending on # images and image type
if ~isempty(Scan2)
    cb1 = colorbar(ax1, 'Location', 'Eastoutside');
    cb2 = colorbar(ax2, 'Location', 'Eastoutside');
    
    if size(Scan1, 3) == 1
        ylabel(cb1, 'z [um]');
        cb2.Visible = 'off';
    elseif size(Scan2, 3) == 1
        ylabel(cb1, 'z [um]');
        cb1.Visible = 'off';
    end
elseif size(Scan1, 3) == 1
    cb1 = colorbar(ax1, 'Location', 'Eastoutside');
end

% Make ax1 and ax2 translucent
ax1.Visible = 'off';
ax2.Visible = 'off';

set(axLabel, 'TickDir', 'out');

% Set title
if ~isempty(Scan2)
    titleStr = 'RGB and 3d image';
else
    if size(Scan1, 3) == 3
        titleStr = 'RGB image';
    else
        titleStr = '3d image';
    end
end

% Set figure Name
fig.NumberTitle = 'off';
if ~isempty(name)
    fig.Name = ['Display ' name];
    titleStr = [titleStr ' of ' name];
else
    fig.Name = ['Display scan'];
end

title(axLabel, titleStr, 'Interpreter', 'none');

set(axLabel, 'Tag', 'axesLabel');
set(ax1, 'Tag', 'axes1');

% set([ax1], 'DataAspectRatioMode', 'manual');
% set([ax2], 'DataAspectRatioMode', 'manual');




% CALLBACKS ***************************************************************
 
    function slider_callback1(~, ~)
       
        val = get(sl, 'Value');
        alIm1 = val;
        alIm2 = 1-val;
        
        alpha(im1, alIm1);
        alpha(im2, alIm2);
        
    end

    function fig_ResizeFun(~, ~)
        
        set([panelIm panelCtr], 'Visible', 'off');
        
        pos = getpixelposition(fig);
        posPCtr = getpixelposition(panelCtr);

        set(panelCtr, ...
            'Position', [posPCtr(1) 0 posPCtr(3) pos(4)]);
        set(panelIm, ...
            'Position', [posPCtr(3) 0 pos(3)-posPCtr(3) pos(4)]);
        
%         axPosRel = get(ax1, 'Position');
%         panelPosRel = get(panelCtr, 'Position');
%         
%         set(cb, 'Units', 'normalized', ...
%             'Position', [panelPosRel(1)+0.1*panelPosRel(3) axPosRel(2) ...
%             0.4*panelPosRel(3) axPosRel(4)]);
        
        set([panelIm panelCtr], 'Visible', 'on');

    end

    function panelIm_ResizeFun(~, ~)
        
        set(axLabel, 'Visible', 'off');
        
        pos = getpixelposition(ax1);
        
        set(axLabel, 'Units', 'pixels', 'Position', pos);
        
        set(axLabel, 'Visible', 'on');

    end

%     function zoom_callbacl(~, ~

end

