function im = addFigures(figs)
    global counter;
    
    for i=1:numel(figs)
        counter = counter + 1;
        
        filename = sprintf('fig%u.png', counter);

        % Save figures temporarily
        mypath = fullfile('/tmp', filename); 
        saveas(figs(i), mypath);

        im(i) = mlreportgen.dom.Image(mypath);
        
        % Sync figure and image size
        pos = get(figs(i), 'Position');
        im(i).Height     = [num2str(pos(4)) 'px'];
        im(i).Width      = [num2str(pos(3)) 'px'];
    end
end