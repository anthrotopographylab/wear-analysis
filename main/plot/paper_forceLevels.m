%paper_forceLevels Plot script for 2nd paper: cartesian s force comared.
% Plot trajectories of cartesian force in z for each force level.

% Set-up the environment 
clear all; close all;

run startMWA;

import mlreportgen.dom.*;

% Load images and selections and height parameters
load(fullfile(DIR_DATA, 'DS_NewYork.mat'));

% Construct data objects
runNo = unique(PARA.Logbook{:,'RunNo'});

runs = arrayfun(@ExpRun, runNo(runNo>0), 'UniformOutput', false);
runs = sort([runs{:}]);

scannames = unique(PARA.IndexScans{:,'Scanname'});
isEmpty = cellfun(@isempty, scannames);

scans = cellfun(@AliconaImage, PARA.IndexScans{:,'Scanname'}, 'UniformOutput', false);
scans = sort([scans{:}]);

% Container objects;
meas = Measurement(scans);
samples = SampleHistory(runs, meas);

% Link images and selections
[a, b] = selAll.isRelated(scans, {'imageID'}, {'Name'});
selAll(a).linkTo(scans(b));

% Link selections and height parameters
[a, b] = selAll.isRelated(paraAll, {'ID'}, {'polygonID'});
selAll(a).linkTo(paraAll(b))


% Link runs and images
% Cannot use isRelated here because it does not provide dynamic properties
% used by runs. Instead, convert to tables and use innerjoin.
valRuns = [{runs.IterationNo}', {runs.SampleID}'];
valScans = [num2cell(scans.getProp('IterationNo'))', num2cell(scans.getProp('SampleID'))'];

varnames = strcat('v', cellstr(categorical(1:2)));
[~, iR, iS] = innerjoin( ...
    cell2table(valRuns, 'VariableNames', varnames), ...
    cell2table(valScans, 'VariableNames', varnames));

runs(iR).linkTo(scans(iS));


%% Set format styles
% fonst size
fsTitle = 10;
fsAxesLabel = 10;
fsAxes = 10;

% Text interpreter
tinterp = 'Latex';

% Figure size [cm]
figsizeFDC = [0 20 18 20];
figsizeBox = [0 20 21 15];


%% Plot force trajectories of 4 runs in one figure

fig = figure; 
fig.Units = 'centimeters';
fig.Position = [0 20 21 18];

ha(1) = subplot(4, 1, 1);
runs(2).Trajectory.CartForceN_Z.plot
grid on;
ha(2) = subplot(4,1,2);
runs(1).Trajectory.CartForceN_Z.plot
grid on;
ha(3) = subplot(4,1,3);
runs(28).Trajectory.CartForceN_Z.plot
grid on;
ha(4) = subplot(4,1,4);
runs(5).Trajectory.CartForceN_Z.plot
grid on;

xlim = [0 200];
[ha.XLim] = deal(xlim);
[ha.YLim] = deal([-10 150]);

myht = [ha.Title];
[myht.String] = deal('');

myhyl = [ha.YLabel];
[myhyl.String] = deal('');

myhxl = [ha(1:3).XLabel];
[myhxl.String] = deal('');

ha(4).XLabel.Interpreter = tinterp;
ha(4).XLabel.FontSize = fsAxesLabel;

[ha.TickLabelInterpreter] = deal(tinterp);
[ha.FontSize] = deal(fsAxes);

line(xlim, [110 110], 'Parent', ha(1), 'Color', 'black', 'linestyle', '--');
line(xlim, [90 90], 'Parent', ha(2), 'Color', 'black', 'linestyle', '--');
line(xlim, [60 60], 'Parent', ha(3), 'Color', 'black', 'linestyle', '--');
line(xlim, [30 30], 'Parent', ha(4), 'Color', 'black', 'linestyle', '--');

text(-70, 275, 'Z Force [N]', ...
    'Rotation', 90, ...
    'VerticalAlignment', 'middle', ...    
    'FontSize', fsAxesLabel, ...
    'Interpreter', tinterp, ...
    'Units', 'pixel', ...
    'Parent', ha(4));

% Save as eps
filename = fullfile(DIR_RESULTS, 'paper_forceLevels.eps');
print('-r300', fig, filename, '-depsc');
system(sprintf('exiftool -Title=''%s'' %s', 'Tajectories of z force', filename), '-echo');


