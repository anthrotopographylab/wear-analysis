%paper_exampleHist.m Plot a nice example of the height value histogram

% Set-up the environment 
clear all; close all;

run startMWA;

% Choose the following ROI:
prettyScan = 'S123_use1_ori50_pos20_x100';
prettyRoi = 148965823;


% Load images and selections and height parameters
load(fullfile(DIR_DATA, 'DS_NewYork.mat'));
load(fullfile(DIR_DATA, 'Selection6.mat'));
load(fullfile(DIR_RESULTS, 'results_paper.mat'));

% Make variable names consistent
selAll = [selEdge100 selFlank100 selMisc selMisc50];

clear selEdge100 selFlank100 selMisc selMisc50;

% Find duplicates in mySelection
[~, idx] = unique([selAll.ID]);
selAll = selAll(idx);

% Make sure that objects are not linked jet
selAll.breakLinks;


scan = AliconaImage(prettyScan);
roi = selAll([selAll.ID] == prettyRoi);
roi.linkTo(scan);

% get roi of filtered z values
[para, ~] = EvaluateSelectionsHL(roi);


%% Set format styles
% fonst size
fsTitle = 12;
fsAxesLabel = 10;
fsAxes = 10;

% Text interpreter
tinterp = 'Latex';
fontAll = 'TimesRoman';

% Figsize in [cm]
figsizeFDC = [20 0 18 20];
figsizeBox = [20 0 21 13];
figsizeBoxSmall = [20 0 8 13];



%% Make a nice plot
fig = figure; h = histogram(para{2}.frameZmean(para{2}.frameRoi));

fig.Units = 'centimeters';
fig.Position = [20 0 18 15];

mytitle = sprintf('Histogram of height values of the filtered ROI (bin size: %3.3f)', h.BinWidth);
fig.Children.Title.Interpreter = tinterp;
%fig.Children.Title.String = sprintf( ...
%     'Histogram of height values of the filtered ROI; Bin size: %3.3f', h.BinWidth);
fig.Children.FontSize = fsTitle;

fig.Children.FontSize = fsAxes;
fig.Children.TickLabelInterpreter = tinterp;
fig.Children.FontName = fontAll; % has no effect

fig.Children.XLabel.Interpreter = tinterp;
fig.Children.XLabel.FontSize = fsAxesLabel;
fig.Children.XLabel.String = 'Z value $[\mu m]$';

fig.Children.YLabel.Interpreter = tinterp;
fig.Children.YLabel.FontSize = fsAxesLabel;
fig.Children.YLabel.String = 'Frequency';


%% Save
% As a matlab figure
savefig(fig, fullfile(DIR_RESULTS, 'plot', 'paper_hp_exampeHist.fig'));

% As vector graphics
filename = fullfile(DIR_RESULTS, 'plot', 'paper_hp_exampleHist.eps');
print('-r300', fig, filename, '-depsc');

% use exiftool to set eps title field
system(sprintf('exiftool -Title=''%s'' %s', mytitle, filename), '-echo');

