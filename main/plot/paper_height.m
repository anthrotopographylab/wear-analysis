%paper_height Plot script for 2nd paper: height parameter plots.
% - Height parameters over force/ suration
% - Height parameters over classification labels 50x and 100x
% - Raw variability 50x and 100x
% - Labels vs. measurement quality (repeatability)

% Set-up the environment 
clear all; close all;

run startMWA;

import mlreportgen.dom.*;

% Load images and selections and height parameters
load(fullfile(DIR_DATA, 'DS_NewYork.mat'));
load(fullfile(DIR_DATA, 'Selection6.mat'));
load(fullfile(DIR_RESULTS, 'results_paper.mat'));

% Make variable names consistent
selAll = [selEdge100 selFlank100 selMisc selMisc50];
paraAll = [myParaAll myPara50];

clear selEdge100 selFlank100 selMisc selMisc50;
clear myParaAll myPara50;

% Find duplicates in mySelection
[~, idx] = unique([selAll.ID]);
selAll = selAll(idx);

% Find duplicates in mySelection
[~, idx] = unique([paraAll.Sa]);
paraAll = paraAll(idx);

% Make sure that objects are not linked jet
selAll.breakLinks;
paraAll.breakLinks;

% Construct data objects
runNo = unique(PARA.Logbook{:,'RunNo'});

runs = arrayfun(@ExpRun, runNo(runNo>0), 'UniformOutput', false);
runs = sort([runs{:}]);

scannames = unique(PARA.IndexScans{:,'Scanname'});
isEmpty = cellfun(@isempty, scannames);

scans = cellfun(@AliconaImage, PARA.IndexScans{:,'Scanname'}, 'UniformOutput', false);
scans = sort([scans{:}]);

% Container objects;
meas = Measurement(scans);
samples = SampleHistory(runs, meas);

% Link images and selections
[a, b] = selAll.isRelated(scans, {'imageID'}, {'Name'});
selAll(a).linkTo(scans(b));

% Link selections and height parameters
[a, b] = selAll.isRelated(paraAll, {'ID'}, {'polygonID'});
selAll(a).linkTo(paraAll(b))


% Link runs and images
% Cannot use isRelated here because it does not provide dynamic properties
% used by runs. Instead, convert to tables and use innerjoin.
valRuns = [{runs.IterationNo}', {runs.SampleID}'];
valScans = [num2cell(scans.getProp('IterationNo'))', num2cell(scans.getProp('SampleID'))'];

varnames = strcat('v', cellstr(categorical(1:2)));
[~, iR, iS] = innerjoin( ...
    cell2table(valRuns, 'VariableNames', varnames), ...
    cell2table(valScans, 'VariableNames', varnames));

runs(iR).linkTo(scans(iS));


% Filter polygon size for 50x and 100x
polyArea = [paraAll.area];

myPara100 = paraAll( ...
    polyArea > 900 & polyArea < 1200 & ...
    paraAll.getLinksTo('Selection').getLinksTo('AliconaImage').getProp('Magnification') == 100);
myPara50 = paraAll( ...
    polyArea > 1900 & polyArea < 2100 & ...
    paraAll.getLinksTo('Selection').getLinksTo('AliconaImage').getProp('Magnification') == 50);


%% Save names of evaluated scans to csv
ss = [myPara50.getLinksTo('Selection').getLinksTo('AliconaImage').Name];
evaluatedScans50 = [sort(unique(ss))];

csvfile50 = fopen(fullfile(DIR_RESULTS, 'paper_height_evaluatedScans50.csv'), 'w+');
cellfun(@(name)fprintf(csvfile50, '%s\n', name), [evaluatedScans50]);
fclose(csvfile50);

ss = [myPara100.getLinksTo('Selection').getLinksTo('AliconaImage').Name];
evaluatedScans100 = [sort(unique(ss))];

csvfile100 = fopen(fullfile(DIR_RESULTS, 'paper_height_evaluatedScans100.csv'), 'w+');
cellfun(@(name)fprintf(csvfile100, '%s\n', name), [evaluatedScans100]);
fclose(csvfile100);


%% Set format styles
% fonst size
fsTitle = 12;
fsAxesLabel = 10;
fsAxes = 10;

% Text interpreter
tinterp = 'Latex';

% Figsize in [cm]
figsizeFDC = [20 0 18 20];
figsizeBox = [20 0 21 13];
figsizeBoxSmall = [20 0 8 13];


%% Plot Sa with force duration correlation, 100x
x = myPara100.getLinksTo('Selection').getLinksTo('AliconaImage') ...
    .getLinksTo('ExpRun').getLinksTo('SampleHistory') ...
    .numCyclesAt(myPara100.getLinksTo('Selection').getLinksTo('AliconaImage').getProp('IterationNo'));

y =  myPara100.getLinksTo('Selection').getLinksTo('AliconaImage').getLinksTo('ExpRun').getProp('Force');
z = [myPara100.Sa];

hfSaFD = plot3dVar(x, y, z, 5); % use variance factor of 5

% f(1) = plot3dVarGroup(x, y, z, 3);
% 
% f(1).Children(1).Title.String='Sa on 100x';
% f(1).Children(4).XLabel.String = 'duration [strokes]';
% 
% text(-70, 0.6, 'Force [N]', ...
%     'Rotation', 90, ...
%     'FontSize', 12, ...
%     'Parent', f(1).Children(4));


% Set size
[hfSaFD.Units] = deal('centimeters');
[hfSaFD.Position] = deal(figsizeFDC);

myha = [hfSaFD.Children];

% Set axies range
[myha.YLim] = deal([0.1102 0.35]);

% Add Title
myha(1).Title.Interpreter = tinterp;
% myha(1).Title.String = 'Sa $[\mu m]$ of ROIs on 100x images';
myha(1).Title.FontSize = fsTitle;

% Add x labels
myhxl = [myha(end).XLabel];    % label only the bottom plot
[myhxl.String] = deal('Number Cycles');
[myhxl.Interpreter] = deal(tinterp);
[myhxl.FontSize] = deal(fsAxesLabel);

% Set y axis label and font
myhyl = [myha.YLabel];
[myhyl.Interpreter] = deal('Latex');
[myhyl.FontSize] = deal(fsAxesLabel);
ylnew = strcat({myhyl.String}, ' N');
[myhyl.String] = deal(ylnew{:});
    
% Set axes value font
[myha.FontSize] = deal(fsAxes);
[myha.TickLabelInterpreter] = deal(tinterp);

% Add overall y label
text(-70, 275, 'Sa $[\mu m]$', ...
    'Rotation', 90, ...
    'FontSize', fsAxesLabel, ...
    'Interpreter', tinterp, ...
    'Units', 'pixel', ...
    'Parent', myha(end));

%% Save as eps
mytitle = 'Sa of 100x images over force and duration';
filename = fullfile(DIR_RESULTS, 'paper_hp_FDCSa.eps');
print('-r300', hfSaFD, filename, '-depsc');
system(sprintf('exiftool -Title=''%s'' %s', mytitle, filename), '-echo');


%% Regression of Sa with force and duration
mdl = fitglm([x', y'], z', 'linear', 'Distribution', 'normal');

[gID, gx, gy] = findgroups(x, y);

% Get all points at 110N
isNumCyc = ismember(gID, find(gy==110));
figure; scatter(x(isNumCyc), z(isNumCyc));

mdl110 = fitglm(x(isNumCyc)', z(isNumCyc)', 'linear', 'Distribution', 'normal');
mdl110.plotSlice;

% Get all points at 90N
is90 = ismember(gID, find(gy==90));
figure; scatter(x(is90), z(is90));

mdl90 = fitglm(x(is90)', z(is90)', 'linear', 'Distribution', 'normal');
mdl90.plotSlice;

% Write to file
fid = fopen(fullfile(DIR_RESULTS, 'paper_hp_FDCSa_reg.txt'), 'w');
fprintf(fid, 'Regression for F = 110N\n');
fprintf(fid, evalc('mdl110.disp'));

fprintf(fid, '\n\nRegression for F = 90N\n');
fprintf(fid, evalc('mdl90.disp'));
fclose(fid);



%% Plot ROI labels with force duration correlation
% Use all ROIs independent form their polygon size for this.
% The only requirement for a ROI to be included is that it is 
% classified with a label.

labels = categorical({paraAll.getLinksTo('Selection').label}, ...
    {'high', 'medium', 'low', 'raw'}, 'Ordinal', true);

x = paraAll.getLinksTo('Selection').getLinksTo('AliconaImage') ...
    .getLinksTo('ExpRun').getLinksTo('SampleHistory') ...
    .numCyclesAt(paraAll.getLinksTo('Selection').getLinksTo('AliconaImage').getProp('IterationNo'));

y =  paraAll.getLinksTo('Selection').getLinksTo('AliconaImage').getLinksTo('ExpRun').getProp('Force');

hfLabelFD = plot3dVar(x, y, double(labels), 1);


% Set size
[hfLabelFD.Units] = deal('centimeters');
[hfLabelFD.Position] = deal(figsizeFDC);

myha = [hfLabelFD.Children];

% Add Title
myha(1).Title.Interpreter = tinterp;
% myha(1).Title.String = 'Labels of all ROIs';
myha(1).Title.FontSize = fsTitle;

% Add x labels
myhxl = [myha(end).XLabel];    % label only the bottom plot
[myhxl.String] = deal('Number Cycles');
[myhxl.Interpreter] = deal(tinterp);
[myhxl.FontSize] = deal(fsAxesLabel);

% Set y axis label font
myhyl = [myha.YLabel];
[myhyl.Interpreter] = deal(tinterp);
[myhyl.FontSize] = deal(fsAxesLabel);
ylnew = strcat({myhyl.String}, ' N');
[myhyl.String] = deal(ylnew{:});

% Set yaxis tick labels
[hfLabelFD.Children.YLim] = deal([0.5 4.5]);
[hfLabelFD.Children.YTick] = deal(1:4);
[hfLabelFD.Children.YTickLabel] = deal({'high', 'med', 'low', 'raw'});

% Set axes value font
[myha.FontSize] = deal(fsAxes);
[myha.TickLabelInterpreter] = deal(tinterp);

% % Add overall y label
% text(-70, 275, 'labels', ...
%     'Rotation', 90, ...
%     'FontSize', fsAxesLabel, ...
%     'Interpreter', tinterp, ...
%     'Units', 'pixel', ...
%     'Parent', myha(end));

% saveas(hfLabelFD, fullfile(DIR_RESULTS, 'paper_hp_FDCLabel'), 'epsc');

%% Save as eps
mytitle = 'Labels of all ROIs';
filename = fullfile(DIR_RESULTS, 'paper_hp_FDCLabel.eps');
print('-r300', hfLabelFD, filename, '-depsc');
system(sprintf('exiftool -Title=''%s'' %s', mytitle, filename), '-echo');


%% Regression of ROI labels with force and duration
mdl = fitglm([x', y'], double(labels)', 'linear', 'link', 'log', 'Distribution', 'Poisson');

figure; mdl.plotResiduals;
mdl.plotSlice;

% Write to file
fid = fopen(fullfile(DIR_RESULTS, 'paper_hp_FDC_label_reg.txt'), 'w');
fprintf(fid, 'Regression for labels over duration (x1) and force (x2)');
fprintf(fid, evalc('mdl.disp'));
fclose(fid);


%% Ridge vs. flank for Roi labels 
% Use proper statistics test for this!!!
% e.g. Mann-Whitney-U-Test, Welch-Test, t-Test
% Non-connect variables

isFlank = isnan(paraAll.getLinksTo('Selection').getLinksTo('AliconaImage').getProp('Orientation'));
isRidge = ~isFlank;

LabelsAll = struct( ...
    'flank', double(labels(isFlank)), ...
    'ridge', double(labels(isRidge)));

hfrvf = PlotStruct(LabelsAll);

% Take points at different force levels 
[gID, gx, gy, giF] = findgroups(x, y, isFlank);

% Get all points at 110N
isNumCyc = ismember(gID, find(gx == 150));
isFlankG = ismember(gID, find(giF));

sum(isFlankG & isNumCyc)
sum(~isFlankG & isNumCyc)

LabelsCyc = struct( ...
    'flank', double(labels(isFlankG & isNumCyc)), ...
    'ridge', double(labels(~isFlankG & isNumCyc)));

hfrvf = PlotStruct(LabelsCyc);



%% Plot Height Parameter boxplots over ROI Labels
i=0;

for para = {myPara100, myPara50}
    i = i+1;
    mypara = para{:};
    
    labels = categorical({mypara.getLinksTo('Selection').label}, ...
        {'high', 'medium', 'low', 'raw'}, 'Ordinal', true)';

    isDefined = ~isundefined(labels);

    Sa = struct( ...
        'raw', [mypara(labels == 'raw').Sa], ...
        'low', [mypara(labels == 'low').Sa], ...
        'med', [mypara(labels == 'medium').Sa], ...
        'hi', [mypara(labels == 'high').Sa]);

    Sq = struct( ...
        'raw', [mypara(labels == 'raw').Sq], ...
        'low', [mypara(labels == 'low').Sq], ...
        'med', [mypara(labels == 'medium').Sq], ...
        'hi', [mypara(labels == 'high').Sq]);

    Ssk = struct( ...
        'raw', [mypara(labels == 'raw').Ssk], ...
        'low', [mypara(labels == 'low').Ssk], ...
        'med', [mypara(labels == 'medium').Ssk], ...
        'hi', [mypara(labels == 'high').Ssk]);

    Sku = struct( ...
        'raw', [mypara(labels == 'raw').Sku], ...
        'low', [mypara(labels == 'low').Sku], ...
        'med', [mypara(labels == 'medium').Sku], ...
        'hi', [mypara(labels == 'high').Sku]);

    hfLabel(i) = PlotStruct(Sa, Sq, Ssk, Sku, 'equalyaxis', false);
end

% Set size
[hfLabel.Units] = deal('centimeters');
[hfLabel.Position] = deal(figsizeBox);

myha = [hfLabel.Children];

% % Add Title
% text(5, 12, 'Height Parameters on 100x images over labels', ...
%     'FontSize', fsTitle, ...
%     'Interpreter', tinterp, ...
%     'Units', 'centimeters', ...
%     'Parent', hfLabel(1).Children(4));
% 
% text(5, 12, 'Height Parameters on 50x images over labels', ...
%     'FontSize', fsTitle, ...
%     'Interpreter', tinterp, ...
%     'Units', 'centimeters', ...
%     'Parent', hfLabel(2).Children(4));

% text(440, 600, 'Height Parameters on 50x images over labels', ...
%     'FontSize', fsTitle, ...
%     'Interpreter', tinterp, ...
%     'Units', 'pixels', ...
%     'Parent', hfLabel(2).Children(4));

% set axes font size
[myha.FontSize] = deal(fsAxes);

% Add y labels
myyl = [myha(1:4,1).YLabel; myha(1:4,2).YLabel]';
[myyl.Interpreter] = deal(tinterp);
[myyl.FontSize] = deal(fsAxesLabel);

[myyl(4,:).String] = deal('$[\mu m]$');
[myyl(3,:).String] = deal('$[\mu m]$');
[myyl(2,:).String] = deal('[-]');
[myyl(1,:).String] = deal('[-]');

% saveas(hfLabel(1), fullfile(DIR_RESULTS, 'paper_hp_boX100'), 'epsc');
% saveas(hfLabel(2), fullfile(DIR_RESULTS, 'paper_hp_boX50'), 'epsc');

% Save
mynames={'paper_hp_boX100.eps', 'paper_hp_boX50.eps'};
mytitles = {'Height parameters at 100x over labels', 'Height parameters at 50x over labels'};

for i=1:2
    filename = fullfile(DIR_RESULTS, mynames{i});
    print('-r300', hfLabel(i), filename, '-depsc');
    system(sprintf('exiftool -Title=''%s'' %s', mytitles{i}, filename), '-echo');
end


%% Plot accuracy (quality map mean) over ROI labels
% Use all ROIs independent form their polygon size for this.
% The only requirement for a ROI to be included is that it is 
% classified with a label.

labels = categorical({paraAll.getLinksTo('Selection').label}, ...
    {'high', 'medium', 'low', 'raw'}, 'Ordinal', true);

% Turn repeatability into resolu (alicona formula) and convert to nm.
ff = sqrt(8)*1e+9; 

Resolution = struct( ...
    'raw', [paraAll(labels == 'raw').rep] * ff, ...
    'low', [paraAll(labels == 'low').rep] * ff, ...
    'med', [paraAll(labels == 'medium').rep] * ff, ...
    'high', [paraAll(labels == 'high').rep] * ff);

hfRep = PlotStruct(Resolution);

% Set size
[hfRep.Units] = deal('centimeters');
[hfRep.Position] = deal(figsizeBoxSmall);

hfRep.Children(1).FontSize = fsAxes;
hfRep.Children(1).YLabel.Interpreter = tinterp;
hfRep.Children(1).YLabel.FontSize = fsAxesLabel;
hfRep.Children(1).YLabel.String = '$[nm]$';
hfRep.Children(1).Title.String = ''; %'Image Accuracy';
hfRep.Children(1).Title.FontSize = fsTitle;
hfRep.Children(1).Title.Interpreter = tinterp;

% saveas(hfRep, fullfile(DIR_RESULTS, 'paper_hp_boxRep'), 'epsc');

%% Save to eps
mytitle = 'Image vertical resolution [nm]';
filename = fullfile(DIR_RESULTS, 'paper_hp_boxRep.eps');
print('-r300', hfRep, filename, '-depsc');
system(sprintf('exiftool -Title=''%s'' %s', mytitle, filename), '-echo');




%% Plot raw ROIs 50x vs 100x

% SELECT 50x IMAGES FIRST

% Select only parameters of raw ROIs
Sa_raw = struct( ...
    'x100', [myPara100(myPara100.getLinksTo('Selection').getLinksTo('AliconaImage').getProp('IterationNo') == 0).Sa], ...
    'x50', [myPara50(myPara50.getLinksTo('Selection').getLinksTo('AliconaImage').getProp('IterationNo') == 0).Sa]);

fprintf('Range of 100x: %f  %f\n', min(Sa_raw.x100), max(Sa_raw.x100));
fprintf('Range of 50x: %f  %f\n', min(Sa_raw.x50), max(Sa_raw.x50));

hfRaw = PlotStruct(Sa_raw);
myha = hfRaw.Children(1);

% Set size
hfRaw.Units = 'centimeters';
hfRaw.Position = figsizeBoxSmall + [0 0 -2 0]; % make a bit thinner

% Add Title
myha.Title.String = ''; % 'Sa of raw ROIs for all images';
myha.Title.FontSize = fsTitle;
myha.Title.Interpreter = tinterp; 

% text(440, 600, 'Sa of raw ROIs for 50x and 100x images', ...
%     'FontSize', fsTitle, ...
%     'Interpreter', tinterp, ...
%     'Units', 'pixels', ...
%     'Parent', hfRaw.Children(1));

myha.FontSize = fsAxes;
myha.YLabel.Interpreter = tinterp;
myha.YLabel.FontSize = fsAxesLabel;
myha.YLabel.String = '$[\mu m]$';

%% Save to eps
% saveas(hfRaw, fullfile(DIR_RESULTS, 'paper_hp_boxRaw'), 'epsc');

mytitle = 'Sa of raw ROIs for all images';
filename = fullfile(DIR_RESULTS, 'paper_hp_boxRaw.eps');
print('-r300', hfRaw, filename, '-depsc');
system(sprintf('exiftool -Title=''%s'' %s', mytitle, filename), '-echo');


