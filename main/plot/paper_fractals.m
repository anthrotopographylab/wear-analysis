%paper_fractals Plot script for 2nd paper: Fractal plots.
% - Fractal parameters over force/ suration
% - Fractal parameters over classification labels
% - Example of a relative-area scale plot

% Set-up the environment 
clear all; close all;

run startMWA;

import mlreportgen.dom.*;

% Load images and selections and fractal parameters
load(fullfile(DIR_DATA, 'DS_NewYork.mat'));
load(fullfile(DIR_DATA, 'Selection6.mat'));
load(fullfile(DIR_RESULTS, 'results_paper.mat'));


% Make variable names consistent
mySelection = [selEdge100 selFlank100 selMisc];

clear selEdge100 selFlank100 selMisc;

% find duplicates
[~, idx] = unique([mySelection.ID]);
mySelection = mySelection(idx);

mySelection.breakLinks;
frac.breakLinks;

% Construct data objects
% TO DO wrap data object construction into separate function so that this
% script becomes a bit cleaner.

runNo = unique(PARA.Logbook{:,'RunNo'});

runs = arrayfun(@ExpRun, runNo(runNo>0), 'UniformOutput', false);
runs = sort([runs{:}]);

scannames = unique(PARA.IndexScans{:,'Scanname'});
isEmpty = cellfun(@isempty, scannames);

scans = cellfun(@AliconaImage, PARA.IndexScans{:,'Scanname'}, 'UniformOutput', false);
scans = sort([scans{:}]);

% Container objects;
meas = Measurement(scans);
samples = SampleHistory(runs, meas);

% Link images and selections
[a, b] = mySelection.isRelated(scans, {'imageID'}, {'Name'});
mySelection(a).linkTo(scans(b));

% Link runs and images
% Cannot use isRelated here because it does not provide dynamic properties
% used by runs. Instead, convert to tables and use innerjoin.
valRuns = [{runs.IterationNo}', {runs.SampleID}'];
valScans = [num2cell(scans.getProp('IterationNo'))', num2cell(scans.getProp('SampleID'))'];

varnames = strcat('v', cellstr(categorical(1:2)));
[~, iR, iS] = innerjoin( ...
    cell2table(valRuns, 'VariableNames', varnames), ...
    cell2table(valScans, 'VariableNames', varnames));

runs(iR).linkTo(scans(iS));

% Link selections and fracs
[a, b] = mySelection.isRelated(frac, {'ID'}, {'polygonID'});
mySelection(a).linkTo(frac(b));


%% Set format styles
% fonst size
fsTitle = 10;
fsAxesLabel = 10;
fsAxes = 10;

% Text interpreter
tinterp = 'Latex';

% Figure size [cm]
figsizeFDC = [0 20 18 20];
figsizeBox = [0 20 15 15];


%% Plot fractal parameters over force and duration
x = frac.getLinksTo('Selection').getLinksTo('AliconaImage') ...
    .getLinksTo('ExpRun').getLinksTo('SampleHistory') ...
    .numCyclesAt(frac.getLinksTo('Selection').getLinksTo('AliconaImage').getProp('IterationNo'));
y = frac.getLinksTo('Selection').getLinksTo('AliconaImage').getLinksTo('ExpRun').getProp('Force');

fracDim     = [frac.Das];
scaleApex   = [frac.scaleD];
relAreaApex = [frac.relAreaD];

% frfilt = filloutliers(fracDim, NaN, 'median');
% scfilt = filloutliers(scaleApex, NaN, 'median');

% Filter out outliers
isOutlier = ...
    isoutlier(fracDim, 'median') | ...
    isoutlier(scaleApex, 'median') | ...
    relAreaApex < 1;

fracDim(isOutlier) = missing;
scaleApex(isOutlier) = missing;
relAreaApex(isOutlier) = missing; 

relAreaApex(isoutlier(relAreaApex)) = missing;

% Generate plots
hfExp(1) = plot3dVar(x, y, fracDim, 150);
hfExp(2) = plot3dVar(x, y, scaleApex, 1);
hfExp(3) = plot3dVar(x, y, relAreaApex, 140);


% Set size
[hfExp.Units] = deal('centimeters');
[hfExp.Position] = deal(figsizeFDC);

myha = [hfExp.Children];

% Add plot titles
% hfExp(1).Children(1).Title.String = 'Fractal Dimension $[\frac{1}{\mu m}]$';
% hfExp(2).Children(1).Title.String = 'Critical Scale $[\mu m]$';
% hfExp(3).Children(1).Title.String = 'Critical Relative Area $[-]$';
myhtt = [myha(1, :).Title];
[myhtt.Interpreter] = deal(tinterp);
[myhtt.FontSize] = deal(fsTitle);

% Add x labels
myhxl = [myha(end,:).XLabel];    % label only the bottom plot
[myhxl.String] = deal('Number Cycles');
[myhxl.Interpreter] = deal(tinterp);
[myhxl.FontSize] = deal(fsAxesLabel);

% Set y axis label names and font
myhyl = [myha.YLabel];
[myhyl.Interpreter] = deal(tinterp);
[myhyl.FontSize] = deal(fsAxesLabel);
ylnew = strcat({myhyl.String}, ' N');
[myhyl.String] = deal(ylnew{:});
    
% Set axes value font
% myhv = [myha.X
[myha.FontSize] = deal(fsAxes);
[myha.TickLabelInterpreter] = deal(tinterp);

% Set range of fractal dimension plot
myha = [hfExp(1).Children];
[myha.YLim] = deal([2 2.005]);

% Add overall y label
xpos = -70;
ypos = 240;
text(xpos, ypos, 'Fractal Dimension $[\frac{1}{\mu m}]$', ...
    'Rotation', 90, ...
    'VerticalAlignment', 'middle', ...    
    'FontSize', fsAxesLabel, ...
    'Interpreter', tinterp, ...
    'Units', 'pixel', ...
    'Parent', hfExp(1).Children(4));

text(xpos, ypos+20, 'Critical Scale $[\mu m]$', ...
    'Rotation', 90, ...
    'VerticalAlignment', 'middle', ...    
    'FontSize', fsAxesLabel, ...
    'Interpreter', tinterp, ...
    'Units', 'pixel', ...
    'Parent', hfExp(2).Children(4));

text(xpos, ypos, 'Critical Relative Area $[-]$', ...
    'Rotation', 90, ...
    'VerticalAlignment', 'middle', ...
    'FontSize', fsAxesLabel, ...
    'Interpreter', tinterp, ...
    'Units', 'pixel', ...
    'Parent', hfExp(3).Children(4));


%% Save
mynames={'paper_fractals_fracDim.eps', 'paper_fractals_fracScale.eps', 'paper_fractals_fracArea.eps'};
mytitles = {'Fractal Dimension [1/um]', 'Critical Scale [um]', 'Critical Relative Area [-]'};

for i=1:2
    filename = fullfile(DIR_RESULTS, mynames{i});
    print('-r300', hfExp(i), filename, '-depsc');
    system(sprintf('exiftool -Title=''%s'' %s', mytitles{i}, filename), '-echo');
end


%% Regression of fractal dimension with force and duration
mdl = fitglm([x', y'], fracDim', 'linear', 'Distribution', 'normal');

[gID, gx, gy] = findgroups(x, y);

% Get all points at 110N
is110 = ismember(gID, find(gy==110));
figure; scatter(x(is110), fracDim(is110));

mdl110 = fitglm(x(is110)', fracDim(is110)', 'linear', 'Distribution', 'normal');
mdl110.plotSlice;

% Get all points at 90N
is90 = ismember(gID, find(gy==90));
figure; scatter(x(is90), fracDim(is90));

mdl90 = fitglm(x(is90)', fracDim(is90)', 'linear', 'Distribution', 'normal');
mdl90.plotSlice;

% Write to file
fid = fopen(fullfile(DIR_RESULTS, 'paper_fractals_fracDim_reg.txt'), 'w');
fprintf(fid, 'Regression for F = 110N\n');
fprintf(fid, evalc('mdl110.disp'));

fprintf(fid, '\n\nRegression for F = 90N\n');
fprintf(fid, evalc('mdl90.disp'));
fclose(fid);



%% Plot Fractal parameters over ROI label
labels = categorical({frac.getLinksTo('Selection').label}, {'high', 'medium', 'low', 'raw'}, 'Ordinal', true)';

isDefined = ~isundefined(labels);

FractalDimension = struct( ...
    'raw', fracDim(labels == 'raw'), ...
    'low', fracDim(labels == 'low'), ...
    'med', fracDim(labels == 'medium'), ...
    'high', fracDim(labels == 'high'));

CriticalScale = struct( ...
    'raw', scaleApex(labels == 'raw'), ...
    'low', scaleApex(labels == 'low'), ...
    'med', scaleApex(labels == 'medium'), ...
    'high', scaleApex(labels == 'high'));

CriticalRelativeArea = struct( ...
    'raw', relAreaApex(labels == 'raw'), ...
    'low', relAreaApex(labels == 'low'), ...
    'med', relAreaApex(labels == 'medium'), ...
    'high', relAreaApex(labels == 'high'));

hfLabel = PlotStruct(FractalDimension, CriticalScale, 'equalyaxis', false);

% Size
hfLabel.Units = 'centimeters';
hfLabel.Position = figsizeBox;

% Add x labels
myhl = [hfLabel.Children.YLabel];
[myhl.Interpreter] = deal(tinterp);
[myhl.FontSize] = deal(fsAxes);

hfLabel.Children(2).YLabel.String = '$\left[\frac{1}{\mu m^2}\right]$';
hfLabel.Children(1).YLabel.String = '$\left[\mu m^2\right]$';
% hfLabel.Children(3).YLabel.String = '[-]';

% Set Title and format
myhxl = [hfLabel.Children(1:2).Title];
[myhxl.FontSize] = deal(fsTitle);

hfLabel.Children(2).Title.String = 'Fractal Dimension';
hfLabel.Children(1).Title.String = 'Critical Scale';
% hfLabel.Children(3).Title.String = 'Critical Relative Area';

%% Save as eps
filename = fullfile(DIR_RESULTS, 'paper_fractals_fracLabels.eps');
print('-r300', hfLabel, filename, '-depsc');
system(sprintf('exiftool -Title=''%s'' %s', 'Fractal Parameters over labels', filename), '-echo');


%% Plot relative area over scale of one pretty frac object

% Pick a nice frac
myfrac = frac(strcmp('S122_use4_pos11_x100', [frac.scanname]));
myfrac = myfrac(1);

myfrac.plot

hfra = gcf;
hfra.Position = [900 100 800 600];
hfra.Children.FontSize = fsAxes;
hfra.Children.TickLabelInterpreter = tinterp;

% delete green line
delete(hfra.Children.Children(1));

lines = findobj(gca, 'Type', 'Line');
lines(1).LineStyle = '--';
lines(1).Color = [0 0 0];

% % horizontal line
% line(hfra.Children.XLim, [myfrac.relAreaD myfrac.relAreaD], ...
%     'linestyle', '--', 'Color', 'black');


hfra.Children.Title.String = ''; % 'Relative Area-Scale Plot';
hfra.Children.Title.FontSize = fsTitle;
hfra.Children.Title.Interpreter = tinterp;

hfra.Children.XLabel.String = 'Scale $[{\mu m}^2]$';
hfra.Children.XLabel.FontSize = fsAxesLabel;
hfra.Children.XLabel.Interpreter = tinterp;
hfra.Children.YLabel.String = 'Relative Area $[-]$';
hfra.Children.YLabel.FontSize = fsAxesLabel;
hfra.Children.YLabel.Interpreter = tinterp;

% Save as eps
% saveas(hfra, fullfile(DIR_RESULTS, 'paper_fractals_fracDemo'), 'epsc');

mytitle = sprintf('Relative Area-Scale Plot (cscale = %4.2f, carea = %4.4f, fdim = %4.4f)', ...
    myfrac.scaleD, myfrac.relAreaD, myfrac.Das);

%% Save as eps
filename = fullfile(DIR_RESULTS, 'paper_fractals_fracDemo.eps');
print('-r300', hfra, filename, '-depsc');
system(sprintf('exiftool -Title=''%s'' %s', mytitle, filename), '-echo');


