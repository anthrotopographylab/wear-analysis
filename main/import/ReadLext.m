function [X, Y, LextD] = ReadLext(filename, imLevel)
% Read image files (lext) of Stemp. The lext files are formated as:
% Level 1: RGB trucolor image
% Level 2: thumbnail of Level one
% Level 3: intensity of Level 1
% Level 4: height map
% Level 5: invalid

% Unit is inch
inch2m = 0.0254;
m2um = 1; %1e+6;



% Read data file
% filename = ['stats_level' '.txt'];
fidp = fopen(filename);
if fidp == -1
    fprintf('file %s not found\n',filename);
    return;
end

lextinfo = imfinfo(filename);
% imLevel = 4;      % 3 = height data

% Data
LextD = imread(filename, 'tiff', imLevel);

textscan(fidp, '%f32%f32%f32', ...
    'delimiter', ' ', 'HeaderLines', 1);

fclose(fidp);

% xres = lextinfo(imLevel).XResolution;
% yres = lextinfo(imLevel).YResolution;
% 
% xgv = [1:size(LextD, 2)] ./ xres .* inch2m * m2um;
% ygv = [1:size(LextD, 1)] ./ yres .* inch2m * m2um;

% range data from Stemp conversation
xrange = 643;
yrange = 643;

xres = xrange/(size(LextD, 2)-1);
yres = yrange/(size(LextD, 1)-1);

xgv = (0:size(LextD, 2)-1) .* xres;
ygv = (0:size(LextD, 1)-1) .* yres;

[X, Y] = meshgrid(xgv, ygv);

%LextD = LextD * inch2m * m2um;

LextD = double(LextD);

% x = X(:);
% y = Y(:);
% z = LextD(:);

end
