function trajectoryData = readLogfile(filename)
%readLogfile Read logfile (trajectory) of the experiment from csv file. 
% Fieldnames are taken from the header of the text. 
% INPUT:
%   filename - Path to the logfile.


% CHECK INPUT *************************************************************
validateattributes(filename, {'char'}, {'row'}, mfilename, 'filename');
source = fopen(filename, 'r');
if source == -1
    error('MWA:ReadMetaData:IOerror', ...
        'Could not open file %s', filename);
end    


% READ HEADER *************************************************************
% Header: fieldnames
header = textscan(source, ...
    '%*s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s', 1, ...
    'HeaderLines', 0);

fieldsNorm={};
format = '';

fieldsRaw = [header{:}];

% % Make fieldnames struct-conform, ie. remove special characters
% fields = regexprep([header{:}], '[\s\[\]]',''); 


% Rename fieldnmames for struct and construct format specifier from
% fieldnames of the header. 

for i=1:length(fieldsRaw)
    switch fieldsRaw{i}
        case 'ZeitInSec'
            fieldsNorm{end+1} = 'TimeSec';
            format = [format '%u'];
        case 'ZeitInNanoSec'
            fieldsNorm{end+1} = 'TimeNsec';
            format = [format '%u'];
        case 'axisTauExtMsr_LBR_iiwa_14_R820_1[0]'
            fieldsNorm{end+1} = 'TauExtNm_0';
            format = [format '%f'];
        case 'axisTauExtMsr_LBR_iiwa_14_R820_1[1]'
            fieldsNorm{end+1} = 'TauExtNm_1';
            format = [format '%f'];
        case 'axisTauExtMsr_LBR_iiwa_14_R820_1[2]'
            fieldsNorm{end+1} = 'TauExtNm_2';
            format = [format '%f'];
        case 'axisTauExtMsr_LBR_iiwa_14_R820_1[3]'
            fieldsNorm{end+1} = 'TauExtNm_3';
            format = [format '%f'];
        case 'axisTauExtMsr_LBR_iiwa_14_R820_1[4]'
            fieldsNorm{end+1} = 'TauExtNm_4';
            format = [format '%f'];
        case 'axisTauExtMsr_LBR_iiwa_14_R820_1[5]'
            fieldsNorm{end+1} = 'TauExtNm_5';
            format = [format '%f'];
        case 'axisTauExtMsr_LBR_iiwa_14_R820_1[6]'
            fieldsNorm{end+1} = 'TauExtNm_6';
            format = [format '%f'];
        case 'axisQMsr_LBR_iiwa_14_R820_1[0]'
            fieldsNorm{end+1} = 'ThetaDeg_0';
            format = [format '%f'];
        case 'axisQMsr_LBR_iiwa_14_R820_1[1]'
            fieldsNorm{end+1} = 'ThetaDeg_1';
            format = [format '%f'];
        case 'axisQMsr_LBR_iiwa_14_R820_1[2]'
            fieldsNorm{end+1} = 'ThetaDeg_2';
            format = [format '%f'];    
        case 'axisQMsr_LBR_iiwa_14_R820_1[3]'
            fieldsNorm{end+1} = 'ThetaDeg_3';
            format = [format '%f'];
        case 'axisQMsr_LBR_iiwa_14_R820_1[4]'
            fieldsNorm{end+1} = 'ThetaDeg_4';
            format = [format '%f'];
        case 'axisQMsr_LBR_iiwa_14_R820_1[5]'
            fieldsNorm{end+1} = 'ThetaDeg_5';
            format = [format '%f'];
        case 'axisQMsr_LBR_iiwa_14_R820_1[6]'
            fieldsNorm{end+1} = 'ThetaDeg_6';
            format = [format '%f'];
        case 'cartForce1_X'
            fieldsNorm{end+1} = 'CartForceN_X';
            format = [format '%f'];
        case 'cartForceVar1_X'
            fieldsNorm{end+1} = 'CartForceVar_X';
            format = [format '%f'];
        case 'cartForce1_Y'
            fieldsNorm{end+1} = 'CartForceN_Y';
            format = [format '%f'];
        case 'cartForceVar1_Y'
            fieldsNorm{end+1} = 'CartForceVar_Y';
            format = [format '%f'];
        case 'cartForce1_Z'
            fieldsNorm{end+1} = 'CartForceN_Z';
            format = [format '%f'];
        case 'cartForceVar1_Z'
            fieldsNorm{end+1} = 'CartForceVar_Z';   
            format = [format '%f'];
        case 'CartPosMsr1_X'
            fieldsNorm{end+1} = 'CartPos_X';
            format = [format '%f'];
        case 'CartPosMsr1_Y'
            fieldsNorm{end+1} = 'CartPos_Y';
            format = [format '%f'];
        case 'CartPosMsr1_Z'
            fieldsNorm{end+1} = 'CartPos_Z';
            format = [format '%f'];            
        otherwise
            error('MWA:ReadIndex:IOerror', ...
                'Fieldname not provided');
    end
end


% READ DATA ***************************************************************
data = textscan(source, format, ...
    'EmptyValue', NaN);
 
% Write to struct and apply new order
trajectoryData = orderfields(cell2struct(data, fieldsNorm, 2), fieldsNorm);
 
% Format time vectory if available
if sum(isfield(trajectoryData, {'TimeSec', 'TimeNsec'})) == 2
    time = double(trajectoryData.TimeSec) + ...
        double(trajectoryData.TimeNsec) .* 1e-9;
    % remove offset
    time = time - time(1);
    trajectoryData.TimeSec = time;
    trajectoryData = rmfield(trajectoryData, 'TimeNsec');
end


end
