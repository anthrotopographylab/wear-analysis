#!/usr/bin/perl
# Generate index of the file structure of the experimental metadata.
# INPUT:
# directory where folders of metadata are located (1). Filename to 
# print output to (2).
# OUTPUT:
# CSV table of the metadata of the database.

use strict;

use Data::Dumper;
use Data::Dump 'dump';
use File::Find;
use Storable 'dclone';	# required to make deep copies of objects

# Input arguments ############################################
my $searchDir 	= @ARGV[0];
my $fileOut 	= @ARGV[1];

if (! -d $searchDir) {die("Input must be a valid directory name, stopped")};


# Search #####################################################
my $patternDocu = "S\d{3}_use\d{1}_docu";

my @meta;

# Do the search
find( \&findDocu, $searchDir);

# Use this sub to do the search:
sub findDocu {
	if (-d $_ && $_ =~ /S\d{3}_use\d{1}_docu/) {
		
		my $docuDir = $_;

		my $S 	= $1 if $docuDir =~ /S(\d+)/;
		my $use	= $1 if $docuDir =~ /use(\d)/;

		opendir(DIR, $docuDir);
		my @fileList = readdir(DIR);

		my @images = grep(/\.JPG$/, @fileList);
		my @logfiles = grep(/\.log$/, @fileList);

		push @meta, {
			SampleID	=> $S,
			IterationNo 	=> $use,
			Directory 	=> $File::Find::name,
			Photo 		=> "@images",
			DocuLogfile 	=> "@logfiles",
		};
	}
}

=pod
## More semantics:
# Search in the metadata for current SampleID-iteration pair.
# Each orientation documentation refers to all scans with the same pos. no. If 
# pos. no is not specified for a documentation all positions belong to
# the same orientation.
for my $iref ( @meta ) {
	my $S   = $iref->{SampleID};
	my $use = $iref->{IterationNo};	
	my $pos = $iref->{PositionNo};	

	if ( $iref->{isDocuOri} ) {
		for my $jref ( @meta ) {	
			if ($S == $jref->{SampleID} && $use == $jref->{IterationNo}) {

				if ($pos eq "" or $pos eq $jref->{PositionNo}) {
					$jref->{DocuOri} = $iref->{Scanname};
				}

				#print "$jref->{Scanname} $S $use\n";	
			}
		}
	}
}
=cut
		
#print Dumper @meta;


# Print data ################################################################
# Open file
my $fhOut; 	# file handle to print output to

if (defined $fileOut) {
	# Write to file
	open($fhOut, ">", $fileOut) or die "Can't write < $fileOut: $!";
} else {
	# No output file specified. Print to stdout.
	$fhOut = *STDOUT;
}

# Prepare
local $, = ";";	# Set value separator to semicolon
my @header = keys %{$meta[0]};
@header = sort ( @header );	# sort

## Header 
print $fhOut @header, "\n";

## Data
for my $iref ( @meta ) {
	for my $key ( @header ) {
		print $fhOut $iref->{$key}.";";
	}
	print $fhOut "\n";
}

