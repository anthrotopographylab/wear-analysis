function [X,Y,Z] = readImageTopo(filename, varargin)
%readImageTopo Read the Alicona topographic image stored as txt file.
%   Returns the topographic image as X Y Z point coordinates in mehgrid
%   format. The z values are resampled on the regular spaced x y meshgrid.
%   Sapcing of the meshgrid is either given as input or estimated from data
%   in the file. Uses customized nearest neighbor for the resampling.
%   The format of text file is: x y z cartesian coordinates of one point
%   in each line. The first line is not counting.
%
% USAGE:
%   [X Y Z] = readImageTopo(filename)
%   filename points to the Alicona topographic image exported as text file.
%   X, Y, Z are the cooordinates of surface points measrued by the Alicona.
%   Regions of missing values are detected automatically and filled with
%   NaNs.
%
%   [X Y Z] = readImageTopo(filename, numPixX, numPixY)
%   numPixX and numPixY are the number of pixels. Based on these numbers
%   the meshgrid is built. Those numbers should come from the RGB image.
%   The grid vertices are equallay spaced.


fidp = fopen(filename);
assert(fidp ~= -1, ['MWA:' mfilename ':fileIOError'], ...
    'Cannot open file %s\n', filename);

% Read the text file
data = textscan(fidp, '%f64%f64%f64', 'delimiter', ' ', 'HeaderLines', 1);

fclose(fidp);

% Return the scattered pixels
x = data{1};
y = data{2};
z = data{3};

if isempty(x) || isempty(y) || isempty(z)
    error(['MWA:' mfilename ':emptyImage'], ...
        'Cannot read Alicona-export file %s. Wrong text format.', scanname);
end

% *************************************************************************
% Redefine the (slighlty) scatterd x y coordinates on regular mesh grid
% and do a (nearest neighbor) interpolation of z values.
% This way the data is structured as a valid image and gaps in the
% xy sampling space are automatically filled with NaNs.

% Image field of view
rangeX = max(x)-min(x);
rangeY = max(y)-min(y);

if nargin == 2      % Parse number of pixels
    validateattributes(varargin{1}, {'numeric'}, {'numel', 2}, mfilename, 'image size');

    % Do an integer test
    assert(mod(varargin{1}(1),1)==0);
    assert(mod(varargin{1}(2),1)==0);
    
    numPixX = varargin{1}(2);   % # columns
    numPixY = varargin{1}(1);   % # rows
    
    resXmean = rangeX / (numPixX-1);
    resYmean = rangeY / (numPixY-1);    
elseif nargin == 1  % Estimate number of pixels 
    % Estimate resolution based on gap-free regions in the xy space.
    % Use isoutlier for gap detection.
    dx = diff(unique(x));
    dy = diff(unique(y));
    resXmean = mean(dx(~isoutlier(dx)));
    resYmean = mean(dy(~isoutlier(dy)));
    
%     resXmed = median(diff(unique(x)));
%     resYmed = median(diff(unique(y)));

    % Estimate num. of pixels
    numPixX = round(rangeX/resXmean);
    numPixY = round(rangeY/resYmean);
else
    error(['MWA:' mfilename ':invalidInput'], ...
        'Number of input variables must be 1 or 3');
end

% Make sure that there are at least as many grid points as data points.
% Otherwise information is lost.
assert(numPixX*numPixY >= numel(x), ...
    ['MWA:' mfilename ':numPixLow'], ...
    'Number of pixels is less than number of data points.');

% Build meshgrid of coordinates based on estimated or parsed image dimensions.
[X, Y] = meshgrid( ...
    linspace(min(x), max(x), numPixX), ...
    linspace(min(y), max(y), numPixY));


if nargin == 2
    % Interpolate data points on the new grid using griddata
    Z = griddata(x, y, z, X, Y, 'nearest');
    
else
    % Estimate regions of missing values:
    % Fill the meshgrid z values using a k=1 nearest neighbor search in the
    % xy space. The result of knnsearch will be a bijective mapping from the
    % scatttered xy coordinates of the text file to the regular spaced
    % XY coordinates of the meshgrid. The mapping maps a meshgrid point to
    % the closest data point, that is not more than half a resolution away.


    % Get index of data points closes to meshgrid points.
    [idxData, ~] = knnsearch([x y], [X(:) Y(:)], 'k', 1, 'distance', 'euclidean');

    %     function dist = chebNormDist(vi, Vj)
    %         % vi is 1 by 2 vector and Vj n by 2 matrix.
    %         % Compute absolute difference between vi and Vj elements
    %         vecDiff = abs(Vj - repmat(vi, size(Vj, 1), 1));
    %         
    %         % Normalize distances with half x and y resolution
    %         vecDiff(:,1) = vecDiff(:,1)/resXmean*2;
    %         vecDiff(:,2) = vecDiff(:,2)/resYmean*2;
    %         
    %         % Do chebychev step
    %         dist = max(vecDiff(:,1), vecDiff(:,2));
    %     end

    % Get a chebychev distance where x and y values are normalized with x and y
    % resolution values. I.e. for distance = 1 the data point lies on the edge
    % of the 'grid rectange', which is centered around the grid point.
    normDist = max((X(:)-x(idxData))/resXmean, (Y(:)-y(idxData))/resYmean) * 2;

%     % Discard mappings from meshgrid points to data points, where the datapoint
%     % lies outside the grid rectangle of the meshgrid point.
%     isInside = normDist <= 1;
    isInside=logical(1:numel(X));

    % % Check that scattered points really lie close to any meshgrid point, i.e.
    % % the meshgrid should be well meshed around the samples (distances small).
    % assert(~any(isoutlier(D, 'mean')), ...
    %     ['MWA:' mfilename ':knnsearchFail'], ...
    %     'Cannot map all scattered points to meshgrid');
    % 
    % % Make the mapping unique, i.e. no data point should be mapped to
    % % more than one meshgrid point. In an ambigous case, keep only the closest
    % % meshgrid point.
    % mapping = sortrows([idxMG, D, z], 2, 'ascend'); % sort after distance
    % [~, idxMap, ~] = unique(mapping(:, 1), 'first'); % remember only the first



    % Assign Z values of meshgrid points that are considered in the mapping.
    % All other points correspond to pixels not measured by the Alicona,
    % therefore set to NaN.
    Z = NaN(size(X)); % Initialize Z values of all meshgrid points to NaNs.

    % Z(mapping(idxMap, 1)) = mapping(idxMap, 3); % Paste data z values

    Z(isInside) = z(idxData(isInside));
end


end
