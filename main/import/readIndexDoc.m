function [metadata] = readIndexDoc(varargin)
%readIndexDoc Read csv index of experimental documentation data.
% The text can be stored in a file in which case the filename must be
% specified or in a string which must be indicated by 'string' as 2nd arg.
% Fieldnames are taken from the header of the text and must be consistent
% with the fieldname variables specified below in order to correctly
% identify the fields.


% Set fieldname variables
FN_DIR            = 'Directory';
FN_SID            = 'SampleID';
FN_ITER_NO        = 'IterationNo';
FN_PHOTO          = 'Photo';
FN_TRAJ_FILE      = 'TrajectoryFile';

% META_NAME           = 'Scanname';
% META_POS_NO         = 'PositionNo';
% META_MAG            = 'Magnification';
% META_ORI            = 'Orientation';
% META_IS_3D          = 'is3D';
% META_IS_RGB         = 'isRGB';
% META_IS_TXT         = 'isTxt';
% META_IS_DOCU_ORI    = 'isDocuOri';
    


% CHECK INPUT *************************************************************
% Input can be either a filename or a char string holding the data.
% In the first case the file must be opened first and the handle is parsed
% to tesxscan. In the second case the information is in the string and
% textscan reads from the char string directly.
if nargin == 1
    filename = varargin{1};
    validateattributes(filename, {'char'}, {'vector'}, mfilename, 'filename');
    source = fopen(filename, 'r');
    if source == -1
        error('MWA:ReadMetaData:IOerror', ...
            'Could not open file %s', filename);
    end    
elseif nargin == 2 && strcmp(varargin{2} , 'string')
    source = varargin{1};     % input holds the data
    validateattributes(source, {'char'}, {'vector'}, mfilename, 'source');
else
    error('MWA:ReadIndex:invalidInput', ...
        ['Usage: Specify filename only or char string with ''string''' ...
        'as second argument']);
end


% MAIN ********************************************************************

% Step 1:  Header
header = textscan(source, '%s%s%s%s%s', 1, ...
    'delimiter', ';', 'HeaderLines', 0, 'Whitespace', '');
if nargin == 1
    frewind(source); % rewind if reading from file
end

% Rename fieldnmames according to definitions in SetGlobalVariables and
% construct format specifier from fields.
fields = regexprep([header{:}],'[ \[\]]',''); % make fields struct-conform

numFields = length(fields);

fieldsNorm      = cell(1, numFields);
format          = '';
units           = cell(1, numFields);
descriptions    = cell(1, numFields);
isBool          = false(1, numFields);
isMultiStr      = false(1, numFields);

for i=1:numFields
    switch fields{i}
%         case 'Scanname'
% %             fields{i} = META_NAME;
%             fieldsNorm{1} = META_NAME;
%             format = [format '%s'];
%             units{i} = '';
%             descriptions{i} = 'Unique name of the scan';
        case 'SampleID'
            fieldsNorm{i} = FN_SID;
            format = [format '%u'];
            units{i} = '';
            descriptions{i} = 'ID of the stone sample';
        case 'IterationNo'
            fieldsNorm{i} = FN_ITER_NO;
            format = [format '%u'];
            units{i} = '';
            descriptions{i} = 'Ordinal number of experimental iteration';
%         case 'PositionNo'
% %             fields{i} = META_POS_NO;
%             fieldsNorm{4} = META_POS_NO;
%             format = [format '%u'];
%             units{i} = '';
%             descriptions{i} = 'Ordinal number of x-y position of microscopic stage';
%         case 'Magnification'
% %             fields{i} = META_MAG;
%             fieldsNorm{5} = META_MAG;            
%             format = [format '%u'];
%             units{i} = '';
%             descriptions{i} = 'Magnification of the scan';
%         case 'Orientation'
% %             fields{i} = META_ORI;
%             fieldsNorm{6} = META_ORI;
%             format = [format '%f'];
%             units{i} = 'deg';
%             descriptions{i} = 'Angle of the rotational stage of the Alicona';
        case 'Directory'
            fieldsNorm{i} = FN_DIR;
            format = [format '%s'];
            units{i} = '';
            descriptions{i} = 'Location of the exp data on disk';
        case 'Photo'
            fieldsNorm{i} = FN_PHOTO;
            format = [format '%s' ];
            units{i} = '';
            isMultiStr(i) = true;
            descriptions{i} = 'Filename of the documentation photos';
        case 'DocuLogfile'
            fieldsNorm{i} = FN_TRAJ_FILE;
            format = [format '%s' ];
            units{i} = '';
            isMultiStr(i) = true;
            descriptions{i} = 'Filename of the trajectory data';
            
        otherwise
            error('MWA:ReadIndex:IOerror', ...
                'Fieldname not provided: %s', fields{i});
    end
end


% Step 2: Data 
% the order of the fields is alphabetic (see below)
data = textscan(source, format, ...
    'delimiter', ';', 'HeaderLines', 1, 'EmptyValue', NaN);


% Reshape data for cell to table conversion
isCell  = cellfun(@iscell, data);
isFloat = cellfun(@isfloat, data);

dataExp = cell(size(data{1}, 1), length(fields));

dataExp(:, isCell&~isMultiStr)  = [data{isCell&~isMultiStr}];
dataExp(:, isMultiStr)          = cellfun(@strsplit, [data{isMultiStr}], ...
    'UniformOutput', false);
dataExp(:, isBool)              = num2cell(boolean([data{isBool}])); 
dataExp(:, isFloat)             = num2cell([data{isFloat}]);

dataExp(:,~isCell&~isBool&~isFloat) = num2cell([data{~isCell&~isBool&~isFloat}]);


% Convert to table
metadata = cell2table(dataExp, 'VariableNames', fieldsNorm);

% Add metadata ...
metadata.Properties.VariableUnits = units;
metadata.Properties.VariableDescriptions = descriptions;
metadata.Properties.UserData = struct( ...
    'this', {'IndexDoc'}, ...
    'keys', {{'SampleID', 'IterationNo'}});


end
