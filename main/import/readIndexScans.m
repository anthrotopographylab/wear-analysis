function [metadata] = readIndexScans(varargin)
%readIndexScans Read metadata of the database from csv text.
% The text can be stored in a file in which case the filename must be
% specified or in a string which must be indicated by 'string' as 2nd arg.
% Fieldnames are taken from the header of the text. They must be consistent
% with definitions in SetGlobalVariables(). The output is a struct which 
% format is defined in ConsistencyMeta().


% Set fieldname variables
META_NAME           = 'Scanname';
META_DIR            = 'Directory';
META_SID            = 'SampleID';
META_ITER_NO        = 'IterationNo';
META_POS_NO         = 'PositionNo';
META_MAG            = 'Magnification';
META_ORI            = 'Orientation';
META_IS_3D          = 'is3D';
META_IS_RGB         = 'isRGB';
META_IS_TXT         = 'isTxt';
META_IS_DOCU_ORI    = 'isDocuOri';
    


% CHECK INPUT *************************************************************
% Input can be either a filename or a char string holding the data.
% In the first case the file must be opend first and the handle is parsed
% to tesxscan. In the second case the information is in the string and
% textscan reads from the char string directly.
if nargin == 1
    filename = varargin{1};
    validateattributes(filename, {'char'}, {'vector'}, mfilename, 'filename');
    source = fopen(filename, 'r');
    if source == -1
        error('MWA:ReadMetaData:IOerror', ...
            'Could not open file %s', filename);
    end    
elseif nargin == 2 && strcmp(varargin{2} , 'string')
    source = varargin{1};     % input holds the data
    validateattributes(source, {'char'}, {'vector'}, mfilename, 'source');
else
    error('MWA:ReadIndex:invalidInput', ...
        ['Usage: Specify filename only or char string with ''string''' ...
        'as second argument']);
end


% MAIN ********************************************************************

% Step 1:  Header
header = textscan(source, '%s%s%s%s%s%s%s%s%s%s%s', 1, ...
    'delimiter', ';', 'HeaderLines', 0, 'Whitespace', '');
if nargin == 1
    frewind(source); % rewind if reading from file
end

% Rename fieldnmames according to definitions in SetGlobalVariables and
% construct format specifier from fields.
fields = regexprep([header{:}],'[ \[\]]',''); % make fields struct-conform

numFields = length(fields);
fieldsNorm      = {};
format          = '';
units           = cell(1, numFields);
descriptions    = cell(1, numFields);
isBool          = false(1, numFields);
isIntDef        = false(1, numFields);

% default value for variables of type integer that need 0 as value
intDef = -99;


for i=1:numFields
    switch fields{i}
        case 'Scanname'
%             fields{i} = META_NAME;
            fieldsNorm{1} = META_NAME;
            format = [format '%s'];
            units{i} = '';
            descriptions{i} = 'Unique name of the scan';
        case 'SampleID'
%             fields{i} = META_SID;
            fieldsNorm{2} = META_SID;
            format = [format '%u16'];
            units{i} = '';
            descriptions{i} = 'ID of the stone sample';
        case 'IterationNo'
%             fields{i} = META_ITER_NO;
            fieldsNorm{3} = META_ITER_NO;
            format = [format '%f32'];
            units{i} = '';
            descriptions{i} = 'Ordinal number of experimental iteration';
%             isIntDef(i) = true;
        case 'PositionNo'
%             fields{i} = META_POS_NO;
            fieldsNorm{4} = META_POS_NO;
            format = [format '%u8'];
            units{i} = '';
            descriptions{i} = 'Ordinal number of x-y position of microscopic stage';
        case 'Magnification'
%             fields{i} = META_MAG;
            fieldsNorm{5} = META_MAG;            
            format = [format '%u16'];
            units{i} = '';
            descriptions{i} = 'Magnification of the scan';
        case 'Orientation'
%             fields{i} = META_ORI;
            fieldsNorm{6} = META_ORI;
            format = [format '%f32'];
            units{i} = 'deg';
            descriptions{i} = 'Angle of the rotational stage of the Alicona';
        case 'Directory'
%             fields{i} = META_DIR;
            fieldsNorm{7} = META_DIR;
            format = [format '%s'];
            units{i} = '';
            descriptions{i} = 'Location of the scan data on disk';
            
%         case 'PathDocu'
%             fields{i} = META_DIR_DOCU;
%             fieldsNorm{8} = META_DIR_DOCU;
%             format = [format '%s'];
%         case 'DocuImage'
%             fields{i} = META_DOCU_IM;
%             fieldsNorm{9} = META_DOCU_IM;   
%             format = [format '%s'];
%         case 'DocuLogfile'
%             fields{i} = META_DOCU_LOG;
%             fieldsNorm{10} = META_DOCU_LOG;
%             format = [format '%s'];
%         case 'DocuOri'
%             fields{i} = META_DOCU_ORI;
%             fieldsNorm{11} = META_DOCU_ORI;
%             format = [format '%s'];
            
        case 'is3D'
%             fields{i} = META_IS_3D;
            fieldsNorm{12} = META_IS_3D;
            format = [format '%u8'];
            isBool(i) = true;
            units{i} = '';
            descriptions{i} = 'Indicates whether scan has topographic data';
        case 'isRGB'
%             fields{i} = META_IS_RGB;
            fieldsNorm{13} = META_IS_RGB;
            format = [format '%u8'];
            isBool(i) = true;
            units{i} = '';
            descriptions{i} = 'Indicates whether scan has trucolor image';
        case 'isExport'
%             fields{i} = META_IS_TXT;
            fieldsNorm{14} = META_IS_TXT;
            format = [format '%u8'];
            isBool(i) = true;
            units{i} = '';
            descriptions{i} = 'Indicates whether topographic data is exported';
        case 'isDocuOri'
%             fields{i} = META_IS_DOCU_ORI;
            fieldsNorm{15} = META_IS_DOCU_ORI;
            format = [format '%u8'];
            isBool(i) = true;
            units{i} = '';
            descriptions{i} = 'Indicates whether this is a scan of the tool cup documenting orientation of the stone sample';
        otherwise
            error('MWA:ReadIndex:IOerror', ...
                'Fieldname not provided: %s', fields{i});
    end
end


% Step 2: Data 
% the order of the fields is alphabetic (see below)
data = textscan(source, format, ...
    'delimiter', ';', 'HeaderLines', 1, 'EmptyValue', NaN);


% Reshape and format data types for cell to table conversion
isCell = cellfun(@iscell, data);
isFloat = cellfun(@isfloat, data);

dataExp = cell(size(data{1}, 1), length(fields));
dataExp(:, isCell)  = [data{isCell}];
dataExp(:, isBool)  = num2cell(boolean([data{isBool}])); 
dataExp(:, isFloat) = num2cell([data{isFloat}]);

% Set default value for some variables of type integer
IntDefault = [data{isIntDef}];
IntDefault(isnan(IntDefault)) = intDef;
dataExp(:, isIntDef) = num2cell(int16(IntDefault));

isOther = ~isCell&~isBool&~isFloat&~isIntDef;
dataExp(:, isOther) = num2cell([data{isOther}]);


% Convert to table and add some metadata
metadata = cell2table(dataExp, 'VariableNames', fields);

metadata.Properties.VariableUnits = units;
metadata.Properties.VariableDescriptions = descriptions;
metadata.Properties.UserData = struct( ...
    'this', {'IndexScans'}, ...
    'keys', {{'Scanname'}});



% DEPRECATED:
% % Insert missing value indicators if necessary
% metadata = standardizemissing(metadata);
% 
% isDocu = metadata.(META_MAG) < 50 | metadata.(META_IS_DOCU_ORI);
% 
% indexDocu = metadata(isDocu, :);
% indexMeas = metadata(~isDocu, :);
% 
% indexDocu.Properties.Description = ...
%     ['Index of scans which serve documentation purposes.'];
% %     'E.g. xy positions of the microscopic table and ' ...
% %     'orientation of the stone sample under objective'];
% 
% indexMeas.Properties.Description = ...
%     ['Index of scans which will be used for metrology analysis.'];
% %     'They have magnification of 50x or mroe'];
% 
% %ConsistencyMeta(metadata);



end
