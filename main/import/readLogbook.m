function logbook = readLogbook(xlsfile)
%readLogbook Read logbook of the database from xls file and return a table.
% Fieldnames are defined  in the switch block below and must match the 
% names in the xls file.


% CHECK INPUT *************************************************************
validateattributes(xlsfile, {'char'}, {'vector'}, mfilename, 'filename');

[~, fnBase, ~] = fileparts(xlsfile);


% READ XLS FILE ***********************************************************
% Use maltabs xlsread. Make sure to include only contiguous data ranges in
% the xls file. Otherwise xlsread fails because it only runs in basic mode.
% See "help xlsread" fore more info.
[~, ~, data] = xlsread(xlsfile, 'Exp', '', 'basic');

% Crop rows and columns holding only NaNs off from the cell array
isnanAny = @(x) any(isnan(x));
isNotAN = cellfun(isnanAny, data);
[numRows, numCols] = size(isNotAN);

isCropRow = isNotAN *ones(numCols,1) == numCols; % rows
isCropCol = isNotAN'*ones(numRows,1) == numRows; % cols
dataCrop = data(~isCropRow, ~isCropCol);


% Rename fieldnmames for table and construct format specifier from
% fieldnames of the header. 
fieldsRaw = dataCrop(1,:);  % Fieldnames should be in the first row
fieldsNorm  = {};
fieldSel    = [];
dataNorm    = {};
units       = {};
descriptions = {};

% Look for these 10 relevant fields
for i=1:length(fieldsRaw)
    switch fieldsRaw{i}
        case 'RunNo'
            fieldsNorm{end+1} = 'RunNo';
            units{end+1} = '';
            descriptions{end+1} = 'Ordinal number of the experiment';
            myfun = @uint16;
        case 'Experiment ID'
            fieldsNorm{end+1} = 'ExperimentID';
            units{end+1} = '';
            descriptions{end+1} = 'ID of the experiment';
            myfun = @uint16;
        case 'Interval No'
            fieldsNorm{end+1} = 'IterationNo';
            units{end+1} = '';
            descriptions{end+1} = 'Ordinal number of the iteration';
            myfun = @single;
        case 'Date'
            fieldsNorm{end+1} = 'Date';
            units{end+1} = '';
            descriptions{end+1} = 'Date when experiment was performed';
            % Convert from xls date format to MATLAB date format
            myfun = @(x) datetime(datevec(x2mdate(x)));
        case 'Sample ID'
            fieldsNorm{end+1} = 'SampleID';
            units{end+1} = '';
            descriptions{end+1} = 'ID of the stone sample';
            myfun = @uint16;
        case 'Workpiece ID'
            fieldsNorm{end+1} = 'WorkpieceID';
            units{end+1} = '';
            descriptions{end+1} = 'ID of the workpiece of the experiment';
            myfun = @char;
        case 'Workpiece Type'
            fieldsNorm{end+1} = 'WorkpieceType';
            units{end+1} = '';
            descriptions{end+1} = 'Class of the workpiece';
            myfun = @char;
        case 'Force [N]'    
            fieldsNorm{end+1} = 'Force';
            units{end+1} = 'N';
            descriptions{end+1} = 'Vertical force during scraping';
            myfun = @single;
        case 'Num Cycles'   
            fieldsNorm{end+1} = 'NumCycles';
            units{end+1} = '';
            descriptions{end+1} = 'Number of cycles performed in this iteration';
            myfun = @single;
        case 'Num Docu Im'
            fieldsNorm{end+1} = 'NumDocuIm';
            units{end+1} = '';
            descriptions{end+1} = 'Number of documentation photos taken';
            myfun = @uint16;
        % Do not read following fields
%         case 'Datafile'
% 
%         case 'Filename'
%  
%         case 'Comments'
            
        otherwise
            % skip
            continue;
    end
    dataNorm(:,end+1) = cellfun(myfun, dataCrop(2:end,i), ...
        'UniformOutput', false);
end

% Warn if not all fields from above were read.
numNorm = length(fieldsNorm);
if numNorm < 10
    warning(['MWA:' mfilename ':badTextFormat'], ...
        ['Did only identify %u fieldnames of %s.\n', ...
        'Check specifications in %s.'], numNorm, fnBase, mfilename);
end

% Convert to table
logbook = cell2table(dataNorm, 'VariableNames', fieldsNorm);

% Add metadata to table ...
logbook.Properties.VariableUnits = units;
logbook.Properties.VariableDescriptions = descriptions;
logbook.Properties.UserData = struct( ...
    'this', {'Logbook'}, ...
    'keys', {{'SampleID', 'IterationNo'}});


end
