#!/usr/bin/perl
# Generate index of the file structure of the database.
# INPUT: directory where folders of data are located and filename
# to print output to.
# OUTPUT: CSV table of the metadata of the database.

use strict;

use Data::Dumper;
use Data::Dump 'dump';
use File::Find;
use Storable 'dclone';	# required to make deep copies of objects

# Input arguments ############################################
my $searchDir 	= @ARGV[0];
my $fileOut 	= @ARGV[1];

if (! -d $searchDir) {die("Input must be a valid directory name, stopped")};


# Main #######################################################
my $patternDocu = "S\d{3}_use\d{1}_docu";

my @meta;

my $numScans = 0; # total number of scans taken (RGB and 3D)

# Go thorugh the whole directory tree of $searchDir
# Step1: Find the scan data
find( \&findScans, $searchDir);

if ($numScans == '0') { die("No data found in the directory specified, stopped") };

# Step2: Find docu data
find( \&findDocu, $searchDir);


# Use this sub to get some metadata of the scans
sub findScans {
	if (-d && /^S\d{3}_.*\$3D$/ && ! /(level|filter|roughness)\$3D$/) { 
		
		# Found valid directory of a scan
		$numScans = $numScans + 1;
		my $dataDir = $_;

		# Extract scan parameters from keys
		my $name	= ( $dataDir =~ /^(.+)\$3D$/	? $1 : "");
		my $S 		= ( $dataDir =~ /S(\d{3})/	? $1 : "");
		my $use 	= ( $dataDir =~ /use(\d+)/ 	? $1 : "");
		my $ori 	= ( $dataDir =~ /ori(\d+)/ 	? $1 : "");
		my $pos		= ( $dataDir =~ /(pos|reg)(\d+)/? $2 : "");
		my $x		= ( $dataDir =~ /x(\d+)/ 	? $1 : "");

		push @meta, {
			Scanname	=> $name,
			SampleID	=> $S,
			IterationNo	=> $use,
			PositionNo	=> $pos,
			Magnification	=> $x,
			Orientation 	=> $ori,
			PathData	=> $File::Find::name,
			is3D		=> (-f "$dataDir/dem.al3d" 	? 1 : 0),
			isRGB 		=> (-f "$dataDir/texture.bmp"	? 1 : 0),
			istxt 		=> (-f "$dataDir/export.txt" 	? 1 : 0),
			isDocuOri 	=> 0,	# default
			PathDocu 	=> "",	# default
			DocuImage 	=> "",	# default
			DocuLogfile 	=> "",	# default
			DocuOri		=> ""	# default
		};
		
		## Innject some semantic rules 
		# Raw scans are always taken on spots which were not stressed jet
		if ($dataDir =~ /raw/) {
			$meta[$#meta]{IterationNo} 	= 0;
			$meta[$#meta]{PositionNo} 	= 1;
		}	
		
		# This flag indicates a scan that documents orientation of the sample
		if ($dataDir =~ /orientation/) {
			$meta[$#meta]{isDocuOri} 	= 1;
			$meta[$#meta]{Magnification} 	= "002";
		}
	}
}


# Use this sub to locate docu files of the scans:
# Images taken during the experiment and trajectory data recorded during the experiment 
# stored in log files.
sub findDocu {
	if (-d $_ && $_ =~ /S\d{3}_use\d{1}_docu/) {
		
		my $docuDir = $_;

		my $S 	= $1 if $docuDir =~ /S(\d+)/;
		my $use	= $1 if $docuDir =~ /use(\d)/;

		opendir(DIR, $docuDir);
		my @fileList = readdir(DIR);

		my @images = grep(/\.JPG$/, @fileList);
		my @logfiles = grep(/\.log$/, @fileList);

		# Search in the metadata for current SampleID-iteration pair.
		for my $iref ( @meta ) {
			if ($S eq $iref->{SampleID} && $use eq $iref->{IterationNo}) {
				
				$iref->{PathDocu} 	= $File::Find::name,
				$iref->{DocuImage}	= "@images",
				$iref->{DocuLogfile}	= "@logfiles",
			}
		}	
	}
}

## More semantics:
# Search in the metadata for current SampleID-iteration pair.
# Each orientation documentation refers to all scans with the same pos. no. If 
# pos. no is not specified for a documentation all positions belong to
# the same orientation.
for my $iref ( @meta ) {
	my $S   = $iref->{SampleID};
	my $use = $iref->{IterationNo};	
	my $pos = $iref->{PositionNo};	

	if ( $iref->{isDocuOri} ) {
		for my $jref ( @meta ) {	
			if ($S == $jref->{SampleID} && $use == $jref->{IterationNo}) {

				if ($pos eq "" or $pos eq $jref->{PositionNo}) {
					$jref->{DocuOri} = $iref->{Scanname};
				}

				#print "$jref->{Scanname} $S $use\n";	
			}
		}
	}
}

			
#print Dumper @meta;


# Print data ################################################################
# Open file
my $fhOut; 	# file handle to print output to

if (defined $fileOut) {
	# Write to file
	open($fhOut, ">", $fileOut) or die "Can't write < $fileOut: $!";
} else {
	# No output file specified. Print to stdout.
	$fhOut = *STDOUT;
}

# Prepare
local $, = ";";	# Set value separator to semicolon
my @header = keys %{$meta[0]};
@header = sort ( @header );	# sort

## Header 
print $fhOut @header, "\n";

## Data
for my $iref ( @meta ) {
	for my $key ( @header ) {
		print $fhOut $iref->{$key}.";";
	}
	print $fhOut "\n";
}

