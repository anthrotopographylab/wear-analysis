function [expdocu] = CollectDocu(metadata)
% Read documentation files of the experiment. I.e. images and logfile.
% The images of the stone samples were taken bevor and after each 
% experimental iteration. The logfile contains the trajectory data of the
% endeffector. 
% Uses index of the dir structure to locate the files in the database.
% The output is a struct array containing the docu images and trajectory
% data.

% metadata fieldname definitions
META_DIR_DOCU = 'DirDocu';
META_DOCU_IM = 'DocuImage';
META_LOGFILE = 'DocuLogfile';
META_ITER_NO = 'IterationNo';
META_SID = 'SampleID';

% Get unique list of folder names from metadata
foldersAll = {metadata.(META_DIR_DOCU)};
isOK = cellfun(@ischar, foldersAll);
folders = unique(foldersAll(isOK));


numFolders = length(folders);

% Initialize struct with NaNs by default
fields = { ...
    'DocuImages', ...
    'Trajectory', ...
    'NumDocuIm', ...
    'Directory', ...
    'SampleID', ...
    'IterationNo'};
expdocu = cell2struct(num2cell(NaN(length(fields), numFolders)), fields, 1);


for i=1:numFolders
    indexMeta = find(strcmp(foldersAll, folders(i)), 1);
    expdocu(i).SampleID = metadata(indexMeta).(META_SID);
    expdocu(i).IterationNo = metadata(indexMeta).(META_ITER_NO);
    expdocu(i).Directory = folders{i};
    
    % Read docu image
    docuImStr = metadata(indexMeta).(META_DOCU_IM);
    if ~isnan(docuImStr)
        docuIm = strsplit(docuImStr);
        numDocuIm = length(docuIm);
    
        images = cell(1, numDocuIm);

%         for j=1:numDocuIm
%             images{j} = imread(fullfile(folders{i}, docuIm{j}));
%         end

        expdocu(i).DocuImages = docuIm;
        expdocu(i).NumDocuIm = numDocuIm;
    else
        expdocu(i).DocuImages = NaN;
        expdocu(i).NumDocuIm = 0;
    end
    
    % Read logfile
    logfileStr = metadata(indexMeta).(META_LOGFILE);
    if ~isnan(logfileStr)
        logfile = strsplit(logfileStr);
        numLogfiles = length(logfile);
        
        for j=1:numLogfiles
            trajectoryData(j) = ReadLogfile(fullfile(folders{i}, logfile{j}));
        end
        
        expdocu(i).Trajectory = trajectoryData;
    else
        expdocu(i).Trajectory = NaN;
    end
    
end
   

end


