#!/bin/bash
##########################################################################
# Make an update of the database of this experimental session using rsync.
# The directory where the data is saved localy is given as input argument.
##########################################################################

# set -x

# Source folder mounted locally:
source1='/media/johannes/iwf-alicona/Johannes Pfleging/SessionForce$fld/exp$prj/'
source2='/media/johannes/iwf-alicona/Johannes Pfleging/SessionForce$fld/constantin$prj/'

# List of files and folders to sync, specified relative to source
#filelist='
#SessionForce$fld/exp$prj/
#automation'


# Input ##################################################################
# destination=/media/johannes/Daten/polybox/experiments/Session_Force/exp/data/
destination="$1"

if [ ! -d "$destination" ]; then
	echo "Destination folder does not exist." >&2
	exit 1
fi


# Main ##################################################################
## Mount alicona drive if necessary and Check whether locations exist
if [ ! -d "$source1" ]; then
	sudo mount -t cifs '//iwf-alicona.ethz.ch/alicona' \
		/media/johannes/iwf-alicona/ \
		-o 'username=pfleginj,uid=1000,gid=1000';
	if [ ! $? -eq "0" ]; then
		echo "Could not mount iwf server." >&2
		exit 1;
	fi
fi


## Do the backup in update mode
# r: 			recursive
# t: 			keep file modification time of source
# n: 			dry run
# v: 			more command line logging
# u: 			copy only files from source which are newer
#			than destination (update)
# --ignore-existing: 	Copy only new files. Do not touch existing files.
# --exclude=profiles.xml
# --filter		Select/Deselect certain files from sync
rsync -rtv --prune-empty-dirs --ignore-existing \
	--filter='+ */' \
	--filter='+ *dem.al3d' \
	--filter='+ *info.xml' \
	--filter='+ *icon.bmp' \
	--filter='+ *texture.bmp' \
	--filter='+ *qualitymap.bmp' \
	--filter='+ *export.txt' \
	--filter='- *' \
	"$source1" "$source2" "$destination"

#--filter='+ *dem.al3d' --filter='- *' --filter='+ */' --ignore-existing --prune-empty-dirs


## Show what is not synced
#diff -r "$source" "$destination" | grep "Only in"'
