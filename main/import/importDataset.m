%importDataset Import all experimental data and image to MATLAB.
% Necessary steps are: updating the database from iwf server,
% analysis of the directory tree, setup of table relationships, reading of
% the images and saving to mat file.
% The directory tree is read out with help of bash-scripts called from here.

clear all;

% Initialize some variables
run startMWA.m

global ...
    DIR_DATA ...
    DIR_SCANS ...
    DIR_DOC ...
    SYSTEM_FUN_PULL_ALICONA ...
    PATH_LOGBOOK


% Set DATASET NAME
% Set new name to read all data save to new mat file, OR
% give name of an existing mat file to update the data in it.
DATASET = 'DS_NewYork.mat';
pathDataset = fullfile(DIR_DATA, DATASET);

if exist(pathDataset, 'file')
    load(pathDataset);
end


%% Pull data from server to local folder
[status, output] = system([SYSTEM_FUN_PULL_ALICONA ' ' DIR_SCANS], '-echo');
if status
    error('Updating database failed:\n%s', output);
end


%% Build relationship map
% Specify relationships between the tables loaded. This information is
% necessary for the querry of data records later.

% List table names and corresponding ID variable and the name of the 
% container struct the table is stored in.
tablenames  = {'IndexScans', 'IndexDoc', 'Logbook', 'Scans', 'ExpDoc'};
identifier  = {'Scanname', 'Directory', 'RunNo', 'Scanname', ''};
container   = {'PARA', 'PARA', 'PARA', 'IMAG', 'IMAG'};

% List key variables for the join opertaion
keys = { ...
    {'SampleID', 'IterationNo'}; ...
    {'SampleID', 'IterationNo'}; ...
    {'SampleID'}; ...
    {'RunNo'}};

% Specify relationships as table objects
Edges = table([1 3; 2 3; 1 4; 2 5], keys, ...
    'VariableNames', {'EndNodes', 'Keys'});
Nodes = table(tablenames', container', identifier', ...
    'VariableNames', {'Name', 'Ref', 'ID'});

% Create map and show
MAP = graph(Edges, Nodes);

plot(MAP, 'NodeLabel', MAP.Nodes.Name);


%% Read directory structure, experimental data, and meta-data

% Read dir tree of scans
[status, output] = system(sprintf('./main/import/indexScans.pl %s', DIR_SCANS));
if status
    error('Indexing database failed:\n%s', output);
end
IndexScans = readIndexScans(output, 'string');

% Read dir tree of doc files
[status, output] = system(sprintf('./main/import/indexDoc.pl %s', DIR_DOC));
if status
    error('Indexing database failed:\n%s', output);
end
IndexDoc = readIndexDoc(output, 'string');

% Read logbook from xls file
Logbook = readLogbook(PATH_LOGBOOK);

% See if new entries were pulled from server
if exist('PARA', 'var')
    [~, isNewScan] = setdiff( ...
        IndexScans(:,'Scanname'), PARA.IndexScans(:,'Scanname'));

    fprintf('Found the following new Scans: \n');
    display(IndexScans{isNewScan, 'Scanname'});
end

% Store tables in container struct
PARA.IndexScans = IndexScans;
PARA.IndexDoc   = IndexDoc;
PARA.Logbook    = Logbook;



%% Read alicona images
Scans = table();
numScans = height(IndexScans);

% Initialize the whole table
Scans{1:numScans, 'Scanname'}    = {''};
Scans{1:numScans, 'Info'}        = readScanInfo();
Scans{1:numScans, 'ImageRGB'}    = {NaN};
Scans{1:numScans, 'ImageTopo'}   = {NaN};
Scans{1:numScans, 'Qualitymap'}  = {NaN};
Scans{1:numScans, 'XGrid'}       = {NaN};
Scans{1:numScans, 'YGrid'}       = {NaN};

% Load missing or load all
if exist('IMAG', 'var')
    % Do an update of the existing dataset: Load only the new entries
    % indicated by isNewScan.
    MyIndex = IndexScans(isNewScan, :);
else
    % Load all images listet in IndexScans
    MyIndex = IndexScans;
end

for i=1:height(MyIndex)
    fprintf('Read Image %s\n', MyIndex{i, 'Scanname'}{:});
    Scans{i, 'Scanname'} = MyIndex{i, 'Scanname'};
    
    % Try Alicona import
    try 
       [ Info, ImageRGB, ImageTopo, Qualitymap, XGrid, YGrid ] ...
            =  importAliconaImage(MyIndex{i, 'Directory'}{:});   
        
        if ~strcmp(Info.Scanname, MyIndex{i, 'Scanname'}{:})
            error('Scannames of folder and info file mismatch.');
        end
    
    catch ME
        warning('Could not import Alicona Image %s: %s', ...
            MyIndex{i, 'Scanname'}{:}, ME.message);
        
        % If Alicona import failed, assign missing values.
        Info        = readScanInfo();
        ImageRGB    = [];
        ImageTopo   = [];
        Qualitymap  = [];
        XGrid       = [];
        YGrid       = [];
    end
    
    % Assign values. 
    Scans{i, 'Info'}        = Info;
    Scans{i, 'ImageRGB'}    = {ImageRGB};
    Scans{i, 'ImageTopo'}   = {ImageTopo};
    Scans{i, 'Qualitymap'}  = {Qualitymap};
    Scans{i, 'XGrid'}       = {XGrid};
    Scans{i, 'YGrid'}       = {YGrid};
end

if exist('IMAG', 'var')
    % Append new images 
    IMAG.Scans = [IMAG.Scans; Scans];
else
    % Store images in struct
    IMAG.Scans  = Scans;
end


%% Read docu images
Expdoc = table();

for i=1:height(IndexDoc)
    
    % Read logfile
    logfileCell = IndexDoc{i, 'TrajectoryFile'};

%     for j=1:numLogfiles
%         trajectoryData(j) = ReadLogfile(fullfile(folders{i}, logfile{j}));
%     end
%     expdocu(i).Trajectory = trajectoryData;

    % Read doc images
    % ...

end


IMAG.Expdoc = Expdoc;


%% Save dataset to mat file
if exist(fullfile(DIR_DATA, DATASET), 'file') == 0     % file does not exist jet
    save(fullfile(DIR_DATA, DATASET), 'DATASET', 'IMAG', 'PARA', 'MAP', '-v7.3');
else
    answer = questdlg( ...
        sprintf('File %s already exists. Do you want to overwrite?', DATASET), ...
        'Overwrite file?');
    
    if strcmpi(answer, 'yes')
        save(fullfile(DIR_DATA, DATASET), 'DATASET', 'IMAG', 'PARA', 'MAP', '-v7.3');
    else
        % Do not save, just quit.
    end
end



%% Clear temporary variables

clearvars -except DATASET MAP IMAG PARA;



