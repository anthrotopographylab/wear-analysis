function metadata = readScanInfo(filename)
%readScanInfo Read metadata of a scan from xml file formated by Alicona.
%   Not all fields of the xml file are read (see code below).
%   This function makes use of MATLABs xmlread.
%   INPUT:
%   The xml filename.
%   OUTPUT:
%   Struct storing the data of the xml file..


if nargin == 0
    % Return struct with missing value markers
    metadata.Scanname       = {''};
    metadata.Creator        = {''};
    metadata.CreationTime   = NaT;
    metadata.Magnification  = NaN;
    metadata.MinVal         = NaN;
    metadata.Shift          = NaN;
    metadata.DurationSec    = NaN;
    metadata.ExposureSec    = NaN;
    metadata.Contrast       = NaN;
    metadata.ResVertUm      = NaN;
    metadata.ResLatUm       = NaN;
    metadata.Polarizer      = false;
    
    return;
end


% READ XML ****************************************************************
root = xmlread(filename);

% Save selected values to struct
metadata = struct();

metadata.Scanname   = getElementData('name');
metadata.Creator    = getElementData('creatorName');
metadata.CreationTime = datetime( ...
    getElementData('creationDateTime'), ...
    'InputFormat', 'uuuu-MM-dd''T''HH:mm:ss');
metadata.Magnification = str2double(getElementData('magnification'));

metadata.MinVal = [];
metadata.Shift = [];

%getElementData('depthQuality')

if ~isempty(root.getElementsByTagName('depthQuality').item(0))
    
    minVal = regexp( ...
        char(root.getElementsByTagName('depthQuality').item(0).getAttributes.item(1)), ...
        '"(.*)"', 'tokens');

    shift = regexp( ...
        char(root.getElementsByTagName('depthQuality').item(0).getAttributes.item(2)), ...
        '"(.*)"', 'tokens');

    metadata.MinVal = str2double(minVal{:});
    metadata.Shift = str2double(shift{:});
end



% READ DESCRIPTION ********************************************************
% Most of the data is encoded in only one element named "description". Use
% perls regexp command to find and copy values from string description.
description = getElementData('description');

% nameValid = [];

fields = {'Elapsed Time:', 'Benötigte Zeit:'};
[value, unit] = getFieldData(fields);
if isnan(value); errorValue(fields); end
switch unit
    case 'h'
        metadata.DurationSec = value * 60 * 60;
    case 'min'
        metadata.DurationSec = value * 60;
    case 's'
        metadata.DurationSec = value;
    case 'ms'
        metadata.DurationSec = value * 1e-3;
    otherwise
        errorUnit(fields);
end
    
fields = {'Exposure Time:', 'Belichtungszeit:'};
[value, unit] = getFieldData(fields);
if isnan(value); errorValue(fields); end
switch unit
    case 'µs'
        metadata.ExposureSec = value * 1e-6;
    case 'ms'
        metadata.ExposureSec = value * 1e-3;
    case 's'
        metadata.ExposureSec = value;
    otherwise
        errorUnit(fields);
end

fields = {'Contrast:', 'Kontrast:'};
[value, unit] = getFieldData(fields);
if isnan(value)
    errorValue(fields);
else
    metadata.Contrast = value;
end

fields = {'Estimated Vertical Resolution:', 'Geschätzte Vertikale Auflösung:'};
[value, unit] = getFieldData(fields);
if isnan(value)
    metadata.ResVertUm = NaN;
else
    switch unit
        case 'µm'
            metadata.ResVertUm = value;
        case 'm'
            metadata.ResVertUm = value * 1e+6;
        otherwise
            errorUnit(fields);
    end
end

fields = {'Estimated Lateral Resolution:', 'Geschätzte Laterale Auflösung:'};
[value, unit] = getFieldData(fields);
if isnan(value)
    metadata.ResLatUm = NaN;
else 
    switch unit
        case 'µm'
            metadata.ResLatUm = value;
        case 'm'
            metadata.ResLatUm = value * 1e+6;
        otherwise
            errorUnit(fields);
    end
end

fields = {'Polarizer:', 'Polarisator:'};
[value, unit] = getFieldData(fields);
switch lower(unit)
    case {'aktiviert', 'activated'}
        metadata.Polarizer = true;
    case {'deaktiviert', 'deactivated'}
        metadata.Polarizer = false;
    otherwise
        errorUnit(fields); 
end


    % Helper to access data according to xmls DOM
    function data = getElementData(tagname)
        element = root.getElementsByTagName(tagname).item(0);
        data = char(element.getFirstChild.getData);
    end

    function [myvalue, myunit] = getFieldData(fieldnames)
        cmdValue =  'echo ''%s'' | perl -0777 -we ''<>=~/%s([.\\s\\d]+)([µ\\w]*)/ ? print $1 : print "NaN"''';
        cmdUnit =   'echo ''%s'' | perl -0777 -we ''<>=~/%s([.\\s\\d]+)([µ\\w]*)/ ? print $2 : print "NaN"''';
        
        for name = fieldnames
            myvalue = 0;
            myunit = 0;
            [statusV, resultV] = system(sprintf(cmdValue, description, name{1}));
            [statusU, resultU] = system(sprintf(cmdUnit, description, name{1}));
            
            if statusV > 0 || statusU > 0
                error('MWA:ReadScanInfo:systemCmdFail', ...
                    'Could not evaluate perl command');
            end
            
            if strcmp(resultU, 'NaN')
                % name not found
                myunit = NaN;
            end
            
            if strcmp(resultV, 'NaN')
                % name not found
                myvalue = NaN;
            end
            
            if isnan(myunit) && isnan(myvalue)
                % try next fieldname or return function
            elseif xor(isnan(myunit), isnan(myvalue))
                % should not happen
                assert(false) 
            else
                myvalue = str2double(resultV); 
                % now myvalue can also be a NaN 
                myunit = strtrim(resultU);
                break;
            end
        end
    end

    function errorValue(fieldname)
        error('MWA:ReadScanMeta:invalidValueFormat', ...
            'Could not read field value for ''%s''.', fieldname{1});
    end

    function errorUnit(fieldname)
        error('MWA:ReadScanMeta:invalidUnitFormat', ...
            'Could not read field unit for ''%s''.', fieldname{1});
    end

end
