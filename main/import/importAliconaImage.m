function [ info, ImageRGB, ImageTopo, Qualitymap, XGrid, YGrid ] = importAliconaImage( aliconaDir )
%importAliconaImage Reads all data of an Alicona image stored in dirpath.
%   The Alicona image contains an info file, RGB image and optionally a
%   topographic image with point coordinates and corresponding qualitymap.
%   All files are read out with separate functions and
%   the images and coordinates are aligned if required.
%   Outputs the info file as struct and the images as double arrays.
%   In case the topo is not available, returns empty arrays for 
%   topo and the following dependent images.

% Validate Input
assert(logical(exist(aliconaDir, 'dir')), ...
    ['MWA:' mfilename ':dirNotFound'], ...
    'Directory does not exist');

% Read info file
info = readScanInfo(fullfile(aliconaDir, 'info.xml'));

% Read RGB image and flip yaxis
ImageRGB = imread(fullfile(aliconaDir, 'texture.bmp'));

ImageRGB = flipud(ImageRGB); % TO DO: flip yaxis of topo image instead
sizeRGB  = [size(ImageRGB, 1), size(ImageRGB, 2)];  % Ignore 3. dim

% Read topographic image if available and do the preprocessing
imTopoPath = fullfile(aliconaDir, 'export.txt');

% Empty values in case topographic image is unavailable
ImageTopo   = NaN;
Qualitymap  = NaN;
XGrid = NaN;
YGrid = NaN;

if exist(imTopoPath, 'file')
    
    % Create topographic image from point cloud stored in text file
    % using nearest neighbor interpolation for the 'gridding' step.
    [XGrid, YGrid, ImageTopo] = readImageTopo(imTopoPath, sizeRGB);
    
    % Mark the missing values with NaNs
    isMiss = ~logical(sum(ImageRGB, 3));     % black pixels in RGB image
    ImageTopo(isMiss) = NaN;
    
    Qualitymap = imread(fullfile(aliconaDir, 'qualitymap.bmp'));
    Qualitymap = flipud(Qualitymap); % TO DO: flip topo image instead
    

    
%     % *********************************************************************
%     % Align topographic to RGB image. A possibel size mismatch may result
%     % from cropping during the to-text export of the topo image through the
%     % Alicona software and corresponding resampling in readImageTopo.
%     
%     % 1. Check whether RGB image is smaller in any dimension. If so, resample
%     % RGB image to be >= the size of the topo image in all dimensions.
%     sizeTopo = size(ImageTopo);
%     sizeRGB  = [size(ImageRGB, 1), size(ImageRGB, 2)]; % Ignore 3. dim
%     
%     if any(sizeTopo > sizeRGB)
%         % Resample using nearest neighbour interpolation
%         ImageRGB = imresize(ImageRGB, max(sizeTopo, sizeRGB), 'nearest');
%         sizeRGB  = [size(ImageRGB, 1), size(ImageRGB, 2)]; % update
%     end
%     
%     % 2. Check whether topo image is smaller than the new RGB image. If so,
%     % do image registration by crosscorelation and crop the RGB according
%     % to the correlation peak such that RGB and topo match size. The
%     % cropped parts in the RGB should be complete row or cols of outliers
%     % (black pixels).
%     if any(sizeRGB > sizeTopo)
%         % Compute the crosscorelation of binary images. Threshold are the
%         % outliers.
%         Xcorr = normxcorr2(imbinarize(ImageTopo), logical(sum(ImageRGB, 3)));
%         
%         % At the peak the images coincide best.
%         [rowPeak, colPeak] = find(Xcorr == max(Xcorr(:)));
%         rowMin = rowPeak-sizeTopo(1)+1;
%         colMin = colPeak-sizeTopo(2)+1;
%         
%         if ( sizeRGB(1) < rowPeak || sizeRGB(2) < colPeak || ...
%             rowMin > rowPeak || colMin > colPeak )
%             error('Could not crop RGB image of %s', info.Name');
%         end
%         
%         ImageRGB   = ImageRGB(rowMin:rowPeak, colMin:colPeak, :);
%     end
        

end
end

