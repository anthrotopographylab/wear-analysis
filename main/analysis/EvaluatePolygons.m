function [ parameters ] = EvaluatePolygons( scanD, X, Y, qmap, polygons, sigma)
% Compute surface parameters for one or more polygon regions of scanD.
% polygons must be formated as described in Polygons2Roi. 
% It is also possible to parse polygons encapsulated in a cell array. The
% content of each cell must follow the format described in Polygons2Roi.
% If polygons is is an empty array, the whole scanD is evaluated.
% Polygons which contain NaNs are not evalutated.
% INPUT:
% scanD:    Depth image given as 2d array.
% X:        x world coordinates of the image in grid format.
% Y:        y world coordinates of the image in grid format.
% polygons: Polygon vertices formated either as cell or as numeric array as
%           described above.
% sigma:    Width of the gaussian filtering in world coordinates, given as
%           two element vector.


% CHECK USER INPUT ********************************************************

validateattributes(scanD, {'numeric'}, {'ndims', 2}, '', 'depth scan');
validateattributes(X, {'numeric'}, {'ndims', 2}, '', 'x coordinates');
validateattributes(Y, {'numeric'}, {'ndims', 2}, '', 'y coordinates');
% Add Roi as optional input



if isempty(sigma)
    % do not filter scan
    scanSurf = scanD;
    warning(['MWA:' mfilename ':noFiltering'], ...
        'No filter parameter given. Skip form removal.');
else
    validateattributes(sigma, {'numeric'},{'vector','numel', 2}, mfilename, 'sigma');
    scanSurf = filterScan(scanD);
end

% validateattributes(sigmaY, {'numeric'}, {'scalar'}, '', 'sigma y');
% sigmaWorld = [sigmaX, sigmaY];


% MAIN ********************************************************************
% Call function one or mutliple times depending on format of polygons
if iscell(polygons)
    validateattributes(polygons, {'cell'}, {'vector', 'nonempty'}, '', 'polygons');
    
    % Evaluate each polygon
    parameters = cellfun(@evaluateSinglePolygon, polygons);
       % 'UniformOutput', false);
    
    % Convert cell to struct
    %parameters = [parameters{:}];
else
    validateattributes(polygons, {'numeric'}, {'ndims', 2}, '', 'polygons');
    
    % Evaluate single polygon
    parameters = evaluateSinglePolygon(polygons);
end        
 

% CALLBACKS ***************************************************************

    function Zfilt = filterScan(Z)
        
        % Interpolate NaNs and remove plane
        [Zplane, ~] = RemoveForm(X, Y, Inpaint_nans(Z, 1), 'plane');
        
        % Filter out the form 
        [Zsurf, ~] = RemoveForm(X, Y, Zplane, 'gauss', sigma);
        
        % Filter out outliers, filter size: 20, threshold: 1um
        Zfilt = RemoveOutlier(Zsurf, 20, 1); 
        
        % Set invalid areas back to NaN
        Zfilt( isnan(Z) ) = NaN;

    end


    function res = evaluateSinglePolygon(polyVertices)
        
        if isempty(polyVertices)
            Roi = true(size(scanSurf));
        else
            Roi = Polygon2Roi(scanSurf, polyVertices, X, Y);
        end

        if any(isnan(scanSurf(Roi)))
            warning('MWA:EvaluatePolygons:nansNotAllowed', ...
                'Polygon has NaN inside. Skip this one');
            
            res(1).Sq = NaN;
            res(1).Sa = NaN;
            res(1).Ssk= NaN;
            res(1).Sku= NaN;
            
            return;
        end

        xroi = X(Roi);
        yroi = Y(Roi);
        
        % Substract plane and mean for this polygon
        [zsurf, ~, planeRMS] = RemoveForm(xroi,yroi, scanSurf(Roi), 'plane');
        [zmean, ~] = RemoveMeanVec(xroi, yroi, zsurf);
        
%         % display filtered selection for checkup
%         [~, ~, frameRaw] = FrameArray(Roi, X, Y, scanD);

        % frame 
        frameRoi    = FrameArray(Roi, Roi);
        frameZmean  = FrameArray(frameRoi, zmean);
        
        [row, col] = find(Roi);
        frameX = X(min(row):max(row), min(col):max(col));
        frameY = Y(min(row):max(row), min(col):max(col));

%         frameFiltVec = zeros(size(frameRoi));
%         frameFiltVec(logical(frameRoi)) = zmean;
%         
%         frameFilt = reshape(frameFiltVec, size(frameRoi));
%         
%         DisplayScan(frameZmean, frameX, frameY);
        
        
        [a, b, c, d] = SurfaceParaVec(xroi, yroi, zmean);

        % Determine mean repeatability
        if isempty(qmap)
            repMean = [];
        else
            repMean = mean(qmap(Roi));
        end
        
        res(1).Sq = a;
        res(1).Sa = b;
        res(1).Ssk= c;
        res(1).Sku= d;
        
        % Also add filtered data
        res(1).frameRoi = frameRoi;
        res(1).frameX = frameX;
        res(1).frameY = frameY;
        res(1).frameZmean = frameZmean;
        res(1).planeRMS = planeRMS;
        res(1).sigmaWorld = sigma;
        res(1).area = IntegrateVec(xroi, yroi, ones(size(xroi)));
        res(1).repeatability = repMean;
    end


%     function zsurf = removeFormGauss(ZRaw, Roi)
%         [~, ~, Zframe] = FrameArray(Roi, X, Y, ZRaw);
%         [~, ~, RoiFrame] = FrameArray(Roi, X, Y, Roi);
%         
%         Zsurf = Zframe - imgaussfilt(Zframe, sigmaGauss);
%         
%         zsurf = Zsurf(logical(RoiFrame));
%     end


%     function res = evaluateSinglePolygon2(polyVert)
%  
%         Roi = Polygon2Roi(scanD, polyVert, X, Y);
% 
%         xs = X(Roi);
%         ys = Y(Roi);
%         zs = scanD(Roi);
% 
%         % Skip if selection contains NaNs
%         if any(isnan(zs))
%             warning('MWA:EvaluateScanRegion:nansNotAllowed', ...
%                 'Polygon has NaN inside. Skip this one');
%             
% %             res(1) = NaN;
%             
%             res(1).Sq = NaN;
%             res(1).Sa = NaN;
%             res(1).Ssk= NaN;
%             res(1).Sku= NaN;
%             
%             return;
%         end
% 
%         [zsurf, ~] = RemoveFormVec(xs, ys, zs);
%         [zmean, ~] = RemoveMeanVec(xs, ys, zsurf);
% 
%         [a, b, c, d] = SurfaceParaVec(xs, ys, zmean);
% 
%         % [Xs, Ys, Zs] = Vec2Grid(xs, ys, zmean);
%         % DisplayD(Xs, Ys, Zs);
% 
%         res(1).Sq = a;
%         res(1).Sa = b;
%         res(1).Ssk= c;
%         res(1).Sku= d;
% 
%         % parameters = [a, b, c, d];
%     end

end

