function [ polygonFrameWindowed ] = WindowPolygonFrame( polygonFrame, deltaW )
% Apply 2D windowing to framed polygon region.
% The frame is the smallest possible array that can carry the polygon
% completely. Elements of the frame that do not belong to the polygon are
% padded with zeros. This function therefore looks for nonzero entries of
% the frame and sets the window according to those entreis.
% Uses MATLABs window function to generate the window.
% The window is only applied to a region which is offset from the boundary
% of the polygon in x-y direction. The offset is specified by deltaW.

% 

w = window(@hann, deltaW*2);
wHalf = w(1:deltaW);
wHalf(end+1) = 1;   % account for indexing in the for loop below

% Do windowing in x direction
frameC = mat2cell(polygonFrame, ones(1, size(polygonFrame, 1)));
frameWx = cell2mat(cellfun(@windowSlice, frameC, 'UniformOutput', false));

% Do windowing in y direction
frameWxC = mat2cell(frameWx', ones(1, size(frameWx, 2)));
frameWxy = cell2mat(cellfun(@windowSlice, frameWxC, 'UniformOutput', false));

polygonFrameWindowed = frameWxy';

    function sliceW = windowSlice(slice)
        assert( size(slice, 1) == 1 );  % slice should be row vector
        
        isNotZero = logical(slice);
        
        wpad = double(isNotZero);   % windows go in here
        
        % Search for chains in the slice, ie. segments of ones (Runtime
        % encoding).
        % There should be multiple chains for non-convex polygons. 
        origin = double(isNotZero);
        
        % ensure that chains are not treated with periodic boundary 
        % condition. i.e. a chain may not cross the end of the slice.
        origin(end+1) = 0;
        shift = circshift(origin, 1, 2);
        
        i_start = find( (origin-shift) > 0 );
        i_end = find( (origin-shift) < 0 ) - 1;
        
        % each chain must have a start and an end.
        assert(length(i_start) == length(i_end));
        
        % Compute windows for each chain
        numEl = i_end-i_start + 1;  % length of each chain
                
        for i=1:length(numEl)
            a = i_start(i);
            b = i_end(i);
            
            % treat chains that are shorter than deltaW separately
            if numEl(i)/2 < deltaW
                delta = ceil(numEl(i)/2) -1;
                wpad(a:a+delta) = wHalf(1:1+delta);
                wpad(b-delta:b) = flip(wHalf(1:1+delta));
            else
                % window for beginning of chain
                wpad(a:a+deltaW) = wHalf;   
                
                % window for end of chain
                wpad(b-deltaW:b) = flip(wHalf);   
                
            end
            
        end
        
        sliceW = wpad.*slice;   % apply windows
        
    end
        
end

