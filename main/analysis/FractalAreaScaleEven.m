function [ Rel, area ] = FractalAreaScaleEven( X, Y, Z )
% Relative area for area scale fractal analysis of 3D topographic map.
%
% INPUT:
% X     X Coordinates
% Y     Y Coordinates
% Z     Z values of topographic map.
% OUTPUT:
% Rel   Vector of relative area for scale s.
% s     Vector of scales corresponding to elements of Rel.


% Get resolution
% xres = mean(diff(logX(1, :)));
% yres = mean(diff(Y(:, 1)));

xmin = min(min(X));
xmax = max(max(X));
ymin = min(min(Y));
ymax = max(max(Y));

[numPxlX, numPxlY] = size(Z);
nmin = min(numPxlX, numPxlY);

% numSmax = max([numSy numSx]);
% numSmin = min([numSy numSx]);

xres = mean(diff(X(1,:)));
yres = mean(diff(Y(:,1)));

xrange = xmax-xmin;
yrange = ymax-ymin;

dxmin = xrange / (nmin-1);
dymin = yrange / (nmin-1);


% Define linear function for area
% area = (dxmin*dymin) * t
% therefore
% x(t) = dxmin * sqrt(t)
% y(t) = dymin * sqrt(t)
% with following domain for t:
tmin = 1;
tmax = (xrange * yrange) / (dxmin * dymin);

% Make linear spacing of N values for t
Vnorm = 500;
t = linspace(tmin, tmax, Vnorm+1);

% area = dxmin*dymin .* t;

Rel = zeros(1, nmin);
area = zeros(1, nmin);

ez = [0,0,1];   % unit vector in z

for n=1:nmin   % divide each axis in n parts
    
    fprintf('Iteration %u\n', n);

%     dx = dxmin * sqrt(t(i));
%     dy = dymin * sqrt(t(i));
%     
%     a = dx * dy;
%         
%     % compute n = sqrt(# tiles that fit into image) (integer)
%     nx = floor(xrange / dx);
%     ny = floor(yrange / dy);
%     assert(nx == ny);
%     n = nx;
    
%     % generate grid
%     xg = xmin + (0:dx:(n*dx));
%     yg = ymin + (0:dy:(n*dy));
    
    xg = linspace(xmin, xmax, n+1);
    yg = linspace(ymin, ymax, n+1);
    
    a = (xg(2)-xg(1)) * (yg(2)-yg(1));

%     % avoid numerical problems of n*dx > xmin
%     if xg(end) > xmax
%         xg(end) = [];
%     end
%     % likewise
%     if yg(end) > ymax
%         yg(end) = [];
%     end
    
%     % resulting tile size
%     sx = xg(1);
%     sy = yg(2);

    % make grid
    [Xs, Ys] = meshgrid(xg, yg);

%     % take only points that ly within the selcted polygon
%     xs = Xs(selection);
%     ys = Ys(selection);
%     zs = Zs(selection);
    
    % interpolate
    Zs = interp2(X, Y, Z, Xs, Ys, 'linear');

%     DisplayScan(Zs, Xs, Ys);
    
    % triangulate, delaunay: nearest neighbour, etc.
    % DT = delaunayTriangulation(Xs(:), Ys(:), Zs(:));

    tri = delaunay(Xs(:),Ys(:));

    T = triangulation(tri, Xs(:), Ys(:), Zs(:));

    % trisurf(tri,Xs(:),Ys(:),Zs(:));

    % compute normals for each triangle
    Vnorm = faceNormal(T);


    % compute mean normal vector
    vnorm = mean(Vnorm, 1);
    
    % compute angle
    cosTmean = dot(vnorm, ez)/(norm(vnorm)*norm(ez));
    
    Rel(n) = 1/cosTmean;
    area(n) = a;
end

% cosTmean = dot(Nmean,ez)/(norm(Nmean)*norm(ez));
% 
% Rel = 1/cosTmean;


end

