function [ Zout ] = RemoveOutlier( Z, varargin )
% Removes outliers from scan by median filtering and thresholding.
% First apply the 2d median filter to Z with specified neighbourhood size.
% Substract the filtered image from Z and search for valuea larger than the
% threshold.

global MEDFILT_KERNEL_DEF MEDFILT_THRESH_DEF

if isempty(MEDFILT_KERNEL_DEF) || isempty(MEDFILT_THRESH_DEF)
    error('MWA:RemoveOutlier:globalVariableUndefined', ...
        'Global variables are not set.');
end

% CHECK INPUT *************************************************************

validateattributes(Z, {'numeric'}, {'2d'}, mfilename, 'Z');

kernelsize = MEDFILT_KERNEL_DEF;
threshold = MEDFILT_THRESH_DEF;

if nargin > 1
    kernelsize = varargin{1};
end
if nargin > 2
    threshold = varargin{2};
end
if nargin > 3
    error('MWA:RemoveOutlier:invalidInput', ...
        'Not more that 3 inputs allowed');
end

validateattributes(kernelsize, {'numeric'}, {'scalar'}, mfilename, 'kernelsize');
validateattributes(threshold, {'numeric'}, {'scalar'}, mfilename, 'threshold');

% FILTER ******************************************************************

Zmed = medfilt2(Z, [kernelsize kernelsize]);

RoiOut = abs( Z - Zmed ) > threshold;

Zfilt = Z;

Zfilt(RoiOut) = Zmed(RoiOut);

% Extend the filtered area about 3 pixels
m = ones(6);
RoiNaN = zeros(size(RoiOut));
RoiNaN(RoiOut) = NaN;

RoiOutExt = filter2(m, RoiNaN);
RoiOutExt(isnan(RoiOutExt)) = true;

% Lowpass filter the exptended areas to get smooth transition
H = fspecial('average', kernelsize/4);

Zfilt = roifilt2(H, Zfilt, RoiOutExt);

if isempty(Zfilt)
    Zout = Z;
else
    Zout= Zfilt;
end

% RoiOut = double(RoiOut);

end

