function [ frameA ] = FrameArray( Roi, A)
% Cut frame around region of interest of array A and X, Y.
% The frame is the smallest possible array that can carry the region
% completely.
% Parts inside the frame for A that do not belong to Roi are padded with
% zeros.
% A can be either in vector format or grid format (grid is a 2D array).

validateattributes(Roi, {'logical'}, {'ndims', 2}, '', 'Roi');
validateattributes(A, {'numeric', 'logical'}, {'ndims', 2}, '', 'array A');

[rows, cols] = find(Roi);

frameRoi = Roi(min(rows):max(rows), min(cols):max(cols));

% frameX = X(min(rows):max(rows), min(cols):max(cols));
% frameY = Y(min(rows):max(rows), min(cols):max(cols));

if isvector(A)
    % Here A is a vector, i.e. linear indexing
    if length(A) ~= sum(sum(Roi))
        error('MWA:FrameArray:invalidInput', ...
            'Vector A should have same length as size of region in Roi.');
    end
    
    frameA = zeros(size(frameRoi));
    frameA(frameRoi) = A;
    
else
    % Here, A is a 2d Array, i.e. grid format
    frameA = A(min(rows):max(rows), min(cols):max(cols)) .* frameRoi;
    
    if islogical(A)
        frameA = logical(frameA);
    end
end

end

