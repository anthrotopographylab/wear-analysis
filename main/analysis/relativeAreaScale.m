function [ relA, scale ] = relativeAreaScale( xg, yg, Z, varargin)
%relativeAreaScale Compute Relative Area over scale of 3D topographic map.
% The algorithm resamples the image at at coarser scales and uses
% interpolated z values to compute the the quotient of the nominal surface
% area at that scale and the projected surface area. The function also
% accepts a region of interest (Roi) parsed as polygon coordinates. 
%
% INPUT:
% xg    X Coordinates grid vector
% yx    Y Coordinates grid vector
% Z     Image of Z values of topographic map.
% 
% Optional: relativeAreaScale(..., filter)
%     Switch filtering of triangles on (VERY SLOW) or off.
%     The filter deselects triangles which
%     are not rectangular, therefore are not not lying within the regular
%     sampling grid. Those triangles can appear because of the boundary
%     drawing step if the shrinking factor is not well set. 
%     filter = false: filtering off (default)
%     filter = true:  filtering on 
%
% OUTPUT:
% relA  Vector of relative area for correponding scales.
% scale Vector of scales corresponding to elements of relA.


% INPUT *******************************************************************
filterTriangles = false; % default value

% Check that both grid vectors are equally spaced vector arrays
% TO DO: MODIFY TO ALLOW NON-UNIFORM GRIDS TOO
validateattributes(xg, {'numeric'}, {'vector'}, mfilename, 'xg');
validateattributes(yg, {'numeric'}, {'vector'}, mfilename, 'yg');

tol = 1e-2;     % numeric threshold to evaluate grid spacing
if ~ (all(diff(diff(xg)) < tol) && all(diff(diff(yg))) < tol)
    error('Grid vectors are not equally spaced');
end

if nargin == 3
    % Polygon not parsed -> Roi covers the whole image. 
    
    hasRoi = false;
    
elseif nargin >= 4
    % Polygon parsed - Set it up
    
    validateattributes(varargin{1}, {'numeric'}, {'2d', 'ncols', 2}, mfilename, 'polygon');    
    polygon = varargin{1};
    hasRoi = true;

elseif nargin == 5
    % Switch filtering of triangles on (VERY SLOW).
    
    validateattributes(varargin{2}, {'logical'}, {'scalar'}, mfilename, 'corner flag');
    filterTriangles = varargin{2};
    
else
    error('Invalid # input arguments');
end
    
% SET-UP ******************************************************************
cBoundFrameS = 0;

% Get resolution
% xres = mean(diff(logX(1, :)));
% yres = mean(diff(Y(:, 1)));

% Build a frame around the ROI coverd by the polygon. All following
% processing steps will be applied to this frame only instead of the
% whole image, therefore reducing computation time. 
if hasRoi
    % Get the initial ROI, which are the pixels of the original image lying
    % within the polygon. 
    Roi0 = roipoly(xg, yg, Z, polygon(:,1), polygon(:,2));
    %Roi0 = Polygon2Roi(Z, polygon, X, Y);

    % Build smallest possible rectangular frame that fully covers Roi
    % Find bottom/right and top/left boundaries
%     [~, br] = find(Roi0, 1, 'last');
%     [~, bl] = find(Roi0, 1, 'first');
%     [~, bb] = find(Roi0', 1, 'last');
%     [~, bt] = find(Roi0', 1, 'first');
    
    coll = find(any(Roi0, 1), 1, 'first');
    colr = find(any(Roi0, 1), 1, 'last');
    rowt = find(any(Roi0, 2), 1, 'first');
    rowb = find(any(Roi0, 2), 1, 'last');
    
    
%     Xframe = X(rowt:rowb, coll:colr);
%     Yframe = Y(rowt:rowb, coll:colr);
    
    [Xframe, Yframe] = meshgrid(xg(coll:colr), yg(rowt:rowb));
    Zframe = Z(rowt:rowb, coll:colr);
else
    % No polygon parsed, therefore frame the whole image
    [Xframe, Yframe] = meshgrid(xg, yg);
    Zframe = Z;
end

% Interpolate NaNs outside and inside of the polygon:
% - NaNs inside the polgon are interpolated to be included in the 
% evaluation starting from the smallest scale.
% - NaNs outside the polygon need to be interpolated in order to prevent
% problems with sampling points generated that ly in the margin between
% grid points of the initial ROI and the polygon boundary, which is close
% to NaNs outside the polygon. Those sampling points would be extrapolated
% to NaN by interp2 and could therefore not be processed further.
% Use Jon D'Erricos code for that. 2 means fast linear (not sure)
% interpolation.
if any(any(isnan(Zframe)))
    Zframe = Inpaint_nans(Zframe, 2); % Use John D'Erricos code
end

xmin = min(min(Xframe));
xmax = max(max(Xframe));
ymin = min(min(Yframe));
ymax = max(max(Yframe));

[numPxlX, numPxlY] = size(Zframe);
nmin = min(numPxlX, numPxlY);

% numSmax = max([numSy numSx]);
% numSmin = min([numSy numSx]);

xres = mean(diff(xg));
yres = mean(diff(yg));

xrange = xmax-xmin;
yrange = ymax-ymin;

areaTot = xrange*yrange;

dxmin = xrange / (nmin-1);
dymin = yrange / (nmin-1);


% Define the scales as a linear function of t, the spacing
% parameter, which allows to set the spacing of the scales in the
% area-scale plot. 
% girdScale(t) = (dxmin*dymin) * t
% therefore
% dx(t) = dxmin * sqrt(t)
% dy(t) = dymin * sqrt(t)
% with following domain for t:
tmin = 1;
tmax = (xrange * yrange) / (dxmin * dymin);

% Generate N intervals for t, logarithmically spaced
N = 200;
t = logspace(log10(tmin), log10(tmax), N+1); % N+1 cuts

% area = dxmin*dymin .* t;

relA = zeros(1, N);
scale = zeros(1, N);

numTri = zeros(1, N);
triAreaVar = zeros(1, N);

% Supress warning
% warning('off', 'MATLAB:triangulation:PtsNotInTriWarnId');


for i=1:N
    
%     fprintf('Iteration %u\n', i);

    % Compute grid tile size (grid scale) from t
    dx = dxmin * sqrt(t(i));
    dy = dymin * sqrt(t(i));
    
    % Store scale at index i, scale = triangle tile size
    scale(i) = dx * dy / 2;    
    
    % compute n = sqrt(# tiles that fit into image) (integer)
    nx = floor(xrange / dx);
    ny = floor(yrange / dy);
    
    % assert(nx == ny);
    
    % Use shift to move grid to the center of the frame
    shiftx = mod(xrange, dx);
    shifty = mod(yrange, dy);
    
    % generate new sampling points as grid vectors
    xgs = xmin + (0:dx:(nx*dx)) + shiftx/2;
    ygs = ymin + (0:dy:(ny*dy)) + shifty/2;

    
%     % Avoid numerical problems of n*dx > xmin
%     if xg(end) > xmax
%         xg(end) = [];
%     end
%     % likewise in y
%     if yg(end) > ymax
%         yg(end) = [];
%     end
    
%     % resulting tile size
%     sx = xg(1);
%     sy = yg(2);
    
    % Convert grid vectors to meshgrid format
    [Xs, Ys] = meshgrid(xgs, ygs);
    
%     % take only points that lie within the selcted polygon
%     xs = Xs(selection);
%     ys = Ys(selection);
%     zs = Zs(selection);
    
    if i>85
%         Zs = interp2(X, Y, Z, Xs, Ys, 'linear');
%         DisplayScan(Zs, Xs, Ys);
%         impoly(gca, polygon);
%         figure;
%         hold on;
%         plot(Xs, Ys, 'o');
%         plot(polygon(:,1), polygon(:,2), 'rx');
%         hold off;
        %keyboard;
    end
    
%     if i == 88
%         keyboard;
%     end

    if hasRoi  
        % Build the ROI for scale(i). It can be either defined by the
        % boundary of the polygon, wich is the larger option (1), or the
        % boundary of the initial ROI, Roi0, which is the smaller
        % option (2).
        
        % 1: Roi are sample points lying inside of the polygon
        RoiS = roipoly(xgs, ygs, Xs, polygon(:,1), polygon(:,2));
        
        % 2: Roi are sample points lying inside initial ROI
        
%         [~, br] = find(Xs <= max(Xs(Roi)), 'last');
%         isL = find(Xs >= min(Xs(Roi)));
        
%         % Find top and bottom boundaries
%         isT = Ys <= max(Ys(Roi));
%         isB = Ys >= min(Ys(Roi));
        
        %isFrame = isR & isL & isB & isT;
                
%        % Option: include polygons corners as vertices for triangulation
%        if includePolyVert
%             % Stack polygon vertices onto column vectors of gird
%             % vertices.
%             xVertRoiS = [polygon(:,1); Xs(RoiS)];
%             yVertRoiS = [polygon(:,2); Ys(RoiS)];
% 
% %             % Compute # polygon corners
% %             ncV = size(polygon, 1);
% %             cPoly = [(1:ncV-1)', (2:ncV)'; ncV, 1];
%        else
%             % Do not include polygon vertices
%             xVertRoiS = Xs(RoiS);
%             yVertRoiS = Ys(RoiS);
%        end
       
        xVertRoiS = Xs(RoiS);
        yVertRoiS = Ys(RoiS);
       
        % Draw boundary around vertices. Use shrink factor to trade of
        % between a convex hull (0) and a boundary shrinking towards the
        % interior of the grid (1). Shrinking into the grid might occur due to
        % numerical reasons, mainly, i guess. 
        % Boundaries which do not follow non-convex polygon shapes can be
        % corrected by filtering out non-rectangular triangles (see below).
        [idxBoundRoiS, ~] = boundary(xVertRoiS, yVertRoiS, 1);

        % Convert linear boundary indices from the RoiS domain to the 
        % resampled frame domain
        idxBoundFrameS = (1:numel(RoiS))';
        idxBoundFrameS = idxBoundFrameS(RoiS);
        idxBoundFrameS = idxBoundFrameS(idxBoundRoiS);
        
        % Convert linear connectivity list into paired connectivity list
        cBoundFrameS = [ ...
            idxBoundFrameS(1:end-1), ...
            circshift(idxBoundFrameS(1:end-1), 1)];
        
    else
        % Roi is the whole resampled image
        RoiS = true(size(Xs));
    end
    
    
    % Check exit conditions depending on current scale:
    % 1. Num pixel inside the polygon is too low
    % (2. All pixels are collinear)
    if sum(sum(RoiS)) < 6 
        
        % Crop output and finish
        relA     = relA(1:i-1);
        scale    = scale(1:i-1);
        break;
    end

    
    % Interpolate to find z values corresponding to vertice locations
    % in xy plane.
%    Zs = interp2(X, Y, Z, Xs, Ys, 'linear');
    
    %Zfull = Inpaint_nans(Z, 1);
    
    % Interplolate using spline method. This way the extrapolation is
    % enbled in case a resamples grid point lies outside the frame due to
    % numerical reasons.
    zVertS = interp2(Xframe, Yframe, Zframe, Xs(:), Ys(:), 'spline');
    
    
%     F = griddedInterpolant(X', Y', Z', 'linear');
%     Zs = F(Xs', Ys');
    
    % Find triangles between vertices in xy plane using Delaunay algorithm.
    % Use all grid points of the frame for the triangulation step. In a 
    % second step mark all triangles lying outside the boundary, fully or
    % partially. 
    if hasRoi
        tri2D = delaunayTriangulation([Xs(:), Ys(:)], cBoundFrameS);
        isIn = isInterior(tri2D);
    else
        tri2D = delaunayTriangulation([Xs(:), Ys(:)]);
        isIn = true(tri2D.size(1), 1);        
    end
    
    if filterTriangles
        % Compute inner angles of each triangle in xy plane (projected area) 
        triAngles = arrayfun( ...
            @(j)polyangles( ...
                tri2D.Points(tri2D.ConnectivityList(j, :), 1), ...
                tri2D.Points(tri2D.ConnectivityList(j, :), 2)), ...
            1:tri2D.size(1), ...
            'uniformOutput', false);

        % Filter out triangles which are not rectangular. 
        isFilterRect = cellfun(@(x)any(eq(x, pi/2)), triAngles)';
    else
        isFilterRect = true(tri2D.size(1), 1);
    end
    
    % Check exit conditions depending on current scale:
    % Num filtered triangle lying fully inside the the polygon is low
    if sum(isIn & isFilterRect) < 2
        
        % Crop output and finish
        relA     = relA(1:i-1);
        scale    = scale(1:i-1);
        break;
    end
    
%     triAreaVar(i) = var(triArea);
    
    % Construct 3D triangulation using xy coordinates from tri of 
    % triangles lying fully inside the polygon.

    warning('off', 'MATLAB:triangulation:PtsNotInTriWarnId');
    
    tri3D = triangulation(tri2D.ConnectivityList(isIn & isFilterRect, :), Xs(:), Ys(:), zVertS);
    
    warning('on', 'MATLAB:triangulation:PtsNotInTriWarnId');

    % compute mean normal vector
%     Nmean = mean(N, 1);
    
    % *** FORMULA FOR AREAL FRACTAL SCALE ***
    % RelA_i = 1/n_i * sum_j( 1/cos(theta_ij) )
    % 
    % n_i       # triangles at scale i
    % theta_ij  inclination angle of triangle j at scale i
    % i         index of scales 
    % j         index of triangles
    % 
    
    % compute normals for each triangle, with norm = 1
    NFace = faceNormal(tri3D);
    
    % compute cosine of inclination angles through scalar product
    cosTheta = dot(NFace, repmat([0, 0, 1], size(NFace, 1), 1), 2); 
    
    % compute sum of the inverse of the cosine and divide by n
    relA(i) = mean(arrayfun(@inv, cosTheta));
    
    % Store number of triangles
    numTri(i) = sum(isIn);
    
%     % compute cos of mean angle
%     cosThetaMean = cos(mean(Theta));
%     
% %     cosTheta = dot(Nmean, ez)/(norm(Nmean)*norm(ez));
%     
%     Rel(i) = 1/cosThetaMean;
        
    if false % relA(i) > relA(1) %i==71 || i==1 %|| Rel(i) > Rel(1)
        relA(i)
        i
        
    figure;
    trisurf(tri3D.ConnectivityList, tri3D.Points(:,1), tri3D.Points(:,2), tri3D.Points(:,3));
    
%     [X0, Y0]=meshgrid(xg, yg);
%     figure; hold on; plot(xVertRoiS, yVertRoiS, 'x'); plot(X0(Roi0), Y0(Roi0), '.');
    
    keyboard
    end
    
end


% warning('on', 'MATLAB:triangulation:PtsNotInTriWarnId');

% cosTmean = dot(Nmean,ez)/(norm(Nmean)*norm(ez));
% 
% Rel = 1/cosTmean;

% figure; plot(numTri);

end

function angles = polyangles(xVert, yVert)
    % Compute angles inside the polygon specified by the nodes xVert, yVert
    
    validateattributes(xVert, {'numeric'}, {'column'});
    validateattributes(yVert, {'numeric'}, {'size', size(xVert)});
    
    Edge = [circshift(xVert, -1) - xVert, circshift(yVert, -1) - yVert];
    EdgeP1 = circshift(Edge, -1);
    
    numVert = size(Edge, 1);
    angles = zeros(numVert, 1);
    
    for j=1:numVert
        % Formula for scalar product of vectors. Note the directionality!
        angles(j) = acos( dot(EdgeP1(j, :), -Edge(j, :)) ...
            /norm(EdgeP1(j, :))/norm(Edge(j, :)) );
    end
    
%     angles = rad2deg(angles);
end
