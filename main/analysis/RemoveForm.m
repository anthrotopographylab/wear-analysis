function [zvalSurf, zvalForm, varargout] = RemoveForm(xval, yval, zval, varargin)
% Filters form and surface data using linear fit or gaussian filter.
% Scan data can be given in grid or vector format.
% The fit option does noallow NaNs in the data.
% Depending on the filter option parsed:
% 'plane': (default) Fit a plane to the scan and remove from it.
% 'gauss': apply gaussin low-pass filter with std deviation sigma, given as
% 2 element vector.

% TO DO: Change comparson of resolution in x and y. 
%        Change attribute of filter parameter from optional to mandatory. 

global SIGMA_50_DEF SIGMA_100_DEF

% CHECK USER INPUT ********************************************************
validateattributes(xval, {'numeric'}, {'2d'}, mfilename, 'x coordinates');
validateattributes(yval, {'numeric'}, {'size', size(xval)}, mfilename, 'y coordinates');
validateattributes(zval, {'numeric'}, {'size', size(xval)}, mfilename, 'z coordinates');

if isvector(xval) && isvector(yval) && isvector(zval)
    isVector = true;
else
    isVector = false;
end
    
if (nargin-3) > 0
    filtertype = varargin{1};
else
    % default option
    filtertype = 'plane';
end

switch filtertype
    case 'plane' 
        if isVector        % format of scan
            x = xval;
            y = yval;
            z = zval;
        else
            x = xval(:);
            y = yval(:);
            z = zval(:);
        end
        
        plane = fit([x,y], z, 'poly11');    % linear fit

        zform = feval(plane, [x,y]);
        
        planeRMS = rms([plane.p10 plane.p01]);
        
        % remove form
        zsurf = z - zform;
        
        if isVector
            zvalSurf = zsurf;
            zvalForm = zform;
        else
            zvalSurf = reshape(zsurf, size(zval));
            zvalForm = reshape(zform, size(zval));
        end
        
        varargout{1} = planeRMS;
        
    case 'gauss'
        
        if isVector
            error('MWA:RemoveFormVec:invalidInput', ...
                'For gaussian filtering data must be in grid format');
        end
        
        % Choose defualt filter parameter if not parsed
        if (nargin-3) > 1 && ~isempty(varargin{2})
            sigmaWorld = varargin{2};
            
            validateattributes(sigmaWorld, {'numeric'}, {'numel', 2}, ...
                mfilename, 'sigmaWorld');
            
        else
            sigmaWorld = [SIGMA_50_DEF, SIGMA_100_DEF];
        end
        
        % Convert filter parameter sigma from world to intrinsic
        % coordinates.
        xres = round(mean(diff(xval(1,:)), 2), 2);
        yres = round(mean(diff(yval(:,1)), 1), 2);

        if xres ~= yres
            warning('MWA:RemoveForm:semanticFail', ...
                ['X and Y coordinates are not equally spaced. ' ...
                'The average relative difference is %6.4f'], (xres-yres)/xres);
        end  
        
        % convert to intrinsic coordinates
        sigmaGauss(1) = sigmaWorld(1) / xres;
        sigmaGauss(2) = sigmaWorld(2) / yres;
        
        zvalForm = imgaussfilt(zval, sigmaGauss);
        zvalSurf = zval - zvalForm;
            
end

end
