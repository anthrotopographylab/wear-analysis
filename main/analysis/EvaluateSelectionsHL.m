function [ Parameters, HP ] = EvaluateSelectionsHL( selections, varargin)
% Evaluates Selections taken on Scans.
% The Scans and corresponding Selections must be parced as imput
% arguments. Scans is a cell array carrying the name, D scan, RGB 
% scan and coordinates. The Selection carries the name of the
% scan it corresponds to, polygons coordinates and labels. The
% output is a cell array of parameters correponding to the selections, 
% Parameters (1) and an array of HeightParameters objects, HP.
% The data is processed following the order of Scans.
%
% USAGE
% [ Parameters, HP ] = EvaluateSelectionsHL( Selections ) 
% SelectionsIn: Array of Selection objects linked to GenericImage objects.
% Parameters:   Cell array of structs of the parameters computed on 
%               SelectionsIn.
% [ Parameters, HP ] = EvaluateSelectionsHL( SelectionsIn, sigma50, sigma100 ) 
% sigma50:      Filter parameter for 50x scans.
% sigma100:     Filter parameter for 100x scans.

global SIGMA_100_DEF SIGMA_50_DEF

% Set fieldname variables
FN_NAME     = 'Scanname';
FN_MAG      = 'Magnification';
FN_IS_3D    = 'is3D';
FN_IS_TXT   = 'isTxt';


% CHECK INPUT *************************************************************
% validateattributes(Scans, {'cell'}, {'ncols', 5}, mfilename, 'Scans');

validateattributes(selections, {'Selection'}, {'vector'}, mfilename, 'SelectionsIn');

%ConsistencyMeta(META);
%ConsistencyScans(Scans);
% do also check for content of the cells

% Optional Input
if isempty(varargin)
    sigma_50 = repmat(SIGMA_50_DEF, 1, 2);       % [20 20];    % [um]
    sigma_100 = repmat(SIGMA_100_DEF, 1, 2);     % [10 10];     % [um]
else
    validateattributes(varargin{1}, {'numeric'}, {'scalar'}, mfilename, 'sigma 50x');
    validateattributes(varargin{2}, {'numeric'}, {'scalar'}, mfilename, 'sigma 100x');
    sigma_50  = repmat(varargin{1},1,2);
    sigma_100 = repmat(varargin{2},1,2);
end
if length(varargin) > 2
    error('MWA:%s:invalidInput', ...
        'Max num. input arguments is 4', mfilename);
end


% MAIN ********************************************************************
% Only take valid scans for processing
myscans = selections.getLinksTo('AliconaImage');
isValid = [myscans.is3D] & [myscans.isvalid];

myscans = myscans(isValid);
myselect = selections(isValid);

% myselect = myscans.getLinksTo('Selection');

% [metaLocal, indexSelect] = QuerryMeta({FN_NAME FN_MAG}, ...
%     [myscans.Name], ...
%     '-eq', FN_IS_3D, true, ...
%     '-eq', FN_IS_TXT, true);
% 
% Selections = selections(indexSelect);
% Scans = Selections.getLinksTo('AliconaImage');

numSelect = length(myselect);

Parameters = cell(numSelect, 3);       
Parameters(:,1) = [myscans.Name]';             % copy names
Parameters(:,3) = {myselect.label}';           % copy labels

sigmaVec = cell(1, numSelect);
sigmaVec(myscans.getProp(FN_MAG) == 50) = {sigma_50};
sigmaVec(myscans.getProp(FN_MAG) == 100) = {sigma_100};

% % Evaluate polygons
% Parameters(:, 2) = cellfun(@EvaluatePolygons, ...
%     {myscans.ImageTopo}, ...
%     {myscans.XGrid}, ...
%     {myscans.YGrid}, ...
%     {myscans.Qualitymap}, ...
%     {myselect.VertWorld}, ...
%     sigmaVec, ...
%     'UniformOutput', false, ...
%     'ErrorHandler', @errorDealer);

% Save parameters additionally in specialized class
HP = HeightParameters.empty(0,numSelect);

for i=1:numSelect
    fprintf('Polygon %u of scan %s\n', myselect(i).ID, myscans(i).Name{:});
    
    Parameters{i, 2} = EvaluatePolygons( ...
        myscans(i).ImageTopo, ...
        myscans(i).XGrid, ...
        myscans(i).YGrid, ...
        myscans(i).Qualitymap, ...
        myselect(i).VertWorld, ...
        sigmaVec{i});
    
    
    para = Parameters{i, 2};  % return struct of parameters

    if ~ismissing(para.Sa)
        % Construct new HeightParameters object
        HP(i) = HeightParameters(myscans(i).Name{:}, myselect(i).ID);
        HP(end).Sa          = para.Sa;
        HP(end).Sq          = para.Sq;
        HP(end).Ssk         = para.Ssk;
        HP(end).Sku         = para.Sku;
        HP(end).area        = para.area;
        HP(end).sigmaWorld  = para.sigmaWorld;
        HP(end).rep         = para.repeatability;
        
        % Link to Selection object
        HP(end).linkTo(myselect(i));
    end
end
    
%     % Error handling for the cellfun call
%     function result = errorDealer(S, varargin)
%         error(S.identifier, ...
%             'Could not evaluate scan %s: %s', ...
%             myselect(S.index).image.Name, S.message);
%         result = [];
%     end

end

