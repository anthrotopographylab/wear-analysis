function [Sq, Sa, Ssk, Sku] = SurfaceParaVec(x,y,z)
% Compute surface parameters of scan data given in vector format.
% The data can be also in non-rectangular grid format, e.g. rois from
% polygons. Use trapezoidal method for integration.
% INPUT:
% x, y, z:  Grid data in column format. Each datum i is specified by the
%           x(i) y(i) z(i) tripple. 
%           The mean should already be substracted from height values z.
% OUTPUT:
% [Sq Sa Ssk Sku]: Surface roughness parameters. For explanation see below.



% CHECK USER INPUT ********************************************************
validateattributes(x, {'numeric'}, {'column'}, '', 'x coordinates');
validateattributes(y, {'numeric'}, {'size', size(x)}, '', 'y coordinates');
validateattributes(z, {'numeric'}, {'size', size(x)}, '', 'z coordinates');


% PROCESS DATA ************************************************************


% Compute projected surface area
area = IntegrateVec(x, y, ones(size(z)));

% Surface Parameters
zq = z.^2;
za = abs(z);
zsk = z.^3;
zku = z.^4;

Sq = sqrt(  IntegrateVec(x, y, zq)   / area );           % RMS [um]
Sa =        IntegrateVec(x, y, za)   / area ;            % Arithmetic mean [um]
Ssk =       IntegrateVec(x, y, zsk)  / area / (Sq^3);    % Skewness [ ]
Sku =       IntegrateVec(x, y, zku)  / area / (Sq^4);    % Kurtosis [ ]

% keyboard


end

