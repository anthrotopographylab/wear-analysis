% Plot force vs duration for dataset Knaben.
% Exclude certain scans from the plot.

% Criteria for data to be included:
% - scan of edge
% - selection shows wear
% - 

clear all; close all;

SetGlobalVariables();

load('../data/Data_Knaben.mat');

% update variable names
mySelection = Selection2;

%% 
% Index 3D measurements (no docu, no RGB, no ...)
isMeas = ([META.is3D] & [META.isTxt] & ~[META.isDocuOri]);

isValid = [META.isValid];

% Index magnifications
is50x = [META.Magnification] == 50;
is100x = [META.Magnification] == 100;

% Index "high claim"
isHighClaim = [META.ForceN] == 110 | [META.NumCycAccu] > 300;

% Index additional samples
% # cycles ~ 150
isExtra = [META.NumCycAccu] > 140 & [META.NumCycAccu] < 160;


% Take out some measurements by hand
deselectMan = { ...
    'S120_use1_pos1_x050', ...
    'S120_use1_pos1_x100', ...
    'S120_use2_pos2_x050', ...
    'S121_use1_pos2_x100', ...
    'S121_use2_pos1_x100', ...
    'S121_use4_pos2_x100', ...
    'S121_use1_pos1_x100', ...
    'S230_use4_pos2_x050', ...
    'S231_use1_pos1_x050', ...
    'S233_use2_pos1_x100', ...
    'S236_use4_pos1_x050', ...
    'S237_use1_pos2_x020', ...
    'S236_use4_pos1_x050', ...
    'S236_use4_pos1_x100', ...
    'S237_use1_pos2_x020', ...
    'S237_use3_pos3_x050', ...
    'S238_use1_pos2_x050', ...
    'S238_use1_pos2_x100', ...
    'S252_use1_pos1', ...
    'S252_use1_pos1_x100', ...
    'S252_use2_pos1_x050', ...
    'S253_use2_pos1_x050', ...
    'S253_use2_pos1_x100', ...
    'S253_use3_pos1_x050', ...
    'S256_use2_pos1_x050', ...
    'S257_use1_pos1_x100', ...
    'S259_use4_pos11_x100', ...
    'S259_use4_pos11_x100_1', ...
    'S260_use1_pos1_x050', ...
    'S260_use1_pos1_x100', ...
    'S260_use2_pos1_x500', ...
    'S256_use5_ori55_reg1_x100'};
    
isDeselected = ismember({META.Scanname}, deselectMan);

% Select edges
isEdgeOriPos = [META.Orientation] >= 10 | [META.PositionNo] > 9;

isEdgeMan = ismember({META.Scanname}, ...
    {'S126_use1_pos2_x100', ...
    'S259_use4_pos1_x100'});
    
isEdge = isEdgeOriPos | isEdgeMan;

% Index raw areas
isRaw = [META.NumCycAccu] == 0;


%% Selection all
isOK50 = isMeas & isValid & ~isDeselected & is50x;
isOK100 = isMeas & isValid & ~isDeselected & is100x;

%% Selection for edges only
isOK50 = isMeas & isValid & ~isDeselected & is50x & (isEdge | isRaw);
isOK100 = isMeas & isValid & ~isDeselected & is100x & (isEdge | isRaw);


%% Selection for high claim
isOK50 = (isMeas & isValid & ~isDeselected & is50x) & (isHighClaim | isRaw);
isOK100 = (isMeas & isValid & ~isDeselected & is100x) & (isHighClaim | isRaw);


%% Selection for high claim and Edge
isOK50 = (isMeas & isValid & ~isDeselected & is50x) & ((isHighClaim & isEdge) | isExtra | isRaw);
isOK100 = (isMeas & isValid & ~isDeselected & is100x) & ((isHighClaim & isEdge) | isExtra | isRaw);


%% Select polygons

[~, a, b] = IntersectMulti(META(isOK100), Scans);
[Sel, ~] = SelectionToolGUI(Scans(b, :), mySelection);


%% Evaluate polygons
[~, a, b] = IntersectMulti(META(isOK100), Scans);
myParaAll = EvaluateSelectionsHL(Scans(b,:), mySelection, 6, 4);


%% Filter polygon size
isParaOK = false;
myPara = myParaAll;

for i=1:size(myParaAll, 1)
    numS = length(myParaAll{i,2});
    parameters = myParaAll{i,2};
    for j=1:numS
        area = parameters(j).area;
   
        if( area < 900 || 1200 < area )
            parameters(j).Sq = [];
            parameters(j).Sa = [];
            parameters(j).Ssk = [];
            parameters(j).Sku = [];
        end
    end
    myPara{i,2} = parameters;
    
end

% myPara = myParaAll(isParaOK, :);


%% Plot Force vs. Duration

% Sort by force then by duration increasingly
idSort = sortrows([[META.ForceN]', [META.NumCycAccu]', (1:length(META))'], [2 1]);
namesSort = META(idSort(:,3));

[~, ~, indexMeta, indexPara] = IntersectMulti(namesSort, META(isOK50), myPara);
paraSort50 = myPara(indexPara, :);

[~, ~, indexMeta, indexPara] = IntersectMulti(namesSort, META(isOK100), myPara);
paraSort100 = myPara(indexPara, :);
m1 = META(isOK100);
m2 = m1(indexMeta);



% PlotForceDurationHL(paraSort50, 'Sa');
 PlotForceDurationHL(paraSort100, 'Sa');

% Plot in multi-plot
PlotFD2HL(paraSort100, 'Sa');



%% Prints to PDF
%ReportData(Scans, Selection, paraSort50);

ReportData(Scans, mySelection, paraSort100);



