classdef AbstractFeature < handle
    %AbstractFeature Abstract class to be derived by image feature objects
    %   Provides an interface for methods like report generation.
    %   Feature can be objects like HeightParameters, FractalAreaScale. 
    
    properties
        featureID
    end
    
    methods (Abstract)
        % Generate section to be added to a mlreportgen.dom report
        section = reportData(obj) 
    end
    
end

