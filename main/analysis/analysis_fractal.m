%analysis_fractal Analyse dataset with fractal algorithm on polygons.
% Analysis includes the following steps:
% - building data objects
% - selecting scans
% - selecting polygons
% - evaluating fractal algorithm on polygons
% - plotting fractal parameters
% - plotting fractal parameters over labels
% - write results into html report

% Set-up the environment 
clear all; close all;

run startMWA;

import mlreportgen.dom.*;

% Load images and selections
load(fullfile(DIR_DATA, 'DS_NewYork.mat'));
load(fullfile(DIR_DATA, 'Selection6.mat'));

% Make variable names consistent
mySelection = [selEdge100 selFlank100 selMisc];

clear selEdge100 selFlank100 selMisc;

% find duplicates
[~, idx] = unique([mySelection.ID]);
mySelection = mySelection(idx);


%% Construct data objects
% TO DO wrap data object construction into separate function so that this
% script becomes a bit cleaner.

runNo = unique(PARA.Logbook{:,'RunNo'});

runs = arrayfun(@ExpRun, runNo(runNo>0), 'UniformOutput', false);
runs = sort([runs{:}]);

scannames = unique(PARA.IndexScans{:,'Scanname'});
isEmpty = cellfun(@isempty, scannames);

scans = cellfun(@AliconaImage, PARA.IndexScans{:,'Scanname'}, 'UniformOutput', false);
scans = sort([scans{:}]);

% Container objects;
meas = Measurement(scans);
samples = SampleHistory(runs, meas);

% Link images and selections
[a, b] = mySelection.isRelated(scans, {'imageID'}, {'Name'});
mySelection(a).linkTo(scans(b));

% Link runs and images
% Cannot use isRelated here because it does not provide dynamic properties
% used by runs. Instead, convert to tables and use innerjoin.
valRuns = [{runs.IterationNo}', {runs.SampleID}'];
valScans = [num2cell(scans.getProp('IterationNo'))', num2cell(scans.getProp('SampleID'))'];

varnames = strcat('v', cellstr(categorical(1:2)));
[~, iR, iS] = innerjoin( ...
    cell2table(valRuns, 'VariableNames', varnames), ...
    cell2table(valScans, 'VariableNames', varnames));

runs(iR).linkTo(scans(iS));


%% Define groups of scans
% Build index arrays for grouping the array of all scans. 

isMeas = [scans.is3D] & ~scans.getProp('isDocuOri') & scans.getProp('isExport');

% Magnifications
is50x   = [scans.getProp('Magnification')] == 50;
is100x  = [scans.getProp('Magnification')] == 100;

numCyclesCum = scans.getLinksTo('ExpRun').getLinksTo('SampleHistory') ...
    .numCyclesAt(scans.getProp('IterationNo'));

% "high claim" = high number of cycles
isHighClaim = scans.getLinksTo('ExpRun').getProp('Force') == 110 | ...
    numCyclesCum > 300;

% "extra" = between 140 and 160 cycles
% # cycles ~ 150
isExtra = numCyclesCum > 140 & numCyclesCum < 160;


% Manual deselection of scans
deselectMan = { ...
    'S120_use1_pos1_x050', ...
    'S120_use1_pos1_x100', ...
    'S120_use2_pos2_x050', ...
    'S121_use1_pos2_x100', ...
    'S121_use2_pos1_x100', ...
    'S121_use4_pos2_x100', ...
    'S121_use1_pos1_x100', ...
    'S230_use4_pos2_x050', ...
    'S231_use1_pos1_x050', ...
    'S233_use2_pos1_x100', ...
    'S236_use4_pos1_x050', ...
    'S237_use1_pos2_x020', ...
    'S236_use4_pos1_x050', ...
    'S236_use4_pos1_x100', ...
    'S237_use1_pos2_x020', ...
    'S237_use3_pos3_x050', ...
    'S238_use1_pos2_x050', ...
    'S238_use1_pos2_x100', ...
    'S252_use1_pos1', ...
    'S252_use1_pos1_x100', ...
    'S252_use2_pos1_x050', ...
    'S253_use2_pos1_x050', ...
    'S253_use2_pos1_x100', ...
    'S253_use3_pos1_x050', ...
    'S256_use2_pos1_x050', ...
    'S257_use1_pos1_x100', ...
    'S259_use4_pos11_x100', ...
    'S259_use4_pos11_x100_1', ...
    'S260_use1_pos1_x050', ...
    'S260_use1_pos1_x100', ...
    'S260_use2_pos1_x500'};
    
isDeselected = ismember([scans.Name], deselectMan);

% "edges" = images taken from the flake edge
isEdgeOriPos = scans.getProp('Orientation') >= 10 | scans.getProp('PositionNo') > 9;
isEdgeMan = ismember([scans.Name], ...
    {'S126_use1_pos2_x100', ...
    'S259_use4_pos1_x100'});
isEdge = isEdgeOriPos | isEdgeMan;

% Index raw areas
isRaw = scans.getProp('IterationNo') == 0;



%% Extensive Selection
isOK50 = isMeas &  ~isDeselected & is50x;
isOK100 = isMeas & ~isDeselected & is100x;

%% Selection for edges only
isOK50 = isMeas & ~isDeselected & is50x & (isEdge | isRaw);
isOK100 = isMeas & ~isDeselected & is100x & (isEdge | isRaw);

%% Selection for not edges only
isOK50 = isMeas &  ~isDeselected & is50x & (~isEdge | isRaw);
isOK100 = isMeas & ~isDeselected & is100x & (~isEdge | isRaw);

%% Selection for high claim
isOK50 = (isMeas & ~isDeselected & is50x) & (isHighClaim | isRaw);
isOK100 = (isMeas & ~isDeselected & is100x) & (isHighClaim | isRaw);


%% Selection for high claim and Edge
isOK50 = (isMeas & ~isDeselected & is50x) & ((isHighClaim & isEdge) | isRaw);
isOK100 = (isMeas & ~isDeselected & is100x) & ((isHighClaim & isEdge) | isRaw);


if false
%% Select new polygons
[Sel] = SelectionToolGUI(scans(isOK100));

end


%% Do Fractal Analysis
myscans = scans(isOK100);
myscans = myscans(:);
% if exist('frac', 'var')
%     frac.breakLinks;
% end

% Make sure Selections are not linked to FractalAreaScale objects more than
% once.
myscans.getLinksTo('Selection').getLinksTo('FractalAreaScale').breakLinks;

[~, frac] = EvaluateFractal(myscans);


%% Plot fractal parameters over force and duration

x = frac.getLinksTo('Selection').getLinksTo('AliconaImage') ...
    .getLinksTo('ExpRun').getLinksTo('SampleHistory') ...
    .numCyclesAt(frac.getLinksTo('Selection').getLinksTo('AliconaImage').getProp('IterationNo'));
y = frac.getLinksTo('Selection').getLinksTo('AliconaImage').getLinksTo('ExpRun').getProp('Force');

fracDim     = [frac.Das];
scaleApex   = [frac.scaleD];
relAreaApex = [frac.relAreaD];

% frfilt = filloutliers(fracDim, NaN, 'median');
% scfilt = filloutliers(scaleApex, NaN, 'median');

% Filter out outliers
isOutlier = isoutlier(fracDim, 'median') | isoutlier(scaleApex, 'median') | ...
    relAreaApex < 1;

fracDim(isOutlier) = missing;
scaleApex(isOutlier) = missing;
relAreaApex(isOutlier) = missing; 

relAreaApex(isoutlier(relAreaApex)) = missing;

hf(1) = plot3dVar(x, y, fracDim, 20);
hf(2) = plot3dVar(x, y, scaleApex, 1);
hf(3) = plot3dVar(x, y, relAreaApex, 40);

hf(1).Children(1).Title.String = 'Fractal Dimension [1/um]';
hf(2).Children(1).Title.String = 'Critical Scale [um]';
hf(3).Children(1).Title.String = 'Critical Relative Area [-]';

ha = [hf.Children];
ht = [ha(end,:).XLabel];    % label only the bottom plot
[ht.String] = deal('#Cycles');


%% Plot Fractal parameters over ROI label

labels = categorical({frac.getLinksTo('Selection').label}, {'high', 'medium', 'low', 'raw'}, 'Ordinal', true)';

isDefined = ~isundefined(labels);

FractalDimension = struct( ...
    'raw', fracDim(labels == 'raw'), ...
    'low', fracDim(labels == 'low'), ...
    'med', fracDim(labels == 'medium'), ...
    'high', fracDim(labels == 'high'));

CriticalScale = struct( ...
    'raw', scaleApex(labels == 'raw'), ...
    'low', scaleApex(labels == 'low'), ...
    'med', scaleApex(labels == 'medium'), ...
    'high', scaleApex(labels == 'high'));

CriticalRelativeArea = struct( ...
    'raw', relAreaApex(labels == 'raw'), ...
    'low', relAreaApex(labels == 'low'), ...
    'med', relAreaApex(labels == 'medium'), ...
    'high', relAreaApex(labels == 'high'));

hfLabel = PlotStruct(FractalDimension, CriticalScale, CriticalRelativeArea, 'equalyaxis', false);



%% Write fractal results into report
myfracs = frac(:);

report = Document(fullfile(DIR_RESULTS, 'report/fractalAnalysis'), 'html');
report.StreamOutput = true; open(report);

p = Paragraph('Fractal Analysis - Edges Only');
p.Style = {OuterMargin('0.5in','0in','0in','14pt')};
append(report, p);
append(report, ['Creation date: ', date]);

% Table of contents
% Need MATLAB 2016b for TOC!!!
toc = append(report, TOC(3,' '));
toc.Style = {PageBreakBefore(true)};

% reset counter for temp. storing of figures on disk, see addFigures()
global counter
counter = 1;

% Add force duration plots from previous section
report.append(Heading(2, 'Force Duration Plots'));

if exist('hf', 'var') && ~isempty(hf)
    report.append(addFigures(hf(1)));
    report.append(addFigures(hf(2)));
    report.append(addFigures(hf(3)));
else
    report.append(Text('Force duration plots not available'));
end

% Add label plots from previous section
report.append(Heading(2, 'Fractal parameters over labels'));

if exist('hfLabel', 'var') && ~isempty(hfLabel)
    report.append(addFigures(hfLabel));
else
    report.append(Text('Label plot not available'));
end

myscans = unique(myfracs.getLinksTo('Selection').getLinksTo('AliconaImage'));

for i=1:length(myscans)
    report.append(Heading(2, sprintf('Scan: %s', myscans(i).Name{:})));
    
    % Add RGB image with polygons to the report
    fig = SelectScanRegionHL(myscans(i)); set(fig, 'Visible', 'off');
    report.append(Heading(3, sprintf('Alicona Image and regions of interest')));
    report.append(addFigures(fig));
    
    % Add topographic image
    DisplayScan(myscans(i).ImageTopo, myscans(i).XGrid, myscans(i).YGrid);
    set(gcf, 'Visible', 'off');
    report.append(addFigures(gcf));
    
%     % Use custom function to add content of data as mlreportgen.dom.Table
%     report.append(mlReportTable(data(i,:)));

    myfracsScan = myscans(i).getLinksTo('Selection').getLinksTo('FractalAreaScale');
    
    for j=1:length(myfracsScan)
        
    try
        
        sectionFrac = myfracsScan(j).reportData(report);
        sectionFrac.Children(1).OutlineLevel = 4    ;
        report.append(sectionFrac);
        
        report.append(Text(sprintf('Label: %s', myfracsScan(j).getLinksTo('Selection').label)));
        
    catch ME
        report.append(Text(sprintf('Fractal plot failed: %s', ME.message)));
%         keyboard
    end
    end
    
end

% Finish and view
close(report);
rptview(report.OutputPath);

