function varargout = elasticErf( beta, x )
%elasticErf Represents Error function which is stretchable in x and y.
%   Intended be used for fit to the FractalScale data.

if nargin == 0 
    error('Number inputs must be 1 or 2');
end
if nargin >= 1
    validateattributes(beta, ...
        {'numeric'}, {'vector', 'numel', 4}, mfilename, 'beta');
    
    % Extract parameters
    a = beta(1);    % stretch in x
    b = beta(2);    % shift in x
    c = beta(3);    % stretch in y
    d = beta(4);    % shift in y
    
    % Compute max slope and corresponding x value using analytic expression
    % x value
    xs = -b/a;

    % evaluate firstDerivative at x value
    %D = firstDerivative(0);
    
    D = a*c*2/sqrt(pi);
end
if nargin == 2
    validateattributes(x, {'numeric'}, {'vector'}, mfilename, 'x');
    
    % Scale and shift x
    xt = a*x + b;

    % Get function values for all elements in vector x and return as vector y
    y = c*erf(xt) - d;
    
    
end
if nargin > 2 
    error('Number inputs must be 1 or 2');
end

% Hand over output
if nargin == 1
    varargout{1} = D;
    varargout{2} = xs;
else
    % nargin = 2
    varargout{1} = y;
end

    function yt = firstDerivative(xt)
        yt = 0;
    end
end

