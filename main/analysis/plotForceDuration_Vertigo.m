% Plot force vs duration for dataset Knaben.
% Exclude certain scans from the plot.

% Criteria for data to be included:
% - scan of edge
% - selection shows wear
% - ...

clear all; close all;


global META

load('../data/DS_Vertigo2.mat');

meta = QuerryMeta({ ...
    'Scanname', ...
    'Magnification', 'Orientation', 'PositionNo', ...
    'Force', 'NumCycCum', 'IterationNo', ...
    'is3D', 'isExport', 'isDocuOri', 'isValid'});


%% Load selections
load('../data/Selection4.mat');

% update variable names
Scans = BuildImageObjects(META.tables.IndexScans.Scanname);
selections = selAll;

% link scans and selections
selections.linkToImage(Scans);


%% 
% Index 3D measurements (no docu, no RGB, no ...)
isMeas = ([meta.is3D] & [meta.isExport] & ~[meta.isDocuOri]);

isValid = [meta.isValid];

% Index magnifications
is50x   = [meta.Magnification] == 50;
is100x  = [meta.Magnification] == 100;

% Index "high claim"
isHighClaim = [meta.Force] == 110 | [meta.NumCycCum] > 300;

% Index additional samples
% # cycles ~ 150
isExtra = [meta.NumCycCum] > 140 & [meta.NumCycCum] < 160;


% Take out some measurements by hand
deselectMan = { ...
    'S120_use1_pos1_x050', ...
    'S120_use1_pos1_x100', ...
    'S120_use2_pos2_x050', ...
    'S121_use1_pos2_x100', ...
    'S121_use2_pos1_x100', ...
    'S121_use4_pos2_x100', ...
    'S121_use1_pos1_x100', ...
    'S230_use4_pos2_x050', ...
    'S231_use1_pos1_x050', ...
    'S233_use2_pos1_x100', ...
    'S236_use4_pos1_x050', ...
    'S237_use1_pos2_x020', ...
    'S236_use4_pos1_x050', ...
    'S236_use4_pos1_x100', ...
    'S237_use1_pos2_x020', ...
    'S237_use3_pos3_x050', ...
    'S238_use1_pos2_x050', ...
    'S238_use1_pos2_x100', ...
    'S252_use1_pos1', ...
    'S252_use1_pos1_x100', ...
    'S252_use2_pos1_x050', ...
    'S253_use2_pos1_x050', ...
    'S253_use2_pos1_x100', ...
    'S253_use3_pos1_x050', ...
    'S256_use2_pos1_x050', ...
    'S257_use1_pos1_x100', ...
    'S259_use4_pos11_x100', ...
    'S259_use4_pos11_x100_1', ...
    'S260_use1_pos1_x050', ...
    'S260_use1_pos1_x100', ...
    'S260_use2_pos1_x500' ...
    %'S256_use5_ori55_reg1_x100', ...   % outlier
    };  
    
isDeselected = ismember(meta.Scanname, deselectMan);

% Select edges
isEdgeOriPos = [meta.Orientation] >= 10 | [meta.PositionNo] > 9;

isEdgeMan = ismember(meta.Scanname, ...
    {'S126_use1_pos2_x100', ...
    'S259_use4_pos1_x100'});
    
isEdge = isEdgeOriPos | isEdgeMan;

% Index raw areas
isRaw = [meta.IterationNo] == 0;


%% Extensive Selection
isOK50 = isMeas & isValid & ~isDeselected & is50x;
isOK100 = isMeas & isValid & ~isDeselected & is100x;

%% Selection for edges only
isOK50 = isMeas & isValid & ~isDeselected & is50x & (isEdge | isRaw);
isOK100 = isMeas & isValid & ~isDeselected & is100x & (isEdge | isRaw);

%% Selection for not edges only
isOK50 = isMeas & isValid & ~isDeselected & is50x & (~isEdge | isRaw);
isOK100 = isMeas & isValid & ~isDeselected & is100x & (~isEdge | isRaw);


%% Selection for high claim
isOK50 = (isMeas & isValid & ~isDeselected & is50x) & (isHighClaim | isRaw);
isOK100 = (isMeas & isValid & ~isDeselected & is100x) & (isHighClaim | isRaw);


%% Selection for high claim and Edge
isOK50 = (isMeas & isValid & ~isDeselected & is50x) & ((isHighClaim & isEdge) | isExtra | isRaw);
isOK100 = (isMeas & isValid & ~isDeselected & is100x) & ((isHighClaim & isEdge) | isExtra | isRaw);


%% Select polygons
[~, a, b] = IntersectMulti(meta.Scanname(isOK50), {Scans.name}');
[Sel] = SelectionToolGUI(Scans(b));


%% Evaluate polygons
[~, a, b] = IntersectMulti(meta.Scanname(isOK50), {Scans.name}');
selections = [Scans(b).selections];
[~, myParaAll] = EvaluateSelectionsHL(selections, 6, 4);


%% Filter polygon size

polyArea = [myParaAll.area];

isSizeOK100 = polyArea > 900 & polyArea < 1200;
isSizeOK50 = polyArea > 1900 & polyArea < 2100;

myPara100 = myParaAll(isSizeOK100);
myPara50 = myParaAll(isSizeOK50);

mySel100 = selections(isSizeOK100);
mySel50 = selections(isSizeOK50);

% myPara = myParaAll(isParaOK, :);


%% Plot Force vs. Duration

% % Sort by duration then by force increasingly
% idSort = sortrows([meta.Force, meta.NumCycCum, (1:height(meta))'], [2 1]);
% namesSort = meta.Scanname(idSort(:, 3));
% 
% [~, ~, indexMeta, indexPara] = IntersectMulti(namesSort, meta.Scanname(isOK50), {myPara.scanname});
% paraSort50 = myPara(indexPara, :);
% 
% [~, indexMeta, indexPara] = IntersectMulti(meta.Scanname(isOK100), {myPara.scanname});

[isPara, ~] = ismember({myPara100.scanname}, meta.Scanname(isOK100));
paraSort100 = myPara100(isPara);
selSort100 = mySel100(isPara);

[isPara, ~] = ismember({myPara50.scanname}, meta.Scanname(isOK50));
paraSort50 = myPara50(isPara);
selSort50 = mySel50(isPara);

% m1 = meta(isOK100);
% m2 = m1(indexMeta);


% PlotForceDurationHL(paraSort50, 'Sa');
%  PlotForceDurationHL(paraSort100, 'Sa');

% PlotFD2HL(paraSort100, 'Sa');
% PlotFD2HL(paraSort50, 'Sa');

[metaMag100] = QuerryMeta( ...
    {'Scanname', 'Magnification', 'Force', 'NumCycCum'}, ...
    {paraSort100.scanname});

x = [metaMag100.NumCycCum];
y = [metaMag100.Force];

z = [paraSort100.Sa]';

f(3) = plot3dVar(x, y, z, 3);
f(1) = plot3dVarGroup(x, y, z, 3);

[metaMag50] = QuerryMeta( ...
    {'Scanname', 'Magnification', 'Force', 'NumCycCum'}, ...
    {paraSort50.scanname});

x = [metaMag50.NumCycCum];
y = [metaMag50.Force];

z = [paraSort50.Sa]';

f(2) = plot3dVar(x, y, z);

f(1).Children(1).Title.String='Sa on 100x';
f(2).Children(1).Title.String='Sa on 50x';

f(1).Children(4).XLabel.String = 'duration [strokes]';
f(2).Children(4).XLabel.String = 'duration [strokes]';

text(-70, 0.6, 'Force [N]', ...
    'Rotation', 90, ...
    'FontSize', 12, ...
    'Parent', f(1).Children(4));

text(-70, 0.6, 'Force [N]', ...
    'Rotation', 90, ...
    'FontSize', 12, ...
    'Parent', f(2).Children(4));



%% Plot Force Duration Labels

[metaMag] = QuerryMeta( ...
    {'Scanname', 'Magnification', 'Force', 'NumCycCum'}, ...
    {selSort100.imageName});

labels = categorical({selSort100.label}, {'high', 'medium', 'low', 'raw'}, 'Ordinal', true)';

isDefined = ~isundefined(labels);

f(1) = plot3dVar(metaMag{isDefined, 'NumCycCum'}, metaMag{isDefined, 'Force'}, single(labels(isDefined)));


[metaMag] = QuerryMeta( ...
    {'Scanname', 'Magnification', 'Force', 'NumCycCum'}, ...
    {selSort50.imageName});

labels = categorical({selSort50.label}, {'high', 'medium', 'low', 'raw'}, 'Ordinal', true)';

isDefined = ~isundefined(labels);

f(2) = plot3dVar(metaMag{isDefined, 'NumCycCum'}, metaMag{isDefined, 'Force'}, single(labels(isDefined)));



[metaMag] = QuerryMeta( ...
    {'Scanname', 'Magnification', 'Force', 'NumCycCum'}, ...
    {selections.imageName});

labels = categorical({selections.label}, {'high', 'medium', 'low', 'raw'}, 'Ordinal', true)';

isDefined = ~isundefined(labels);

f(3) = plot3dVar(metaMag{isDefined, 'NumCycCum'}, metaMag{isDefined, 'Force'}, single(labels(isDefined)));


f(1).Children(1).Title.String='100x Categories';
f(2).Children(1).Title.String='50x Categories';
f(3).Children(1).Title.String='';

[f(1).Children.YLim] = deal([0.5 4.5]);
[f(1).Children.YTick] = deal(1:4);
[f(1).Children.YTickLabel] = deal({'high', 'med', 'low', 'raw'});

[f(2).Children.YLim] = deal([0.5 4.5]);
[f(2).Children.YTick] = deal(1:4);
[f(2).Children.YTickLabel] = deal({'high', 'med', 'low', 'raw'});

[f(3).Children.YLim] = deal([0.5 4.5]);
[f(3).Children.YTick] = deal(1:4);
[f(3).Children.YTickLabel] = deal({'high', 'med', 'low', 'raw'});

f(1).Children(4).XLabel.String = 'duration [strokes]';
f(2).Children(4).XLabel.String = 'duration [strokes]';
f(3).Children(4).XLabel.String = 'duration [strokes]';

text(-70, 6, 'Force [N]', ...
    'Rotation', 90, ...
    'FontSize', 12, ...
    'Parent', f(1).Children(4));

text(-70, 6, 'Force [N]', ...
    'Rotation', 90, ...
    'FontSize', 12, ...
    'Parent', f(2).Children(4));

text(-70, 6, 'Force [N]', ...
    'Rotation', 90, ...
    'FontSize', 12, ...
    'Parent', f(3).Children(4));



%% Plot labels
fig100 = PlotParametersHL(paraSort100, selAll, 'label');


fig50 = PlotParametersHL(paraSort50, selAll, 'label');


%% Plot raw material

[isPara100, ~] = ismember({paraSort100.scanname}, meta.Scanname(isRaw & is100x));
[isPara50, ~] = ismember({paraSort50.scanname}, meta.Scanname(isRaw & is50x));

Sa_raw = struct( ...
    'x100', [paraSort100(isPara100).Sa], ...
    'x50', [paraSort50(isPara50).Sa]);

PlotStruct(Sa_raw);


%% Prints to PDF
%ReportData(Scans, Selection, paraSort50);

ReportData(Scans, mySelection, paraSort100);



