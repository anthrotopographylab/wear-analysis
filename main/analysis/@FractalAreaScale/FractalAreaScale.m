classdef FractalAreaScale < AbstractLinkable & AbstractFeature
    %FractakAreaScake Feature for relative area scale of topographic image
    
    
    % Inherited from AbstractLinkable
    properties (Constant = true, GetAccess = protected)
        linkableTypes = {'Selection'};
    end
    
    properties (Access = public)
        relArea     = NaN; % Vector of relative area corresponding to scale
        scale       = NaN; % Vector of scales corresponding to relArea
        
        mdl         = NonLinearModel;
        beta        = [];
        
        scanname    = '';
        polygonID   = [];
        polygonNo   = uint16(0);
    end
    
    properties (Dependent = true)
        Das         % Area scale fractal dimension (<=> max slope)
        scaleD      % Scale corresponding to Das
        relAreaD    % Relative area corresponding to Das
    end
    
    properties (Constant = true)
        fun     = @elasticLogLogErf;
        beta0   = [0.4 -1.5 -0.02 -1.02]; % Initialization for regression
    end
    
    methods
        function obj = FractalAreaScale(newScanname)
            % HeightParameters  Default constructor.
            
            if nargin > 0
                validateattributes(newScanname, {'char'}, {'row'}, '', ...
                    'newScanname');
                
                obj.scanname    = {newScanname};            
            end
        end
        
        function obj = set.scale(obj, value)
            if isnumeric(value)
                obj.scale = double(value);
            else
                error('Property value must be numeric');
            end
        end
        
        function obj = set.relArea(obj, value)
            if isnumeric(value)
                obj.relArea = double(value);
            else
                error('Property value must be numeric');
            end
        end
        
        function out = get.Das(obj)
            % Returns fractal dimension, which is ...
            % D = 2 - 2 * slope, 
            % where slope is the apex (steepest decent) of the fittet fun
            % Computed with loglog scale error fun (obj.fun)
            if isempty(obj.beta)
                obj.fitfun;
            end
            [slopeM, scaleM] = obj.fun(obj.beta);
            out = 2 - 2 * slopeM;
        end
        
        function out = get.scaleD(obj)
            % Returns scale at point of max slope
            % Computed with log scale error fun (obj.fun)
            if isempty(obj.beta)
                obj.fitfun;
            end
            [~, out] = obj.fun(obj.beta);
        end
        
        function out = get.relAreaD(obj)
            % Returns the relative area, that corresponds to the critical
            % scale
            out = obj.fun(obj.beta, obj.scaleD);
        end
        
        function plot(obj)
            % Plot area scale vs. relative area with log scale on x axis.
            % Runs on arrays of obj.
            for i=1:length(obj)
                
                myscale = obj(i).scale;
                myrel = obj(i).relArea;
                
                figure;
                loglog(myscale, myrel);
                xlabel('tile size [um²]');
                ylabel('relative area [ ]');
                title(sprintf('Image %s, polygon no. %u', ...
                    obj(i).scanname{:}, obj(i).polygonNo), ...
                    'Interpreter', 'none');
                
                % Overlay fitted function and parameters
                if ~isempty(obj(i).beta)               
                    hold on;
                    loglog(myscale, obj(i).model(myscale));
                    
                    % Conversion to log-scale
                    % z = c*log(x) + k
                    c = (myscale(end)-myscale(1))/(log(myscale(end))-log(myscale(1)));
                    k = myscale(1)-c*log(myscale(1));
                    
                    % Draw max slope and its location
                    line([obj(i).scaleD obj(i).scaleD], ...
                        [min(myrel) max(myrel)], ...
                        'color', 'red');
                    
                    % Draw slope as tangent to apex
                    % y = m*z + b
                    [m, xs] = obj(i).fun(obj(i).beta);
                    
                    ys = obj(i).fun(obj(i).beta, xs); % get y value
                    zs = c*log(xs) + k; % convert to lin scale
                    b = ys - m*zs; % get y axis crossing
                    
                    % Evaluate mapping at first and last z value
                    y1 = m*myscale(1) + b;
                    y2 = m*myscale(end) + b;
                    
                    line([myscale(1), myscale(end)], [y1, y2], ...
                        'color', 'g');
                    
                    axis([min(myscale), max(myscale), min(myrel), max(myrel)]);
                    
                    hold off;
                end
                
            end
        end
        
        function section = reportData(obj, rpt)
            % Generate and return mlreport.dom.DocumenPart object to 
            % add this data to a mlreportgen.dom report.
            
            import mlreportgen.dom.*;
            
            % Create document part to hold section
            section = DocumentPart(rpt.Type);
            
            % Put heading first
            section.append(Heading1(sprintf('Fractal Analysis')));
            
            obj.fitfun;
            obj.plot;
            set(gcf, 'Visible', 'off');
            set(gcf, 'Position', [600 500 800 500]);
            set(gca, 'XLim', [1e-1, 1e+2]);

            % Add relative area scale plot
            section.append(addFigures(gcf));
            
            % Add table with fractal parameters 
            section.append(mlReportTable( ...
                table(obj.Das, obj.scaleD, obj.relAreaD, ...
                    'VariableNames', ...
                    {'FractalDimension', 'CriticalScale', 'CriticalRelativeArea'})));  
        end
        
        function obj = fitfun(obj)
            % Fit function to fractal data and store coefficients in obj.
            % The fractal data should follows the natural 
            % switch-like shape of the scale-relarea plot.
            % The handle to the function to be fitted is stored as a
            % constant in obj.
            % Runs on arrays of objects.
            
            arrayfun(@fitfunSingle, obj);
            
            function fitfunSingle(obj)
                % Run MATLABs nonlinear regression
                obj.mdl = fitnlm(obj.scale, obj.relArea, obj.fun, obj.beta0);

                % Store coefficients
                obj.beta = obj.mdl.Coefficients.Estimate;
            end
        end
        
        function y = model( obj, x )
            if isempty(obj.beta)
                obj.fitfun;
            end
            
            y = obj.fun(obj.beta, x);
        end
        
    end
    
end

