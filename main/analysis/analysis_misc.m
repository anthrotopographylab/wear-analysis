%analysis_mis Analyse dataset on several features.
% Features are HeightParameters, fractal dimension, ...
% Include images matching properties:
% - scan of edge
% - selection shows wear
% - ...
% To Do Consider doing the analysis using container objects like
% Measurement, SampleHistory, etc...

clear all; close all;

run startMWA;

import mlreportgen.dom.*;

% Load images and selections
load(fullfile(DIR_DATA, 'DS_Brussels.mat'));
load(fullfile(DIR_DATA, 'Selection5.mat'));

% Make variable names consistent
mySelection = selAll;

clear selAll;


%% Construct data objects
% para = QuerryMeta({ ...
%     'Scanname', 'RunNo', ...
%     'Magnification', 'Orientation', 'PositionNo', ...
%     'Force', 'NumCycCum', 'IterationNo', ...
%     'Directory_right', ...
%     'is3D', 'isExport', 'isDocuOri', 'isValid'});

% TO DO wrap data object construction into separate function so that this
% script becomes a bit cleaner.

runNo = unique(PARA.Logbook{:,'RunNo'});

runs = arrayfun(@ExpRun, runNo(runNo>0), 'UniformOutput', false);
runs = sort([runs{:}]);

scannames = unique(PARA.IndexScans{:,'Scanname'});
isEmpty = cellfun(@isempty, scannames);

scans = cellfun(@AliconaImage, PARA.IndexScans{:,'Scanname'}, 'UniformOutput', false);
scans = sort([scans{:}]);

% Container objects;
meas = Measurement(scans);
samples = SampleHistory(runs, meas);

% Link images and selections
[a, b] = mySelection.isRelated(scans, {'imageID'}, {'Name'});
mySelection(a).linkTo(scans(b));

% Link runs and images
% Cannot use isRelated here because it does not provide dynamic properties
% used by runs. Instead, convert to tables and use innerjoin.
valRuns = [{runs.IterationNo}', {runs.SampleID}'];
valScans = [num2cell(scans.getProp('IterationNo'))', num2cell(scans.getProp('SampleID'))'];

varnames = strcat('v', cellstr(categorical(1:2)));
[~, iR, iS] = innerjoin( ...
    cell2table(valRuns, 'VariableNames', varnames), ...
    cell2table(valScans, 'VariableNames', varnames));

runs(iR).linkTo(scans(iS));


%% Selection indices
% Index 3D measurements (no docu, no RGB, no ...)
% isMeas = ([para.is3D] & [para.isExport] & ~[para.isDocuOri]);

isMeas = [scans.is3D] & ~scans.getProp('isDocuOri') & scans.getProp('isExport');

% isValid = [para.isValid];
% TO DO implement validity check in AliconaImage

% Index magnifications
is50x   = [scans.getProp('Magnification')] == 50;
is100x  = [scans.getProp('Magnification')] == 100;

% Index "high claim"
numCyclesCum = [scans.getLinksTo('ExpRun').getLinksTo('SampleHistory').numCyclesTot];

isHighClaim = scans.getLinksTo('ExpRun').getProp('Force') == 110 | ...
    numCyclesCum > 300;

% Index additional samples
% # cycles ~ 150
isExtra = numCyclesCum > 140 & numCyclesCum < 160;


% Take out some measurements by hand
deselectMan = { ...
    'S120_use1_pos1_x050', ...
    'S120_use1_pos1_x100', ...
    'S120_use2_pos2_x050', ...
    'S121_use1_pos2_x100', ...
    'S121_use2_pos1_x100', ...
    'S121_use4_pos2_x100', ...
    'S121_use1_pos1_x100', ...
    'S230_use4_pos2_x050', ...
    'S231_use1_pos1_x050', ...
    'S233_use2_pos1_x100', ...
    'S236_use4_pos1_x050', ...
    'S237_use1_pos2_x020', ...
    'S236_use4_pos1_x050', ...
    'S236_use4_pos1_x100', ...
    'S237_use1_pos2_x020', ...
    'S237_use3_pos3_x050', ...
    'S238_use1_pos2_x050', ...
    'S238_use1_pos2_x100', ...
    'S252_use1_pos1', ...
    'S252_use1_pos1_x100', ...
    'S252_use2_pos1_x050', ...
    'S253_use2_pos1_x050', ...
    'S253_use2_pos1_x100', ...
    'S253_use3_pos1_x050', ...
    'S256_use2_pos1_x050', ...
    'S257_use1_pos1_x100', ...
    'S259_use4_pos11_x100', ...
    'S259_use4_pos11_x100_1', ...
    'S260_use1_pos1_x050', ...
    'S260_use1_pos1_x100', ...
    'S260_use2_pos1_x500'};
    
isDeselected = ismember([scans.Name], deselectMan);

% Select edges
isEdgeOriPos = scans.getProp('Orientation') >= 10 | scans.getProp('PositionNo') > 9;

isEdgeMan = ismember([scans.Name], ...
    {'S126_use1_pos2_x100', ...
    'S259_use4_pos1_x100'});
    
isEdge = isEdgeOriPos | isEdgeMan;

% Index raw areas
isRaw = scans.getProp('IterationNo') == 0;



%% Extensive Selection
isOK50 = isMeas &  ~isDeselected & is50x;
isOK100 = isMeas & ~isDeselected & is100x;

%% Selection for edges only
isOK50 = isMeas & ~isDeselected & is50x & (isEdge | isRaw);
isOK100 = isMeas & ~isDeselected & is100x & (isEdge | isRaw);

%% Selection for not edges only
isOK50 = isMeas &  ~isDeselected & is50x & (~isEdge | isRaw);
isOK100 = isMeas & ~isDeselected & is100x & (~isEdge | isRaw);


%% Selection for high claim
isOK50 = (isMeas & ~isDeselected & is50x) & (isHighClaim | isRaw);
isOK100 = (isMeas & ~isDeselected & is100x) & (isHighClaim | isRaw);


%% Selection for high claim and Edge
isOK50 = (isMeas & ~isDeselected & is50x) & ((isHighClaim & isEdge) | isExtra | isRaw);
isOK100 = (isMeas & ~isDeselected & is100x) & ((isHighClaim & isEdge) | isExtra | isRaw);


%% Select new polygons
[Sel] = SelectionToolGUI(scans(isOK100));


%% Do Fractal Analysis
[~, frac] = EvaluateFractal(scans(isOK100));


%% Plot fractal parameters in 3d with variance

x = [frac.getLinksTo('Selection').getLinksTo('AliconaImage').getLinksTo('ExpRun').getLinksTo('SampleHistory').numCyclesTot];
y =  frac.getLinksTo('Selection').getLinksTo('AliconaImage').getLinksTo('ExpRun').getProp('Force');

fracDim     = [frac.Das];
scaleApex   = [frac.scaleD];

% frfilt = filloutliers(fracDim, NaN, 'median');
% scfilt = filloutliers(scaleApex, NaN, 'median');

% Filter out outliers
isOutlier = isoutlier(fracDim, 'median') | isoutlier(scaleApex, 'median');

fracDim(isOutlier) = missing;
scaleApex(isOutlier) = missing;

f(1) = plot3dVar(x, y, fracDim, 1);
f(2) = plot3dVar(x, y, scaleApex, 1);

f(1).Children(1).Title.String = 'Fractal Dimension';
f(2).Children(1).Title.String = 'Critical Scale';


%% Write fractal results into report
report = Document(fullfile(DIR_RESULTS, 'report/fractalAnalysis'), 'html');
report.StreamOutput = true; open(report);

p = Paragraph('Fractal Analysis - Edges');
p.Style = {OuterMargin('0.5in','0in','0in','14pt')};
append(report, p);
append(report, ['Creation date: ', date]);

% Table of contents
% Need MATLAB 2016b for TOC!!!
toc = append(report, TOC(3,' '));
toc.Style = {PageBreakBefore(true)};

% reset counter for temp. storing of figures on disk, see addFigures()
global counter
counter = 1;

myscans = unique(frac.getLinksTo('Selection').getLinksTo('AliconaImage'));

for i=1:length(myscans)
    report.append(Heading(2, sprintf('Scan: %s', myscans(i).Name{:})));
    
    % Add RGB image with polygons to the report
    fig = SelectScanRegionHL(myscans(i)); set(fig, 'Visible', 'off');
    report.append(Heading(3, sprintf('Alicona Image and regions of interest')));
    report.append(addFigures(fig));
    
%     % Use custom function to add content of data as mlreportgen.dom.Table
%     report.append(mlReportTable(data(i,:)));

    myfracs = myscans(i).getLinksTo('Selection').getLinksTo('FractalScale');
    
    for j=1:length(myfracs)
    
    try
        myfracs(j).plot;
        set(gcf, 'Visible', 'off');
        set(gcf, 'Position', [600 500 800 500]);

        report.append(Heading(3, sprintf('Fractal Analysis')));
        report.append(addFigures(gcf));
    catch ME
        report.append(Text(sprintf('Fractal plot failed: %s', ME.message)));
%         keyboard
    end
    end
    
end

% Finish and view
close(report);
rptview(report.OutputPath);



%% Evaluate Height Parameters
[~, myParaAll] = EvaluateSelectionsHL(scans(isOK100).getLinksTo('Selection'), 6, 4);

% Filter polygon size
polyArea = [myParaAll.area];

isSizeOK100 = polyArea > 900 & polyArea < 1200;
isSizeOK50 = polyArea > 1900 & polyArea < 2100;

myPara100 = myParaAll(isSizeOK100);
myPara50 = myParaAll(isSizeOK50);

% mySel100 = selections(isSizeOK100);
% mySel50 = selections(isSizeOK50);


%% Plot height parameters in 3d with variance

% [metaMag100] = QuerryMeta( ...
%     {'Scanname', 'Magnification', 'Force', 'NumCycCum'}, ...
%     {paraSort100.scanname});
% 
% x = [metaMag100.NumCycCum];
% y = [metaMag100.Force];

x = [myPara100.getLinksTo('Selection').getLinksTo('AliconaImage').getLinksTo('ExpRun').getLinksTo('SampleHistory').numCyclesTot];
y =  myPara100.getLinksTo('Selection').getLinksTo('AliconaImage').getLinksTo('ExpRun').getProp('Force');
z = [myPara100.Sa]';

f(3) = plot3dVar(x, y, z, 3);
f(1) = plot3dVarGroup(x, y, z, 3);

% [metaMag50] = QuerryMeta( ...
%     {'Scanname', 'Magnification', 'Force', 'NumCycCum'}, ...
%     {paraSort50.scanname});
%
% x = [metaMag50.NumCycCum];
% y = [metaMag50.Force];
% 
% z = [paraSort50.Sa]';

x = [myPara50.getLinksTo('Selection').getLinksTo('AliconaImage').getLinksTo('ExpRun').getLinksTo('SampleHistory').numCyclesTot];
y =  myPara50.getLinksTo('Selection').getLinksTo('AliconaImage').getLinksTo('ExpRun').getProp('Force');
z = [myPara50.Sa]';

f(2) = plot3dVar(x, y, z);


% Add description
f(1).Children(1).Title.String='Sa on 100x';
f(2).Children(1).Title.String='Sa on 50x';

f(1).Children(4).XLabel.String = 'duration [strokes]';
f(2).Children(4).XLabel.String = 'duration [strokes]';

text(-70, 0.6, 'Force [N]', ...
    'Rotation', 90, ...
    'FontSize', 12, ...
    'Parent', f(1).Children(4));

text(-70, 0.6, 'Force [N]', ...
    'Rotation', 90, ...
    'FontSize', 12, ...
    'Parent', f(2).Children(4));





