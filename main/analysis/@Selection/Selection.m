classdef Selection < AbstractLinkable
    % Represents a ROI selection on an image.
    % ...
    
    % Inherited from AbstractLinkable
    properties (Constant = true, GetAccess = protected)
        linkableTypes = {'AliconaImage', 'HeightParameters', 'FractalAreaScale'};
    end
    
    properties (SetAccess = public)
        VertPixel  = [];
        VertWorld  = [];
        label      = '';
    end
    properties (SetAccess = private)
        image = AliconaImage.empty;
    end
    properties (SetAccess = private)
        imageID = '';
    end
    properties (Dependent = true)
        ID % Unique ID generated from world coordinates
    end
    
    methods
        function obj = Selection(coordinates, varargin)
            % USAGE:
            % Selection(coordinates)
            % Selection(coordinates, image)
            % Selection(coordinates, image, label)
            % coordinates:  Numeric 2 col array of polygon vertices in 
            %               world coordinates. Col 1 are x, col 2 y.
            % image:        AliconaImage that is associated with this
            %               selection. Creates link to image.
            % label:        String as one-word label for the polygon.
            
            if nargin >= 1
                validateattributes(coordinates, ...
                    {'numeric'}, {'2d', 'ncols', 2}, '', 'coordinates');
                obj.VertWorld = coordinates;
            end
            if nargin >= 2
                validateattributes(varargin{1}, ...
                    {'AliconaImage'}, {'scalar'}, '', 'image');
                obj.linkTo(varargin{1});
                obj.imageID = varargin{1}.Name;
%                 varargin{1}.addSelection(obj);    % DEPRECATED
            end
            if nargin == 3
                validateattributes(varargin{2}, ...
                    {'char'}, {'2d'}, '', 'label');
                obj.label = varargin{2};
            end
            if nargin > 3
                error('Invalid number of input arguments');
            end
            if nargin == 0
                % Use default values
            end
        end
        
        function delete(obj)
            % Delete linkage between image and this Selection
            if ~isempty(obj.image)
                isThis = obj.image.selections == obj;
                obj.image.selections = obj.image.selections(~isThis);
            end
        end
        
        function out = get.ID(obj)
            % Generate unique ID from world coordinates through elementwise
            % multiplication and conversion to 32 bit integer.
            % Runs on arrays of objects. 
            out = arrayfun(@idSingle, obj);
            
            function id = idSingle(x)               
                gen = prod(abs(x.VertWorld(1,:)));
                coeff=gen/10^(floor(log10(gen)));
                
                id = int32(coeff*1e+8);
            end
        end
        
%         function addImage(obj, image)
%             validateattributes(image, ...
%                 {'GenericImage'}, {'scalar'}, '', 'image');
%             obj.image = image;
%         end

%         function obj = set.image(obj, image)
%             validateattributes(image, {'GenericImage'}, {'scalar'});
%             
%             % Check size of the image 
%             if      image.maxX < max(obj.VertWorld(:,1)) || ...
%                     image.minX > min(obj.VertWorld(:,1)) || ...
%                     image.maxY < max(obj.VertWorld(:,2)) || ...
%                     image.minY > min(obj.VertWorld(:,2))
%                 
%                 error(['MWA:' mfilename ':dataInconsistency'], ...
%                 'Data inconsistent. Selection is outside of image');
%             end
%             
%             obj.image = image;
%             
%             % TO DO: handle difference between RGB image and 3D image.
%             % TO DO: conversion from world to image coordinates.
%         end
        
        function Roi = getRoi(obj)
            if isempty(obj.image)
                warning('Could not compute Roi. No image associated.');
                Roi = [];
            else
                Roi = Polygon2Roi(obj.image, obj.VertPixel); % TO DO
            end
        end
        
        function linkToImage(obj, AliconaImages)
            validateattributes(AliconaImages, {'AliconaImage'}, {'vector'});
            arrayfun(@linkToImageSingle, obj);
            
            function linkToImageSingle(objSingle)
                [found, indexAI] = ismember(objSingle.imageID, {AliconaImages.Name});
                if found
                    % Deprecated methods:
                   % AliconaImages(indexAI).addSelection(objSingle);
                    
                    % New method:
                    AliconaImages(indexAI).linkTo(objSingle);
                end
            end
        end
        
        function linkAIs(obj, aliconaImages)
            validateattributes(aliconaImages, {'AliconaImage'}, {'vector'});
            arrayfun(@linkSingle, obj);
            
            function linkSingle(objSingle)
                [found, indexAI] = ismember(objSingle.imageName, {aliconaImages.name});
                if found
                    objSingle.linkTo(aliconaImages(indexAI));
                end
            end
        end
        
        function sobj = saveobj(obj)
            % Save everything but link to image.
            sobj.VertWorld  = obj.VertWorld;
            sobj.VertPixel  = obj.VertPixel;
            sobj.label      = obj.label;
            sobj.imageID    = obj.imageID;
        end
            
    end
    
    methods (Static = true)
        function obj = loadobj(data)
            % Construct again
            obj = Selection(data.VertWorld);
                
            % Define order of property initialization for object loading.
            obj.VertPixel   = data.VertPixel;
            obj.label       = data.label;
            
            % Manage backward compatiblity
            if isfield(data, 'imageName')
                obj.imageID     = data.imageName;
            elseif isfield(data, 'imageID')
                obj.imageID     = data.imageID;
            end
        end
    end
    
end
    
    
