%analysis_hieght Analyse dataset on height parameters.
% Analysis includes the following steps:
% - building data objects
% - selecting scans
% - selecting polygons
% - evaluating height parameters on polygons
% - plotting height parameters

% Set-up the environment 
clear all; close all;

run startMWA;

import mlreportgen.dom.*;

% Load images and selections
load(fullfile(DIR_DATA, 'DS_NewYork.mat'));
load(fullfile(DIR_DATA, 'Selection6.mat'));

% Make variable names consistent
mySelection = selMisc50; %[selEdge100 selFlank100 selMisc selMisc50];

%clear selEdge100 selFlank100 selMisc selMisc100;


%% Construct data objects
% TO DO wrap data object construction into separate function so that this
% script becomes a bit cleaner.

runNo = unique(PARA.Logbook{:,'RunNo'});

runs = arrayfun(@ExpRun, runNo(runNo>0), 'UniformOutput', false);
runs = sort([runs{:}]);

scannames = unique(PARA.IndexScans{:,'Scanname'});
isEmpty = cellfun(@isempty, scannames);

scans = cellfun(@AliconaImage, PARA.IndexScans{:,'Scanname'}, 'UniformOutput', false);
scans = sort([scans{:}]);

% Container objects;
meas = Measurement(scans);
samples = SampleHistory(runs, meas);

% Link images and selections
[a, b] = mySelection.isRelated(scans, {'imageID'}, {'Name'});
mySelection(a).linkTo(scans(b));

% Link runs and images
% Cannot use isRelated here because it does not provide dynamic properties
% used by runs. Instead, convert to tables and use innerjoin.
valRuns = [{runs.IterationNo}', {runs.SampleID}'];
valScans = [num2cell(scans.getProp('IterationNo'))', num2cell(scans.getProp('SampleID'))'];

varnames = strcat('v', cellstr(categorical(1:2)));
[~, iR, iS] = innerjoin( ...
    cell2table(valRuns, 'VariableNames', varnames), ...
    cell2table(valScans, 'VariableNames', varnames));

runs(iR).linkTo(scans(iS));


%% Define groups of scans
% Build index arrays for grouping the array of all scans. 

isMeas = [scans.is3D] & ~scans.getProp('isDocuOri') & scans.getProp('isExport');

% Magnifications
is50x   = [scans.getProp('Magnification')] == 50;
is100x  = [scans.getProp('Magnification')] == 100;

numCyclesCum = scans.getLinksTo('ExpRun').getLinksTo('SampleHistory') ...
    .numCyclesAt(scans.getProp('IterationNo'));

% "high claim" = high number of cycles
isHighClaim = scans.getLinksTo('ExpRun').getProp('Force') == 110 | ...
    numCyclesCum > 300;

% "extra" = between 140 and 160 cycles
% # cycles ~ 150
isExtra = numCyclesCum > 140 & numCyclesCum < 160;


% Manual deselection of scans
deselectMan = { ...
    'S120_use1_pos1_x050', ...
    'S120_use1_pos1_x100', ...
    'S120_use2_pos2_x050', ...
    'S121_use1_pos2_x100', ...
    'S121_use2_pos1_x100', ...
    'S121_use4_pos2_x100', ...
    'S121_use1_pos1_x100', ...
    'S230_use4_pos2_x050', ...
    'S231_use1_pos1_x050', ...
    'S233_use2_pos1_x100', ...
    'S236_use4_pos1_x050', ...
    'S237_use1_pos2_x020', ...
    'S236_use4_pos1_x050', ...
    'S236_use4_pos1_x100', ...
    'S237_use1_pos2_x020', ...
    'S237_use3_pos3_x050', ...
    'S238_use1_pos2_x050', ...
    'S238_use1_pos2_x100', ...
    'S252_use1_pos1', ...
    'S252_use1_pos1_x100', ...
    'S252_use2_pos1_x050', ...
    'S253_use2_pos1_x050', ...
    'S253_use2_pos1_x100', ...
    'S253_use3_pos1_x050', ...
    'S256_use2_pos1_x050', ...
    'S257_use1_pos1_x100', ...
    'S259_use4_pos11_x100', ...
    'S259_use4_pos11_x100_1', ...
    'S260_use1_pos1_x050', ...
    'S260_use1_pos1_x100', ...
    'S260_use2_pos1_x500', ...
    'S260_use3_ori45_x50_reg2'}; % scan too large; analysis results in out of memory error
    
isDeselected = ismember([scans.Name], deselectMan);

% "edges" = images taken from the flake edge
isEdgeOriPos = scans.getProp('Orientation') >= 10 | scans.getProp('PositionNo') > 9;
isEdgeMan = ismember([scans.Name], ...
    {'S126_use1_pos2_x100', ...
    'S259_use4_pos1_x100'});
isEdge = isEdgeOriPos | isEdgeMan;

% Index raw areas
isRaw = scans.getProp('IterationNo') == 0;

error('Stop');

%% Extensive Selection
isOK50 = isMeas &  ~isDeselected & is50x;
isOK100 = isMeas & ~isDeselected & is100x;

%% Selection for edges only
isOK50 = isMeas & ~isDeselected & is50x & (isEdge | isRaw);
isOK100 = isMeas & ~isDeselected & is100x & (isEdge | isRaw);

%% Selection for not edges only
isOK50 = isMeas &  ~isDeselected & is50x & (~isEdge | isRaw);
isOK100 = isMeas & ~isDeselected & is100x & (~isEdge | isRaw);

%% Selection for high claim
isOK50 = (isMeas & ~isDeselected & is50x) & (isHighClaim | isRaw);
isOK100 = (isMeas & ~isDeselected & is100x) & (isHighClaim | isRaw);


%% Selection for high claim and Edge
isOK50 = (isMeas & ~isDeselected & is50x) & ((isHighClaim & isEdge) | isRaw);
isOK100 = (isMeas & ~isDeselected & is100x) & ((isHighClaim & isEdge) | isRaw);


if false
%% Select new polygons
[Sel] = SelectionToolGUI(scans(isOK50));


end

%% Evaluate Height Parameters
% Evaluate all selections
% isOK100 = true(1, length(scans));
mySel = scans(isOK50).getLinksTo('Selection');

[~, myParaAll] = EvaluateSelectionsHL(mySel, 6, 4);

% Filter polygon size
polyArea = [myParaAll.area];

isSizeOK100 = polyArea > 900 & polyArea < 1200;
myPara100 = myParaAll(isSizeOK100);


isSizeOK50 = polyArea > 1900 & polyArea < 2100;
myPara50 = myParaAll(isSizeOK50);

% % Link HeightParameter objects and selections
% mySel = scans(isOK100).getLinksTo('Selection');
% [a, b] = myPara100.isRelated(mySel, {'scanname'}, {'imageID'});
% myPara100(a).linkTo(mySel(b));


% mySel100 = selections(isSizeOK100);
% mySel50 = selections(isSizeOK50);



%% Plot height parameters in 3d with variance 100x

x = myPara100.getLinksTo('Selection').getLinksTo('AliconaImage') ...
    .getLinksTo('ExpRun').getLinksTo('SampleHistory') ...
    .numCyclesAt(myPara100.getLinksTo('Selection').getLinksTo('AliconaImage').getProp('IterationNo'));

y =  myPara100.getLinksTo('Selection').getLinksTo('AliconaImage').getLinksTo('ExpRun').getProp('Force');
z = [myPara100.Sa];

f(3) = plot3dVar(x, y, z, 3);
f(1) = plot3dVarGroup(x, y, z, 3);

f(1).Children(1).Title.String='Sa on 100x';
f(1).Children(4).XLabel.String = 'duration [strokes]';

text(-70, 0.6, 'Force [N]', ...
    'Rotation', 90, ...
    'FontSize', 12, ...
    'Parent', f(1).Children(4));


%% Plot height parameters in 3d with variance 50x

x = myPara50.getLinksTo('Selection').getLinksTo('AliconaImage') ...
    .getLinksTo('ExpRun').getLinksTo('SampleHistory') ...
    .numCyclesAt(myPara50.getLinksTo('Selection').getLinksTo('AliconaImage').getProp('IterationNo'));

y =  myPara50.getLinksTo('Selection').getLinksTo('AliconaImage').getLinksTo('ExpRun').getProp('Force');
z = [myPara50.Sa];

f(2) = plot3dVar(x, y, z, 3);

% Add description
f(2).Children(1).Title.String='Sa on 50x';
f(2).Children(4).XLabel.String = 'duration [strokes]';

text(-70, 0.6, 'Force [N]', ...
    'Rotation', 90, ...
    'FontSize', 12, ...
    'Parent', f(2).Children(4));





