function [NewSelection] = SelectionToolGUI( varargin )
% This function takes images as input and depicts all in overview format.
% Images are shown in pseudo color.
% Spatial dimensiona are in pixels
%
% USAGE:
% [ Selection ] = SelectionToolGUI( Scans )
%   Input
% Scans:        Scans to be evaluated.
%   Output
% Selection:    Selection objects drawn on Scans.
% Para:         Cell array of surface parameters corresponding to
%               Selection.

PA_NAME     = 'Scanname';
PA_IS_3D    = 'is3D';
PA_IS_EXP   = 'isExport';

% ***************** CHECK USER INPUT **************************************
% ConsistencyMeta(META);

% Handle input options
if isempty(varargin)
    error('MWA:SelectionToolGUI:invalidInput', ...
        'The number of input arguments is to low');
end
if length(varargin) >= 1
    validateattributes(varargin{1}, {'AliconaImage'}, {'vector'}, mfilename, 'ScansIn');
    
    % parse only valid 3D images
    Scans = varargin{1}([varargin{1}.is3D]);

    % Validate format of Scans
    %ConsistencyScans(Scans);

%     [meta, indexScan] = QuerryMeta({PA_NAME, PA_IS_3D, PA_IS_EXP}, ...
%         {ScansIn.Name}, ...
%         '-eq', PA_IS_3D, true, ...
%         '-eq', PA_IS_EXP, true);
    
%     % Filter out non depth data
%     idxOK = find( ...
%          [META.(META_IS_3D)] & ...
%          [META.(META_IS_TXT)] & ...
%         ~[META.isDocuOri]);
%     [names, indexScan, indexMeta] = IntersectMulti(Scans, META(idxOK));
% 
%     meta = META(idxOK(indexMeta));

%     names = meta{:, PA_NAME};

    names = [Scans.Name];
    
    numTiles = length(Scans);
    
    XCoordinates =  {Scans.XGrid};
    YCoordinates =  {Scans.YGrid};
    scansD       =  {Scans.ImageTopo};
    scansRGB     =  {Scans.ImageRGB};
    
    
    Parameters  = cell(numTiles, 3);
end
% if length(varargin) >= 2
%     % Here an initial selection of polygon vertices is parced additionally.
%     SelectionIn = varargin{2};
%     
%     % format: [names; vertice coordinates; labels]  WRITE CONSISTENCY FUN
%     validateattributes(SelectionIn, {'Selection'}, ...
%         {'vector'}, '', 'selection');
%     
%     % sync order of Selection with arrays of other input data
%     [~, indexMeta, indexSelectIn] = IntersectMulti(names, SelectionIn);
%     Selection(indexMeta, :) = SelectionIn(indexSelectIn, :);
%     
%     % Sync order of Parameters with order of Selection
%     Parameters(:,1) = Selection(:,1);   % copy names
% end
if length(varargin) > 1
    error('MWA:SeletionToolGUI:invalidInput', ...
        'The number of input arguments is to high');
end


% DEFINE GUI LAYOUT PARAMETERS ********************************************

polygonColor = 'red';

% Initial figure and overall layout dimensions
numCols = 4;

figPosXIni      = 900;
figPosYIni      = 300;
figWidthIni     = 1050;
figHeightIni    = 600;
% space between edge of figure and tile:
figMargin       = figWidthIni*0.02; 

tileXspace = 50;    % spacing between tiles
tileYspace = 50;

tileWidth  = 200; %(figWidth-2*figMargin-axXspace)/numAxes;
tileHeight = 300; %(figWidth-2*figMargin-axYspace)/numAxes;

tileXdelta = tileWidth + tileXspace;    
tileYdelta = tileHeight + tileYspace;

axXposRel   = 0.1;    % relative position of image on tile
axYposRel   = 0.2;
axXwidth    = 0.8;
axYheight   = 0.8;

slWidth = 20;       % slider width


% ********************** BUILDING PANELS***********************************
% Create figure
hfig = figure;

set(hfig, 'Position', [figPosXIni figPosYIni figWidthIni figHeightIni]);
set(hfig, 'Name', 'Selection Tool', 'NumberTitle', 'off');
set(hfig, 'ResizeFcn', {@panelFig_ResizeFun});

% Colomap of GUI
map = colormap(hsv(64));  % colorcode and depth
set(hfig, 'Colormap', map);

% Create three panels:
% panelFig separates content from the slider. It has static position. 
% panel1 is the window to panel2. It has relative position to panelFig.
% panel2 carries the images. Its larger in height than panel1 and moves
% behind the window (panel1) whenever the slider is used. 
panelFig = uipanel('Parent', hfig);
panel1 = uipanel('Parent', panelFig);
panel2 = uipanel('Parent', panel1);

% Set panelFig
set(panelFig, 'Units', 'pixels', ...
    'Position', [0 0 figWidthIni-slWidth figHeightIni], ...
    'ResizeFcn', {@panelFig_ResizeFun});

% Set panel1
set(panel1, 'Position', [0 0 1 1]);
set(panel1, 'ResizeFcn', {@panel1_ResizeFun});

% panel2 dimensions taken from axes and layout specifications
numRows = ceil(numTiles / numCols);
panel2Width     = numCols * tileXdelta - tileXspace;
panel2Height    = numRows * tileYdelta;

set(panel2, 'Units', 'pixels', 'Position', ...
    [figMargin figHeightIni-figMargin-panel2Height ...
    panel2Width panel2Height], ...
    'BorderType', 'none');

% Add slider
hsl = uicontrol('Style','Slider', ...
    'Parent', hfig, ...
    'Units', 'pixels', ...
    'Position', [figWidthIni-slWidth 0 slWidth figHeightIni], ...
    'Value', 1, ...
    'Callback', {@slider_callback1});


% BUILDING CONTENT ********************************************************
% Generate a panel (tiles) for each image and insert axes and images
hax = zeros(numTiles, 1);
him = zeros(numTiles, 1);
htx = zeros(numTiles, 1);
hpb = zeros(numTiles, 3);

thumbs = cell(numTiles, 1);

PanelTiles = zeros(1, numTiles);

for itile = 1:numTiles
    
    j = mod(itile-1, numCols);   % column index -1
    i = ceil(itile/numCols);     % row index
   
    PanelTiles(itile) = uipanel('Parent', panel2, ...
         'Units', 'pixels', 'Position', ...
        [1+j*tileXdelta panel2Height-i*tileYdelta tileWidth tileHeight]);
    
    hax(itile) = axes('Parent', PanelTiles(itile), ...
        'Units', 'normalized', ...
        'Position', [axXposRel axYposRel axXwidth axYheight]);

    % take current elements
    X = XCoordinates{itile};
    Y = YCoordinates{itile};
    Zd = scansD{itile};
    Zrgb = scansRGB{itile};
    row2colRatio = size(Zrgb, 1)/size(Zrgb, 2);
    thumbsize = [row2colRatio*tileWidth tileWidth];
    
    polygons = {Scans(itile).getLinksTo('Selection').VertWorld};

    
    thumbs{itile} = imresize(Zrgb, thumbsize); 

    % Draw selection on thumb if any
    if isempty(polygons)
        him(itile) = imshow( ...
            thumbs{itile}, ...
            'Parent', hax(itile));
    else       
        him(itile) = imshow( ...
            imresize( ...
                drawPolygonsRGB( ...
                    Zrgb, polygons, X, Y), ...
                thumbsize), ...
            'Parent', hax(itile)); 
        
%         % Evaluate the polygon
%         [Parameters{itile, 2}, ~] = EvaluateSelectionsHL( Scans(itile).selections );
% %             {names{itile}, X, Y, Zd, Zrgb}, ...
% %             Select(itile, :));
    end
           
    
    % Add name on tile
    htx(itile) = uicontrol(PanelTiles(itile), 'Style', 'text', ...
        'String', names{itile}, ...
        'Units', 'normalized', ...
        'Position', [0.1 0.12 0.8 0.1]);
    
    % Add push buttons on tile
    hpb(itile, 1) = uicontrol(PanelTiles(itile), 'Style', 'Pushbutton', ...
        'String', 'Show', ...
        'Units', 'normalized', ...
        'Position', [0.2 0.02 0.2 0.1]);    
    set(hpb(itile, 1), 'Callback', {@pushbutton_show, itile});
    
    hpb(itile, 2) = uicontrol(PanelTiles(itile), 'Style', 'Pushbutton', ...
        'String', 'New', ...
        'Units', 'normalized', ...
        'Position', [0.4 0.02 0.2 0.1]);
    set(hpb(itile, 2), 'Callback', {@pushbutton_new, itile});
    
    hpb(itile, 3) = uicontrol(PanelTiles(itile), 'Style', 'Pushbutton', ...
        'String', 'Edit', ...
        'Units', 'normalized', ...
        'Position', [0.6 0.02 0.2 0.1]);
    set(hpb(itile, 3), 'Callback', {@pushbutton_edit, itile});
end

set(panel1, 'BorderType', 'none');

% GUI IS SETUP

% wait until the figure is closed by user
waitfor(hfig);  

% Make-up output
% % Take out scans that have no corresponding selection
% isSelection = ~cellfun(@isempty, Parameters);
% NewParameters = Parameters(isSelection, :);

NewSelection = Scans.getLinksTo('Selection');


% DONE


% ********************* Define Callbacks **********************************
    % This one gets evoked whenever the slider is moved.
    function slider_callback1(~, ~)
        tmp = 1e-2;
        set(panel2, 'Visible', 'off');  % avoid error
        
        % get slider position
        val = get(hsl, 'Value');
        val = -(val-1); % invert direction
        
        % read figure/panel1 size
        pos = getpixelposition(panel1, true);
        
        % DeltaScroll is the panel2-y pos range (difference of upper and 
        % lower slider pos).
        deltaScroll = panel2Height-(pos(4) - 2*figMargin);
        
        % Set new panel2 position taking into account actual figure/ panel1
        % height (pos(4)) and slider postition (val).
        set(panel2, 'Position', ...
            [figMargin pos(4)-figMargin-panel2Height+val*deltaScroll ...
            panel2Width panel2Height]);
                
        % workaround to keep axes-panel connection while scrolling
        set(hax, 'Position', [axXposRel axYposRel+tmp axXwidth axYheight]);
        tmp = (-1)*tmp; 
        
        set(panel2, 'Visible', 'on');
    end
    
    % This one is called whenever the size of panel1's parent changes.
    % Almost the same as slider_callback1 except for the workaround.
    function panel1_ResizeFun(~, ~)
        set(panel2, 'Visible', 'off');
        val = get(hsl, 'Value');
        val = -(val-1);
        pos = getpixelposition(panel1, true);
        deltaScroll = panel2Height-(pos(4) - 2*figMargin);
        set(panel2, 'Position', ...
            [figMargin pos(4)-figMargin-panel2Height+val*deltaScroll ...
            panel2Width panel2Height]);
        set(panel2, 'Visible', 'on');
    end

    function panelFig_ResizeFun(~, ~)
        set(panelFig, 'Visible', 'off');
        posFig = getpixelposition(hfig, true);
        set(hsl, 'Position', [posFig(3)-slWidth 0 slWidth posFig(4)]);
        set(panelFig, 'Position', [0 0 posFig(3)-slWidth posFig(4)]);
        set(panelFig, 'Visible', 'on');
    end

    function pushbutton_show(~, ~, tileidx)
        % display the tile in large for inspection    
        X_ = XCoordinates{tileidx};
        Y_ = YCoordinates{tileidx};
        
        Scans(tileidx).displayIm;
        
%         selection = [Scans(tileidx).selections];
%         
%         DisplayScan({names{tileidx}, X_, Y_, scansD{tileidx}, ...
%             drawPolygonsRGB(scansRGB{tileidx}, {selection.VertWorld}, X_, Y_)}); 
    end

    function pushbutton_new(~, ~, tileidx)
        % discard current selection and open selection tool which allows to
        % select multiple polygons in a row.
        
        X_ = XCoordinates{tileidx};
        Y_ = YCoordinates{tileidx};
        
        row2colRatio_    = size(X_, 1)/size(X_, 2);
        thumbsize_       = [row2colRatio_*tileWidth tileWidth];
        
        % Call external function for interactive selection
        [selection, ~] = ...
            SelectScanRegionHL( Scans(tileidx) );
%                 {names{tileidx}, ...
%                 X_, ...
%                 Y_, ...
%                 scansD{tileidx}, ...
%                 scansRGB{tileidx}});
        
        % Update image on tile    
        % You have to first draw on the original image and then resize due
        % to the polygon beeing defined in world coordinates. 
        set(him(tileidx), 'CData', ...
            imresize( ...
                drawPolygonsRGB( ...
                    scansRGB{tileidx}, {selection.VertWorld}, X_, Y_), ...
                thumbsize_));
    end

    function pushbutton_edit(~, ~, tileidx)
        % Add more polygons to the current selection
        
%         selectionOld = [Scans(tileidx).selections];
%         
%         if isempty(selectionOld)
%             pushbutton_new([], [], tileidx);
%             return;
%         end
        
        X_ = XCoordinates{tileidx};
        Y_ = YCoordinates{tileidx};
        
        row2colRatio_    = size(X_, 1)/size(X_, 2);
        thumbsize_       = [row2colRatio_*tileWidth tileWidth];
        
        % Call external function for interactive selection
        [selection, ~] = ...
            SelectScanRegionHL( Scans(tileidx) );
%                 {names{tileidx}, ...
%                 X_, ...
%                 Y_, ...
%                 scansD{tileidx}, ...
%                 scansRGB{tileidx}}, ...
%                 selectionOld);

        set(him(tileidx), 'CData', ...
            imresize( ...
                drawPolygonsRGB( ...
                    scansRGB{tileidx}, {selection.VertWorld}, X_, Y_), ...
                thumbsize_));
    end


    function ImageRGBdraw = drawPolygonsRGB(ImageRGB, polygons, X, Y)
       assert( size(ImageRGB, 3) == 3 );
       
       if isempty(polygons)
           ImageRGBdraw = ImageRGB;
           return
       end
       
       validateattributes(polygons, {'cell'}, {'vector'}, '', 'polygons');
       
       ImageRGBdraw = ImageRGB;
       
       for ic=1:length(polygons)
           
           assert( isnumeric(polygons{ic}) );
           
           x = polygons{ic}(:,1);
           y = polygons{ic}(:,2);
           
           % Convert world coordinates to image indices
           minX = @(x) min(abs(X(1,:)-x), [], 2);
           minY = @(y) min(abs(Y(:,1)-y), [], 1);
                      
           [~, col] = arrayfun(minX, x, 'UniformOutput', false);
           [~, row] = arrayfun(minY, y, 'UniformOutput', false);
           
           % interweave x and y in a row vector (i.e. [x1 y1 x2 y2 ...])
           polyFormat = [[col{:}]; [row{:}]];
           polyFormat = polyFormat(:)';
           
           ImageRGBdraw = insertShape(ImageRGBdraw, ...
            'Polygon', polyFormat, ...
            'Color', polygonColor, ...
            'LineWidth', 2);
       end
        
    end

end