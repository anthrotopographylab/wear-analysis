classdef HeightParameters < AbstractLinkable
    % Represents several height parameters for surface metrology.
    % Provided parameters are:
    % - Sa      Arithmetic mean height
    % - Sq      Root mean square height
    % - Ssk     Skewness
    % - Sku     Kurtosis
    
    % Inherited from AbstractLinkable
    properties (Constant = true, GetAccess = protected)
        linkableTypes = {'Selection'};
    end
    
    properties (Access = public)
        Sa          = missing;
        Sq          = missing;
        Ssk         = missing;
        Sku         = missing;
        area        = missing;
        sigmaWorld  = missing;
        rep         = missing;
        
        scanname    = {''};
        polygonID   = -99;
    end
    
    methods
        function obj = HeightParameters(newScanname, newPolygonID)
            % HeightParameters  Default constructor.
            % newScanname: cell string, newpolygonID: integer
            
            if nargin > 0
                if ischar(newScanname) && isinteger(newPolygonID)
                    obj.scanname    = {newScanname};
                    obj.polygonID   = newPolygonID;
                else
                    error('scanname must be string and polygonID integer');
                end
            end
        end
        
        function obj = set.Sa(obj, value)
            if isnumeric(value)
                obj.Sa = double(value);
            else
                error('Property value must be numeric');
            end
        end
        
        function obj = set.Sq(obj, value)
            if isnumeric(value)
                obj.Sq = double(value);
            else
                error('Property value must be numeric');
            end
        end
        
        function obj = set.Ssk(obj, value)
            if isnumeric(value)
                obj.Ssk = double(value);
            else
                error('Property value must be numeric');
            end
        end
        
        function obj = set.Sku(obj, value)
            if isnumeric(value)
                obj.Sku = double(value);
            else
                error('Property value must be numeric');
            end
        end
        
        function obj = set.sigmaWorld(obj, value)
            if isnumeric(value)
                obj.sigmaWorld = double(value);
            else
                error('Property value must be numeric');
            end
        end
        
        function obj = set.area(obj, value)
            if isnumeric(value)
                obj.area = double(value);
            else
                error('Property value must be numeric');
            end
        end
        
        function obj = set.rep(obj, value)
            if isnumeric(value)
                obj.rep = double(value);
            else
                error('Property value must be numeric');
            end
        end
        
    end
    
end

