function [ varargout ] = SelectScanRegionHL( Scan )
% Select polygons regions on scan and compute surface parameters.
% Opens a GUI to select regions on a scan. The surface parameters of each 
% selection are evaluateed directly and diplayed on the figure. 
%
% USAGE
% [Selection, Para] = SelectScanRegionHL( Scan ) 
% Selection:    Array of Selection objects of the ROIs which were selected.
% Para:         Corresponding surface parameters in cell array of structs.
% Scans:        Single GenericImage object to be drawn on.
% 
% [figure] = SelectScanRegionHL( Scan ) 
% figure:       Display scan with polygon in a pretty window for reporting.
%               figure is handle to the window.


global SIGMA_100_DEF SIGMA_50_DEF

FN_NAME = 'Scanname';
FN_MAG  = 'Magnification';

% CHECK INPUT *************************************************************
validateattributes(Scan, {'AliconaImage'}, {'scalar'}, mfilename, 'Scan');

scanD   = Scan.ImageTopo;
scanRGB = Scan.ImageRGB;
X       = Scan.XGrid;
Y       = Scan.YGrid;
name    = Scan.Name{:};

validateattributes(scanD,   {'numeric'}, {'2d'}, mfilename, '3d scan');
validateattributes(scanRGB, {'numeric'}, {'3d'}, mfilename, 'RGB scan');
validateattributes(X,       {'numeric'}, {'2d'}, mfilename, 'x coordinates');
validateattributes(Y,       {'numeric'}, {'2d'}, mfilename, 'y coordinates');

polyVert = [];
polyLabel = [];

if ~isempty(Scan.getLinksTo('Selection'))
    polyVert = {Scan.getLinksTo('Selection').VertWorld};
    polyLabel = {Scan.getLinksTo('Selection').label};
    
    % Get number of linked selections
    numSelInit = numel(Scan.getLinksTo('Selection'));
    
    % Delete links to selections. Links will be recreated later.
    %Scan.getLinksTo('Selection').delete;
else
    numSelInit = 0;
end


% % Deal with optional input
% if length(varargin) == 1
%     % This should be a selection. It must be formated as cell array holding
%     % [name polygon label] having type [char cell cell].
%     selectionIn = varargin{1};
%     validateattributes(selectionIn, ...
%         {'cell'}, {'row', 'numel', 3}, mfilename, 'Selection');
%     
%     if ~strcmp(name, selectionIn{1})
%         error('MWA:%s:invalidInput', ...
%             'Selection does not correspond to Scan', mfilename);
%     end
%     
%     polyVert = selectionIn{2};
%     polyLabel = selectionIn{3};
%     
%     validateattributes(polyVert, ...
%         {'cell'}, {'vector'}, '', 'polygon vertices');
%     validateattributes(polyLabel, ...
%         {'cell'}, {'vector'}, '', 'polygon lables');
% end
% if length(varargin) > 1
%     error('MWA:SelectScanRegion:invalidInput', ...
%         'Too many input arguments.');
% end


% Set default filter paraemter
if      Scan.Magnification == 50
    sigmaDefault = [SIGMA_50_DEF SIGMA_50_DEF];
elseif  Scan.Magnification == 100
    sigmaDefault = [SIGMA_100_DEF SIGMA_100_DEF];
else
    sigmaDefault = [20 20];
    warning('MWA:%s:invalidInput', ...
            'Using default Gauss filter value %u.', ...
            mfilename, sigmaDefault(1));
end

% if mod(size(Polygons, 2), 2) == 1 % can you divide num cols by 2?
%     error('Polygons must be specified with two coordinates per node');
% end


% SET-UP GUI ************************************************************** 

tabWidth = 450;

% Display RGB scan
fig = DisplayScan(scanRGB, X, Y);

posFig = fig.Position;
fig.Position = [posFig(1) posFig(2) posFig(3)+tabWidth+100 posFig(4)];

if ~isempty(name)
    fig.Name = ['Select ' name];
else
    fig.Name = 'Select scan';
end

% Get axis for polygon drawing
axIm = findobj(fig, 'Tag', 'axes1');

% Add Table and control to figure
panelCtr = findobj(fig, 'Tag', 'panelControls');

posPC = panelCtr.Position;
panelCtr.Position = [posPC(1) posPC(2) tabWidth+100 posPC(4)];

% Table
panelTab = uipanel('parent', panelCtr, ...
    'Units', 'pixels', ...
    'Position', [50 100 tabWidth 200]);

colnames = {'No', 'Sq', 'Sa', 'Ssk', 'Sku', 'Area', 'Label'};
labelopt = {'raw', 'low', 'medium', 'high'};

table = uitable(panelTab, ...
    'ColumnName', colnames, ...
    'ColumnFormat', {'numeric','numeric','numeric','numeric','numeric','numeric', labelopt}, ...
    'RowName', [], ...
    'ColumnEditable', [false false false false false false true], ...
    'ColumnWidth', {25 60 60 60 60 60 'auto'}, ...
    'CellEditCallback', @table_labelEdit, ...
    'Units', 'normalized', ...
    'Position', [0 0 1 1]);

% Buttons
pbPosBase = [50 50 100 30];
pbPosDelta = 105;
pb_done = uicontrol(panelCtr, 'Style', 'Pushbutton', ...
        'String', 'Done', ...
        'Units', 'pixels', ...
        'Position', pbPosBase+[0 0 0 0]);    
set(pb_done, 'Callback', {@pushbutton_close});

pb_add = uicontrol(panelCtr, 'Style', 'Pushbutton', ...
        'String', 'Add', ...
        'Units', 'pixels', ...
        'Position', pbPosBase+[pbPosDelta 0 0 0]);    
set(pb_add, 'Callback', {@pushbutton_add});

pb_eval = uicontrol(panelCtr, 'Style', 'Pushbutton', ...
        'String', 'Evaluate', ...
        'Units', 'pixels', ...
        'Position', pbPosBase+[2*pbPosDelta 0 0 0]);    
set(pb_eval, 'Callback', {@pushbutton_eval});

% States
% col 1: polygon handles
% col 2: polygon no
% col 3: tag objects
% col 4: label strings
% col 5: polygon coordinates
% col 6: polygon surface parameters
States = cell(0, 6);
parameters = 0;
polyCounter = 0;

% Initialize selection from function input
if ~isempty(polyVert)
    for i=1:length(polyVert)
        polyCounter = polyCounter + 1;
        
        validateattributes(polyVert{i}, {'numeric'}, {'2d'}, '', 'polygon');

        States(end+1, :) = cell(1, 6);  % add empty row
        
        polygon = impoly(axIm, polyVert{i});
        pos = round(getPosition(polygon));
        tag = text(pos(1,1), pos(1,2), ...
            ['A' num2str(polyCounter)], 'Color', 'r', 'Parent', axIm);
        
        States{end, 1} = polygon;
        States{end, 2} = polyCounter;
        States{end, 3} = tag;
        States(end, 4) = polyLabel(i);

        set(States{end, 1}, 'DeleteFcn', {@polygon_delete, polyCounter});

        % Do not evaluate and update the table if in diplay mode
        if nargout == 1
            parameters(i) = 0;
        else
            pushbutton_eval([],[])  
        end
    end
end

if nargout == 2
    waitfor(fig);   % wait until GUI is closed
elseif nargout == 1
    % Set figure format to values suitable for printing
    panelCtr.Position = [0 0 0 posFig(4)];
    
%     axLabel = findobj(fig, 'Tag', 'axesLabel');
%     title(axLabel, ['3d image of ' name], 'Interpreter', 'none');
    
    set(fig, 'Position', [posFig(1) posFig(2) 700 600]);
    %fig.fig_ResizeFun([], []) % initialize new layout
    varargout{1} = fig;
else
%     error('MWA:SelectScanRegion:invalidOutput', ...
%         'Number output arguments must be either 1 or 2');
end


% DEFINE CALLBACKS ********************************************************
    function pushbutton_add(~, ~)
        polyCounter = polyCounter + 1;
        
        States(end+1, :) = cell(1, 6);  % add empty row
        
        newPolygon = impoly(axIm);
        
        % Constrain polygon pos to image size
        fcn = makeConstrainToRectFcn('impoly', ...
            get(axIm,'XLim'), ...
            get(axIm,'YLim'));
        newPolygon.setPositionConstraintFcn(fcn);
        
%         % set callback for position change
%         fcn = makeNewPositionCallback(newPolygon);
%         newPolygon.addNewPositionCallback({@polygon_positionChange});
        
        posRo = round(getPosition(newPolygon));
        tagA = ['A' num2str(polyCounter)];
        
        States{end, 1} = newPolygon;
        States{end, 2} = polyCounter;
        States{end, 3} = text(posRo(1,1), posRo(1,2), tagA, 'Color', 'r');
        States{end, 4} = '';
                
        set(States{end, 1}, 'DeleteFcn', {@polygon_delete, polyCounter});
        
        pushbutton_eval([],[])  % evaluate and update the table
        % MAKE FOR THIS A SEPERATE FUNCITON WHICH EVALUATES THE CURRENT
        % SELECTION ONLY.
        
    end

    function pushbutton_eval(~, ~)

        % Get Polygon coordinates
%         getIntPos = @(h) getPosition(h);
        cPolygons = cellfun(@getPosition, States(:,1), 'UniformOutput', false);

        % Evaluate scan regions
%         evalRegions = @(poly) EvaluateScanRegion(scanD, X, Y, poly);
%         data = cellfun(evalRegions, cPolygons, ...
%              'UniformOutput', false, 'ErrorHandler', @evalRegionERROR);
        
        if isempty(cPolygons)
            data = [];
        else
            data = EvaluatePolygons(scanD, X, Y, [], cPolygons, sigmaDefault);
        end
            
        % Save results
        States(:,5) = cPolygons;
        parameters = data';
            
        % Take out polygons that have NaNs inside
        if ~isempty(parameters)
            while any(isnan([parameters.Sa]))
                invalid = find(isnan([parameters.Sa]));
                States{invalid(1), 1}.delete;
            end
        end

        updateTable();
        
    end

    function pushbutton_close(~,~)
        pushbutton_eval([],[])  % evaluate last selection     
        
%         % Return output in array of Selection objects.
%         % Construct selection objects only for new polygons.
%         if polyCounter > numSelInit
%             StatesNew = States(numSelInit+1:end, :);
% 
%             parametersOut = {name, parameters(numSelInit+1:end), StatesNew(:, 4)'};
%             
%         else
%             StatesNew = States(1:numSelInit, :);
% 
%             parametersOut = {name, parameters(1:numSelInit, :), States(:,4)'};
%             
%             varargout{1} = Selection.empty;
%         end
        
        % Take out selections that were already existing at start of
        % function
        selectionsOld = Scan.getLinksTo('Selection');
        isOld = false(1, size(States, 1));

        for h=1:size(States, 1)
            for j=1:length(selectionsOld)
                if isequal(States{h, 5}, selectionsOld(j).VertWorld)
                    isOld(h) = true;
                    break;
                end
            end
        end

        % Break links to selections of deleted polygons
        isDeleted = true(1, length(selectionsOld));
        for j=1:length(selectionsOld)
            for h=1:size(States, 1)
                if isequal(States{h, 5}, selectionsOld(j).VertWorld)
                    isDeleted(j) = false;
                    break;
                end
            end
        end
        breakLinks(selectionsOld(isDeleted));
        
        
        selectionsOut = cellfun( ...
            @(polygon,label)Selection(polygon, Scan, label), ...
            States(~isOld ,5), States(~isOld, 4), ...
            'UniformOutput', false);
        
        varargout{1} = [selectionsOld selectionsOut{:}];
        varargout{2} = [];
        
        close(fig); % finish GUI
    end

    function polygon_delete(~, ~, index)
        % Delete indicated rows of States. index is either a polygon number
        % or a logical index array of the array States.
        
        if isscalar(index)
            % input is polygone number
            polyNo = find([States{:,2}] == index);
%             States{polyNo, 1}.delete
            States{polyNo, 3}.delete
            States(polyNo, :) = [];
            parameters(polyNo) = [];
        else
            % input is index of States      NOT GOOD!
            validateattributes(index, {'logical'}, ...
                {'size', [1 size(States,1)]});
%             States{index, 1}.delete;
            States{index, 3}.delete;
            States(index, :) = [];
            parameters(index) = [];
        end
            
    end

  
    function table_labelEdit(hObject, callbackdata)
        % Here we take into account that the data shown in table might not
        % be up to date with the current selection. There might be more
        % polygons listet than selected on the image. 
        idx = callbackdata.Indices;
        polyNo = hObject.Data{idx(1)};
        isPoly = [States{:, 2}] == polyNo;
        
        if sum(isPoly) == 0
            % polygon was already deleted ... skip
        elseif sum(isPoly) == 1
            % polygon exists but polygon no and row no must not be the same
            States{isPoly, 4} = callbackdata.EditData;
        else
            error('MWA:SelectScanRegion:polygoneNoInconsistent', ...
                'Polygone numbers must be unique ');
        end
        
        updateTable();
    end

    function updateTable()
        % copy interesting parts of States into the table
        if isempty(States)
            tableData = cell(0,7);
        else
            % Poly No; Surface Para; Labels
            tableData = [States(:,2) {parameters.Sq}' {parameters.Sa}' ...
                {parameters.Ssk}' {parameters.Sku}' {parameters.area}' States(:,4)];
        end
        
        set(table, 'data', tableData);
        
        % Set polygon tags as rownames
        numPoly = size(States, 1);
        rownames = cell(1, numPoly);
        for i=1:numPoly
            rownames{i} = States{i,3}.String;
        end
        
        set(table, 'RowName', rownames);
    end
end

