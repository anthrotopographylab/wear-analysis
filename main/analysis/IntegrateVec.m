function [ integral ] = IntegrateVec( myX, myY, myZ )
% Integrate over 2D function z with sample points x and y using trapz.
% The smple points must be from an recangular grid but must not have an
% rectangular encasing. The x-y-z tripples must be given in vector format.
% Output is the double integral over z using MATLABs trapz.


% CHECK USER INPUT ********************************************************
validateattributes(myX, {'numeric'}, {'column'}, '', 'x coordinates');
validateattributes(myY, {'numeric'}, {'size', size(myX)}, '', 'y coordinates');
validateattributes(myZ, {'numeric'}, {'size', size(myX)}, '', 'z coordinates');


% COMPUTE *****************************************************************
% Make sure that coordinates are monotonically increasing, whereas x takes
% precedence over y. This is import for correct use of the trapz function.
B = sortrows([myX myY myZ], [1 2]);
x = B(:, 1);
y = B(:, 2);
z = B(:, 3);

% Vector of unique x coordinates of all sample points
xu = unique(x);     
% numXu = length(xu);

% Cut out the columns of the grid and store in cell array. Then each cell 
% holds samples with constant x coordinate (xu).
getAllYofX = @(xq) y(ismember(x, xq));
YofXCell = arrayfun(getAllYofX, xu, 'UniformOutput', false);
getAllZofX = @(xq) z(ismember(x, xq));
ZofXCell = arrayfun(getAllZofX, xu, 'UniformOutput', false);

% Check that columns of y and z values have the same size
isSameSize = @(A, B) isequal(size(A), size(B));
assert( all( cellfun(isSameSize, YofXCell, ZofXCell) ) );

% Columns that contain only one sample point must be treated separately.
% They can not be parsed to the trapz function.
isScalar = cellfun(@isscalar, YofXCell);
isVec = ~isScalar;

% Collect z values of scalar columns
xsum = zeros(size(xu));
xsum(isScalar) = [ZofXCell{isScalar}];

% Integral along y axis by evaluating each cell element
xsum(isVec) = cellfun(@trapz, YofXCell(isVec), ZofXCell(isVec));

% Integral along x axis
integral = trapz(xu, xsum);

end

