function Parameters = SurfaceParaHL(Scans, Selections, Meta)
% High level function to compute surface paramters.
% It goes throught the chain of preprocessing steps before the surface
% parameters are computed. Each step is based on lower level functions.
% Input is an array of scans and an array of selections. 
% Output is an array of surface parameters. 

% ( for later: )Elements of scans for which no selection exist are
% evaluated over the whole scan.

% CHECK INPUT *************************************************************

validateattributes(Scans, {'cell'}, {'ncols', 5}, '', 'scans');
validateattributes(Selections, {'cell'}, {'ncols', 3}, '', 'selections');
% do also check for content of the cells

% SEMANTIC CHECK HERE

Parameters = cell(size(Selections, 1), 2);  % collect parameters in here

sigma_100 = 6;     % [um]
sigma_050 = 10;    % [um]

for i=1:size(Selections, 1)

    if isempty(Selections(i, 2))    % should be done in input check above
        continue
    end
    
    polygons = Selections{i, 2};   % all selections of scan i
    
    scanname = Selections{i,1};
    isScan = strcmp(scanname, Scans(:,1));
    
    assert(sum(isScan) == 1);  % there should not be dublicate in the Scans
                               % put this to the beginning
    
    X = double(Scans{isScan, 2});
    Y = double(Scans{isScan, 3});
    Z = double(Scans{isScan, 4});

    % Interpolate non existend sampling points if any.
    % Use function inpaint_nans by John D'Errico (mathworks.com).
%     Zinterp = Inpaint_nans(Z, 1);       % 1=linear

    parameters = struct('Sq', {}, 'Sa', {}, 'Ssk', {}, 'Sku', {});
    mag = [];
    
    % go through the polygons of current scan
    for j = 1:length(polygons)
        
        Roi = Polygon2Roi(Z, polygons{j}, X, Y);
        
        [frameX, frameY, frameZ] = FrameArray( Roi, X, Y, Z);
        
        mag = Meta.Magnification(strcmp(Meta.Scanname, scanname));
        
        % Skip if frame contains NaNs
        if any(any(isnan(frameZ)))
            warning('MWA:EvaluateScanRegion:nansNotAllowed', ...
                'Frame %u of scan %s has NaNs inside. Skip this one', ...
                j, scanname);
            parameters(j).Sq  = NaN;
            parameters(j).Sa  = NaN;
            parameters(j).Ssk = NaN;
            parameters(j).Sku = NaN;
        else
            if mag == 50
                parameters(j) = evaluateFrame(frameX, frameY, frameZ, sigma_050);
            elseif mag == 100
                parameters(j) = evaluateFrame(frameX, frameY, frameZ, sigma_100);
            else
                error('MWA:SurfaceParaHL:dataInconsistent', ...
                    'Cant evaluate scans which are not 50x or 100x.');
            end
        end
        
    end

    Parameters{i, 1} = scanname;
    Parameters{i, 2} = parameters;

end


    function paraScan = evaluatePolygon(scan, polygon)
        
%         assert(sum(isScan) == 1);  % the database should not have duplicates
% 
%         X = double(data{isScan, 2});
%         Y = double(data{isScan, 3});
%         Z = double(data{isScan, 4});
        
    end


    function paraFrame =  evaluateFrame(Xframe, Yframe, Zframe, sigmaGauss)
        
        paraFrame = struct('Sq', {}, 'Sa', {}, 'Ssk', {}, 'Sku', {});
        
        % Remove form by gaussian filtering
        Zsurf = Zframe - imgaussfilt(Zframe, sigmaGauss);

        % Remove mean
        [zmean, ~] = RemoveMeanVec(Xframe(:), Yframe(:), Zsurf(:));
        
        % Compute parameters
        [a, b, c, d] = SurfaceParaVec(Xframe(:), Yframe(:), zmean);
                
        paraFrame(1).Sq  = a;
        paraFrame(1).Sa  = b;
        paraFrame(1).Ssk = c;
        paraFrame(1).Sku = d;
        
    end



end