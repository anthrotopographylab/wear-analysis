function [ varargout ] = ...
    SelectScanRegion( scanD, scanRGB , X, Y, varargin)
% Select polygons regions on scan and compute surface parameters.
% Optionally a second image can be overlayed on top of the scan and axis
% coordinates can be given. 


% CHECK INPUT *************************************************************
validateattributes(scanD, {'numeric'}, {'2d'}, '', 'depth scan');
validateattributes(scanRGB, {'numeric'}, {'3d'}, '', 'RGB scan');
validateattributes(X, {'numeric'}, {'2d'}, '', 'x coordinates');
validateattributes(Y, {'numeric'}, {'2d'}, '', 'y coordinates');

polyVert = [];
polyLabel = [];
name = [];

if length(varargin) == 1
    % This should be a selection. It must be formated as cell array with
    % name as first element and another cell array as second.
    selectionIn = varargin{1};
    validateattributes(selectionIn, ...
        {'cell'}, {'vector'}, '', 'selectionIn');
    
    name = selectionIn{1};
    polyVert = selectionIn{2};
    polyLabel = selectionIn{3};
    
    if ischar(name)
        if ~isempty(polyVert)
            validateattributes(polyVert, ...
                {'cell'}, {'vector'}, '', 'polygon vertices');
            validateattributes(polyLabel, ...
                {'cell'}, {'vector'}, '', 'polygon lables');
        end
    else
        error('MWA:SelectScanRegion:invalidInput', ...
            ['selectionIn is badly formated. It must be a cell array ' ...
            'with name as first argument and another cell of polygon' ...
            'vertices as second argument']);
    end
end
if length(varargin) > 1
    error('MWA:SelectScanRegion:invalidInput', ...
        'Number of inputs is to high');
end

% if mod(size(Polygons, 2), 2) == 1 % can you divide num cols by 2?
%     error('Polygons must be specified with two coordinates per node');
% end


% SET-UP GUI ************************************************************** 
fig = figure;

figTitle = ['Profile scan ' name];
figPosIni = [400 400 1150 600];
set(fig, 'Name', figTitle, 'NumberTitle', 'off');
set(fig, 'Units', 'pixels');
set(fig, 'Position', figPosIni);
% set(fig, 'DeleteFun', {'

% Image
axIm = axes('Parent', fig, ...
    'Units', 'pixels', ...
    'Position', [80 70 600 500]);

% Resample RGB scan if necessary
if ~isequal(size(scanD), size(scanRGB(:,:,1)))
    scanRGB = imresize(scanRGB, size(scanD));
end

% Display RGB scan 
image([X(1,1) X(end,end)], [Y(1,1) Y(end,end)], scanRGB,...
    'Parent', axIm);

xlabel('x [um]');
ylabel('y [um]');

% Axes for profile plot

% % Table
% panelTab = uipanel('parent', fig, ...
%     'Units', 'pixels', ...
%     'Position', [730 300 400 200]);
% 
% colnames = {'No', 'Sq', 'Sa', 'Ssk', 'Sku', 'Label'};
% 
% table = uitable(panelTab, ...
%     'ColumnName', colnames, ...
%     'RowName', [], ...
%     'ColumnEditable', [false false false false false true], ...
%     'ColumnWidth', {25 60 60 60 60 'auto'}, ...
%     'CellEditCallback', @table_labelEdit, ...
%     'Units', 'normalized', ...
%     'Position', [0 0 1 1]);

% Buttons
pbPosBase = [750 100 100 30];
pbPosDelta = 105;
pb_done = uicontrol(fig, 'Style', 'Pushbutton', ...
        'String', 'Done', ...
        'Units', 'pixels', ...
        'Position', pbPosBase+[0 0 0 0]);    
set(pb_done, 'Callback', {@pushbutton_close});

pb_add = uicontrol(fig, 'Style', 'Pushbutton', ...
        'String', 'Add', ...
        'Units', 'pixels', ...
        'Position', pbPosBase+[pbPosDelta 0 0 0]);    
set(pb_add, 'Callback', {@pushbutton_add});

pb_eval = uicontrol(fig, 'Style', 'Pushbutton', ...
        'String', 'Evaluate', ...
        'Units', 'pixels', ...
        'Position', pbPosBase+[2*pbPosDelta 0 0 0]);    
set(pb_eval, 'Callback', {@pushbutton_eval});

% States
% col 1: polygon handles
% col 2: polygon no
% col 3: tag objects
% col 4: label strings
% col 5: polygon coordinates
% col 6: polygon surface parameters
States = cell(0, 5);
parameters = [];
polyCounter = 0;

% Initialize selection from function input
if ~isempty(polyVert)
    for i=1:length(polyVert)
        polyCounter = polyCounter + 1;
        
        validateattributes(polyVert{i}, {'numeric'}, {'2d'}, '', 'polygon');

        States(end+1, :) = cell(1, 5);  % add empty row
        
        polygon = impoly(axIm, polyVert{i});
        pos = round(getPosition(polygon));
        tag = ['A' num2str(polyCounter)];
        
        States{end, 1} = polygon;
        States{end, 2} = polyCounter;
        States{end, 3} = text(pos(1,1), pos(1,2), tag, 'Color', 'r');
        States(end, 4) = polyLabel(i);

        set(States{end, 1}, 'DeleteFcn', {@polygon_delete, polyCounter});

        pushbutton_eval([],[])  % evaluate and update the table
    end
end


waitfor(fig);   % wait until GUI is closed

% if nargout == 2
%     waitfor(fig);   % wait until GUI is closed
% elseif nargout == 1
%     % Set figure format to values suitable for printing
%     set(fig, 'Position', figPosIni - [0 0 450 0]);
%     varargout{1} = fig;
% else
% %     error('MWA:SelectScanRegion:invalidOutput', ...
% %         'Number output arguments must be either 1 or 2');
% end


% DEFINE CALLBACKS ********************************************************
    function pushbutton_add(~, ~)
        polyCounter = polyCounter + 1;
        
        States(end+1, :) = cell(1, 5);  % add empty row
        
        newPolygon = impoly(axIm);
        
        % Constrain polygon pos to image size
        fcn = makeConstrainToRectFcn('impoly', get(axIm,'XLim'),...
            get(axIm,'YLim'));
        newPolygon.setPositionConstraintFcn(fcn);
        
%         % set callback for position change
%         fcn = makeNewPositionCallback(newPolygon);
%         newPolygon.addNewPositionCallback({@polygon_positionChange});
        
        posRo = round(getPosition(newPolygon));
        tagA = ['A' num2str(polyCounter)];
        
        States{end, 1} = newPolygon;
        States{end, 2} = polyCounter;
        States{end, 3} = text(posRo(1,1), posRo(1,2), tagA, 'Color', 'r');
                
        set(States{end, 1}, 'DeleteFcn', {@polygon_delete, polyCounter});
        
        pushbutton_eval([],[])  % evaluate and update the table
        % MAKE FOR THIS A SEPERATE FUNCITON WHICH EVALUATES THE CURRENT
        % SELECTION ONLY.
        
    end

    function pushbutton_eval(~, ~)

        % Get Polygon coordinates
%         getIntPos = @(h) getPosition(h);
        cPolygons = cellfun(@getPosition, States(:,1), 'UniformOutput', false);

        % Evaluate scan regions
%         evalRegions = @(poly) EvaluateScanRegion(scanD, X, Y, poly);
%         data = cellfun(evalRegions, cPolygons, ...
%              'UniformOutput', false, 'ErrorHandler', @evalRegionERROR);
        
        if isempty(cPolygons)
            data = [];
        else
            data = EvaluatePolygons(scanD, X, Y, cPolygons);
        end
            
         
        % Save results
        States(:,5) = cPolygons;
        parameters = data';
            
        % Take out polygons that have NaNs inside
        if ~isempty(parameters)
            while any(isnan([parameters.Sa]))
                invalid = find(isnan([parameters.Sa]));
                States{invalid(1), 1}.delete;
            end
        end

        updateTable();
        
    end

    function pushbutton_close(~,~)
        pushbutton_eval([],[])  % evaluate last selection     
        
        % Return output
        selectionOut = {name, States(:,5)', States(:,4)'};
        parametersOut = {name, parameters, States(:,4)'};

        varargout{1} = selectionOut;
        varargout{2} = parametersOut;
        
        close(fig); % finish GUI
    end

    function polygon_delete(~, ~, index)
        % Delete indicated rows of States. index is either a polygon number
        % or a logical index array of the array States.
        
        if isscalar(index)
            % input is polygone number
            polyNo = find([States{:,2}] == index);
%             States{polyNo, 1}.delete
            States{polyNo, 3}.delete
            States(polyNo, :) = [];
            parameters(polyNo) = [];
        else
            % input is index of States      NOT GOOD!
            validateattributes(index, {'logical'}, ...
                {'size', [1 size(States,1)]});
%             States{index, 1}.delete;
            States{index, 3}.delete;
            States(index, :) = [];
            parameters(index) = [];
        end
            
    end

  
    function table_labelEdit(hObject, callbackdata)
        % Here we take into account that the data shown in table might not
        % be up to date with the current selection. There might be more
        % polygons listet than selected on the image. 
        idx = callbackdata.Indices;
        polyNo = hObject.Data{idx(1)};
        isPoly = [States{:, 2}] == polyNo;
        
        if sum(isPoly) == 0
            % polygon was already deleted ... skip
        elseif sum(isPoly) == 1
            % polygon exists but polygon no and row no must not be the same
            States{isPoly, 4} = callbackdata.EditData;
        else
            error('MWA:SelectScanRegion:polygoneNoInconsistent', ...
                'Polygone numbers must be unique ');
        end
        
        updateTable();
    end

    function updateTable()
        % copy interesting parts of States into the table
        if isempty(States)
            tableData = cell(0,6);
        else
            % Poly No; Surface Para; Labels
            tableData = [States(:,2) {parameters.Sq}' {parameters.Sa}' ...
                {parameters.Ssk}' {parameters.Sku}' States(:,4)];
        end
        
        set(table, 'data', tableData);
        
        % Set polygon tags as rownames
        numPoly = size(States, 1);
        rownames = cell(1, numPoly);
        for i=1:numPoly
            rownames{i} = States{i,3}.String;
        end
        
        set(table, 'RowName', rownames);
    end
end

