% Plot force vs duration for dataset Knaben.
% Exclude certain scans from the plot.

% Criteria for data to be included:
% - scan of edge
% - selection shows wear
% - ...

clear all; close all;

SetGlobalVariables();

load('../data/Data_tmp.mat');

% update variable names
mySelection = SelectionAll;

meta = QuerryMeta({ ...
    'Scanname', ...
    'Magnification', 'Orientation', 'PositionNo', ...
    'Force', 'NumCycAccu', ...
    'is3D', 'isExport', 'isDocuOri', 'isValid'});



%% 
% Index 3D measurements (no docu, no RGB, no ...)
isMeas = ([meta.is3D] & [meta.isExport] & ~[meta.isDocuOri]);

isValid = [meta.isValid];

% Index magnifications
is50x   = [meta.Magnification] == 50;
is100x  = [meta.Magnification] == 100;

% Index "high claim"
isHighClaim = [meta.Force] == 110 | [meta.NumCycAccu] > 300;

% Index additional samples
% # cycles ~ 150
isExtra = [meta.NumCycAccu] > 140 & [meta.NumCycAccu] < 160;


% Take out some measurements by hand
deselectMan = { ...
    'S120_use1_pos1_x050', ...
    'S120_use1_pos1_x100', ...
    'S120_use2_pos2_x050', ...
    'S121_use1_pos2_x100', ...
    'S121_use2_pos1_x100', ...
    'S121_use4_pos2_x100', ...
    'S121_use1_pos1_x100', ...
    'S230_use4_pos2_x050', ...
    'S231_use1_pos1_x050', ...
    'S233_use2_pos1_x100', ...
    'S236_use4_pos1_x050', ...
    'S237_use1_pos2_x020', ...
    'S236_use4_pos1_x050', ...
    'S236_use4_pos1_x100', ...
    'S237_use1_pos2_x020', ...
    'S237_use3_pos3_x050', ...
    'S238_use1_pos2_x050', ...
    'S238_use1_pos2_x100', ...
    'S252_use1_pos1', ...
    'S252_use1_pos1_x100', ...
    'S252_use2_pos1_x050', ...
    'S253_use2_pos1_x050', ...
    'S253_use2_pos1_x100', ...
    'S253_use3_pos1_x050', ...
    'S256_use2_pos1_x050', ...
    'S257_use1_pos1_x100', ...
    'S259_use4_pos11_x100', ...
    'S259_use4_pos11_x100_1', ...
    'S260_use1_pos1_x050', ...
    'S260_use1_pos1_x100', ...
    'S260_use2_pos1_x500'};
    
isDeselected = ismember(meta.Scanname, deselectMan);

% Select edges
isEdgeOriPos = [meta.Orientation] >= 10 | [meta.PositionNo] > 9;

isEdgeMan = ismember(meta.Scanname, ...
    {'S126_use1_pos2_x100', ...
    'S259_use4_pos1_x100'});
    
isEdge = isEdgeOriPos | isEdgeMan;

% Index raw areas
isRaw = [meta.NumCycAccu] == 0;


%% Extensive Selection
isOK50 = isMeas & isValid & ~isDeselected & is50x;
isOK100 = isMeas & isValid & ~isDeselected & is100x;

%% Selection for edges only
isOK50 = isMeas & isValid & ~isDeselected & is50x & (isEdge | isRaw);
isOK100 = isMeas & isValid & ~isDeselected & is100x & (isEdge | isRaw);

%% Selection for not edges only
isOK50 = isMeas & isValid & ~isDeselected & is50x & (~isEdge | isRaw);
isOK100 = isMeas & isValid & ~isDeselected & is100x & (~isEdge | isRaw);


%% Selection for high claim
isOK50 = (isMeas & isValid & ~isDeselected & is50x) & (isHighClaim | isRaw);
isOK100 = (isMeas & isValid & ~isDeselected & is100x) & (isHighClaim | isRaw);


%% Selection for high claim and Edge
isOK50 = (isMeas & isValid & ~isDeselected & is50x) & ((isHighClaim & isEdge) | isExtra | isRaw);
isOK100 = (isMeas & isValid & ~isDeselected & is100x) & ((isHighClaim & isEdge) | isExtra | isRaw);


%% Select polygons
[~, a, b] = IntersectMulti(meta.Scanname(isOK100), Scans);
[Sel, ~] = SelectionToolGUI(Scans(b, :), SelectAreaNorm);


%% Evaluate polygons
[~, a, b] = IntersectMulti(meta.Scanname(isOK100), Scans);
[~, myParaAll] = EvaluateSelectionsHL(Scans(b,:), mySelection, 6, 4);


%% Filter polygon size

polyArea = [myParaAll.area];

isSizeOK = polyArea > 900 & polyArea < 1200;

myPara = myParaAll(isSizeOK);

% 
% isParaOK = false;
% myPara = myParaAll;
% 
% for i=1:size(myParaAll, 1)
%     numS = length(myParaAll{i,2});
%     parameters = myParaAll{i,2};
%     for j=1:numS
%         area = parameters(j).area;
%    
%         if( area < 900 || 1200 < area )
%             parameters(j).Sq = [];
%             parameters(j).Sa = [];
%             parameters(j).Ssk = [];
%             parameters(j).Sku = [];
%         end
%     end
%     myPara{i,2} = parameters;
%     
% end

% myPara = myParaAll(isParaOK, :);


%% Plot Force vs. Duration

% Sort by duration then by force increasingly
idSort = sortrows([meta.Force, meta.NumCycAccu, (1:height(meta))'], [2 1]);
namesSort = meta.Scanname(idSort(:, 3));

[~, ~, indexMeta, indexPara] = IntersectMulti(namesSort, meta.Scanname(isOK50), {myPara.scanname});
paraSort50 = myPara(indexPara, :);

[~, ~, indexMeta, indexPara] = IntersectMulti(namesSort, meta.Scanname(isOK100), {myPara.scanname});
paraSort100 = myPara(indexPara, :);

% m1 = meta(isOK100);
% m2 = m1(indexMeta);


% PlotForceDurationHL(paraSort50, 'Sa');
%  PlotForceDurationHL(paraSort100, 'Sa');

PlotFD2HL(paraSort100, 'Sa');


%% Prints to PDF
%ReportData(Scans, Selection, paraSort50);

ReportData(Scans, mySelection, paraSort100);



