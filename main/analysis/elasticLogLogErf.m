function varargout = elasticLogLogErf( beta, x )
%elasticErf Log scaled Error Function which is stretchable in x and y.
%   Intended be used for fit to the FractalScale data.

if nargin == 0 
    error('Number inputs must be 1 or 2');
end
if nargin >= 1
    validateattributes(beta, ...
        {'numeric'}, {'vector', 'numel', 4}, mfilename, 'beta');
    
    % Extract parameters
    a = beta(1);    % stretch in x
    b = beta(2);    % shift in x
    c = beta(3);    % stretch in y
    d = beta(4);    % shift in y
    
    % Compute max slope and corresponding x value using analytic expression
    % x value
    maxSlopeXvalue = exp(-b/a);

    % evaluate firstDerivative at x value
    %D = firstDerivative(0);
    
    maxSlope = a*c*2/sqrt(pi)/maxSlopeXvalue;
end
if nargin == 2
    validateattributes(x, {'numeric'}, {'vector'}, mfilename, 'x');
    
    % Log-transform, scale and shift x
    xt = a*log(x) + b;

    % Get function values for all elements in vector x and return as 
    % vector of log(y) values
    y = exp( c*erf(xt) - d );
    
    
end
if nargin > 2 
    error('Number inputs must be 1 or 2');
end

% Hand over output
if nargin == 1
    varargout{1} = maxSlope;
    varargout{2} = maxSlopeXvalue;
%     varargout{3} = maxSlopeYvalue;
else % nargin = 2
    varargout{1} = y;
end


end

