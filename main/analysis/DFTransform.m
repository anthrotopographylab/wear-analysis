function [ FxCrop, FyCrop, ZfftNorm ] = DFTransform( X, Y, Z )
% Perform 2D discrete fourier transform.

validateattributes(X, {'numeric'}, {'2d'}, '', 'X');
validateattributes(Y, {'numeric'}, {'size', size(X)}, '', 'Y');
validateattributes(Z, {'numeric'}, {'size', size(X)}, '', 'scanD');

% Sampling distance
Dx = mean(diff(X(1,:)));
Dy = mean(diff(Y(:,1)));

m = size(Y, 1); % X and Y have the same size
n = size(X, 2);

% Vector of frequencies
fx = (0:1:(n-1)) / ((n-1)*Dx);  % [1/um]
fy = (0:1:(m-1)) / ((m-1)*Dy);

% Indices of Nyquist frequency (or closest larger for even n, m resp.)
i_center = floor(m/2)+1;
j_center = floor(n/2)+1;

% rotate quadrants
Zfft = fftshift(fft2(Z));

% Cut out new first quadrant (only up to nyquist freq)
ZfftQuad1 = Zfft(1:i_center, j_center:end);

% Normalize with polygonsize, do not count zero padded samples
ZfftNorm = ZfftQuad1 / length(find(Z));

Fx = fftshift(fx);
Fy = fftshift(fy);

assert(Fy(i_center) == 0);
assert(Fx(j_center) == 0);

FxCrop = fx(1:j_center);
FyCrop = fy(1:i_center);
% keyboard

end

