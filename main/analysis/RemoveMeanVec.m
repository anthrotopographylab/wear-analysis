function [zmean, mean] = RemoveMeanVec(x, y, z)
% Remove the mean height from the scan and return result.
% Scan data must be gridded and in vector format.


% CHECK USER INPUT ********************************************************
validateattributes(x, {'numeric'}, {'vector'}, '', 'x coordinates');
validateattributes(y, {'numeric'}, {'size', size(x)}, '', 'y coordinates');
validateattributes(z, {'numeric'}, {'size', size(x)}, '', 'z');

if any([isnan(x) isnan(y) isnan(z)])
    error('Can not work with NaNs');
end


area = IntegrateVec(x, y, ones(size(x)));

mean = IntegrateVec(x, y, z) / area;

zmean = z - mean;

end

