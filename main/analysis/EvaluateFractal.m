function [ out, outObj ] = EvaluateFractal( scans, varargin )
% EvaluateFractal Evaluate AliconaImage objects with fractal analysis

validateattributes(scans, {'AliconaImage'}, {'vector'}, '', 'scans');

% if nargin == 2
%     validateattribuets(varargin{1}, {'Selections'}, {'vector'}, '', 'selections');
%     selections = varargin{1};
% 
%     % Store present links temporarily
%     selLinkTmp = scans.getLinksTo('Selection');
% 
%     % Break present and set links to parsed selections
%     selLinkTmp.breakLinks;
%     selections.linkToImage(scans);
% end
if nargin > 1
    error('Too many input arguments');
end



% Iterate through scans
out = cell.empty(0, 2);

outObj = FractalAreaScale.empty;

for i=1:length(scans)
    
    if scans(i).is3D && ~scans(i).isDocuOri && scans(i).isExport
    
        XGrid = scans(i).XGrid;
        YGrid = scans(i).YGrid;
        Image = scans(i).ImageTopo;
        
        % Check whether grid is uniformly spaced.
        % If not, then the polygon coordinates might be invalid due to bug
        % in axis vectors of the display function. Can not evaluate those
        % polygons by now.
        tol = 1e-2; % numeric threshold
        if ~ (all(diff(diff(XGrid(1,:))) < tol) && all(diff(diff(YGrid(:,1)))) < tol)
            fprintf('Grid vectors are not equally spaced. Skip %s.\n', scans(i).Name{:});
            continue;
            %error('Grid vectors are not equally spaced');
        end

        selections = scans(i).getLinksTo('Selection');
        numSel = length(selections);

        if isempty(selections) && ~scans(i).hasOutlier
            out(end+1, :) = cell(1,2);
            
            fprintf('Evaluate whole image of %s\n', scans(i).Name);
            [out{end,1}, out{end,2}] = FractalAreaScale(XGrid(1,:), YGrid(:,1), Image);
            
        elseif ~isempty(selections)
            %out(end+1:numSel, :) = cell(numSel, 2);

            for j=1:numSel
                polygon = selections(j).VertWorld;
                
                
                fprintf('Evaluate polygon %u of %s\n', j, scans(i).Name{:});
                [relA, scale] = relativeAreaScale(XGrid(1,:), YGrid(:,1), Image, polygon, false);

                out{numSel+1-j, 1} = relA;
                out{numSel+1-j, 2} = scale;
                
                % Save as FractalScale object
                outObj(end+1)            = FractalAreaScale(scans(i).Name{:});
                outObj(end).scale        = scale;
                outObj(end).relArea      = relA;
                outObj(end).polygonNo    = uint16(j);
                outObj(end).polygonID    = selections(j).ID;
                
                outObj(end).linkTo(selections(j));
            end
        end
    end
end


if nargin == 2
    % Recreate original links
    selections.breakLinks;
    selLinkTmp.linkToImage(scans);
end

end



