function [ ROI ] = Polygon2Roi( I, Polygons,  varargin )
% Take polygon coordinates and returns a Roi mask for image I.
% The roi mask contains all polygons defined in the array Polygons.
% The core of this function is MATLABs roipoly.
% The polygon coordinates given by Polygons can be formated in two ways:
% weaved or matrix format.
% In the WEAVED FORMAT each row of the matrix describes one polygon, e.g.:
% [x11 y11 x12 y12 ... ;
%  x21 y21 x22 y22 ... ], 
% with xij beeing x value for vertice j of polygon i.
% In MATRIX FORMAT col 1 takes the x-values and col 2 the y-values. Here
% only one polygon can be described, e.g.:
% [x1 y1;
%  x2 y2;
%  x3 y3;
%  ...  ]
%       


% CHECK USER INPUT ********************************************************
validateattributes(I,        {'numeric'}, {'ndims', 2}, '', 'image');
validateattributes(Polygons, {'numeric'}, {'2d'}, '', ...
    'polygon coordinates');

X=[];
Y=[];

% Check the type of format of Polygons
% Polygons has not more than 2 dimensions (checked above)
if size(Polygons, 2) < 2
    error('MWA:Polygon2Roi:invalidInput', ...
        ['Polygons must be an array of dimension 2. Check', ...
        'the description for correct format']);
elseif size(Polygons, 2) > 2    % weaved format
    if mod(size(Polygons, 2), 2) == 1 % can you divide num cols by 2?
        error('MWA:Polygon2Roi:invalidInput', ...
            'Polygons must be specified with two coordinates per node');
    end
    numPolygons = size(Polygons, 1);
    isWeavedFormat = true;
elseif size(Polygons, 2) == 2  % matrix format
    numPolygons = 1;
    isWeavedFormat = false;
end

if isempty(varargin)
    % no optional input
end
if length(varargin) == 2
    X = varargin{1};
    Y = varargin{2};
    validateattributes(X, {'numeric'}, {'ndims', 2}, '', 'x coordinates');
    validateattributes(Y, {'numeric'}, {'ndims', 2}, '', 'y coordinates');
if length(varargin) > 2
    error('MWA:Polygon2Roi:invalidInput', ...
        'The number of optional input arguments is invalid');
end
    
% % check content of the cell arary selection
% checkInt = @(x) validateattributes(x, {'array'}, {'integer'});
% cellfun(checkInt, selection);   % element must be indexes of I

ROI = false(size(I));

for i=1:numPolygons
    if isWeavedFormat
        % Extract x and y coordinates from the weaved format
        polygon = Polygons(i, :);
        SelRes = reshape(polygon', [2 size(polygon, 2) / 2]);
        cols = SelRes(1, :);
        rows = SelRes(2, :);
    else
        cols = Polygons(:,1);
        rows = Polygons(:,2);
    end

    if isempty(X) && isempty(Y)     % intrinsic coordinates
        if size(I, 1) < max(rows) || size(I, 2) < max(cols)
            error('MWA:Polygon2Roi:dataInconsistency', ...
            'Polygon intrinsic coordinates lie outside the image');
        end
        Roi = roipoly(I, cols, rows);
    else                            % world coordinates
        if max(max(Y)) < max(rows) || max(max(X)) < max(cols)
            
            warning('MWA:Polygon2Roi:dataInconsistency', ...
            'Polygon world coordinates lie outside the image');
        end
        Roi = roipoly([X(1,1) X(end,end)], [Y(1,1) Y(end,end)], ...
            I, cols, rows);
    end
    
    % Collect and fuse Rois into one mask
    ROI = ROI | Roi;
    
end

end

