classdef (Abstract) AbstractLinkable < handle
    % Subclasses derived from this can establish links between instances. 
    % These links (or associations) can be usefull to easily express
    % relationships between objects and querry data stored in other
    % objects.
        
    properties (Abstract = true, Constant = true, Access = protected)
        linkableTypes      % Defines subclasses that are allowed to link 
    end
    
    properties (SetAccess = private, GetAccess = public)
        links = struct;     % Stores handles to AbstractLinkable objects
    end
    
    methods
        function obj = AbstractLinkable()
            % Use constant strings defined in subclass to fill links with
            % empty objects of linkable classes. 
            
            types = obj.linkableTypes;
            
            validateattributes(types, {'cell'}, {'vector'});
            % TO DO : check if classes <linkableTypes> are defined
            
            obj.links           = struct;
            
            % Construct empty objects
            for type = types
                obj.links.(type{:}) = feval([type{:} '.empty'], 1, 0);
            end
        end
        
        function out = getLinksTo(obj, TYPE)
            % Get all linked objects of class, TYPE for this object array.
            % In case each element of this array stores one or zero links,
            % to TYPE, the elements of the output array pair with elements
            % of this array at the same index position. I.e. this array and
            % the output array will have the same size.
            % In case of two or more links, the output array does not
            % pair with this array and their sizes will differ most likely.

            validateattributes(TYPE, {'char'}, {'row', 'nonempty'}, '', 'TYPE');
            
            if ~ismember(TYPE, obj(1).linkableTypes)
                error('%s is not a linkable type', TYPE);
            end
            
            % Get structs of alle elements holding links of different types
            linkstructs = [obj.links];
            
            % Scalar objects may return empty structs.
            if isscalar(obj)
                out = [linkstructs.(TYPE)];
                return
            end
            
            % For each struct get the number of links to TYPE objects
            numLinks = arrayfun(@(x)length(x.(TYPE)), linkstructs);
            
            if all(numLinks <= 1)
                % Insert default valued objects at locations where no
                % correspondance exists.
                isEmpty = numLinks == 0;
                out = repmat(feval(TYPE), size(obj)); % Default values
                out(~isEmpty) = [linkstructs.(TYPE)]; % Insert available
            else
                % Ouput array can not be paired with input array
                out = [linkstructs.(TYPE)];
            end
        end
        
        function linkTo(obj, target)
            % Link this object with target object.
            % In case of equal length vector arrays for obj and target,
            % it links pairs of elements residing at same index position.
            
            if isempty(obj) || isempty(target)
                return;
            end
            
            validateattributes(obj, {'AbstractLinkable'}, ...
                {'vector'}, '', 'obj');
            validateattributes(target, {'AbstractLinkable'}, ...
                {'vector', 'numel', length(obj)}, '', 'target');
            
            % Sync dimensions 
            target = reshape(target, size(obj));
            
            % Link all elements
            arrayfun(@linkSingle, obj, target);
            
            function linkSingle(o, t)
                % Manage entry of target object
                t.setLink(o);

                % Manage entry of this object
                o.setLink(t);
            end
        end
        
        function [iO, iT] = isRelated(obj, target, propO, propT)
            % Checks wheather this object is related to target objects.
            % A relationship is given if obj and target share the same
            % value for properties, <propO> and <propT>. Returns 
            % equal length index vectors, iO and iT, specifying pairs of
            % elements in obj (iO) and target (iT) beeing related.
            % propO and propT are cell arrays of strings in corresponding
            % order.
            % Runs on array of objects. 
            
            % Check input types
            validateattributes(target, {'AbstractLinkable'}, {'vector'});
            validateattributes(propO, {'cell'}, {'vector'});
            validateattributes(propT, {'cell'}, {'size', size(propO)});
            
            if ~all(cellfun(@ischar, [propO, propT]))
                error('Properties must be cells of strings');
            end
            
            numProp = length(propO);
            numO    = length(obj);
            numT    = length(target);
            
            dataO = cell(numO, numProp);
            dataT = cell(numT, numProp);
            
            for i=1:numProp
                dataO(:,i) = {obj.(propO{i})}';
                dataT(:,i) = {target.(propT{i})}';
            end
            
            % Use the same pseudo names for corresponding properties
            varnames = strcat('v', cellstr(categorical(1:numProp)));
            
            % Use tables innerjoin to find identical pairs in arrays of
            % property values
            [~, iO, iT] = innerjoin( ...
                cell2table(dataO, 'VariableNames', varnames), ...
                cell2table(dataT, 'VariableNames', varnames));
            
        end
        
        function breakLinks(obj)
            % Delete entries in 'links' of linked objects and this object.
            % Entries in linked objects are only deleted if they are
            % links to this obj. Runs on array of objects. 
            
            % For each object in array, obj:
            arrayfun(@breakLinksSingle, obj)
            
            function breakLinksSingle(objSingle)
                % Go through arrays of each field of struct, 'links'
                structfun( ...
                    @(a)arrayfun( ...
                        @cleanLinkedObject, ...
                        a), ...
                    objSingle.links);

                % Clean up entries in linked object
                function cleanLinkedObject(linkedObject)
                    isThis = linkedObject.links.(class(objSingle)) == objSingle;
                    linkedObject.links.(class(objSingle))(isThis) = [];
                end

                % Clear everything in 'links' of this obj
                for type = objSingle.linkableTypes
                    objSingle.links.(type{:}) = feval([type{:} '.empty'], 1, 0);
                end
            end
        end
        
        function delete(obj)
            % Break links and delete object.
            obj.breakLinks;
            
            % Use handle class destructor
            delete@handle(obj);
        end
    end
    
    methods (Access = private)
        function setLink(obj, target)
            % Insert a copy of handle, target into obj.links. target must
            % also be of a class which is linkable from obj.
            validateattributes(target, obj.linkableTypes, {'scalar'});
            
            targetType = class(target);

            % Check if target is already linked and add only if new       
            isLinked = obj.links.(targetType) == target;
                
            if sum(isLinked) == 1
                % do nothing
            elseif sum(isLinked) > 1
                % Should not happen
                assert(false);
            else
                % append to array
                obj.links.(targetType)(end+1) = target;
            end
        end
    end
end

