classdef AbstractRecord < dynamicprops
    %AbstractRecord Interface to MATLAB tables through custom objects.
    % This abstract class uses dynamicprops to project the variables of 
    % tables in the workspace to objects properties, which are accessible
    % like ordinary properties in classes implementing this class. The
    % access to the tables in the workspace is managed by an RelatedTable
    % object.
    % To querry the property of an array of objects, use getProp.
    % ID is used to specify the row of this record.
    
    properties (Abstract = true, Constant = true)
        % Names of related tables. The primary table (table associated with
        % the primary variable) is determined by the RelatedTable object.
        tableNames  
    end
    
    properties
        RT = RelatedTable.empty; % Manages the tables in ws
        
        % Store dynamic property objects initialized for this class.
        % Might be necessary for customization afterwards.
        dynprop = meta.DynamicProperty.empty(1,0);
        
        % ID of this record associated with the primary variable. The table
        % assoviated with primary variable is called the primary table. The
        % primary variable is (or ID variable) is specified in 
        ID = missing;
    end
    
    methods
        function obj = AbstractRecord(id)
            % Construct this oject using the record id associated with the
            % primary variable.
            
            % Construct the related table object
            obj.RT = RelatedTables(obj.tableNames);
            
            % Check validity of the record id
            if ~(nargin == 1 && isscalar(id)) 
                error('Constructor needs a scalar as ID');
            end
            
            if isnumeric(id)
                idTest = double(id);
            else
                idTest = id;
            end

            if ~(ismissing(idTest) || ...
                ismember(idTest, obj.RT.getColumn(obj.RT.primaryVariable)))
                error('No record found for the ID given');
            end
                
            obj.ID = id;
            
            % Construct class properties (dynamic properties) from 
            % variables in RT. First filter out existing names of derived
            % classes and of dynamic properties already created.
            % Then set property name and access method.
            propsNew = setdiff(obj.RT.variableNames, properties(obj));
            
            obj.dynprop = meta.DynamicProperty.empty(0, length(propsNew));
            
            for i=1:length(propsNew)
                obj.dynprop(i) = addprop(obj, propsNew{i});
                obj.dynprop(i).GetMethod = @(myobj)getValue(myobj, propsNew{i});
            end
                
        end
        
        function out = getValue(obj, varName)
            % Return the variable value of varName associated with this
            % record. If the record ID is a missing value indicator,
            % return 'missing'. These outputs constitute default valued
            % objects. 
            
             if ismissing(obj.ID)
                 % Treat integers separately and use -99 as missing value
                 % indicator.
                 % For variables of type cell, wranp missing value
                 % indicator into cell.
                 
                 if isinteger(obj.RT.getColumn(varName))
                     out = int16(-99);
                 elseif iscell(obj.RT.getColumn(varName))
                     out = {missing};
                 else
                     out = missing;
                 end
                    
                return; 
             end
            
            % Check that <varName> belongs to a workspace table in RT.
            isVar = strcmp(varName, obj.RT.variableNames);
            
            if      sum(isVar) == 1
                % All good
            elseif  sum(isVar) == 0
                error('%s does not belong to this table record.', varName);
            else
                error('Design fail: eponymic properties');
            end

            rec = obj.RT.getFullRecord(obj.ID);
            out = rec{:, varName};
            
            % Return missing value indicators if value is missing, also
            % for content in cells.
            % Cast integers to signed 16 bit types such that missing value
            % indicators (-99) can be inserted if needed.
            if iscell(out)
                if isempty(out{:})
                    out = {missing};
                end
            else
                if isempty(out)
                    out = missing;
                elseif isinteger(out)
                    out = int16(out);
                end
            end
        end
        
        function out = getProp(obj, propName)
            % Return the values of propName associated with multiple
            % records, i.e. for querries on an array of objects.

            values = arrayfun(@(x)getValue(x, propName), ...
                obj, 'UniformOutput', false);
            
            out = [values{:}];   % Pop out cell contents 
        end
    end
    
end

