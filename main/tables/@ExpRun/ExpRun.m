classdef ExpRun <  AbstractRecord & AbstractLinkable
    % ExpRun    Class for records of table Logbook.
    % Instances can be linked to objects of type AliconaImage.
    % 
    
    % Inherited from AbstractLinkable
    properties (Constant = true, GetAccess = protected)
        linkableTypes = {'Measurement', 'AliconaImage', 'SampleHistory'};
    end
    
    % Inherited from AbstractRecord
    properties (Constant = true)
        tableNames = {'Logbook', 'IndexDoc'};   % Name of the primary table
    end
    
    % Own properties
    properties (SetAccess = private)        
         RunNo           = int16(-99);
         IterationNo     = int16(-99);
         SampleID        = int16(-99);
    end
    
    properties (Dependent = true)
        Trajectory
    end
   
    methods
        function obj = ExpRun(id)
            % ExpRun    Construct record from table, Logbook
            % USAGE
            % ExpRun( id )
            % id: Integer and unique ID of table Logbook (= RunNo)              

            if nargin == 0
                % Create default valued object, indicated by missing object
                myid = missing;
                
            elseif nargin == 1
                validateattributes(id, {'numeric'}, {'scalar'});

                if mod(id, 1) ~= 0 || id < 1
                    error('id must be a positive integer.');
                end

                myid = int16(id);
            else
                error('Too many input arguments.');
            end
            
            % Super class construction
            obj@AbstractRecord(myid);
            
            % Set a few frequently used values
            obj.RunNo = obj.getValue('RunNo');
            obj.SampleID = obj.getValue('SampleID'); 
            obj.IterationNo = obj.getValue('IterationNo'); 
        end
        
        function out = get.Trajectory(obj)
            validateattributes(obj, {'ExpRun'}, {'scalar'}, '', 'obj');
            
            if isempty(obj.TrajectoryFile)
                out = tscollection;     % empty collection
                return;
            else
                if isequal(obj.TrajectoryFile{:}, {''})
                    out = tscollection;     % empty collection
                    return;
                else
                    path = fullfile(obj.Directory, obj.TrajectoryFile{:});
                end
            end
            
            data = readLogfile(path{1});
            
            % Format as MATLABs timeseries object
            out = tscollection({ ...
                timeseries(data.CartPos_X, data.TimeSec, 'Name', 'CartPosX'),  ...
                timeseries(data.CartPos_Y, data.TimeSec, 'Name', 'CartPosY'),  ...
                timeseries(data.CartPos_Z, data.TimeSec, 'Name', 'CartPosZ'),  ...
                timeseries(data.CartForceN_X, data.TimeSec, uencode(data.CartForceVar_X, 8, 10, 'signed'), 'Name', 'CartForceN_X'), ...
                timeseries(data.CartForceN_Y, data.TimeSec, uencode(data.CartForceVar_Y, 8, 10, 'signed'), 'Name', 'CartForceN_Y'), ...
                timeseries(data.CartForceN_Z, data.TimeSec, uencode(data.CartForceVar_Z, 8, 10, 'signed'), 'Name', 'CartForceN_Z')}, ...
                'Name', 'Experimental Trajectory');
            
            out.TimeInfo.Units = 'seconds';
        end
        
        function out = cumulateCycles(obj)
            % Returns cumulated cycle numbers of current and previous runs.
            % Requires all obj elements to be linked to SampleHistory objects,
            % so the cycles numbers can be looked up. All elements, for
            % which this is not the case, return NaN as output. 
            
            % Create arrays of run-history pirs 
            mylinks = [obj.links];
            isLinked = cellfun(@length, {mylinks.SampleHistory}) == 1;
            myruns = obj(isLinked);
            myHist = myruns.getLinksTo('SampleHistory');
            
            cycles = arrayfun(@numCyclesAt, myHist, [myruns.IterationNo]);
            
            out = double.empty(0, length(obj));
            
            out(~isLinked) = NaN;
            out(isLinked) = cycles;
        end
        
        
        
        
%         function res = loadValues(obj)
%                 % Load values from table
%                 
%                 id  = obj.ID;
%                 
%                 tn  = obj.tableName;
%                 rec = {obj.idName, id};
%                 
%                 field = 'ExperimentID';
%                 value = QuerryTable(tn, rec, field);
%                 if isempty(value)
%                     obj.errMissing(field, id);
%                     res = ExpRun.empty(1,0);
%                     return;
%                 else
%                     obj.(field) = value; 
%                 end
%                 
%                 field = 'IterationNo';
%                 value = QuerryTable(tn, rec, field);
%                 if isempty(value)
%                     obj.errMissing(field, id);
%                     res = ExpRun.empty(1,0);
%                     return;
%                 else
%                     obj.(field) = value; 
%                 end
% 
%                 field = 'SampleID';
%                 value = QuerryTable(tn, rec, field);
%                 if isempty(value)
%                     obj.errMissing(field, id);
%                     res = ExpRun.empty(1,0);
%                     return;
%                 else
%                     obj.(field) = value; 
%                 end
%                 
%                 field = 'Force';
%                 value = QuerryTable(tn, rec, field);
%                 if isempty(value)
%                     obj.errMissing(field, id);
%                     res = ExpRun.empty(1,0);
%                     return;
%                 else
%                     obj.(field) = value; 
%                 end
%                 
%                 field = 'NumCycles';
%                 value = QuerryTable(tn, rec, field);
%                 if isempty(value)
%                     obj.errMissing(field, id);
%                     res = ExpRun.empty(1,0);
%                     return;
%                 else
%                     obj.(field) = value; 
%                 end
%                 
%                 field = 'NumCycCum';
%                 value = QuerryTable(tn, rec, field);
%                 if isempty(value)
%                     obj.errMissing(field, id);
%                     res = ExpRun.empty(1,0);
%                     return;
%                 else
%                     obj.(field) = value; 
%                 end
%                 
%                 field = 'WorkpieceID';
%                 value = QuerryTable(tn, rec, field);
%                 if isempty(value)
%                     obj.errMissing(field, id);
%                     res = ExpRun.empty(1,0);
%                     return;
%                 else
%                     obj.(field) = value; 
%                 end
%                 
%                 field = 'WorkpieceType';
%                 value = QuerryTable(tn, rec, field);
%                 if isempty(value)
%                     obj.errMissing(field, id);
%                     res = ExpRun.empty(1,0);
%                     return;
%                 else
%                     obj.(field) = value; 
%                 end
%                 
%                 field = 'Date';
%                 value = QuerryTable(tn, rec, field);
%                 if isempty(value)
%                     obj.errMissing(field, id);
%                     res = ExpRun.empty(1,0);
%                     return;
%                 else
%                     obj.(field) = value; 
%                 end
%                             
%                 
%                 % Return updated data
%                 res = obj;
%         end
        
        % TO DO: Implement sorting after a certain property
%         function out = sort(obj)
%             [~, index] = sort(obj.getProp('IterationNo'));
%             out = obj(index);
%         end
    end
end

