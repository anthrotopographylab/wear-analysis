classdef AbstractTableRecord < dynamicprops
%AbstractTableRecord Represents one row of a RelatedTable object.
%   It holds several instances of class RelatedTable, a 
%   primary instance and multiple others related to the primary one. 
%   The table variables of the primary and related instances are directly 
%   accessible through the property names of this class. If the property of
%   an array of objects is querried, getProp should be used.
%   ID is used to specify the row of the primary instance.
% 
    
    properties (Abstract = true, Constant = true)
        tableNames  % Names of related tables; primary table first.
    end
    
    properties (Access = public)
        primTable       = RelatedTable.empty;           % Primary table
        relTables       = RelatedTable.empty(1,0);      % Related tables
    end
    
    properties
        % Store variable names of all tables.
        % Needed for 'manual' querry via getProp.
        variableNames   = cell(1,0);  
        
        % Store dynamic property objects initialized for this class.
        % Needed for later customization.
        dynprop = meta.DynamicProperty.empty(1,0);
        
        % ID of this record associated with the primary table. Can be
        % numeric or sting.
        ID = [];    
    end
    
    methods
        function obj = AbstractTableRecord( id )
            % AbstractTableRecord Record of primary table with ID = id.
            % Can only be called from implementing subclasses.
            % If default valued object is demanded, indicated by
            % id = NaN, property values are missing value indicators.
            
            % Check that the derived class implements at least one table
            if ~iscellstr(obj.tableNames) || length(obj.tableNames) < 1
                error('%s must specify minimum 1 table in tableNames.', ...
                    class(obj));
            end
            
            % Check input
            if nargin == 1 && isscalar(id)
                % Check that ID is valid type for primTable
                % TO DO: Check if id exists.
                if ~isequaln(id, NaN)
                    validateattributes(id, ...
                        {RelatedTable(obj.tableNames{1}).idType}, ...
                        {'scalar'}, '', 'id');
                end
            else
                error('One input required: ID, a scalar value or NaN.');
            end
            
            
            for i=1:length(obj.tableNames)

                tablename = obj.tableNames{i};

                if i==1 % First table is primary table
                    propsExisting = {};
                    obj.primTable = RelatedTable(tablename);
                    propsNew = obj.primTable.variables;

                    obj.ID = id;
                else
                    propsExisting = [obj.primTable.variables, ...
                        [obj.relTables.variables]];
                    obj.relTables(i-1) = RelatedTable(tablename);
                    propsNew = obj.relTables(i-1).variables;
                end

                % Filter out already existing properties and static
                % properties of classes derived from this one.
                propsAdd = propsNew(~ismember( ...
                    propsNew, [propsExisting properties(obj)']));

                % Add table variables to properties
                for prop = propsAdd
                    obj.dynprop(end+1) = addprop(obj, prop{:});
                    obj.dynprop(end).GetMethod =  ...
                        @(myObj)dynGet(myObj, tablename, prop{:});
                end
                
                % Store variable names of all tables 
                obj.variableNames = [obj.variableNames propsNew];

            end
            
            % Remove duplicates
            obj.variableNames = unique(obj.variableNames);
            
%             newtable = RelatedTable(tablename);
%             
%             obj.relTables(end+1) = newtable;
%             
% %             myvar = setxor(table.variables, desired);
%             
%             % Separate hard coded property names and keynames from 
%             % table variable names.
%             hardProps = properties(obj);
%             keys = obj.primTable.getKeysTo(tablename);
%             propsAdd = setdiff(newtable.variables, [keys, hardProps']);
% 
%             % Add table variables to properties
%             for prop = propsAdd
%                obj.dynprop(end+1) = addprop(obj, prop{:});
%                obj.dynprop(end).GetMethod =  ...
%                    @(myObj)myObj.primTable.getValue(myObj.ID, [tablename '.' prop{:}]);  
%            end
                
%             else
%                 error('Too many input arguments');
        end
                

        
%         function addTable(obj, tablename)
%             % Add another table to array of RelatedTable objects.
%             % Incorporates table variables to properies of this obj. A list
%             % of desired properties can be specified in desired.
%             % Check first if not already listed and related to primary.
%                         
%             if ~obj.primTable.isRelatedTo(tablename)
%                 error('Table must be related to primarytable');
%             end
%             
%             newtable = RelatedTable(tablename);
%             
%             obj.relTables(end+1) = newtable;
%             
% %             myvar = setxor(table.variables, desired);
%             
%             % Separate hard coded property names and keynames from 
%             % table variable names.
%             hardProps = properties(obj);
%             keys = obj.primTable.getKeysTo(tablename);
%             newProps = setdiff(newtable.variables, [keys, hardProps']);
% 
%             % Add table variables to properties
%             for prop = newProps
%                obj.dynprop(end+1) = addprop(obj, prop{:});
%                obj.dynprop(end).GetMethod =  ...
%                    @(myObj)myObj.primTable.getValue(myObj.ID, [tablename '.' prop{:}]);  
%            end
%         end
        
        function values = getProp(obj, propName)
            % Querry dynamic property, propName of array of objects.
            % It first figures out the table which stores variable 
            % <propName> and passes both on to dynGet().
            % Does not compare with existing dynamic property objects 
            % but with all variable names from tables stored in
            % variableNames. This is because dynamic property objects might
            % be overwritten by static properties from subclasses.
            
            validateattributes(propName, {'char'}, {'row'}, '', 'propName');
            
            % Check that property named <propName> is a dynamic property
            % TO DO: combine this check with check on RelatedTables below.
            isProp = strcmp(propName, obj(1).variableNames);
            
            if sum(isProp) == 1
                % All good
            elseif sum(isProp) == 0
                error('Dynamic property %s not found.', propName);
            else
                error('Design fail: eponymic properties');
            end
            
            % Determine table corresponding to propName
            mytables = [obj.primTable, obj.relTables];
            for i=1:length(mytables)
                if any(strcmp(propName, mytables(i).variables))
                    tablename = mytables(i).name;
                end
            end
            
            
            
            % Get values for each element of obj
%             if isnumeric(dynGet(obj(1), tablename, propName))
%                 values = arrayfun(...
%                     @(x)dynGet(x, tablename, propName), ...
%                     obj, 'UniformOutput', true);
%             else
%                 values = arrayfun(...
%                     @(x)dynGet(x, tablename, propName), ...
%                     obj, 'UniformOutput', false);
%             end
            
            values = arrayfun(...
                @(x)dynGet(x, tablename, propName), ...
                obj, 'UniformOutput', false);
            
            values = [values{:}];   % Pop out cell contents 
        end
    end
    
    
    
    methods (Access = protected)       
        function out = dynGet(obj, tablename, propName)
            % Search reltables for propName and return its value.
            % TO DO refactor handling of missing values, and values in
            % cells stored in the table. This is not consistent by now.
            
            % For default value objects return missing value indicator
            if isequaln(obj.ID, NaN)
                
                % Treat integers separately because elements of integer
                % arrays can not be assigned to 'missing'.
                if isinteger(obj.primTable.getTable{1, propName})
                    out = 0;
                else
                    out = missing;
                end
                            
            % Else, receive the value from table
            else
                value = obj.primTable.getValue(obj.ID, tablename, propName);

                % If its an enpty cell, set missing value marker
                if iscell(value) && isfloat(value{:})
                    out = {NaN};
                else
                    out = value;
                end
            end
        end
    end

        
        
        
    
end

