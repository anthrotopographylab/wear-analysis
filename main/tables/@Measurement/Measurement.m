classdef Measurement < AbstractLinkable
%Measurement Container class for AliconaImage objects.
%   Holds multiple 3D AliconaImage objects with the same sample ID,
%   iteration no and microscope objective position on the sample.
%   It represents a topographic stone model with fixed 
%   temporal and spatial coordinates. 
    
    % Inherited from AbstractLinkable
    properties (Constant = true, GetAccess = protected)
        linkableTypes = {'ExpRun'};
    end
    
    properties
        Im50x   = AliconaImage.empty(1, 0);
        Im100x  = AliconaImage.empty(1, 0);
        PosDoc  = AliconaImage.empty(1, 0);
        OriDoc  = AliconaImage.empty;
        SampleID    = uint16.empty;
        IterationNo = uint16.empty;
        PositionNo  = uint16.empty;
    end
    
    properties (Dependent = true)

    end
    
    methods
        function obj = Measurement(scans)
            % Constructor
            
            if nargin > 0
                validateattributes(scans, ...
                    {'AliconaImage'}, {'2d', 'nonempty'}, '', 'scans');
                
                SampleID    = scans.getProp('SampleID');
                IterationNo = scans.getProp('IterationNo');
                PositionNo  = scans.getProp('PositionNo');
                
                [g, sid, iterno, posno] = findgroups( ...
                    SampleID, ...
                    IterationNo, ...
                    PositionNo);
                
                is50x   = scans.getProp('Magnification') == 50;
                is100x  = scans.getProp('Magnification') == 100;
                isOri   = scans.getProp('isDocuOri');
                
                % Allocate vector of Measurement objects depending on
                % groups found in scans.
                obj = Measurement.empty;
                
                for i=1:length(sid)
                    % Groups with posno==0 might be orientaion doc scans.
                    % Make extra checks for the orientation doc scans.
                    if all(isOri(g==i))
                        % This group consists of ori doc scans only.
                        % Do not build object for it.
                    else
                        % Instantiate another empty object.
                        obj(end+1) = Measurement;
                        
                        obj(end).SampleID     = sid(i);
                        obj(end).IterationNo  = iterno(i);
                        obj(end).PositionNo   = posno(i);

                        obj(end).Im50x  = scans(g==i & is50x);
                        obj(end).Im100x = scans(g==i & is100x);
                        obj(end).PosDoc = scans(g==i & ~is50x & ~is100x & ~isOri);
                        
                        % Check whether ori doc available for this group
                        if any(g==i & isOri)
                            % Take posno specific ori doc
                            obj(end).OriDoc = scans(g==i & isOri);
                        else
                            % Take ori doc with posno == 0
                            isSID       = SampleID      == sid(i);
                            isIterNo    = IterationNo   == iterno(i);
                            isPosNo     = PositionNo    == 0;
                            
                            obj(end).OriDoc = scans(isSID & isIterNo & isPosNo & isOri);
                        end
                    end
                end
                
%                 % Sort runs chronologically
%                 [~, index] = sort(obj.Runs.getProp('IterationNo'));
%                 obj.Runs = obj.Runs(index);
            end
            
        end
        
        function out = unique(obj)
            if isempty(obj)
                out = Measurement.empty;
                return;
            end
            
            [~, index, ~] = unique( ...
                [[obj.SampleID]', [obj.IterationNo]', [obj.PositionNo]'], ...
                'rows');
            out = obj(index);
        end
            
        
%         function out = get.numRuns(obj)
%             out = length(obj.Runs);
%         end
%         
%         function out = get.Workpiece(obj)
%             out = obj.Runs(1).WorkpieceType;
%         end
%         
%         function out = get.WorkpieceID(obj)
%             out = obj.Runs(1).WorkpieceID;
%         end
       
    end
    
end

