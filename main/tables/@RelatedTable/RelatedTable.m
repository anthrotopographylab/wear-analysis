classdef RelatedTable 
%RelatedTable Interface to tables stored in MATLABs base workspace.
%   Access to table objects in base ws through use of MATLABs evalin.
%   Links between RelatedTable objects are defined in graph MAP, which 
%   must be located in base ws too. Provides functionality to acces single
%   entries, rows (correponding to one or several tables), or the whole
%   table. Each table must have an ID variable used to specify a row. 
% TO DO Refactoring! This class is a mess.
% Remove function getValue. The function with most similar functionality
% should return a whole table row from which the desired variable can be
% easily extracted.
    
    properties (Constant = true, Access = private)
        % Structs in base ws qualified for search after groupname
        wsGroups = {'PARA', 'IMAG'};
        tablemap = 'MAP';   % Name of ws variable holding table relations
    end
    
    properties (SetAccess = private)
        name    = '';  % Name of this table (same as table object in base ws)
        idVar   = '';  % Name of table variable that is unique ID
        idType  = '';    % Data type of variable <idVar>
        variables = {};  % Table variables as defined in table object
        groupname = '';  % Name of the struct in base ws storing this table
    end
    
    methods
        function obj = RelatedTable(name)
            % Construct new RetaltedTable with reference <name>.
            % Sets table properties name, ID and keys.
            
            if nargin > 0
                validateattributes(name, {'char'}, {'row'}, '', 'name');
                
                nodeNo = evalin('base', sprintf('%s.findnode(''%s'');', ...
                        obj.tablemap, name));
                
                if nodeNo                    
                    obj.name = name;
                else
                    error('Table named %s not found', name);
                end
                
                obj.idVar = evalin('base', sprintf('%s.Nodes{%u, ''ID''}{:}', ...
                        obj.tablemap, nodeNo));
                
                obj.groupname = evalin('base', sprintf('%s.Nodes{%u, ''Ref''}{:}', ...
                        obj.tablemap, nodeNo));
                    
                if ~obj.isLoaded
                    obj.errLoading;
                end  
                
                % Figure out type of the ID variable
                obj.idType = class(obj.getTable.(obj.idVar)(1));
                
                obj.variables = obj.getTable.Properties.VariableNames;
                
            end
        end
        
%         function out = get.groupname(obj)
%             out = obj.wsGroups{1};
%         end
%         
%         function out = get.idType(obj)
%             table = obj.getTable;
%             out = class(table(1, obj.idVar));
%         end
        
        function flag = isLoaded(obj)
            % TO Should be static!
            % Check if associated table is loaded in base workspace
            flag = false;
            
            if evalin('base', sprintf('exist(''%s'', ''var'');', ...
                obj.groupname)) > 0
                
                if evalin('base', sprintf('isfield(%s, ''%s'');', ...
                    obj.groupname, obj.name))
                    
                    flag = true;
                end
            end
        end
        
        function list = getRelated(obj)
            % Get names of tables related to this one as row cellstring.
            
            list = evalin('base', sprintf('%s.neighbors(''%s'');', ...
                obj.tablemap, obj.name))';
        end
        
        function flag = isRelatedTo(obj, target)
            % Checks whether this obj is relatd to table named <target>.
            
            validateattributes(target, {'char'}, {'row'}, '', 'target');
            flag = ismember(target, obj.getRelated());
        end
        
        function tablename = getTablename(obj, varname)
            % Return name of table corresponding to varname.
            
            tablename = [];     % empty in case varname not found;
            
            list = [getRelated(obj), obj.name];
            
            for tn = list
                t = RelatedTable(tn{:});
                if any(strcmp(varname, t.variables))
                    tablename = t.name;
                end
            end
        end
        
        function keys = getKeysTo(obj, target)
            % Get keys relating this table to table named <target>.
            
            if obj.isRelatedTo(target)
                idx = evalin('base', sprintf('%s.findedge(''%s'', ''%s'');' , ...
                    obj.tablemap, obj.name, target));
                keys = evalin('base', sprintf('%s.Edges{%u, ''Keys''}{:}', ...
                    obj.tablemap, idx));
            else
                error('Target is not related to this table');
            end
        end
        
%         function out = findGroupname(obj)
%             for gn = obj.wsGroups
%                                 
%                 tn = evalin('base', sprintf('fieldnames(%s.tables', gn));
%                 
%                 if strcmp(tn, obj.name)
%                     out = group;
%                     return;
%                 end 
%             end
%             error('Could not find table');
%         end
        
        function table = getTable(obj)
            % Return table object associated with this RelatedTable.

            % Check status
            if ~obj.isLoaded
                obj.errLoading;
            end
            
            table = evalin('base', sprintf('%s.%s', ...
                obj.groupname, obj.name));
        end
        
        function row = getRow(obj, id)
            % Querry table row which has ID = id. Return as table object.
            
            mytable = obj.getTable;
            
            % Look-up row depending on type of id variable
            if isnumeric(id)
                isRow = mytable.(obj.idVar) == id;
            elseif iscell(id) && ischar(id{:}) || ischar(id)
                isRow = strcmp(mytable.(obj.idVar), id);
            else
                error('Undefined id type.');
            end
            
            if sum(isRow) ~= 1
                error('id is not unique');
            end
            
            row = mytable(isRow, :);
        end
        
        function row = getRowRelated(obj, id, reltable)
            % Querry row of this table which has ID = id and related
            % rows of reltable.
                        
            if obj.isRelatedTo(reltable) || strcmp(reltable, obj.name)
                myrow = obj.getRow(id);
                
                keys = obj.getKeysTo(reltable);
                
                row = innerjoin(myrow, getTable(RelatedTable(reltable)), 'Keys', keys);
            else
                error('%s is not related to this table', reltable);
            end
        end
        
        function type = getType(obj, varname)
            % Determine Type of variable <varname>.
            
            mytable = RelatedTable(getTablename(obj, varname));
            
            type = class(mytable.getTable{1, varname});
        end
        
        function value = getValue(obj, id, tablename, varname)
            % Querry value of this or related table.
            % The value to querry is specified by ID = id and variable =
            % varname. The variable must belong to table, <tablename>. 
            % The ouput is checked for missing values and marked with
            % missing value indicators if required.
            
            validateattributes(tablename, {'char'}, {'vector'}, '', 'tablename');
            validateattributes(varname, {'char'}, {'vector'}, '', 'varname');
            
%             [C, match] = strsplit(indicator, '.');
%             n = length(match);
            
            % Return missing value indicators if desired
            % TO DO: Move responsibility to AbstractTableRecord
            if isequaln(id, NaN)
                % missing value indicators
               
                reltable = RelatedTable(tablename);
                value = missingValueIndicator(reltable.getTable{1, varname});
                return;
            end
            
            if strcmp(tablename, obj.name)
                row = obj.getRow(id);
            else
                row = obj.getRowRelated(id, tablename);
            end

            value = row{:, varname}; % Get value from table object
            
            % Set missing value indicator if empty. 
            if isempty(value)
                value = missingValueIndicator(value);
            end
            
        end
        

        
        function errLoading(obj)
            error('Table data not loaded or table %s not existent', obj.name);
        end
            
    end
    
end

