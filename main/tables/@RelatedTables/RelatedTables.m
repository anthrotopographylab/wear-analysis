classdef RelatedTables
    %RelatedTables Interface to access values of several related tables.
    %   Use this class to querry one or multiple variables of different
    %   tables. The tables are stored in the workspace and the relationship
    %   between them is stored in ws variable MAP. In this calss the tables
    %   are not stored as table objects under properties, but the names as
    %   references to the table objects in the workspace.
    
    properties
        primaryTable    = '';   % Table storing the primary Variable
        primaryVariable = '';   % Variable associated with the record ID
        allTables       = {''}; % Store primary and related tables in here
        numTables       = 0;    % Total number of tables
        numVariables    = 0;    % Total number of variables
        variableNames   = {};
        variableTableIdx = [];  % Index of table where variable is stored
    end
    
    properties (Dependent = true, Access = private)
        map     % Return table MAP from workspace
    end
    
    methods
        function obj = RelatedTables(tableNames)
            % Construct RelatedTable object from table names.
            % The first name in 'tables' will be the primary table.
            
            if nargin > 0
                % Check input               
                if ~(iscellstr(tableNames) && numel(tableNames) > 0)
                    error(['tablesNames must be cellstring holding the' ...
                        'primary table first following optionally related tables.']);
                end
%                 validateattributes(tables, {'cellstring'}, {'nonempty'}, '', 'tables');
                
%                 % Check that MAP is in workspace
%                 if ~exist('MAP', 'var')
%                     error('Can not find MAP in workspace');
%                 end
                
                % Get table map information
                nodes = obj.map.Nodes;

                obj.primaryTable = tableNames{1};
                obj.allTables = {tableNames{:}};
                obj.numTables = length(obj.allTables);

                % Get the primary variable of the primary table
                obj.primaryVariable = nodes.ID{obj.map.findnode(obj.primaryTable)};
                
                % Save all variable names and corresponding table index                
                for i = 1:obj.numTables
                    tn = tableNames{i};
                    container = nodes.Ref{obj.map.findnode(tn)};
                    
                    % Variable names from workspace tables properties:
                    obj.variableNames = unique([ ...
                        obj.variableNames, ...
                        evalin('base', sprintf( ...
                            '%s.%s.Properties.VariableNames', ...
                            container, tn))]);
                    
                    % Table idx:
                    obj.variableTableIdx(end+1:length(obj.variableNames)) = i;
                end
                
                obj.numVariables = length(obj.variableNames);
            end
            
        end
        
        function out = get.map(obj)
            % Return MAP from workspace
            
            out = evalin('base', 'MAP');
        end
            
        
        function out = getTableIdx(obj, idx)
            % Return table having index <idx> in allTables.
            
            container = obj.map.Nodes.Ref{obj.map.findnode(obj.allTables{idx})};
            out = evalin('base', sprintf('%s.%s', container, obj.allTables{idx}));
        end
        
        function out = getColumn(obj, varName)
            % Return the column of variable <varName>.
            
            tableIdx = obj.variableTableIdx(strcmp(obj.variableNames, varName));
            
            if isempty(tableIdx)
                error('Variable name %s does not belong to tables', varName);
            end
            
            tt = obj.getTableIdx(tableIdx);
            out = tt{:, varName};
        end
            
        function out = getFullRecord(obj, recID)
            % Return full record, i.e. all variable related to one row of
            % the primary table. This row is specified thorugh the primary
            % variable. The resulting table is created using innerjoin.
            
            % Get primary table 
            PT = obj.getTableIdx(1);
        
            % Determine the row number corresponding to the record id
            if      iscellstr(PT.(obj.primaryVariable))
                isRec = ismember(PT.(obj.primaryVariable), recID);
                
            elseif  isnumeric(PT.(obj.primaryVariable))
                isRec = PT.(obj.primaryVariable) == recID;
                
            else
                error('Primary variable type is not supported');
            end
            
            % Check number of rows of jointRecord
            if sum(isRec) == 0
                error('Could not find record with given ID');
            end
            if sum(isRec) > 1
                error('The primary variable is not unique');
            end
            
            % Initialize record as table with one row
            jointRecord = PT(isRec, :);

            % Go through related tables (if any) and join with the primary table
            for i = 2:obj.numTables
                % Get keys relating to the next table
                keys = obj.map.Edges.Keys{obj.map.findedge(obj.primaryTable, obj.allTables{i})};
                
                % Join with the next table. Exclude entries of the next
                % table that do not match jointRecrod (tyep: left).
                jointRecord = outerjoin(jointRecord, obj.getTableIdx(i), ...
                    'type', 'left', 'mergekeys', true);
            end
            
            out = jointRecord;
        end
        
        
    end
    
end

