classdef AliconaImage < AbstractRecord & AbstractLinkable
%AliconaImage Represents an image taken by the Alicona microscope.
%   This class provides access to the image data . The images are 
%   accessible as object properties.
%   An AliconaImage holds a RGB image and possibly also a topographic
%   image of the specimen, wchich also includes world coordinates and a
%   Qualitymap describing the measurement uncertainty.
%   The image data is either read from wokrspace if loaded or from
%   disk if not. In the latter case the images are stored temporarily in
%   the object.
    
    % Inherited from AbstractLinkable
    properties (Constant = true, GetAccess = protected)
        linkableTypes = {'Selection', 'ExpRun'};
    end
    
    % Inherited from AbstractRecord
    properties (Constant = true)
        tableNames = {'IndexScans', 'Scans'}; 
    end
    
    properties (Access = private)
        % Use as storage if images are not loaded in ws
        imageDataTmp = cell.empty(0,5); 
    end

    properties (Dependent = true)
        % Do not inherit images from AbstractRecord but define
        % separate handling like integrity checks and 
        % reading from disk if data not in workspace.
        ImageRGB    = [];
        ImageTopo   = [];
        Qualitymap  = [];
        XGrid       = [];
        YGrid       = [];
        Info        = struct.empty;
        
        pathRGB     = '';               % File path to RGB image
        is3D        = logical.empty;
        hasOutlier  = logical.empty;
    end
    
    properties (SetAccess = private)
        Name            = '';
%         SampleID        = uint8.empty(1,0);
%         IterationNo     = uint8.empty(1,0);
%         PositionNo      = uint8.empty(1,0);
%         Magnification   = uint16.empty(1,0);
%         Orientation     = [];
%         Directory       = '';
%         
    end
    
    methods
        function obj = AliconaImage( name )         
            % AliconaImage  Construct AliconaImage from name.
            % The Scanname, <name>, is the unique ID in table IndexScans.
             
            if nargin == 0
                myname = missing;
            else
                myname = {name};
            end
            
            % Set-up a record with <name> as ID. Create default valued
            % object in case of no-arg contruction.
            obj@AbstractRecord(myname); 
            
            % Call scan name from table workspace. At the same time this is
            % a check whether a record with this name exists. 
            obj.Name = obj.getValue('Scanname');
        end
        
        
%         function varargout = subsref(obj, S)
%             % Implements indexing by name.
% 
%              switch S(1).type
%                 case '()'
%                    if iscell(S(1).subs) && ischar(S(1).subs{:})
%                        [~, index] = ismember(S(1).subs, {obj.Name});
%                        varargout = {obj(index)};
%                    else
%                        % Enable dot notation for all properties and methods
%                        varargout = {obj(S(1).subs{:})};
%                    end
%                  case '.'
%                      %varargout = {builtin('subsref',obj,S(1))};
%                      varargout = {obj.(S(1).subs)};
%                  case '{}'
%                      varargout = {builtin('subsref', obj, S(1))};
%              end
%         end
        
        function out = get.Info(obj)
            % Querry info file of alicona measurement
            
            try % to get image from workspace variable
                out = obj.getProp('Info');
            catch ME1
                % Turn error into warning
                warning(ME1.identifier, 'Could not querry Info from workspace:\n%s', ME1.message);

                % try to read image from disk 
                out = obj.getImagesFromDisk{1};                  
            end
            
            % Check integrity
            validateattributes(out, {'struct'}, {'2d'}, '', 'Info');
        end

        function out = get.ImageRGB(obj)
            % out = QuerryScans(obj.Name, 'ImageRGB'); DEPRECATED
            
            try % to get image from workspace variable
                out = obj.getProp('ImageRGB');
                out = out{:}; % pop out image array
            catch ME1
                % Turn error into warning
                warning(ME1.identifier, 'Could not querry ImageRGB from workspace: %s', ME1.message);

                % try to read image from disk 
                out = obj.getImagesFromDisk{2};                  
            end
            
            % Check integrity
            validateattributes(out, {'numeric'}, {'3d', 'nonempty'}, '', 'ImageRGB');
        end
        
        function out = get.ImageTopo(obj)
            % out = QuerryScans(obj.Name, 'ImageRGB'); DEPRECATED
            
            try % to get image from workspace variable
                out = obj.getProp('ImageTopo');
                out = out{:}; % pop out image array
            catch ME1
                % Turn error into warning
                % TO DO use rethrow instead
                warning(ME1.identifier, 'Could not querry ImageTopo from workspace:\n%s', ME1.message);

                % try to read image from disk 
                out = obj.getImagesFromDisk{3}; 
            end
            
            % Check integrity if available
            if ~ismissing(out)
                validateattributes(out, {'numeric'}, {'size', imsizePixel(obj)}, '', 'ImageTopo');
            end
        end
        
        function out = get.Qualitymap(obj)
            
            try % to get image from workspace table
                rawmap = obj.getProp('Qualitymap');
                rawmap = rawmap{:}; % pop out image array
            catch ME1
                % Turn error into warning
                warning(ME1.identifier, 'Could not querry Qualitymap from workspace:\n%s', ME1.message);

                % try to read image from disk 
                rawmap = obj.getImagesFromDisk{4};                 
            end
            
            % Check integrity
            if ~ismissing(rawmap)
                validateattributes(rawmap, {'uint8'}, {'size', imsizePixel(obj)}, '', 'Qualitymap');
            
                % Convert Qualitymap into meaningful values, in this case:
                %                    repeatability
                % Units are in [um] ???
                % Relationship to resolution is: res = sqrt(8) * rep

                out = (obj.Info.MinVal + double(rawmap) * obj.Info.Shift) / sqrt(8);
            else
                out = NaN;
            end
        end
        
        function out = get.XGrid(obj)
            % out = QuerryScans(obj.Name, 'ImageRGB'); DEPRECATED
            
            try % to get image from workspace variable
                out = obj.getProp('XGrid');
                out = out{:}; % pop out image array
            catch ME1
                % Turn error into warning
                warning(ME1.identifier, 'Could not querry XGrid from workspace:\n%s', ME1.message);

                % try to read image from disk 
                out = obj.getImagesFromDisk{5};
            end
            
            % Check integrity
            if ~ismissing(out)
                validateattributes(out, {'numeric'}, {'size', imsizePixel(obj)}, '', 'XGrid');
            end
        end
        
        function out = get.YGrid(obj)
            % out = QuerryScans(obj.Name, 'ImageRGB'); DEPRECATED
            
            try % to get image from workspace variable
                out = obj.getProp('YGrid');
                out = out{:}; % pop out image array
            catch ME1
                % Turn error into warning
                warning(ME1.identifier, 'Could not querry YGrid from workspace:\n%s', ME1.message);

                % try to read image from disk 
                out = obj.getImagesFromDisk{6};                   
            end
            
            % Check integrity
            if ~ismissing(out)
                validateattributes(out, {'numeric'}, {'size', imsizePixel(obj)}, '', 'YGrid');
            end
        end
        
                
        function out = get.pathRGB(obj)
            c = fullfile(obj.Directory, 'texture.bmp');
            out = c{:};
        end
        
        function out = get.is3D(obj)
            out = ~isequaln(obj.ImageTopo, NaN);
        end
        
%         function obj = set.ImageTopo(obj, value)
%             validateattributes(value, {'numeric'}, {'size', imsizePixel(obj)}, '', 'value');
%             obj.ImageTopo = double(value);
%         end
% 
%         function obj = set.XGrid(obj, value)
%             validateattributes(value, {'numeric'}, {'size', imsizePixel(obj)}, '', 'value');
%             obj.XGrid = double(value);
%         end
%         
%         function obj = set.YGrid(obj, value)
%             validateattributes(value, {'numeric'}, {'size', imsizePixel(obj)}, '', 'value');
%             obj.YGrid = double(value);
%         end

        function out = get.hasOutlier(obj)
            if obj.is3D
                out = any(any(isnan(obj.ImageTopo), 1));
            else
                out = false;
            end
        end

        function out = imsizePixel(obj)
            % Return image size in pixel coordinates
            sizeRGB = size(obj.ImageRGB);
            out = [sizeRGB(1) sizeRGB(2)];
        end
        
        function out = imsizeWorld(obj)
            % Return image size in world coordinates as [deltaY, deltaX].
            if isempty(obj.XGrid) || isempty(obj.YGrid)
                warning('X coordinates not given');
                out = [];
            else
                out =   [max(max(obj.YGrid))-min(min(obj.YGrid)) ...
                         max(max(obj.XGrid))-min(min(obj.XGrid))];
            end
        end
        
        % Determine max and min world coordinates
        function out = maxX(obj)
            out = max(max(obj.XGrid));
        end
        
        function out = minX(obj)
            out = min(min(obj.XGrid));
        end
        
        function out = maxY(obj)
            out = max(max(obj.YGrid));
        end
        
        function out = minY(obj)
            out = min(min(obj.YGrid));
        end
        
        function displayIm(obj)
            if      ~isempty(obj.ImageTopo)
                DisplayScan(obj.ImageTopo, obj.ImageRGB, obj.XGrid, obj.YGrid);
            elseif  ~isempty(obj.ImageRGB)
                DisplayScan(obj.ImageRGB);
            else
                warning(['MWA:' mfilename ':emptyObject'], ...
                    'Object is empty.');
            end
        end
        
%         function flag = get.is3D(obj)
%             flag = ~isempty(obj.ImageRGB) && ~isempty(obj.ImageTopo) ...
%                 && ~isempty(obj.XGrid) && ~isempty(obj.YGrid);
%         end

        function out = unique(obj)
            % Filter out objects, which have duplicate scan names.
            
            [~, idx, ~] = unique([obj.Name]);
            out = obj(idx);
        end
        
    end
    
    methods (Access = private)
        function images = getImagesFromDisk(obj)
            % Calls importAliconaImage to read image data from disk. This 
            % is the default procedure if the data is not accessible  in
            % the workspace. Do the importAliconImage call once for all
            % images and store as private variable for next time.
            
            % Do the one-time call
            if isempty(obj.imageDataTmp)
                [obj.imageDataTmp{1}, ...
                    obj.imageDataTmp{2}, ...
                    obj.imageDataTmp{3}, ...
                    obj.imageDataTmp{4}, ...
                    obj.imageDataTmp{5}, ...
                    obj.imageDataTmp{6}] ...
                        = importAliconaImage(obj.Directory{:});
            end
            
            % Return images in one cell
            images = obj.imageDataTmp;    
        end        
    end
end
    
    
    