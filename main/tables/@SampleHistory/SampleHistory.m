classdef SampleHistory < AbstractLinkable
%SampleHistory Container class for ExpRun objects.
%   Holds multiple ExpRun objects with the same SampleID. It is intended
%   to track the history of one stone sample. Provides functionality to
%   compute some metadata of the sample, like total number cycles, total
%   number of runs, etc. 
    
    % Inherited from AbstractLinkable
    properties (Constant = true, GetAccess = protected)
        linkableTypes = {'ExpRun'};
    end

    properties
%         Runs        = ExpRun.empty(1, 0);
        Meas        = Measurement.empty(1, 0);
        SampleID    = int16(-99);   % Initialize with missing value ind.
    end
    
    properties (Dependent = true)
        % Total number of runs (linked ExpRuns) for this sample
        numRuns
        
        % Total number of cycles ran with this stone sample
        numCyclesTot

        Workpiece
        WorkpieceID
    end
    
    methods
        function obj = SampleHistory(varargin)
            %SampleHistory(runs, meas) Construct from array of
            % ExpRuns and (optionally) Measurements.
            % Builds groups of ExpRun objects and corresponding 
            % measurements and stores groups in an array of
            % SampleHistories.
            
            if nargin > 0
                runs = varargin{1};
                validateattributes(runs, ...
                    {'ExpRun'}, {'2d', 'nonempty'}, '', 'runs');       
                
                if nargin == 2
                    meas = varargin{2};
                    validateattributes(meas, ...
                        {'Measurement'}, {'2d', 'nonempty'}, '', 'meas');
                end
                
                [g, sid] = findgroups([runs.SampleID]);
                
                % Allocate vector of empty SampleHistory objects
                obj = SampleHistory.empty(0, length(sid));
                
                % Iterator over unique sample IDs
                for i=1:length(sid)  
                    myruns = runs(g==i);
                    
                    % Sort runs after iteration no, increasingly
                    [~, idxSort] = sort([myruns.IterationNo]);
                    
                    % Check that this SampleHistory has a complete sequence
                    % of exerimental iterations. If there is a gap in the
                    % sequence discard this SampleHistory.
                    if isequal([myruns.IterationNo], 1) || ...
                        all(diff([myruns(idxSort).IterationNo]) == 1)
                    
                        obj(i) = SampleHistory;     % No-arg constructor
                        obj(i).SampleID = sid(i);   % Fill data
                    else
                        % Skip this sample
                        continue;
                    end
                    
                    % Replicate the current SapmpleID object into array
                    % with size equal to num of of ExpRuns and link to them.
                    linkTo(repmat(obj(i), 1, length(myruns)), myruns(idxSort));
                    
                    % Store runs and link runs with measurements, if given.
                    % TO DO Linking is not required. Should be done outside
                    % if desired.
                    if nargin == 2
                        keys = {'SampleID', 'IterationNo'};
                        [a, b] = isRelated(myruns, meas, keys, keys);
                        myruns(a).linkTo(meas(b));

                        % Store measurements 
                        obj(i).Meas = unique(meas(b));
                    end
                    
                end
                
                % Remove empty value elements caused by missing runs
                obj = obj([obj.SampleID] ~= -99);
            end
        end
        
        function out = get.numRuns(obj)
            out = length(obj.getLinksTo('ExpRun'));
        end
        
        function out = get.numCyclesTot(obj)
            % Return total number of cycles run with this sampel.
            % Is only correct if all ExpRuns are linked to this obj.
            
            runs = obj.getLinksTo('ExpRun');
            out = sum(runs.getProp('NumCycles'));
        end
        
        function out = get.Workpiece(obj)
            out = obj.Runs(1).WorkpieceType{:};
        end
        
        function out = get.WorkpieceID(obj)
            out = obj.Runs(1).WorkpieceID{:};
        end
        
        function out = numCyclesAt(obj, iterNo)
            % Return number of cycles ran with this sample up to iterNo.
            % Works only if all ExpRuns are linked to this obj.
            
            validateattributes(iterNo, {'numeric'}, {'numel', numel(obj)});
            
            out = zeros(size(obj));
            
            for i=1:numel(obj)
                % Deal with default valued sample history
                if obj(i).SampleID == -99
                    out(i) = NaN;
                    continue;
                end
                
                myruns = obj(i).getLinksTo('ExpRun');
                
                myIterations = [myruns.IterationNo];
  
                assert( ...
                    max(myIterations) >= iterNo(i), ...
                    'Iteration no. is > than the max of Sample %u', ...
                    obj(i).SampleID);

                isBefore = myIterations <= iterNo(i);
                out(i) = sum(myruns(isBefore).getProp('NumCycles'));
            end
        end
        
        function out = unique(obj)
            % Discriminate objects by SampleID and return unique elements.
            [~, idx, ~] = unique(obj.SampleID);
            out = obj(idx);
        end
    end
    
end

