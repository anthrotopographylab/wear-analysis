% Plot data of lext files that Stemp sent.

close all;

dirStemp = '../../../../RoboCut/StempData/';

locLexts = regexp(ls([dirStemp 'B*'], '-1'), '(.*?)\n', 'tokens');

for i=1:5 %length(locLexts)
    
    filename = locLexts{i}{:};
    
    fileInfo = imfinfo(filename);
    
    [~, ~, Zrgb] =  ReadLext(filename, 1);
    [X, Y, Zd] = ReadLext(filename, 4);
    
   
    DisplayScan(Zd, Zrgb, X, Y);
    
    [zsurf, ~] = RemoveFormVec(X(:), Y(:),  Zd(:));
    [~, ~, Zsurf] = Vec2Grid(X(:), Y(:), zsurf);
    ZWin = WindowPolygonFrame(Zsurf, 10);
    [fx, fy, Zft] = DFTransform(X, Y, ZWin);
   
    figure;
    imDFT = image(fx, flip(fy), log(abs(Zft)), 'CDataMapping', 'scaled');
    set(gca, 'YDir', 'normal');
    
end