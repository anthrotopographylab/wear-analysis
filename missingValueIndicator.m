function indicator = missingValueIndicator( OBJECT )
%missingValueIndicator Returns standard missing value indicator for OBJECT.
%   Depending on TYPE following missing value indicator is returned:

% assert(isscalar(OBJECT), 'Input must be scalar');

%OBJECT = feval(sprintf('%s.empty', TYPE));

if isinteger(OBJECT)
    indicator = 0;
elseif isfloat(OBJECT)
    indicator = NaN;
elseif ischar(OBJECT)
    indicator = '';
elseif iscell(OBJECT)
    indicator = {};
elseif iscellstr(OBJECT)
    indicator = {''};
elseif isdatetime(OBJECT)
    indicator = NaT;
else
    % Call no arg-constructor of OBJECT
    indicator = feval(sprintf('%s', class(OBJECT)));
end
    

end

