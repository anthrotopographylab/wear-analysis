function [Tmeta, varargout] = QuerryMeta( specifiers, varargin )
% Querry metadata and apply optional formating of the resulting table.
%
% The pattern of the specifier is <table>.<attribute>

% USAGE
% [Tmeta] = QuerryMeta(attributes)
% [Tmeta] = QuerryMeta(attributes, rownames)
% [Tmeat, optOut] = QuerryMeta(..., Name, Value)
% 
%
% INPUT
% attributes:   Variable names to be querried as cell array.
% rownames:     Rownmaes to be querried, referring to the
%               first variable in attributes. As cell array of strings.
%
% Options parsed as Name-Value combinations:
% Specify equality condition as ATTRIBUTE = VALUE. Tmeta will only have
% records that fullfill this condition.
% Name:         -eq     
% Value1:       ATTRIBUTE 
% Value2:       VALUE
% 
% OUTPUT
% Tmeta:        Table holding the metadata querried. Rownames for which no
%               metadata could be found are filled with missing value
%               indicators.
% optOut:       -eq: optOut is the index of the rows of the metadata for
%               which the condition is true.

global PARA;

% CHEK INPUT **************************************************************

validateattributes(specifiers, {'cell'}, {'vector'}, mfilename, 'attributes');

if any(~cellfun(@ischar, specifiers))
    error(['MWA:' mfilename ':invalidInput'], ...
        'Attributes must be specified as cell vector of strings');
end  

rownames    = [];
option      = [];
optValue    = {[]};

optionsAll = {'-eq'};

if nargin >= 2
    % Second input is cell of rownames. The corresponding variable is defined
    % as the first attribute in the input.
    
    rownames = varargin{1};
    rnKey = specifiers{1};
    
    validateattributes(rownames, {'cell'}, {'vector'}, mfilename, 'rownames');
    
    if any(~cellfun(@ischar, rownames))
        error(['MWA:' mfilename ':invalidInput'], ...
            'Rownmaes must be specified as cell vector of strings');
    end
    
    numRows = length(rownames);
end
if nargin >= 4
    % Second and more inputs are options specified as name-value pair.
    
    option = varargin{2};
    optValue = varargin(3:end);
    
    if ~any(strcmp(option, optionsAll))
        error(['MWA:' mfilename ':invalidOption'], ...
            'Option must be one of %s', strjoin(optionsAll));
    end
    
    validateattributes(optValue{1}, {'char'}, {'vector'}, mfilename, 'ATTRIBUTE');
    validateattributes(optValue{2}, {'numeric', 'logical'}, {'scalar'}, mfilename, 'VALUE');
    
    
end


% MAIN ********************************************************************
% numAtt = length(specifiers);
% 
% attributes  = cell(1, numAtt);
% tables      = cell(1, numAtt);
% 
% isAttTabRel = false(numAtt, 3);
% 
% Tables = {'Expdoc', 'Scans', 'Tbig'};
% % Separate attributes of struct of images from attributes of the big table
% % 
% % attImages = fieldnames(META.Scans);
% % isAttIm = ismemeber(attributes, attImages);
% % 
% 
% % Split attribute specifiers into names of tables and attributes.
% % The pattern of the specifier is <table>.<attribute>
% specSplit = cellfun(@(a)regexp(a,'\.','split'), specifiers, ...
%     'UniformOutput', false);
% 
% isDual = cellfun(@length, specSplit) == 2;
% 
% attributes(~isDual) = specifiers(~isDual);
% 
% if any(isDual)
%     specDual = vertcat(specSplit{isDual});
%     attributes(isDual)  = specDual(:,2);
%     tables(isDual)      = specDual(:,1);
% 
%     % Save names of tables parsed through input. CHECK IF TABLES EXIST!!!
%     indexDual = find(isDual);
%     [is_t, indexT] = ismember(tables(indexDual), Tables);
%     isAttTabRel(indexDual(is_t), indexT) = true;
% 
% end
% 
% 
% varExpdoc = META.Expdoc.Properties.VariableNames;
% varScans = META.Scans.Properties.VariableNames;
% varTbig = META.Tpara.Properties.VariableNames;
% 
% % keyExpdoc = META.Expdoc.Properties.UserData.keys;
% % keyScans = META.Scans.Properties.UserData.keys;
% % keyTbig = META.Tpara.Properties.VariableNames;
% 
% nameLists = {varExpdoc, varScans, varTbig};
% 
% for i=1:length(nameLists)
%     %isTable = all(ismember(attributes, table));
%     
%     isAttTabRel(~isDual,i) = ismember(attributes(~isDual), nameLists{i});
%     
% end
% 
% % Choose combination of tables, which takes the least no of tables.
% ranking = sum(isAttTabRel, 1);
% isMulti = sum(isAttTabRel, 2) > 1;
% 
% [~, weight] = sort(ranking);
% 
% weightAttTabRel = isAttTabRel.*repmat(weight, numAtt, 1);
% 
% [~, indexTabOpt] = max(weightAttTabRel, [], 2);
% 
% tables = Tables(indexTabOpt);


attributes = specifiers;

% Find attributes in metadata. Also parse attribute for optional condition
% testing if given.
% Tmeta = JoinMeta( [attributes optValue{1}] );

Tmeta = PARA.Tpara( :, [attributes optValue{1}] );

% Find rows in metadata (optional)
if ~isempty(rownames)
    
    % Check key attribute
    key = Tmeta{1, rnKey};
    if iscell(key)
        key = key{:};
    end
    if ~ischar(key)
        error(['MWA:' mfilename ':noMeta'], ...
            'Key attribute (attributes{1}) must be of type string.');
    end
    
    % Show which rows can not be found in the metadata.
    [rDiff, ~] = setdiff(rownames, Tmeta{:, rnKey});
        
    if length(rDiff) == numRows
        error(['MWA:' mfilename ':noMeta'], ...
            'No metadata found for the records querried.');
    elseif ~isempty(rDiff)
        warning(['MWA:' mfilename ':missingMeta'], ...
            'Could not find %u out of the %u records querried.', ...
            length(rDiff), numRows); 
    end
    
    % Rearrange rows in Tmeta according to the rownames querried. 
    [hasMeta, indexTmeta] = ismember(rownames, Tmeta.(rnKey));
    
    rowIndex = 1:numRows;
    
    Tmeta(rowIndex(hasMeta), :) = Tmeta(indexTmeta(hasMeta), :);
    
    Tmeta.(rnKey)(rowIndex) = rownames;   % add remaining names
    
    % Fill rows without values with missing value markers
    if any(~hasMeta)
        for i=2:width(Tmeta)
            if iscell(Tmeta{:,i})
                Tmeta{rowIndex(~hasMeta), i} = '';
            elseif isnumeric(Tmeta{:,i})
                Tmeta{rowIndex(~hasMeta), i} = NaN;
            else
                Tmeta{rowIndex(~hasMeta), i} = NaN;
            end
        end
    end
    
    %     Tmeta = standardizeMissing(Tmeta); 
    
    % Crop tail if required
    if height(Tmeta) > numRows
        Tmeta(numRows+1:end, :) = [];
    end
    
end


% attImages = setdiff(fieldnames(META.Scans), 'Scanname');
% 
% isAttIm = ismemeber(attributes, attImages);
% 
% [indexRN, indexSN] = ismemeber(rownames, META.Scans.Scanname);


    
% Apply optional conditions
if strcmp(option, '-eq')

    indexCondition = find(Tmeta{:, optValue{1}} == optValue{2});
    
    Tmeta = Tmeta(indexCondition, attributes);

    varargout{1} = indexCondition;
end

end

