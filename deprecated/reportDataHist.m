% Plot slections and corresponding parameters in a report

% clear all;
close all;

SetGlobalVariables();

global ...
    LOC_LOGBOOK ...
    LOC_METADATA ...
    DIR_BASE ...
    DIR_RESULTS

% get data from matfile
locationData = [DIR_BASE 'exp/Data_Erdogan.mat'];
dirResults = DIR_RESULTS;

% Data variables
Selections = SelectBig;
Scans = Scans;
Para = ParaBig;

sigmaWorld = [60 40];

[namelist, indexMeta, indexScan, indexSelect, indexPara] = ...
    IntersectMulti(META, Scans, Selections, Para);

MetaSort = META(indexMeta);
ScanSort = Scans(indexScan, :);
SelectSort = Selections(indexSelect, :);
ParaSort = Para(indexPara, :);

fileList = {};

% time to pause before figure printin to account for bug [sec]
pauseTime = 0.01; 

% Title figure to be placed at top of a page
figTitle = figure;
set(figTitle, 'Units', 'centimeters', 'Position', [30 5 15 2]);
set(figTitle, 'resize', 'off');
set(figTitle, 'ToolBar', 'none');
set(figTitle, 'MenuBar', 'none');
txTitle = text(0.05, 0.5, 'Title');
set(txTitle, 'FontSize', 14);
set(gca, 'Visible', 'off');

% Data figure to show e.g. parameter values
figData = figure;
set(figData, 'Units', 'centimeters', 'Position', [30 5 15 10]);
set(figData, 'resize', 'off');
set(figData, 'ToolBar', 'none');
set(figData, 'MenuBar', 'none');
txData = text(0.05, 0.5, 'Data');
set(txData, 'FontSize', 12);
set(txData, 'VerticalAlignment', 'middle');
set(txData, 'HorizontalAlignment', 'left');
set(gca, 'Visible', 'off');


for i=1:3 %    size(namelist, 1)
    
    scanname = namelist{i};
    X = ScanSort{i,2};
    Y = ScanSort{i,3};
    scanD = ScanSort{i,4};
    scanRGB = ScanSort{i,5};
    
    currentSelection = SelectSort(i,:);    
    currentParameters = ParaSort(i,:);
    
    % Resize if necessary
    if ~isequal([size(scanRGB,1) size(scanRGB,2)], size(scanD));
        scanRGB = imresize(scanRGB, size(scanD));
    end
    
%     % Display the measurement with zoom factor 0.8
%     figWholeScanD = DisplayScan([{scanname}, {X}, {Y}, {scanD}, {[]}], 0.8);
%     figWholeScanRGB = DisplayScan([{[]}, {X}, {Y}, {scanRGB}, {[]}], 0.8);
%     
%     pause(pauseTime);
%     
%     fileList{end+1} = PrintFigures(figWholeScanD, figWholeScanRGB);
%     close(figWholeScanD, figWholeScanRGB);
    
    set(txTitle, 'String',[scanname ' Surface parameters of all selections']);
    
    % MAKE SelectScanRegion SHOWING ONLY
    figWholeSelection = SelectScanRegion(scanD, scanRGB, X, Y, currentSelection);
    
%     paraTable = struct2table(currentParameters{2});
%     paraDisp = evalc('disp(paraTable)');
    
    paraDisp = num2str([[1:length(currentParameters{2})]' ...
        [currentParameters{2}.Sq]' ...
        [currentParameters{2}.Sa]' ...
        [currentParameters{2}.Ssk]' ...
        [currentParameters{2}.Sku]']);
    
%     paraDisp = ['Sq\tSa\tSsk\tSku\n' paraDisp];
    set(txData, 'String', paraDisp);
    
    fileList{end+1} = PrintFigures(figTitle, figWholeSelection, figData);
    close(figWholeSelection);
    
    
    % Filter surface of the scan
    % Interpolate NaNs and remove plane
    [Zplane, ~] = RemoveForm(X, Y, Inpaint_nans(scanD, 1), 'plane');

    % Filter out the form 
    [Zsurf, ~] = RemoveForm(X, Y, Zplane, 'gauss', sigmaWorld);

    % Set invalid areas back to NaN
    Zsurf( isnan(scanD) ) = NaN;
    
    
    % Plot histogram for each polygon
    for j=1:length(currentSelection{2})
        
        polygon = currentSelection{2}{j};
    
        Roi = Polygon2Roi(scanD, polygon, X, Y);
        
%         [zsurf, ~] = RemoveForm(X(Roi), Y(Roi), Zsurf(Roi), 'plane');
        [zmean, ~] = RemoveMeanVec(X(Roi), Y(Roi), Zsurf(Roi));
        
%         [frameX, frameY, frameRaw] = FrameArray(Roi, X, Y, scanD);  
        [frameX, frameY, frameRoi] = FrameArray(Roi, X, Y, Roi);
        
        frameFiltVec = zeros(size(frameRoi));
        frameFiltVec(logical(frameRoi)) = zmean;
        
        frameFilt = reshape(frameFiltVec, size(frameRoi));
        
        % Plot framed and windowed selection with zoom
        figPoly = DisplayScan( ...
            [{sprintf('Area %u, 0.7x zoom', j)} ...
            {frameX} {frameY} {frameFilt} {[]}], 0.7);
        
        figHist = figure;
        histogram(currentParameters{2}(j).data);
        xlabel('Distance [um]');
        ylabel('Ratio');
        title('Deviation from mean height of the filtered scan');
                      
        % Show parameters of this selection
        paraDisp = evalc([ ...
            'disp(currentParameters{2}(j)), ' ...
            'disp(currentSelection{3}(j))']);
        set(txData, 'string', paraDisp);
        
        % Bug fix to show colorbar properly
        pause(pauseTime);
        
        % Print figure to pdf
        fileList{end+1} = PrintFigures(figPoly, figHist, figData);
        close(figPoly, figHist);
        
%         keyboard
    end
    
end

close(figTitle, figData);

% Concatenate pdfs to one single file
space = repmat({' '}, 1, length(fileList));
bsArgFiles = cell2mat(reshape([fileList;space], 1, 2*length(fileList)));

bsCommand = ...
    ['pdftk ' bsArgFiles ' cat output ' dirResults 'report_' date '.pdf'];
system(bsCommand);

cellfun(@delete, fileList);  % delete temporary filess
