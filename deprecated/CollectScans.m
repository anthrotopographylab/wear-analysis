function [S, ScansAlicona] = CollectData(index)
% Read 3D scans from txt files and RGB scans from bmps and write to struct.
% Use the metadata of the database to locate the files.
% The output is the cell array Scans which is formated as described in
% ConsistencyScans() and struct array of the metadata provided by Alicona
% for each scan.

% Load global variables into workspace
% SetGlobalVariables();


META_DIR_DATA       = 'Directory';
META_NAME           = 'Scanname';
META_IS_3D          = 'is3D';
META_IS_RGB         = 'isRGB';
META_IS_TXT         = 'isExport';
META_IS_COMPLETE    = 'isComplete';


metadata = table2struct(index);

numScans = height(index);

Scans = cell(numScans,6);       % store scans in this cell array
% scanInfo = [];                  % store metadata of scans in struct array

ScansAlicona = AliconaImage.empty(0, numScans);

for i=1:numScans

    metadata(i).(META_IS_COMPLETE) = false; % by default
    
    scanname = metadata(i).(META_NAME);
    directory = metadata(i).(META_DIR_DATA);
        
    % Read the info file of this scan and store.
    % If data can not be received throw warning and continue.
    try 
        scaninfo = ReadScanInfo(fullfile(directory, 'info.xml'));
        if strcmp(scaninfo.Scanname, scanname)
%             if isempty(scanInfo)
%                 scanInfo = scaninfo;
%             else
%                 scanInfo(end+1) = scaninfo;
%             end
        else
            error('MWA:CollectData:dataInconsistency', ...
                'Scannames of index and info.xml inconsistent in %s.', ...
                directory);
        end
    catch MA
        warning('MWA:CollectData:IOerror', ...
            'Failure while reading info file of %s:\n%s', scanname, MA.message);
    end
        
    % RGB data:
    if metadata(i).(META_IS_RGB)
        
        scanRGB = imread([directory '/' 'texture.bmp']);
        
        if isempty(scanRGB)
            warning('MWA:CollectData:IOerror', ...
                'Could not read RGB data file of %s. Skip...', scanname);
            continue;
        end
        
        scanRGB = flipud(scanRGB); % sync orientation with D scan
    end
        
     % 3D data:
    if metadata(i).(META_IS_3D) && metadata(i).(META_IS_TXT)

        [x, y, z] = ReadScan([directory '/' 'export.txt']);

        if isempty(x) || isempty(y) || isempty(z)
            warning('MWA:CollectData:IOerror', ...
                'Could not read depth data file of %s. Skip...', scanname);
            continue;
        end

        [X, Y, Z] = Vec2Grid(x, y, z);

        % Read Qualitymap and correct orientation
        Qualitymap = flipud(imread([directory '/' 'qualitymap.bmp']));
        
        if ~isempty(scanRGB)
        
            sizeD   = size(Z);
            sizeRGB = size(scanRGB);

            if sizeD(1) < sizeRGB(1) || sizeD(2) < sizeRGB(2)
                % Crop off rows and columns of RGB image to match the size
                % of the depth scan. Inconsistency in size is caused by the
                % export-to-txt process.

                % Do this using crosscorelation
                             
                Dbw = im2bw(abs(Z));
                RGBbw = im2bw(scanRGB, 0.001);
                
                Xcorr = normxcorr2(Dbw,RGBbw);
                
                [ypeak, xpeak] = find(Xcorr==max(Xcorr(:)));
                ymin = ypeak-size(Dbw, 1)+1;
                xmin = xpeak-size(Dbw, 2)+1;
                
                
                if (sizeRGB(1) < ypeak || sizeRGB(2) < xpeak || ...
                    ymin > ypeak || xmin > xpeak )
                    warning('Could not crop RGB scan of %s', scanname');
                else
                    scanRGB = scanRGB(ymin:ypeak, xmin:xpeak, :);
                    Qualitymap = Qualitymap(ymin:ypeak, xmin:xpeak);
                    fprintf('Cropped RGB scan of %s.\n', scanname);
                end
            end

            % Double check for consistency of size
            sizeRGB = [size(scanRGB,1) size(scanRGB,2)];

            % Do this in the ConsistencyScans():
            
            if isequal(size(X), size(Y)) && ...
                isequal(size(X), sizeD) && ...
                isequal(size(X), sizeRGB) 
            
                % all good
                Scans{i, 2} = double(X);
                Scans{i, 3} = double(Y);
                Scans{i, 4} = double(Z);
                Scans{i, 6} = Qualitymap;
%                 ScansAlicona(i) = AliconaImage(scanname, scanRGB, X, Y, Z);
            elseif  ~isequal(sizeD, sizeRGB)
                % distinguish between small and larger difference in size
                if rms(sizeD-sizeRGB)/rms(sizeD) < 0.05
                    warning('MWA:ConsistencyScans:dataInconsistency', ...
                        ['The size of depth scan and RGB scan ' ...
                        'slightly mismatch for scan %s'], scanname);
                else
                    warning('MWA:ConsistencyScans:dataInconsistency', ...
                        ['The size of depth scan and RGB scan ' ...
                        'mismatch for scan %s. Discard depth data.'], scanname);
                    continue;
                end
%                 ScansAlicona(i) = AliconaImage(scanname, scanRGB);
            else
                % Dimensions of coordinates matrix inconsistent
                warning('MWA:ConsistencyScans:dataInconsistency', ...
                    ['The coordinate matrix size is inconsistent ' ...
                    'for scan %s. Discard'], scanname);
                continue;
            end
        end
        
%         f1=figure;
%         plot(1:size(X,2), X(1,:), 1:length(unique(x)), unique(x));
%         
%         f2=figure;
%         plot(1:size(Y,1), Y(:,1), 1:length(unique(y)), unique(y));
%         
%         DisplayScan(Z, scanRGB, X, Y);
%         
%         close(f1, f2);
        
    else
        
%         ScansAlicona(i) = AliconaImage(scanname, scanRGB);
        
    end
    
    Scans{i, 1} = scanname;
    Scans{i, 5} = scanRGB;

    metadata(i).(META_IS_COMPLETE) = true;
    
%     % Convert scanInfo to table
%     ScansAlicona(i).info = struct2table(scaninfo);
    
end

% Convert cell of scans to struct
S = cell2table(Scans);
S.Properties.VariableNames = ...
    {'Scanname', 'XGrid', 'YGrid', 'ImageTopo', 'ImageRGB', 'Qualitymap'};
S.Properties.UserData = struct( ...
    'this', {'Scans'}, ...
    'keys', {{'Scanname'}});


end


