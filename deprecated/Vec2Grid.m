function [X, Y, Z] = Vec2Grid(x, y, z)
% Converts grid data in vector format to grid format using meshgrid.
% Each x y z tripple corresponds to one point. Points on
% the grid, which have no corresponding x y z tripple in the data 
% are assigned to NaNs. If there are steps in the x or y coordinates, the
% missing regions are filled with NaNs such that one large continuous region
% results.

validateattributes(x, {'numeric'}, {'column'}, mfilename, 'x');
validateattributes(y, {'numeric'}, {'size', size(x)}, mfilename, 'y');
validateattributes(z, {'numeric'}, {'size', size(x)}, mfilename, 'z');

% Threshold factor for classification of steps in x-y spacing
resThr = 1.2;

% input: data holds the point cloud with 
% x = data(:,1)
% y = data(:,2)
% z = data(:,3)

% Find unique vertices which will be used as grid vectors.
xu=unique(x);
yu=unique(y);

% Estimate resolution
xresM = median(diff(xu));
yresM = median(diff(yu));

% Find non-continuous regions in x-y coordinates and fill with sampling
% points if any. Call function for x and for y. The function iterates
% thorugh the gaps in the vector of sampling points until all gaps are
% filled.
xGrid = fillGaps(xu, xresM);
yGrid = fillGaps(yu, yresM);

    function grid = fillGaps(vec, res)
        % Initialize
        grid = vec';
        flag = true;

        while flag
            spacing = diff(grid);

            % 2. Identify steps vector
            steps = find(spacing>res*resThr);
            if isempty(steps)
                idxS = [];
            else
                idxS = steps(1);
            end
            
            % Estimate no of sampling points to add
            noSampAdd = floor(spacing(idxS)/res)-1;
            
            if isempty(noSampAdd) || noSampAdd==0
                flag = false;
            else
                assert(noSampAdd > 0);  % double check

                % Fill the gap and update the grid vector
                grid = [grid(1:idxS) ...
                        grid(idxS)+(1:noSampAdd)*res ...
                        grid(idxS+1:end)]; 
            end
        end
        grid = grid';
    end
    
% % 2. Identify largest continuous region. Variations in spacing are
% % tolerated up to resThr times the actual spacing.
% stepsX = [1 find((diff(xu)>xresM*resThr))+1 length(xu)];
% stepsY = [1 find((diff(yu)>yresM*resThr))+1 length(yu)];
% 
% [~, cx] = max(diff(stepsX));
% [~, cy] = max(diff(stepsY));
% 
% idxXstart = stepsX(cx);
% idxYstart = stepsY(cy);
% 
% idxXend = stepsX(cx+1)-1;
% idxYend = stepsY(cy+1)-1;
% 
% % 3. Crop grid vectors
% xGrid = xu(idxXstart:idxXend);
% yGrid = yu(idxYstart:idxYend);

% Build grid
numVertX = size(xGrid,1); % # vertices in x direction
numVertY = size(yGrid,1); % # vertices in y direction

[X, Y] = meshgrid(xGrid,yGrid);

% build height matrix with NaN where sample is missing
Z = NaN(numVertY, numVertX);

% data = [x y z];

% Go thorugh all samples in [x y z] and fill up the grid for Z
for i=1:length(z)
    [~, idxX] = min(abs(X(1,:)-x(i)));
    [~, idxY] = min(abs(Y(:,1)-y(i)));
    
    Z(idxY, idxX) = z(i);
    
%     % Compare complete row of samples  with possibly incomplete row of
%     % samples dataRowi. Copy available samples in dataRowi to cooresponding
%     % position in Z.
%     vecRowX = data( y==yGrid(i), 1);
%     vecRowZ = data( y==yGrid(i), 3);
%     
%     [isSampleG, isSampleV] = ismember(xGrid, vecRowX);
%     
%     Z(i, isSampleG) = vecRowZ( isSampleV, 3);

end

end