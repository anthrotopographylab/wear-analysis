function surface = SurfaceArea(X, Y, Z)
% Compute the surface area of the given scan.

SpacingX = unique(X, 'rows');
SpacingY = unique(Y','rows');

% Input mesh grid must be regular, i.e. crossings are always rectangular.
assert( size(SpacingX, 1) == 1);
assert( size(SpacingY, 1) == 1);

% Interpolate non existend sampling points if any.
% Use function inpaint_nans by John D'Errico (mathworks.com).
Zinterp = Inpaint_nans(Z, 1); % 1=linear

% Compute normals at each node
[Nx, Ny, Nz]=surfnorm(X,Y,Zinterp);

% Compute 1/cos( sekant angle )
S = sqrt(Nx.^2+Ny.^2+Nz.^2)./Nz; 

% Integrate over S
surface = trapz(SpacingX, S, 2);            % Integral along x
surface = trapz(SpacingY, surface, 1);      % Integral along y



end