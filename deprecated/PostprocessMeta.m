function out = PostprocessMeta( logbook )
% Make some initial processing steps on the metadata:
% - Compute accumulated number cycles up to each iteration.

LB_ITER_NO      = 'IterationNo';
LB_NUM_CYC      = 'NumCycles';
LB_SID          = 'SampleID';
LB_NUM_CYC_CUM  = 'NumCycCum';

% logbook = META.tables.logbook;

for i = 1:height(logbook)
    sid = logbook{i, LB_SID};
    
    if ~isnan(sid)
        ino_i = logbook{i, LB_ITER_NO};

        % Compute total number cycles up to iteration <ino_i>
        cyclesTot = 0;
        for ino_j = ino_i:-1:1    % track back all iterations

            isIterNo_j = logbook.(LB_ITER_NO) == ino_j;
            isSID = logbook.(LB_SID) == sid;

            if sum(isIterNo_j & isSID) == 1
                % all good
                cyclesTot = cyclesTot + logbook{isIterNo_j & isSID, LB_NUM_CYC};
            else
                warning('MWA:FuseStats:dataInconsistency', ...
                    ['No unique sample-iteration pair found for S%u at use%u.' ...
                    ' Experiment might be incomplete.'], ...
                    sid, ino_j);
            end

        end

        % Add to logbook
        logbook{i, LB_NUM_CYC_CUM} = cyclesTot;
    end

end

out = logbook;


end

