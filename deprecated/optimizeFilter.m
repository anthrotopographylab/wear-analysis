% Take regression on surface roughness vs. label and optimize the filter
% parameter such that the RMS of the slopes is maximized. 

global META

SetGlobalVariables();

% Crop metadata
[namelist, indexScan, indexSelect, indexMeta] = IntersectMulti(Scans, SelectBig, META);
MetaSort = META(indexMeta);
ScanSort = Scans(indexScan, :);
SelectSort = SelectBig(indexSelect, :);

% Sort values by parameter
% paraAll = [Parameters{:,2}];

is100x = [MetaSort.Magnification] == 100;
is50x = [MetaSort.Magnification] == 50;


% global Scans Selections

myCost = @(sigma) costParaLabel(sigma, ScanSort(is100x, :), SelectSort(is100x, :));

epsFun = 0.001;    % relative cost function tolerance 
epsX = 0.1;        % relative argument tolerance

% cost50 = myCost(4);

% Options for fminsearch
fminOpts = optimset( ...
    'TolFun', epsFun, ...
    'TolX', epsX, ...
    'FunValCheck', 'on', ...
    'Display', 'iter', ...
    'PlotFcns', @optimplotfval);

warning('off', 'MWA:RemoveForm:semanticFail');
warning('off', 'MWA:ConsistencyScans:dataInconsistency');

% Initialize search with sigma = ... 
xmin = fminsearch(myCost, 4, fminOpts);

warning('on', 'MWA:RemoveForm:semanticFail');
warning('on', 'MWA:ConsistencyScans:dataInconsistency');
