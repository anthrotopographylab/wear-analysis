function [subsetAll, varargout ] = IntersectMulti( varargin )
% Applies Matlabs intersect function multiple times on cell arrays.
% This can be used to find the intersections of 2 or more cell arrays.
% The comparison is done on the first column of the cell array, i.e. where
% Scans, Parameters and Selections have the scanname. 
% The order of the first input argument is taking precedence. If META is 
% given as input it is evaluate on the field META_NAME.

global META_NAME

% CHECK INPUT *************************************************************
numIn = length(varargin);
numOutVar = nargout-1;
input = cell(1, numIn);

if numIn < 2
    error('MWA:SortMetadata:invalidInput', ...
        'The number of inputs must be greater than 1.');
end
   
if numOutVar ~= numIn
    error('MWA:SortMetadata:invalidInput', ...
        'The number of outputs must be number of inputs + 1.');
end

for i=1:numIn
    validateattributes(varargin{i}, ...
        {'cell', 'struct'}, {'2d', 'nonempty'}, '', sprintf('# %u', i));
    if isstruct(varargin{i})
        input{i} = {varargin{i}.(META_NAME)}';
    else
        input(i) = varargin(i);
    end
end


% SORT ********************************************************************

% Find subset common to all inputs
% initialize
[subset, ~, ~] = intersect(input{1}(:,1), input{2}(:,1), 'stable');

if numIn > 2
    for i=3:numIn
        [subset, ~, ~] = intersect(subset, input{i}(:,1), 'stable');
    end
end

subsetAll = subset;
varargout = cell(size(varargin));

% Find indices for each input corresponding to subsetAll
for i=1:numOutVar
    [~, ~, varargout{i}] = intersect(subsetAll, input{i}(:,1), 'stable');
end


% % initialize
% varargout{end} = indexI{end};
% indexSub = indexC{end};
% 
% for i=(numIn-1):-1:2
%     varargout{i} = indexI{i}(indexSub);
%     indexSub = indexC{i}(indexSub);
% end
% 
% varargout{1} = indexI{1}(indexSub);   % finish

end

