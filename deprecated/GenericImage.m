classdef GenericImage < AbstractTableRecord %< AbstractLinkable %& 
    
     % Inherited from TableRecord
    properties (SetAccess = private)
        ID          = '';
    end
    properties (Constant = true)
        tableName   = 'IndexScans';
        idName      = 'Scanname';
    end
    
    properties (SetAccess = private)
        name        = '';
    end
    
    properties %(SetAccess = {?Selection})
        selections  = Selection.empty(1,0);   % array of Select objects
    end
    properties (Dependent = true)
        hasSelection
    end
    
    methods 
%         function obj = GenericImage(name)
%             % USAGE
%             % GenericImage(name)
%             % name:     String as unique name of this image.
%             
%             % Set up linkable types
%             obj.setLinkableTypes({'Selection'});
%             
%             if nargin > 0
%                 obj.name = name;
%             else
%                 % use default values
%             end
%         end
        
        function obj = name.set(obj, name)
            validateattributes(name, {'char'}, {'2d'}, '', 'name');
            obj.name = name;
        end
        
        function flag = get.hasSelection(obj)
            if isempty(obj.selections)
                flag = false;
            else
                flag = true;
            end
        end
        
        function addSelection(obj, selection)
            % Stack new association with selection into array and
            % create reverse reference (to this GenericImage) in selection.
            validateattributes(selection, ...
                {'Selection'}, {'vector'}, '', 'selection');
            
            % Add image to selection 
            selection.image = obj;
            
            % Check wheather selections exist
            isSame = obj.selections == selection;
            obj.selections = obj.selections(~isSame);
            
            if false
                 % Check if selection already exists; use an ID.
            else
                % Concatenate depending on orientation of vector
                if isrow(selection)
                    obj.selections = horzcat(obj.selections, selection);
                else
                    obj.selections = horzcat(obj.selections, selection'); 
                end
            end
        end
        
        function links = breakLinks(obj)
            % Return array of handles to Selection objects
            links = [obj.selections];
            
            % Delete all handles in array, obj
            arrayfun(breakLinkSingle, obj);
            function breakLinkSingle(objSingle)
                objSingle.selections = Selections.empty(1,0);
            end
        end
            
        
        function objSorted = sort(obj)
            [~, index] = sort({obj.name});
            objSorted = obj(index);
        end
        
        function objUni = unique(obj)
            [~, index, ~] = unique({obj.name});
            objUni = obj(index);
        end
        
        function  sobj = saveobj(obj)
            sobj.name = obj.name;
        end
    end
    
    methods (Static = true)
        function obj = loadobj(data)
            obj = GenericImage(data.name);
        end
    end
    
end