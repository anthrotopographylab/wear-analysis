function [ out ] = SortByLabel( Parameters, labels )
% Sort Parameters according to the labels given.
% Returns cell vector having the size of lables and containing parameters
% as structs.

% CHEKC INPUT *************************************************************
% ConsistencyParameters(Parameters)

validateattributes(labels, {'cell'}, {'vector'}, '', 'lables');


for i=1:length(labels)
    
    paraAll = [Parameters{:,2}];
    labelAll = [Parameters{:,3}];
    
    assert(isequal(size(labelAll), size(paraAll)));
    
    isLabel = strcmp(labelAll, labels{i});
    paraLabel = paraAll(isLabel);
    
    if isempty(paraLabel)
        out{i} = struct('Sq', [], 'Sa', [], 'Sku', [], 'Ssk', []);
    else
        out{i} = paraLabel;
    end
end


end

