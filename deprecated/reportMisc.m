% Plot various Data in several figures and generate PDF file from it.
% 
 
% clear all;
close all;

SetGlobalVariables();

global ...
    LOC_LOGBOOK ...
    LOC_METADATA ...
    DIR_BASE ...

% get data from matfile
locationData = [DIR_BASE 'exp/Data_20160512.mat'];
dirResults = [DIR_BASE 'results/'];

% load(locationData);
Selection = [selection1; selection2; selection3];
% Scans = scans(meta.istxt, :);

fileList = {};

% time to pause before figure printin to account for bug [sec]
pauseTime = 0.01; 

% Title figure to be placed at top of a page
figTitle = figure;
set(figTitle, 'Units', 'centimeters', 'Position', [30 5 15 2]);
set(figTitle, 'resize', 'off');
set(figTitle, 'ToolBar', 'none');
set(figTitle, 'MenuBar', 'none');
txTitle = text(0.05, 0.5, 'Title');
set(txTitle, 'FontSize', 14);
set(gca, 'Visible', 'off');

% Data figure to show e.g. parameter values
figData = figure;
set(figData, 'Units', 'centimeters', 'Position', [30 5 15 10]);
set(figData, 'resize', 'off');
set(figData, 'ToolBar', 'none');
set(figData, 'MenuBar', 'none');
txData = text(0.05, 0.5, 'Data');
set(txData, 'FontSize', 12);
set(txData, 'VerticalAlignment', 'middle');
set(txData, 'HorizontalAlignment', 'left');
set(gca, 'Visible', 'off');


for i=1:size(Scans, 1)
    
    scanname = Scans{i, 1};
    X = Scans{i,2};
    Y = Scans{i,3};
    scanD = Scans{i,4};
    scanRGB = Scans{i,5};
    
    if isempty(X) || isempty(Y) || isempty(scanRGB) || isempty(scanD)
        warning('Empty elements at scan %u', i);
        continue;
    end
    
    currentSelection = Selection(strcmp(scanname, Selection(:,1)), :);
    
    currentParameters = parameters(strcmp(scanname, parameters(:,1)), :);
    
    % Resize if necessary
    if ~isequal([size(scanRGB,1) size(scanRGB,2)], size(scanD));
        scanRGB = imresize(scanRGB, size(scanD));
    end
    
    % Display the measurement with zoom factor 0.8
    figWholeScanD = DisplayScan([{scanname}, {X}, {Y}, {scanD}, {[]}], 0.8);
    figWholeScanRGB = DisplayScan([{[]}, {X}, {Y}, {scanRGB}, {[]}], 0.8);
    
    pause(pauseTime);
    
    fileList{end+1} = PrintFigures(figWholeScanD, figWholeScanRGB);
    close(figWholeScanD, figWholeScanRGB);
    
    if isempty(currentSelection) || isempty(currentParameters)
        continue;
    end
    
    if ~strcmp(currentSelection{1}, currentParameters{1})
        warning('Parameters do not match selection of scan %u', i);
        continue;
    end
    
    set(txTitle, 'String', 'Surface parameters of each selection');
    
    figWholeSelection = SelectScanRegion(scanD, scanRGB, X, Y, currentSelection);
    
%     paraTable = struct2table(currentParameters{2});
%     paraDisp = evalc('disp(paraTable)');
    
    paraDisp = num2str([[1:length(currentParameters{2})]' ...
        [currentParameters{2}.Sq]' ...
        [currentParameters{2}.Sa]' ...
        [currentParameters{2}.Ssk]' ...
        [currentParameters{2}.Sku]']);
    
%     paraDisp = ['Sq\tSa\tSsk\tSku\n' paraDisp];
    set(txData, 'String', paraDisp);
    
    % Make DFT of the complete scan.
    % Apply interpolation for corrupted samples (NaNs)
    Zinterp = Inpaint_nans(scanD, 2);
    [zsurf, ~] = RemoveFormVec(X(:), Y(:),  Zinterp(:));
    [~, ~, Zsurf] = Vec2Grid(X(:), Y(:), zsurf);
    ZWin = WindowPolygonFrame(Zsurf, 10);
    [fx, fy, Zft] = DFTransform(X, Y, ZWin);
    
    % Plot DFT
    figDFTAll = figure;
    imDFT = image(fx, flip(fy), log(abs(Zft)), 'CDataMapping', 'scaled');
    set(gca, 'YDir', 'normal');
    xlabel('Spatial frequency in x [1/um]');
    ylabel('Spatial frequency in y [1/um]');
    zlabel('log(abs(F(fx,fy)))');
    title('(Normalized) DFT of the whole scan');
    colormap(jet);
    set(imDFT.Parent, 'Units', 'pixels');
    imPos = get(imDFT.Parent, 'Position');
    cPos = [imPos(1)+imPos(3)+10 imPos(2) 20 imPos(4)]; 
    c = colorbar(imDFT.Parent, 'Units', 'pixels', 'Position', cPos);
    ylabel(c, 'log( |F(fx,fy)| / (n*m*polyFrac) )');
    
    pause(pauseTime); %bug fix
    
    fileList{end+1} = PrintFigures(figDFTAll);
    fileList{end+1} = PrintFigures(figTitle, figWholeSelection, figData);
    close(figWholeSelection, figDFTAll);
    
    % make DFTs of each selection
    for j=1:length(currentSelection{2})
        polygon = currentSelection{2}{j};
    
        Roi = Polygon2Roi(scanD, polygon, X, Y);
        
        zsurf = RemoveFormVec(X(Roi), Y(Roi), scanD(Roi));
        
        [frameX, frameY, frameZsurf] = FrameArray( ...
            Roi, X, Y, RemoveFormVec(X(Roi), Y(Roi), scanD(Roi)));

        frameZWin = WindowPolygonFrame(frameZsurf, 10);

        [fx, fy, Zft] = DFTransform(frameX, frameY, frameZWin);
        
        % Plot framed and windowed selection with zoom
        figPolyWin = DisplayScan( ...
            [{sprintf('Area %u, 3x zoom', j)} ...
            {frameX} {frameY} {frameZWin} {[]}], 3);
        
        % Plot DFT
        figDFT = figure;
        imDFT = image(fx, flip(fy), log(abs(Zft)), 'CDataMapping', 'scaled');
        set(gca, 'YDir', 'normal');
        xlabel('Spatial frequency in x [1/um]');
        ylabel('Spatial frequency in y [1/um]');
        zlabel('log(abs(F(fx,fy)))');
        title('(Normalized) DFT');
        colormap(jet);
        set(imDFT.Parent, 'Units', 'pixels');
        imPos = get(imDFT.Parent, 'Position');
        cPos = [imPos(1)+imPos(3)+10 imPos(2) 20 imPos(4)]; 
        c = colorbar(imDFT.Parent, 'Units', 'pixels', 'Position', cPos);
        ylabel(c, 'log( |F(fx,fy)| / (n*m*polyFrac) )');
        
        % Show parameters of this selection
        paraDisp = evalc([ ...
            'disp(currentParameters{2}(j)), ' ...
            'disp(currentSelection{3}(j))']);
        set(txData, 'string', paraDisp);
        
        % Bug fix to show colorbar properly
        pause(pauseTime);
        
        % Print figure to pdf
        fileList{end+1} = PrintFigures(figPolyWin, figDFT, figData);
        close(figPolyWin, figDFT);
        
%         keyboard
    end
    
end

close(figTitle, figData);

% Concatenate pdfs to one single file
space = repmat({' '}, 1, length(fileList));
bsArgFiles = cell2mat(reshape([fileList;space], 1, 2*length(fileList)));

bsCommand = ...
    ['pdftk ' bsArgFiles ' cat output ' dirResults 'report_' date '.pdf'];
system(bsCommand);

cellfun(@delete, fileList);  % delete temporary filess



