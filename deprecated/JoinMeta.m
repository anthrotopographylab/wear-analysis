function [ out ] = JoinMeta( attributes )
% Join metadata variables from different tables into one tables.
% Uses MATLABs outerjoin for the joining operation. 

% INPUT
% attributes:   Variable names specified in cell vector.
% OUTPUT
% Tjoin:        Table holding desired variables.

global META


% CHECK INPUT *************************************************************
validateattributes(attributes, {'cell'}, {'vector'}, mfilename, 'attributes');


% Find tables corresponding to the desired attributes. Generate an index of
% attributes and corresponding tables, as
% Tidx = [attributes(:)', tables(:)']
tables = META.tables;

Tidx = cell(0,2);

structfun(@generateTidx, tables);

% [~, idxU, ~] = unique(Tidx(:,1));
% Tidx = Tidx(idxU, :);

function generateTidx(mytable)
    % Select attributes that belong to this table
    match = intersect(mytable.Properties.VariableNames, attributes);
    
    % Take out key variables
    match = setdiff(match, mytable.Properties.UserData.keys);
        
    Tidx = cat(1, Tidx, ...
        [match', repmat({mytable.Properties.UserData.this}, length(match), 1)]);
end



% Choose joining operations depending on different combinations of tables.
% Joining takes into account how the tables are related to each other 
% through keys variables.
tablesHit = sort(unique(Tidx(:,2)));

numTables = length(tablesHit);

switch numTables
    case 1
        % No join. Just return the table with querried attributes.
        Tjoin = tables.(tablesHit{1})(:, attributes);
    case 2
        % Joining of indexScans with one other table
        tableMain = 'IndexScans';
        table2 = tablesHit{~strcmp(tableMain, tablesHit)};
        
        if isempty(table2)
            error('MWA:joinMeta:tableNotSupported', ...
                'No implementation jet for the variables requested');
        end
        
        % Set key variables depending on tables involved
        if any(ismember({'Logbook', 'IndexDoc', 'Expdoc'}, tablesHit))
            % tables are related through SID and IterNo
            keys = {'SampleID', 'IterationNo'};
        elseif any(strcmp({'Info'}, tablesHit))
            % tables are related through scanname
            keys = {'Scanname'};
        else
            error('MWA:joinMeta:featureNotSupported', ...
                'No implementation jet for the variables requested');
        end
        
        
        isAttrMain = strcmp(Tidx(:,2), tableMain);
        isAttr2 = strcmp(Tidx(:,2), table2);
        
        Tjoin = outerjoin( ...
             tables.(tableMain), tables.(table2), ...
            'Keys', keys, ...
            'MergeKeys', true, ...
            'Type', 'full', ...
            'leftvariables', Tidx(isAttrMain, 1), ...
            'rightvariables', Tidx(isAttr2, 1));
        
    otherwise
        error('MWA:joinMeta:invalidInput', ...
            'No implementation jet for the variables requested');
end

% Order variable names according to attributes
out = Tjoin(:, attributes);


end

