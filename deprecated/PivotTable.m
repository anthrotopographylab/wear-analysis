function T = PivotTable( mytable, pivot, aggregate )
% Make pivot table by aggregating rows with the same value for a variable.
% The aggregated rows can either be stored as a cell array or ditributed on
% auxiliary variables which are built by enumerating the original variable.
% INPUT
% mytable:      Table to be aggregated.
% pivot:        Variable to be aggregated.
% aggregate:    Flag to specify whether to aggregate into cell array or
%               into auxiliary vriables.

% USER INPUT **************************************************************
validateattributes(mytable, {'table'}, {'ndims', 2}, mfilename, 'table');
validateattributes(pivot, {'char'}, {'vector'}, mfilename, 'pivot');

% aggregate = 'cell';

%pivot = 'ExperimentID';


% MAIN ********************************************************************

% Remove rows which have no correspondence to pivot
validTable = mytable(~isnan(mytable.(pivot)), :);

% Get new rownames
rowNames = unique(validTable.(pivot));
rowNames = cellfun(@num2str, num2cell(unique(validTable.(pivot))), ...
    'UniformOutput', false);

G = findgroups(validTable.(pivot));

% Get new variable names
varNames = mytable.Properties.VariableNames( ...
    ~strcmp(mytable.Properties.VariableNames, pivot));
% isPivot = strcmp(varNames, pivot);
% varNames = varNames(~isPivot);

numSplits = max(splitapply(@length, validTable.(pivot), G));

% Enumerate each variable name
nameNo = strcat( '_', cellfun(@num2str, num2cell(1:numSplits), ...
    'UniformOutput', false));

% Write pivot table in here
T = table('RowNames', rowNames);

cellfun(@joinTables, varNames);

    function joinTables(varname)
        varSplit = splitapply(@collectGroup, validTable.(varname), G);
        Tadd = cell2table(varSplit, 'RowNames', rowNames);
        
        if strcmp(aggregate, 'variable')
            Tadd.Properties.VariableNames = enumerateName(varname);
        elseif strcmp(aggregate, 'cell')
            Tadd.Properties.VariableNames = {varname};
        else
            error('input error');
        end
        
        T = join(T, Tadd, 'keys', 'RowNames');    
    end

    function out = collectGroup(a)
        if strcmp(aggregate, 'variable')
            out = num2cell(NaN(1, numSplits));
            if iscell(a)
                out(1:length(a)) = a;
            else
                out(1:length(a)) = num2cell(a);
            end
        elseif strcmp(aggregate, 'cell')
            out = {a};
        else
            error('input error');
        end
    end
        

    function nameEnu = enumerateName(name)
        nameEnu = strcat(name, nameNo);
    end

end

