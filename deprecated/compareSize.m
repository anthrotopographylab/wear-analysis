% Calculate size (area) of all scans


meta = META([META.Magnification] >= 50 & [META.isTxt] & ~strcmp({META.Scanname}, 'S266_use1_pos1_x050'));

[~, a, b] = IntersectMulti(meta, Scans);

m = meta(a);
s = Scans(b,:);

Size = cell(length(s), 5);

for i=1:length(s)
    Size(i,1) = s(i,1);
    
    X = s{i,2};
    Y = s{i,3};
        
    if isstruct(m(i).ScanInfo)
        Size{i,3} = m(i).ScanInfo.Magnification;
    end
    
    Size{i,4} = m(i).Magnification;
    
    if abs(Size{i,3}-double(Size{i,4}))/double(Size{i,4}) < 0.1
        Size{i,5} = true;
    else
        Size{i,5} = false;
        
        % Set validity flag to zero
        META(strcmp({META.Scanname}, s(i,1))).isValid = false;
        
    end
    
    Size{i,2} = abs(X(1,1)-X(end,end)) * abs(Y(1,1)-Y(end,end));
    
    
    
end



