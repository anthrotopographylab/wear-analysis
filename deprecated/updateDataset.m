function DATA = updateDataset(varargin)
% Updates DATA with new values found in the directory trees.
%
% USAGE:
% updateDataset( ) 
% Reads the whole directory tree and returns new struct will all data.
% 
% updateDataset( DATA )
% Compares directory tree with elements in DATA and adds new elements to
% DATA. Existing elements in DATA are not changed.


global ...
    DIR_DATA ...
    DIR_SCANS ...
    DIR_EXP ...
    PATH_LOGBOOK ...
    SYSTEM_FUN_INDEX_SCANS ...
    SYSTEM_FUN_INDEX_DOC

% INPUT
if nargin == 0;
    DATA = [];
elseif nargin == 1
    DATA = varargin{1};
    validateattributes(DATA, {'struct'}, {'scalar'}, mfilename, 'DATA');
else
    error(['MWA:' mfilename ':invalidInput'], ...
        'Too many input arguments');
end


% COMPARE DIR TREE WITH DATA **********************************************
[status, output] = system(sprintf([SYSTEM_FUN_INDEX_SCANS ' %s'], DIR_SCANS));
if status
    error(['MWA:' mfilename ':scriptError'], ...
        'Indexing database failed:\n%s', output);
end
IndexScans = ReadIndexScans(output, 'string');
TN_IndexScans = IndexScans.Properties.UserData.this;

[status, output] = system(sprintf([SYSTEM_FUN_INDEX_DOC ' %s'], DIR_EXP));
if status
    error(['MWA:' mfilename ':scriptError'], ...
        'Indexing database failed:\n%s', output);
end
IndexDoc = ReadIndexDoc(output, 'string');
TN_IndexDoc = IndexDoc.Properties.UserData.this;

% Read logbook
Logbook = ReadLogbook(PATH_LOGBOOK);
TN_Logbook = Logbook.Properties.UserData.this;

isNewScans  = [];
isNewDoc    = [];
isNewLB     = [];

if isempty(DATA)
    
    % Create new struct DATA
    DATA = struct('tables', struct.empty);
    
    % Consider all elements in the directory tree
    isNewScans = true(1, height(IndexScans));
    isNewDoc = true(1, height(IndexDoc));
    
else
    % Find new values in the directory tree checking only key variables.
    keysScans   = DATA.tables.(TN_IndexScans).Properties.UserData.keys;
    keysDoc     = DATA.tables.(TN_IndexDoc).Properties.UserData.keys;
    keysLB      = DATA.tables.(TN_Logbook).Properties.UserData.keys;
    
    [~, isNewScans] = setdiff(IndexScans(:,keysScans), DATA.tables.(TN_IndexScans)(:,keysScans));
    [~, isNewDoc] = setdiff(IndexDoc(:,keysDoc), DATA.tables.(TN_IndexDoc)(:,keysDoc));
    [~, isNewLB] = setdiff(Logbook(:,keysLB), DATA.tables.(TN_Logbook)(:,keysLB));
    
end

% LOAD NEW ELEMENTS AND ADD TO DATA ***************************************
if isempty(isNewScans) && isempty(isNewDoc) && isempty(isNewLB)
    fprintf('Dataset is up to date');
    return;
else
    if ~isempty(DATA.tables)
        fprintf('Found the following new Scans: \n');
        display(IndexScans{isNewScans, keysScans});

        fprintf('Found new doc files for elements: \n');
        display(IndexDoc{isNewDoc, keysDoc});

        fprintf('Found new logbook entries: \n');
        display(Logbook{isNewLB, keysLB});
    end
    
    [NewScans, ~]  = CollectScans(IndexScans(isNewScans, :));
    NewInfo    = CollectInfo(IndexScans(isNewScans, :));
    TN_Scans    = NewScans.Properties.UserData.this;
    TN_Info     = NewInfo.Properties.UserData.this;

    % Load the whole table for those:
    NewExpdoc = table.empty;       % = CollectDocu(indexDoc(isNewDoc));
    TN_Expdoc = 'Expdoc';       % Expdoc.Properties.UserData.this;

    % Add to DATA
    if isempty(DATA.tables)
        DATA.(TN_Scans)         = NewScans;
        DATA.(TN_Expdoc)        = NewExpdoc;     % [DATA.Expdoc NewInfo];
        DATA.tables(1).(TN_Info)= NewInfo;
    else
        DATA.(TN_Scans)         = [DATA.(TN_Scans); NewScans];
        DATA.(TN_Expdoc)        = NewExpdoc;     % [DATA.Expdoc; NewInfo];
        DATA.tables.(TN_Info)   = [DATA.tables.(TN_Info); NewInfo];
    end
    
    DATA.tables.(TN_IndexScans)  = IndexScans;
    DATA.tables.(TN_IndexDoc)    = IndexDoc;
    DATA.tables.(TN_Logbook)     = PostprocessMeta(Logbook);
end


% Join all tables of 'tables' into one big table

% % Rename Variables with the same name
% tables.indexScans.Properties.VariableNames{ ...
%     strcmp('Directory', tables.indexScans.Properties.VariableNames)} = 'DirScan';
% 
% tables.indexExpData.Properties.VariableNames{ ...
%     strcmp('Directory', tables.indexExpData.Properties.VariableNames)} = 'DirExpData';

Tbig = DATA.tables.(TN_IndexScans);

keysInfo    = DATA.tables.(TN_Info).Properties.UserData.keys;
keysDoc     = DATA.tables.(TN_IndexDoc).Properties.UserData.keys;
keysLB      = DATA.tables.(TN_Logbook).Properties.UserData.keys;

Tbig = outerjoin( ...
    Tbig, DATA.tables.(TN_Logbook), ...
    'keys', keysLB, ...
    'MergeKeys', true, ...
    'Type', 'full');

Tbig = outerjoin( ...
    Tbig, DATA.tables.(TN_IndexDoc), ...
    'keys', keysDoc, ...
    'MergeKeys', true, ...
    'Type', 'full');

Tbig = outerjoin( ...
    Tbig, DATA.tables.(TN_Info), ...
    'keys', keysInfo, ...
    'MergeKeys', true, ...
    'Type', 'full');

DATA.Tpara = ConsistencyCheckMeta(Tbig);



end