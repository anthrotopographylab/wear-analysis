function ConsistencyScans( Scans )
% Check consistency of the dataformat of Scans. 
% The check is done on following attributes:
% - datatype is a 2d cell array
% - dimension is nx3 with n>=1
% - validity of META
% - duplicate elements
% - columns are:
% 1: scanname
% 2: X coordinates
% 3: Y coordinates
% 4: depth data (Z value)
% 5: RGB data
% - consistnecy of element dimensions

global META META_IS_3D META_IS_TXT TOL_ERR_RGBD_REL

% DATATYPE AND DIMENSION
validateattributes(Scans, {'cell'}, {'ncols', 5}, mfilename, 'Scans');

if  all(cellfun(@ischar, Scans(:,1))) && ...
    all(cellfun(@isnumeric, Scans(:,2))) && ...
    all(cellfun(@isnumeric, Scans(:,3))) && ...
    all(cellfun(@isnumeric, Scans(:,4))) && ...
    all(cellfun(@isnumeric, Scans(:,5)))
    % all good
else
    error('MWA:ConsistencyScnas:dataInconsistency', ...
        'Elements of Scans have invalid datatype.');
end

% DUPLICATES
[C, ia, ic] = unique(Scans(:,1));
isDuplicate     = true(size(Scans(:,1)));
isDuplicate(ia) = false;

if any(isDuplicate)
    error('MWA:ConsistencyScnas:dataInconsistency', ...
        'Scans has duplicattes: %s', [Scans{isDuplicate, 1}]);
else
    % all good    
end

% METADATA AVAILABLE
[~, indexMeta, indexScans] = IntersectMulti(META, Scans);

if isequal(size(Scans, 1), length(indexScans))
    % all good
else
    error('MWA:ConsistencyScans:metaOutdated', ...
        'META does not cover all elements of Scans.');
end

meta = META(indexMeta);

isDepth = [meta.(META_IS_3D)] & [meta.(META_IS_TXT)];

% SIZE OF ELEMENTS
cellfun(@checkElementDim, ...
    Scans(isDepth,1), Scans(isDepth,2), Scans(isDepth,3), Scans(isDepth,4), Scans(isDepth,5));

    function checkElementDim(name, X, Y, Zd, Zrgb)
         
%     function checkElementDim(SingleScan)
%         name    = SingleScan{1};
%         X       = SingleScan{2};
%         Y       = SingleScan{3};
%         Zd      = SingleScan{4};
%         Zrgb    = SingleScan{5};
        
        rgbSize = [size(Zrgb,1) size(Zrgb,2)];
        
        % Check whether size of coordinates are consistent with data
        if      isequal(size(X), size(Y)) && ...
                isequal(size(X), size(Zd)) && ...
                isequal(size(X), rgbSize) 
            % all godd
        
        % distinguish between small and larger difference in size
        elseif  ~isequal(size(Zd), rgbSize)
            if ( rms(size(Zd)-rgbSize)/rms(size(Zd) ) ) < TOL_ERR_RGBD_REL
                warning('MWA:ConsistencyScans:dataInconsistency', ...
                    ['The size of depth scan and RGB scan ' ...
                    'slightly mispatch for scan %s'], name);
            else
                warning('MWA:ConsistencyScans:dataInconsistency', ...
                    ['The size of depth scan and RGB scan ' ...
                    'mismatch for scan %s.'], name);
            end
            
        % Dimensions of coordinates mismatch
        else
            error('MWA:ConsistencyScans:dataInconsistency', ...
                ['The coordinate matrix size is inconsistent ' ...
                'for scan %s'], name);
        end
    end
            
            

% All good!

end

