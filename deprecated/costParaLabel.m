function costValue = costParaLabel(sigma, Scans, Selections)
% Take regression on surface roughness vs. label and optimize the filter
% parameter such that the RMS of the slopes is maximized. 

% Evaluate either 50x or 100x scans only

% show cost argument during iteration
disp(sigma);

parameters = EvaluateSelectionsHL(Scans, Selections, [], sigma);

paraLabel = SortByLabel(parameters, {'raw', 'low', 'medium', 'high'});

PlotParametersHL(parameters, 'label');

pause(0.1);     % finish plotting

% Construct target matrix
target{1} = ones(size(paraLabel{1})) * 1;    % raw
target{2} = ones(size(paraLabel{2})) * 2;    % low
target{3} = ones(size(paraLabel{3})) * 3;    % med
target{4} = ones(size(paraLabel{4})) * 4;    % high

%para = fieldnames(paraLabel{1});
para = {'Sq', 'Sa'};    % those two are less sensitive on outlieres. 


for j = 1:length(para)
    Sx = para{j};
    
    % Construct output matrix for each parameter
    para_out{1} = [paraLabel{1}.(Sx)];
    para_out{2} = [paraLabel{2}.(Sx)];
    para_out{3} = [paraLabel{3}.(Sx)];
    para_out{4} = [paraLabel{4}.(Sx)];
    
    % do regression
    [para_r(j), para_m(j), ~] = regression(target, para_out);
    
end

% Cost is the RMS of the slope of Sx(label)
costValue = -rms(para_m);

end

