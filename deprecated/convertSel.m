% convert selections from cell array format to objects of class Selection.

namelist = {ScansAlicona.name};

selections = Selection.empty(1,0);

for i=1:length(Selection2)
    
    scanname = Selection2{i,1};
    
    isScan = strcmp(scanname, namelist);
    
    if sum(isScan) == 1

        for j=1:length(Selection2{i,2})

            selections(end+1) = Selection(Selection2{i,2}{j}, ScansAlicona(isScan));

        end
    end
    
end

