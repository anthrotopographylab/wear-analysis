% Implement map for specification of table relationships

% List tables, that will be represented by nodes and their properties
tablenames = {'Info', 'IndexScans', 'IndexDoc', 'Logbook', 'Scans', 'ExpDoc'};
identifier = {'Scanname', 'Scanname', 'Directory', 'RunNo', 'Scanname', ''};
references = {'PARA', 'PARA', 'PARA', 'PARA', 'IMAG', 'IMAG'};

% Edge information
s = {'Info',        'IndexScans',   'IndexDoc'};
t = {'IndexScans',  'Logbook',      'Logbook'};

keys = {{'Scanname'}, {'SampleID', 'IterationNo'}, {'SampleID', 'IterationNo'}};

s = [1 2 3];
t = [2 4 4];

EdgeTable = table([s' t'], keys', ...
    'VariableNames', {'EndNodes', 'Keys'});
NodeTable = table(tablenames', references', identifier', ...
    'VariableNames', {'Name', 'Ref', 'ID'});

MAP = graph(EdgeTable, NodeTable);
plot(MAP, 'NodeLabel', MAP.Nodes.Name);

