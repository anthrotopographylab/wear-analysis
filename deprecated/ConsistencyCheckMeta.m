function TbigCheck = ConsistencyCheckMeta( Tbig )
% Check consistency of the information content in META. 
% The check is done on following properties:
% - magnification in index and info

FN_NAME = 'Scanname';
FN_MAG  = 'Magnification';
FN_INFO = 'Info';


% MAGNIFICATION ***********************************************************
% Compare magnification between scaninfo and indexScans.
% For entries having a mismatch within a tolerance most likely the
% microscope objective was set incorrectly in the Alicona software before
% the measurement was taken. Those records can not be treated with the
% standard processing pipeline. Set validity flag to false.
% data = QuerryMeta({FN_NAME, FN_MAG, FN_INFO});

magInfo = cellfun(@assignMag, {Tbig.Info.(FN_MAG)})';
    function res = assignMag(value)
        if isempty(value)
            res = 0;
        else
            res = value;
        end
    end

magIndex    = double(Tbig.(FN_MAG));

% compare:
reldiff = abs(magInfo - magIndex) ./ min(abs(magInfo), abs(magIndex));

% define tolerance
isValid = reldiff < 0.3;

TbigCheck = Tbig;

TbigCheck.isValid = isValid;


% % write to table
% [~, iIndex, ~] = intersect(DATA.tables.IndexScans.(FN_NAME), data.(FN_NAME));



% MISC ********************************************************************
% % Datatype and dimension of meta
% validateattributes(meta, {'struct'}, {'vector'}, mfilename, 'meta');
% 
% % Fieldnames 
% if (    ~isfield(meta, META_NAME)       || ...
%         ~isfield(meta, META_SID)        || ...
%         ~isfield(meta, META_ITER_NO)    || ...
%         ~isfield(meta, META_POS_NO)     || ...
%         ~isfield(meta, META_MAG)        || ...
%         ~isfield(meta, META_IS_3D)      || ...
%         ~isfield(meta, META_IS_RGB)     || ...
%         ~isfield(meta, META_IS_TXT)     || ...
%         ~isfield(meta, META_IS_DOCU_ORI)|| ...
%         ~isfield(meta, META_DOCU_IM)    || ...
%         ~isfield(meta, META_DOCU_LOG)   || ...
%         ~isfield(meta, META_DOCU_ORI)   || ...
%         ~isfield(meta, META_DIR_DOCU)   || ...
%         ~isfield(meta, META_DIR_DATA)   || ...
%         ~isfield(meta, META_ORI) )
%     error('MWA:EvaluateSelections:dataInconsistency', ...
%         'The fields of meta are incomplete or fieldnames are wrong.');
% end
% 
% % Datatypes of fields
% if(     any(~cellfun(@ischar, {meta.(META_NAME)})) || ...
%         any(~cellfun(@ischar, {meta.(META_DIR_DATA)})) || ...
%         any(~arrayfun(@isnumeric, [meta.(META_SID)])) || ...
%         any(~arrayfun(@isnumeric, [meta.(META_ITER_NO)])) || ...
%         any(~arrayfun(@isnumeric, [meta.(META_POS_NO)])) || ...
%         any(~arrayfun(@isnumeric, [meta.(META_MAG)])) || ...
%         any(~arrayfun(@islogical, [meta.(META_IS_3D)])) || ...
%         any(~arrayfun(@islogical, [meta.(META_IS_RGB)])) || ...
%         any(~arrayfun(@islogical, [meta.(META_IS_TXT)])) )
%     error('MWA:EvaluateSelections:dataInconsistency', ...
%         'The fields of meta have wrong datatype.');
% end

 
 
% META_NAME           = 'Scanname';
% META_DIR            = 'Directory';
% META_SID            = 'SampleID';
% META_ITER_NO        = 'IterationNo';
% META_POS_NO         = 'PositionNo';
% META_MAG_DOCU       = 'MagnificationDocu';
% META_MAG_MEAS       = 'MagnificationMeas';
% META_MAG            = 'Magnification';
% META_NAMES_DOCU     = 'NamesDocu';
% META_NAMES_MEAS     = 'NamesMeas';
% META_NUM_SCANS      = 'NumScans';
% META_NUM_MEAS       = 'NumMeasurements';
% META_NUM_ITER       = 'NumIterations';
% META_NUM_RAW        = 'NumMeasRaw';
% META_IS_3D          = 'is3D';
% META_IS_RGB         = 'isRGB';
% META_IS_TXT         = 'istxt';
    

% All good!

end

