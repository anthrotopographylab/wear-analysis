function metaFused = FuseMetadata(logbook, index, scanInfo)
% This function combines logbook and index.
% The logbook contains the metadata of the experiments. The index is the
% metadata of the database of surface scans.
% Entries in logbook that have no correspondance in the index are discarded. 
% 

global ...
    META_SID ...
    META_ITER_NO ...
    META_NUM_CYC_ACCU ...
    META_POS_NO ...
    META_NAME ...
    META_DIR_DATA ...
    META_MAG ...
    META_IS_3D ...
    META_IS_RGB ...
    META_IS_TXT ...
    LB_NUM_CYC ...
    LB_ITER_NO ...
    LB_SID ...
    LB_FORCE

% Construct empty struct
fieldsAll = unique([fieldnames(logbook); fieldnames(index)], 'stable');
fieldsAll = [fieldsAll; META_NUM_CYC_ACCU; 'ScanInfo'];
metaBlanc = cell2struct(num2cell(NaN(size(fieldsAll))), fieldsAll, 1);

metaFused = repmat(metaBlanc, 1, length(index));

% Go thorugh each element of the struct array index.
for i=1:length(index)
    
    index(i).(META_NUM_CYC_ACCU) = 0;
    
    % use sample ID and interval no to locate the logbook entry
    name_i = index(i).Scanname;
    ino_i = index(i).(META_ITER_NO);
    sid_i = index(i).(META_SID);
    
    isSID_LB       = sid_i == [logbook.(LB_SID)];
    isIterNo_LB    = ino_i == [logbook.(LB_ITER_NO)];
        
    if sum(isSID_LB & isIterNo_LB) == 1
        % all good, combine structs     
        metaFused(i) = setstructfields(metaFused(i), index(i));
        metaFused(i) = setstructfields(metaFused(i), logbook(isSID_LB & isIterNo_LB));
    elseif ino_i == 0
        % This is a scan of a raw surface, i.e. no logbook entry
        metaFused(i) = setstructfields(metaFused(i), index(i));
        
        metaFused(i).(LB_NUM_CYC)            = 0;
        metaFused(i).(LB_ITER_NO)            = 0;
        
        continue;   % no more metadata to add
    else
        warning('MWA:FuseStats:dataInconsistency', ...
            ['No unique sample-iteration pair found in logbook for %s.'], ...
            index(i).Scanname);
        continue;
    end

    % Compute total number cycles up to iteration <ino_i>
    cyclesTot = 0;
    for ino_j = ino_i:-1:1    % track back all iterations
        
        isIterNo_j = [logbook.(LB_ITER_NO)] == ino_j;
                
        if sum(isIterNo_j & isSID_LB) == 1
            % all good
            cyclesTot = cyclesTot + logbook(isIterNo_j & isSID_LB).(LB_NUM_CYC);
        else
            error('MWA:FuseStats:dataInconsistency', ...
                ['No unique sample-iteration pair found for %s at use%u.' ...
                ' Experiment might be incomplete.'], ...
                index(i).(META_NAME), ino_j);
        end
       
    end
    metaFused(i).(META_NUM_CYC_ACCU) = cyclesTot;
    
    % Find scan info
    isName_info = strcmp({scanInfo.Scanname}, name_i);
    
    if sum(isName_info) == 1
        metaFused(i).ScanInfo = scanInfo(isName_info);
    else
        warning('MWA:FuseStats:dataInconsistency', ...
            'Could not locate unique scan-info entry for %s.', ...
            index(i).Scanname);
    end

end

% Remove elements with empty field for iteration:
% These are useless for the data analysis.
isAssigned = cellfun(@ischar, {metaFused.Scanname});
metaFused = metaFused(isAssigned);


end


