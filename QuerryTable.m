function value = QuerryTable(table, record, variable)
% Interface for reading single value of table, <table> in META.
% The value returned is specified by record and variable.
% INPUT
% table:    String specifying the table.
% record:   Two-component cell array, which specifies the row as the
%           variable, <record(1)> having the value, <record(2)>.
% variable: String specifying the column.

global META

validateattributes(table, {'char'}, {'vector'});
validateattributes(variable, {'char'}, {'vector'});
validateattributes(record, {'cell'}, {'numel', 2});
validateattributes(record{1}, {'char'}, {'vector'});

mytable = META.tables.(table);

% Determine table rows corresponding to record
if iscell(mytable{1, record(1)})
    validateattributes(record{2}, {'char'}, {'vector'});
    isRow = strcmp(record{2}, mytable{:, record(1)});
else
    if ischar(record{2})
        error('record(2) can not be a string');
    else
        isRow = record{2} == mytable{:, record(1)};
    end
end

if      sum(isRow) == 1
    value = mytable{isRow, variable};
elseif  sum(isRow) == 0
    warning(['MWA:' mfilename ':noRecord'], ...
        'No Record found');
    value = [];
else
    error(['MWA:' mfilename ':ambiguousRecord'], ...
        'Record specification is ambiguous');
end

return



