% Build objects for access to data values

clear DATASET MAP PARA IMAG

global DATASET MAP PARA IMAG    % TO DO Check whether global is necessary

load '../data/DS_Brussels.mat'


%% Construct objects
clear runs scans meas samples

runNo = unique(PARA.Logbook{:,'RunNo'});

runs = arrayfun(@ExpRun, runNo(runNo>0), 'UniformOutput', false);
runs = sort([runs{:}]);


scannames = unique(PARA.IndexScans{:,'Scanname'});
isEmpty = cellfun(@isempty, scannames);

scans = cellfun(@AliconaImage, scannames, 'UniformOutput', false);
scans = sort([scans{:}]);

% World objects;
meas = Measurement(scans);
samples = SampleHistory(runs, meas);


% Link objects
% scans.linkToRelated(runs);


%% Selections
load('../data/Selection5.mat');


[a, b] = selAll.isRelated(scans, {'imageID'}, {'Name'});
selAll(a).linkTo(scans(b));



