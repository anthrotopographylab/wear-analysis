%startMWA.m Make workspace functions accessible.

% Add folders to MATLABs searchpath
addpath(genpath(fullfile(pwd, '/main')));

% Set constant variables. Base directory is Session_Force
SetGlobalVariables(pwd);


% Pull some variables into the workspace
global DIR_DATA DIR_RESULTS MVI_INT
