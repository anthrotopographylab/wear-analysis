function [fig50, fig100] = PlotStoneSamples(Parameters, paraName)
% Plot surface roughness of the selections on a grid with duration on the x
% axis and force on the y axis. The value is expressed by a color code. 

% name is the roughness parameter of choice representing surface 
% roughness.

global META

% Specify fieldnames of META
ME_NUM_CYC_ACCU = 'NumCycAccu';
ME_FORCE        = 'ForceN';
ME_SID          = 'SampleID';


% CHECK INPUT *************************************************************
validateattributes(Parameters, {'cell'}, {'ncols', 3}, mfilename, 'Parameters');
paraOptions = {'Sq', 'Sa', 'Ssk', 'Sku'};
if ~any(strcmp(paraName, paraOptions));
    error('MWA:PlotDurationHL:invalidInput', ...
        'paraName must be one of %s.', [paraOptions{:}]);
end


% COLLECT DATA ************************************************************

% Sync with metadata with scans of 
% 50x 
meta50 = META([META.Magnification]==50);

[~, indexMeta, indexPara] = ...
    IntersectMulti(meta50, Parameters);

if ~isempty(indexPara)
    fig50 = plotParameters(meta50(indexMeta), Parameters(indexPara, :));
    fig50.Children(1).Title.String = sprintf('%s of 50x raw areas', paraName);
end

% 100x
meta100 = META([META.Magnification]==100);

[~, indexMeta, indexPara] = ...
    IntersectMulti(meta100, Parameters);

if ~isempty(indexPara)
    fig100 = plotParameters(meta100(indexMeta), Parameters(indexPara, :));
    fig100.Children(1).Title.String = sprintf('%s of 100x raw areas', paraName);
end

% PLOT ********************************************************************

    function fig = plotParameters(metaSort, paraSort)


        [metaSort(isnan([metaSort.(ME_FORCE)])).(ME_FORCE)] = deal(0);
        
        isRaw = [metaSort.IterationNo] == 0;
        
        metaRaw = metaSort(isRaw);
        paraRaw = paraSort(isRaw, :);
        
%         stoneNo = 1:length(paraRaw);
        
        [gStones, stoneIDs] = findgroups([metaRaw.SampleID]);
        
        valuesCell = splitapply(@getSurfacePara, [paraRaw(:,2), num2cell(gStones')] , gStones');
        
%         valuesStoneNo = 1:max(gStones);
        
        
        
        function out = getSurfacePara(paraColumn)    
            % Expand multiple selections from one scan
            
            outCell = cellfun(@expand, paraColumn(:,1), 'UniformOutput', false);
%             outCell(:,2) = paraColumn(1,2);
            
            function outArray = expand(paraScalar)
                outArray = [paraScalar.Sa];
            end
            
            out = {[[outCell{:}]; repmat(paraColumn{1,2}, 1, length(outCell{:}))]};
            
            
        end
        
        
        values=[valuesCell{:}];
        
        fig = figure;
        h = plot(values(2,:), values(1,:), 'x');

        set(h, 'MarkerSize', 10);
        axis([0, max(values(2,:))+1, min(values(1,:))-0.1, max(values(1,:))+0.1 ]); 
        
        labelsSort = sortrows([gStones', [metaRaw.SampleID]'], 1); 
        
        labels = cellstr(num2str(labelsSort(:,2)));
        
        set(gca, 'XTick', labelsSort(:,1));
        set(gca, 'XTickLabel', labels);
        
        xlabel('Stone ID');
        ylabel('um');
        
        grid on;
        

    end

end