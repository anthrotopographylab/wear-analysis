function M = QuerryScans(querrynames, attribute)
% Interface for reading scans in META

global IMAG

validateattributes(querrynames, {'cell'}, {'vector'});
validateattributes(attribute, {'char'}, {'vector'});

[isQN, indexSN] = ismember(querrynames, IMAG.Scans.Scanname);

if all(~isQN)
    warning('Could not find %s of the following:', attribute);
    disp(querrynames{~isQN});
    M = [];
else
    M = IMAG.Scans{indexSN, attribute}{:};
end

return



